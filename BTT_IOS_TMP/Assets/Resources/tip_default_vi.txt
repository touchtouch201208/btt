1	Xếp hạng tốt luôn được đánh giá cao.
2	Không phải những cái đó rẻ hơn ở thành phố cuối cùng sao?
3	Giá? Các trợ lý khác đưa thông tin đó.
4	Mua, bán! Kiếm thật nhiều tiền! Trở nên hạnh phúc!
5	Tiền không phải là tất cả mọi thứ. Nó chỉ tốt!
6	Giá thị trường thay đổi theo thời gian.
7	Một số thành phố đưa ra mức giá tốt hơn với một số vật phẩm.
8	Cố gắng để mở khóa tất cả 26 thành phố.
9	Mở khóa các vật phẩm đắt hơn thông qua Nghiên cứu & Phát triển.
10	Hạn đã thử Trung tâm Nghiên cứu & Phát triển chưa?
11	Các nhà máy làm ra tiền cho bạn cả khi đi vắng.
12	Giá giống nhau cho mọi người.
13	Nhiệm vụ? Tôi nghĩ phần thưởng là tiền mặt và đá quý.
14	Nghiên cứu & Phát triển tạo ra nhiều vật phẩm mới và có giá trị hơn.
15	Tôi nghe nói rằng các trợ lý khác có rất nhiều thông tin.
16	Các quản lý điều hành nhà máy cho bạn.
17	Các quản lý được nâng cấp có thể thúc đẩy sản lượng nhà máy.
18	Thúc đẩy giúp bạn thương thảo giá tốt hơn.
19	Ấn vào các nhà máy để đẩy nhanh các việc lên.
20	Bạn cần trái tim để đi du lịch.
21	Trái tim sẽ được nạp theo thời gian.
22	Mở khóa thành phố mới mỗi 20 chuyến đi.
23	Đi đến thành phố khác để quay trở lại thị trường.
24	Bảo vệ sẽ giữ cho bạn an toàn.
25	Xem quảng cáo để nhận trái tim, đá quý và các món ngon khác.
26	Các nhà máy thật tuyệt để làm đầy các kho bạc của bạn.
27	Nâng cấp túi của bạn để mang thêm được nhiều vật phẩm hơn.
28	Kiểm tra thứ hạng của bạn trong xếp hạng.
29	Có ô trò chuyện để nói chuyện với những người chơi khác.
30	Giá khác nhau giữa các thành phố.
31	Quay trở lại trước khi giá thay đổi!
32	Tiết kiệm chút tiền cho thị trường.
33	Đỏ có nghĩa là cao, xanh tím có nghĩa là thấp.
34	Tội phạm có ở khắp nơi! Chúng có thể ngửi thấy tiền.
35	Đá quý? Tôi nghĩ bạn có thể nhận trong cửa hàng.
36	Kiểm tra menu tab cho những thứ miễn phí.
37	Nghiên cứu & Phát triển không phải là thứ chắc chắn nhưng những cái đó quay trở lại đều tốt.
38	Bạn đã thử ấn vào nhà máy chưa?
39	Có thúc đẩy cho các nhà máy. Hãy kiểm tra.
40	Hãy tận dụng đề nghị gấp đôi trái tim.
41	Tôi nghĩ đã có gói dành cho người mới bắt đầu.
42	Nút bán thật tuyệt để tìm kiếm giao dịch!