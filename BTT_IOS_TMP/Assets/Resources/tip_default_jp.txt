1	あなたからの高評価にいつも感謝しています。
2	それ、この前の都市でもっと安かったんじゃないですか?
3	価格情報?他の秘書たちに聞いてください。
4	買って売る!がっぽり稼いで!幸せを手に入れる!
5	お金が全てじゃないでしょう。でも止められませんよね~
6	相場の変動はいつも起きています。
7	都市によってはアイテムがさらに安くなることもあります。
8	26個の都市のロックを全て解除してみましょう。
9	研究開発で高額なアイテムを獲得しましょう。
10	研究開発センターは使ってみましたか?
11	ゲームをしていない時でも工場では収益が発生します。
12	全員に同じ価格が適用されます。
13	クエスト?報酬としてCASHやジェムがもらえるそうですよ。
14	研究開発でレアな新アイテムを手に入れることができます。
15	多くの情報を持っている秘書がいるらしいですよ。
16	マネージャーがあなたの代わりに工場を運営してくれます。
17	アップグレードしたマネージャーは工場の生産率を高めてくれます。
18	ブーストは価格交渉に役立ちますよ。
19	工場をタップすると生産速度がアップします。
20	他の国に移動するにはハートが必要です。
21	ハートは時間が経つと自動で回復されます。
22	20回旅行する度に新都市のロックが解除されます。
23	市場に戻るには他の都市に移動してください。
24	警備員があなたの安全を保障します。
25	広告を見るとハートやジェムなどの報酬をもらえますよ!
26	金庫の中身を増やすなら工場運営が最適でしょう。
27	カバンをアップグレードすればアイテムをもっと収納できますよ。
28	ランキングで順位を確認しましょう。
29	チャットで他プレイヤーと会話することができますよ。
30	都市によって相場が異なります。
31	価格が変わる前に戻りましょう!
32	市場でお金を使う気なら少し節約してください。
33	赤い色は価格が高い、青い色は価格が低いことを表しています。
34	犯罪者たちが金の匂いを嗅ぎつけて集まってきています!
35	ジェムのことですか?ショップに行けば買えますよ。
36	メニュータブで無料報酬を確認しましょう。
37	研究開発は失敗することもありますが、成功すれば最高の結果をもたらしてくれます。
38	工場をタップしてみましたか?
39	工場に適用可能なブーストがあります。確認してみましょう。
40	ハートを2倍獲得できるチャンスを逃さないでください。
41	スタートパックを購入できます。
42	SALEボタンでより良い取引ができますよ!