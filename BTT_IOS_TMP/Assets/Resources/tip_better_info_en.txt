1	I'm good with the big picture stuff.
2	Don't ask about prices.  I can't be bothered.
3	I've been working on my cute face.
4	I really need a new hat.
5	I can't keep up with actual prices.
6	All cities have their own quirks.
7	Use the boosts to get discounts and bonuses.
8	No, I don't have a boyfriend.
9	R&D is risky, but the rewards are nice.
10	Did you know that pig-tails are popular again?
11	Precious Metals can be cheaper in New York.
12	Agriculture products can be cheaper in Chicago.
13	Elixirs can be cheaper in Los Angeles.
14	Mini-Monsters can be cheaper in San Francisco.
15	Weapons can be cheaper in Miami.
16	Precious Stones can be cheaper in Toronto.
17	Elements can be cheaper in Mexico City.
18	Precious Metals can be cheaper in Seoul.
19	Textiles can be cheaper in Tokyo.
20	Elixirs can be cheaper in Beijing.
21	Agriculture products can be cheaper in Shanghai.
22	Precious Stones can be cheaper in Hong Kong.
23	Mini-Monsters can be cheaper in Taipei.
24	Textiles can be cheaper in Jakarta.
25	Elements can be cheaper in Kuala Lampur.
26	Mini-Monsters can be cheaper in Bangkok.
27	Weapons can be cheaper in Ho Chi Minh.
28	Precious metals can be cheaper in Singapore.
29	Agriculture products can be cheaper in London.
30	Textiles can be cheaper in Paris.
31	Precious Stones can be cheaper in Madrid.
32	Precious Metals can be cheaper in Berlin.
33	Weapons can be cheaper in Moscow.
34	Mini-Monsters can be cheaper in Rome.
35	Elements can be cheaper in Istanbul.
36	Elixirs can be cheaper in Amsterdam.
37	New York prices can be higher for Textiles.
38	Chicago prices can be higher for Mini-Monsters.
39	Los Angeles prices can be higher for Precious Stones.
40	San Francisco prices can be higher for Elixirs.
41	Miami prices can be higher for Elements.
42	Toronto prices can be higher for Agriculture products.
43	Mexico City prices can be higher for Weapons.
44	Seoul prices can be higher for Precious Stones.
45	Tokyo prices can be higher for Elixirs.
46	Beijing prices can be higher for Weapons.
47	Shanghai prices can be higher for Mini-Monsters.
48	Hong Kong prices can be higher for Elements.
49	Taipei prices can be higher for Agriculture products.
50	Jakarta prices can be higher for Precious Metals.
51	Kuala Lampur prices can be higher for Textiles.
52	Bangkok prices can be higher for Elixirs.
53	Ho Chi Minh prices can be higher for Elements.
54	Singapore prices can be higher for Agriculture products.
55	London prices can be higher for Precious Metals.
56	Paris prices can be higher for Mini-Monsters.
57	Madrid prices can be higher for Elixirs.
58	Berlin prices can be higher for Precious Stones.
59	Moscow prices can be higher for Elements.
60	Rome prices can be higher for Agriculture products.
61	Istanbul prices can be higher for Weapons.
62	Amsterdam prices can be higher for Textiles.