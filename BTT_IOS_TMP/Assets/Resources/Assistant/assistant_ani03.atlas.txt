
assistant_ani03.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
assistant_ani03_al_01
  rotate: false
  xy: 646, 403
  size: 114, 79
  orig: 114, 79
  offset: 0, 0
  index: -1
assistant_ani03_al_02
  rotate: false
  xy: 423, 302
  size: 109, 64
  orig: 109, 64
  offset: 0, 0
  index: -1
assistant_ani03_ar_01
  rotate: false
  xy: 199, 72
  size: 115, 79
  orig: 115, 79
  offset: 0, 0
  index: -1
assistant_ani03_ar_02
  rotate: false
  xy: 762, 418
  size: 109, 64
  orig: 109, 64
  offset: 0, 0
  index: -1
assistant_ani03_b_01
  rotate: false
  xy: 2, 26
  size: 195, 456
  orig: 195, 456
  offset: 0, 0
  index: -1
assistant_ani03_b_02
  rotate: false
  xy: 199, 153
  size: 189, 109
  orig: 189, 109
  offset: 0, 0
  index: -1
assistant_ani03_b_03
  rotate: false
  xy: 199, 264
  size: 222, 218
  orig: 222, 218
  offset: 0, 0
  index: -1
assistant_ani03_b_04
  rotate: false
  xy: 316, 82
  size: 63, 69
  orig: 63, 69
  offset: 0, 0
  index: -1
assistant_ani03_eye_01
  rotate: false
  xy: 534, 296
  size: 101, 70
  orig: 101, 70
  offset: 0, 0
  index: -1
assistant_ani03_eye_02
  rotate: false
  xy: 873, 412
  size: 101, 70
  orig: 101, 70
  offset: 0, 0
  index: -1
assistant_ani03_ll_01
  rotate: true
  xy: 423, 426
  size: 56, 221
  orig: 56, 221
  offset: 0, 0
  index: -1
assistant_ani03_lr_01
  rotate: true
  xy: 423, 368
  size: 56, 221
  orig: 56, 221
  offset: 0, 0
  index: -1
assistant_ani03_m_01
  rotate: false
  xy: 2, 2
  size: 49, 22
  orig: 49, 22
  offset: 0, 0
  index: -1
assistant_ani03_m_02
  rotate: true
  xy: 390, 213
  size: 49, 22
  orig: 49, 22
  offset: 0, 0
  index: -1
