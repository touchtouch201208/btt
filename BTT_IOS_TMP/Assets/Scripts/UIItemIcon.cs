﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemIcon : MonoBehaviour {

    private Image ItemIcon = null;
    private Image ItemUpgrade = null;
    private static Sprite UpradeIcon = null;
    
    public void SetItem(int _id)
    {
        if (UpradeIcon == null) UpradeIcon = Resources.Load<Sprite>("UI/Common/icon_item_upgrade");
        if (ItemIcon == null) ItemIcon = GetComponent<Image>();
        if (ItemUpgrade == null) ItemUpgrade = transform.GetChild(0).GetComponent<Image>();

        if (FormulaDB.Instance.FormulaItmes.Contains(_id))
        {
            ItemIcon.sprite = UpradeIcon;
            ItemIcon.color = ItemDB.Instance.GetGroupColor(ItemDB.Instance.ItemDatas[_id].itemGroupID);
            ItemUpgrade.sprite = ItemDB.Instance.GetitemImg(_id);
            ItemUpgrade.gameObject.SetActive(true);
        }
        else
        {
            ItemIcon.sprite = ItemDB.Instance.GetitemImg(_id);
            ItemIcon.color = Color.white;
            ItemUpgrade.gameObject.SetActive(false);
        }
    }
}
