﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;

public class MenuManager : MonoBehaviour {

    public Sprite Source_ImgTabActive;
    public Sprite Source_ImgTabInActive;

    //public GameObject BtnMain;

    [Header("Menu Taps")]
    public Image BtnChat;
    public Image BtnQuest;
    public Image BtnRank;
    public Image BtnSetting;

    [Header("Base Objects")]
    public GameObject BaseRoot;
    public GameObject BaseChat;
    public GameObject BaseQuest;
    public GameObject BaseRank;
    public GameObject BaseSetting;
    private GameObject BeforeBase = null;

    [Header("For Quest")]
    private UIScrollNodeProduct[] QuestNodes;
    public GameObject QuestNotificateMain;
    public GameObject QuestNotificateTab;

    [Header("For Setting")]
    private UIScrollNodeProduct[] SettingNodes;
    public Sprite Source_ImgBGMOn;
    public Sprite Source_ImgBGMOff;
    public Sprite Source_ImgEffectOn;
    public Sprite Source_ImgEFfectOff;
    public Text SettingLabelUID;
    public GameObject SettingLanguagePopup;
    //public Text SettingCurrentLanguageName;
    //public Image SettingCurrentLanguageIcon;
    public ScrollRect SettingLanguagePopupScroll;
    private UICityButtonNodes[] SettingLanguageNodes;

    [Header("For Rank")]
    public RectTransform RankScrollRoot;
    public Image RankBtnTotal;
    public Image RankBtnFriend;
    public Image RankBtnDaily;
    public Image RankBtnWeekly;
    public Image RankBtnMonthly;
    public ScrollRect RankScroll;
    public Button RankBtnLinkFacebook;
    private UIScrollNodeProduct[] RankScrollNodes;
    private JSONArray RankDailyList;
    private JSONArray RankWeeklyList;
    private JSONArray RankMonthlyList;
    private JSONNode RankDailyMyScore;
    private JSONNode RankWeeklyMyScore;
    private JSONNode RankMonthlyMyScore;
    private JSONArray RankNowList;
    private JSONNode RankNowMyScore;
    private int RankScrollPage = 0;
    private int RankState = 0;
    private int RankCategory = 0; // 0 : total, 1 : friends
    private Color RankColorBase = new Color(1, 1, 1, 0.8f);
    private Color RankColorActive = new Color(0.3f, 0.8f, 1.0f, 0.8f);
    public Text RankMyLabel;

    [Header("For Chat")]
    public RectTransform ChatScrollRoot;
    public ScrollRect ChatScroll;
    private UIScrollNodeProduct[] ChatScrollNodes;
    private JSONArray ChatList;
    private int ChatScrollPage = 0;
    public InputField ChatMessageInput;

    public static MenuManager Instance = null;

    void Awake()
    {
        Instance = this;

        //--------------
        QuestNodes = BaseQuest.GetComponentsInChildren<UIScrollNodeProduct>(true).OrderBy(x => x.name).ToArray();
        for (int i = 0; i < QuestNodes.Length; i++)
        {
            int _idx = i;
            QuestNodes[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                GetQuestReward(_idx);
            }
            );
        }

        //--------------
        SettingNodes = BaseSetting.GetComponentsInChildren<UIScrollNodeProduct>().OrderBy(x => x.name).ToArray();
        for (int i = 0; i < SettingNodes.Length; i++)
        {
            int _id = i;
            SettingNodes[i].ProductID = _id;
            SettingNodes[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                switch (_id)
                {
                    case 0:
                        SoundManager.Instance.SetBGM(!SoundManager.isOnBGM());
                        SetSettingInfos();
                        break;

                    case 1:
                        SoundManager.Instance.SetEffect(!SoundManager.isOnEffect());
                        SetSettingInfos();
                        break;

                    case 2:// notice
                        Application.OpenURL("www.rhyzengames.com/privacypolicy");
                        break; 

                    case 3: // contact 

                        string mailto = "contact@rhyzengames.com";
                        string subject = WWW.EscapeURL(string.Format("[{0}] Trade Tycoon Customer Service", NetworkManager.Instance.mUserData.memberUID)).Replace("+", "%20");
                        string body =
                             "Feedback : \n\n\n\n" +
                             "________\n\n" +
                             "User number : " + NetworkManager.Instance.mUserData.memberUID + "\n\n" +
                             "Device Model : " + SystemInfo.deviceModel + "\n\n" +
                             "Device OS : " + SystemInfo.operatingSystem + "\n\n" +
                             "________";
                        body = WWW.EscapeURL(body).Replace("+", "%20");

                        Application.OpenURL("mailto:" + mailto + "?subject=" + subject + "&body=" + body);

                        break;

                    case 4: // game info
                        Application.OpenURL("www.rhyzengames.com");
                        break; 

                    case 5:// facebook

                        LinkFacebook();

                        break; 

                    case 6: // language
                        SettingLanguagePopupScroll.enabled = false;
                        SettingLanguagePopup.SetActive(true);
                        for(int j = 0; j<SettingLanguageNodes.Length; j++)
                        {
                            SettingLanguageNodes[j].transform.Find("Check").gameObject.SetActive(
                                Language.Code == (Language.LanguageCode)SettingLanguageNodes[j].CityID);
                        }
                        Invoke("ActivateSettingLanguageScroll", 0.33f);
                        break; 

                    case 7: IGAWorksManager.CheckCoupon(); break; // restore
                    case 8: MainScene.Instance.OnExitGame(); break;
                }

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON);
            }
            );
        }

        SettingLanguageNodes = SettingLanguagePopup.GetComponentsInChildren<UICityButtonNodes>();
        for(int i =0; i<SettingLanguageNodes.Length; i++)
        {
            int _idx = i;
            Language.LanguageCode _code = (Language.LanguageCode)SettingLanguageNodes[i].CityID;
            SettingLanguageNodes[i].transform.Find("Name").GetComponent<Text>().text = Language.GetLanguageName(_code);
            //SettingLanguageNodes[i].transform.Find("Icon").GetComponent<Image>().sprite = MainScene.Instance.GetCityImgs(Language.GetLanguageCountryID(_code));
            SettingLanguageNodes[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                Language.instance.SetLanguega((Language.LanguageCode)SettingLanguageNodes[_idx].CityID);
                UserData.Instance.SaveData();
                UserData.Instance.LoadData();
                MainScene.isRetuningOnLoad = false;
                SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
            });
        }

        //--------------
        RankScrollNodes = RankScrollRoot.GetComponentsInChildren<UIScrollNodeProduct>(true);

        RankBtnLinkFacebook.onClick.AddListener(delegate
        {
            FBManager.Instance.Login(delegate
            {
                OnFacebookLogin();
            });
        });
        
        //--------------
        ChatScrollNodes = ChatScrollRoot.GetComponentsInChildren<UIScrollNodeProduct>(true);

        IGAWorksManager.AddCouponSuccessDelegate(delegate
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestOfferwallRewardCheck
                (
                delegate (JSONNode N)
                {
                    int _reward = N["reward"].AsInt;
                    if (_reward > 0) PopupManager.Instance.SetPopup(string.Format("get {0} gems!", _reward));
                    else PopupManager.Instance.SetOffPopup();
                },
                delegate (int _code, string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                },
                delegate (string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );
        });

        BaseRoot.SetActive(false);
    }
    
    void Start()
    {
        QuestList.Instance.CheckClearQuest();
    }

    void Update()
    {
        if(BaseChat.activeSelf)
        {
            int _now_page = (int)(ChatScrollRoot.anchoredPosition.y / 115);
            if (_now_page < 0) _now_page = 0;
            if (ChatScrollPage != _now_page)
            {
                ChatScrollPage = _now_page;
                SetChatScrollNodes();
            }
        }
        else if(BaseRank.activeSelf)
        {
            int _now_page = (int)(RankScrollRoot.anchoredPosition.y / 70);
            if (_now_page < 0) _now_page = 0;
            if (RankScrollPage != _now_page)
            {
                RankScrollPage = _now_page;
                SetRankScrollNodes();
            }
        }
    }

    public void SetBase(GameObject _base)
    {
        BaseRoot.SetActive(true);

        if (_base == BaseRoot)
        {
            /*
            if (BaseChat.activeSelf) _base = BaseChat;
            else if (BaseQuest.activeSelf) _base = BaseQuest;
            else if (BaseRank.activeSelf) _base = BaseRank;
            else if (BaseSetting.activeSelf) _base = BaseSetting;
            else _base = BaseSetting;
            */
            if (BeforeBase == null) _base = BaseChat;
            else _base = BeforeBase;
        }

        BaseChat.SetActive(false);
        BaseQuest.SetActive(false);
        BaseRank.SetActive(false);
        BaseSetting.SetActive(false);

        _base.SetActive(true);

        BtnChat.sprite = _base == BaseChat ? Source_ImgTabActive : Source_ImgTabInActive;
        BtnQuest.sprite = _base == BaseQuest ? Source_ImgTabActive : Source_ImgTabInActive;
        BtnRank.sprite = _base == BaseRank ? Source_ImgTabActive : Source_ImgTabInActive;
        BtnSetting.sprite = _base == BaseSetting ? Source_ImgTabActive : Source_ImgTabInActive;

        if (_base == BaseChat)
        {
            BeforeBase = _base;
            ChatRefresh();
            ChatScroll.enabled = false;
            Invoke("ActivateChatScroll", 0.33f);
        }
        else if (_base == BaseQuest)
        {
            BeforeBase = _base;
            QuestNotificateTab.SetActive(false);
            QuestNotificateMain.SetActive(false);
            SetQuestInfos();
        }
        else if (_base == BaseRank)
        {
            BeforeBase = _base;
            SetRankCategory(RankCategory);
            RankScroll.enabled = false;
            Invoke("ActivateRankScroll", 0.33f);
        }
        else if (_base == BaseSetting)
        {
            SetSettingInfos();
        }
    }

    public void ExitMenu()
    {
        if (SettingLanguagePopup.activeSelf) SettingLanguagePopup.SetActive(false);
        else BaseRoot.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    //-----------------------------------------------

    private void SetQuestInfos()
    {
        for (int i = 0; i < QuestNodes.Length; i++)
        {
            //
            if(i >= QuestList.Instance.QuestActivates.Count)
            {
                QuestNodes[i].gameObject.SetActive(false);
                continue;
            }

            var _quest = QuestList.Instance.QuestActivates[i];
            var _info = QuestDB.Instance.QuestDatas[_quest.questID];

            QuestNodes[i].gameObject.SetActive(true);

            int _before_id = QuestNodes[i].ProductID;
            QuestNodes[i].ProductID = _quest.questID;
            if(QuestNodes[i].ProductID != _before_id)
                QuestNodes[i].GetComponent<Animation>().Play();

            QuestNodes[i].transform.Find("Name").GetComponent<Text>().text = _info.name;
            if(_info.rewardType == ShopDB.PRICE_TYPE_MONEY)
            {
                QuestNodes[i].transform.Find("Reward").GetComponent<Text>().text = "$" + _info.rewardAmount.ToString("#,###0");
                QuestNodes[i].transform.Find("Reward").Find("IconGem").gameObject.SetActive(false);
                QuestNodes[i].transform.Find("Reward").Find("IconMoney").gameObject.SetActive(true);
            }
            else if (_info.rewardType == ShopDB.PRICE_TYPE_GEM)
            {
                QuestNodes[i].transform.Find("Reward").GetComponent<Text>().text = _info.rewardAmount.ToString();
                QuestNodes[i].transform.Find("Reward").Find("IconGem").gameObject.SetActive(true);
                QuestNodes[i].transform.Find("Reward").Find("IconMoney").gameObject.SetActive(false);
            }

            QuestNodes[i].GetComponent<Image>().color = _quest.questState == QuestList.QUEST_STATE_CLEAR_WAIT ? Color.white : Color.gray;
        }
    }

    public void GetQuestReward(int _idx)
    {
        int _id = QuestNodes[_idx].ProductID;

        //check quest clear
        if (!QuestList.Instance.QuestMap.ContainsKey(_id)) return;
        if (QuestList.Instance.QuestMap[_id].questState != QuestList.QUEST_STATE_CLEAR_WAIT) return;

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_QUEST_CLEAR);
        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestQuestClear(
            _id,
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();
                QuestList.Instance.SetNewQuset(_id);
                SetQuestInfos();
            },
            delegate (int _code, string _message)
            {
                Debug.Log("on fail : " + _code + " / " + _message);
                PopupManager.Instance.SetPopup(_message);
            },

            delegate (string _message)
            {
                Debug.Log("on error");
                PopupManager.Instance.SetPopup(Language.Str.POPUP_QUEST_FAIL);
            });
    }

    public void SetQusetNotificate()
    {
        if(!(BaseRoot.activeSelf && BaseQuest.activeSelf))
        {
            QuestNotificateTab.SetActive(true);
            QuestNotificateMain.SetActive(true);
        }
    }

    //-----------------------------------------------

    private void RankRefresh()
    {
        if(RankCategory == 0)
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestRankList(
                delegate (JSONNode N)
                {
                    PopupManager.Instance.SetOffPopup();
                    SetRankList(N);
                },
                delegate (int _code, string _message)
                {
                    Debug.Log("on fail : " + _code + " / " + _message);
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_RANK_FAIL);
                },

                delegate (string _message)
                {
                    Debug.Log("on error");
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_RANK_FAIL);
                });
        }
        else if(RankCategory == 1 && FBManager.Instance.isLogin())
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestRankListFacebook(
                delegate (JSONNode N)
                {
                    PopupManager.Instance.SetOffPopup();
                    SetRankList(N);
                },
                delegate (int _code, string _message)
                {
                    Debug.Log("on fail : " + _code + " / " + _message);
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_RANK_FAIL);
                },

                delegate (string _message)
                {
                    Debug.Log("on error");
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_RANK_FAIL);
                });
        }
    }

    public void SetRankList(int _val)
    {
        RankState = _val;
        RankScrollPage = 0;
        
        switch (RankState)
        {
            case 0: RankNowList = RankDailyList; RankNowMyScore = RankDailyMyScore; break;
            case 1: RankNowList = RankWeeklyList; RankNowMyScore = RankWeeklyMyScore; break;
            case 2: RankNowList = RankMonthlyList; RankNowMyScore = RankMonthlyMyScore; break;
        }

        RankBtnTotal.color = RankCategory == 0 ? RankColorActive : RankColorBase;
        RankBtnFriend.color = RankCategory == 1 ? RankColorActive : RankColorBase;
        RankBtnDaily.color = RankState == 0 ? RankColorActive : RankColorBase;
        RankBtnWeekly.color = RankState == 1 ? RankColorActive : RankColorBase;
        RankBtnMonthly.color = RankState == 2 ? RankColorActive : RankColorBase;

        if (RankNowMyScore["rank"].AsInt == -1) RankMyLabel.text = string.Format("{0} : -", Language.Str.RANK_MY);
        else RankMyLabel.text = string.Format("{0} : {1}", Language.Str.RANK_MY, RankNowMyScore["rank"].AsInt);
        
        SetRankScroll();
    }

    private void SetRankList(JSONNode N)
    {
        RankDailyList = N["dailyRankList"].AsArray;
        RankWeeklyList = N["weeklyRankList"].AsArray;
        RankMonthlyList = N["monthlyRankList"].AsArray;
        RankDailyMyScore = N["dailyRank"];
        RankWeeklyMyScore = N["weeklyRank"];
        RankMonthlyMyScore = N["monthlyRank"];
        
        SetRankList(RankState);
    }

    public void SetRankCategory(int _state)
    {
        RankCategory = _state;

        if (RankCategory == 1 && !FBManager.Instance.isLogin())
        {
            RankBtnTotal.color = RankColorBase;
            RankBtnFriend.color = RankColorActive;

            RankScroll.gameObject.SetActive(false);
            RankMyLabel.gameObject.SetActive(false);
            RankBtnLinkFacebook.gameObject.SetActive(true);
            return;
        }

        RankScroll.gameObject.SetActive(true);
        RankMyLabel.gameObject.SetActive(true);
        RankBtnLinkFacebook.gameObject.SetActive(false);
        RankRefresh();
    }

    private void SetRankScroll()
    {
        RankScrollRoot.anchoredPosition = new Vector2(0, RankScrollPage * 70);

        RankScrollRoot.sizeDelta =
            new Vector2(
                RankScrollRoot.sizeDelta.x,
                450 + ((RankNowList.Count - 7) * 70) + 70
                );

        SetRankScrollNodes();
    }

    private void SetRankScrollNodes()
    {
        for (int i = 0; i < RankScrollNodes.Length; i++)
        {
            var _rect = RankScrollNodes[i].GetComponent<RectTransform>();
            _rect.anchoredPosition =
                new Vector2(
                    _rect.anchoredPosition.x,
                    (RankScrollPage * -70)
                    + (i * -70));

            int _node_idx = i + RankScrollPage;
            if (_node_idx >= RankNowList.Count)
            {
                _rect.gameObject.SetActive(false);
            }
            else
            {
                _rect.gameObject.SetActive(true);

                RankScrollNodes[i].transform.Find("Rank").GetComponent<Text>().text = RankNowList[_node_idx]["rank"];
                RankScrollNodes[i].transform.Find("Rank").GetComponent<Text>().color =
                    RankNowList[_node_idx]["rank"].AsInt <= 3 ? Color.yellow : Color.white;
                RankScrollNodes[i].transform.Find("Name").GetComponent<Text>().text =
                    string.Format("Lv.{0} {1}", RankNowList[_node_idx]["level"].AsInt, RankNowList[_node_idx]["nickname"].ToString());
                RankScrollNodes[i].transform.Find("Score").GetComponent<Text>().text = RankNowList[_node_idx]["incExp"].AsLong.ToString("#,###0");

                RankScrollNodes[i].transform.Find("BG").GetComponent<Image>().color =
                    NetworkManager.Instance.mUserData.memberUID == RankNowList[_node_idx]["memberUID"].AsLong ? Color.yellow : Color.white;
            }
        }
    }

    private void ActivateRankScroll()
    {
        RankScroll.enabled = true;
    }

    //-----------------------------------------------

    public void SendChatMessage()
    {
        if(ChatMessageInput.text.Length > 0)
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestSendChatMessage(
                ChatMessageInput.text,
                delegate (JSONNode N)
                {
                    PopupManager.Instance.SetOffPopup();
                    ChatMessageInput.text = "";
                    ChatList = N["listMemberChat"].AsArray;
                    SetChatScroll();
                },
                delegate (int _code, string _message)
                {
                    Debug.Log("on fail : " + _code + " / " + _message);
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_CHAT_SEND_FAIL);
                },

                delegate (string _message)
                {
                    Debug.Log("on error");
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_CHAT_SEND_FAIL);
                });
        }
    }

    public void ChatRefresh()
    {
        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestChatList(
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();
                ChatMessageInput.text = "";
                ChatList = N["listMemberChat"].AsArray;
                SetChatScroll();
            },
            delegate (int _code, string _message)
            {
                Debug.Log("on fail : " + _code + " / " + _message);
                PopupManager.Instance.SetPopup(Language.Str.POPUP_CHAT_GET_FAIL);
            },

            delegate (string _message)
            {
                Debug.Log("on error");
                PopupManager.Instance.SetPopup(Language.Str.POPUP_CHAT_GET_FAIL);
            });
    }

    private void SetChatScroll()
    {
        ChatScrollRoot.anchoredPosition = new Vector2(0, ChatScrollPage * 115);

        ChatScrollRoot.sizeDelta =
            new Vector2(
                ChatScrollRoot.sizeDelta.x,
                511 + ((ChatList.Count - 5) * 115) + 115
                );

        SetChatScrollNodes();
    }

    private void SetChatScrollNodes()
    {
        for (int i = 0; i < ChatScrollNodes.Length; i++)
        {
            var _rect = ChatScrollNodes[i].GetComponent<RectTransform>();
            _rect.anchoredPosition =
                new Vector2(
                    _rect.anchoredPosition.x,
                    (ChatScrollPage * -115)
                    + (i * -115));

            int _node_idx = i + ChatScrollPage;
            if (_node_idx >= ChatList.Count)
            {
                _rect.gameObject.SetActive(false);
            }
            else
            {
                _rect.gameObject.SetActive(true);

                ChatScrollNodes[i].transform.Find("Name").GetComponent<Text>().text = ChatList[_node_idx]["nickname"];
                ChatScrollNodes[i].transform.Find("Message").GetComponent<Text>().text = ChatList[_node_idx]["message"];
                var _time = NetworkManager.Instance.GetServerNow() - NetworkManager.ServerBaseTime.AddMilliseconds(ChatList[_node_idx]["messageCreateTime"].AsLong);
                if(_time.TotalDays >= 1) ChatScrollNodes[i].transform.Find("Date").GetComponent<Text>().text = string.Format(Language.Str.CHAT_FORMAT_DAYAGO, _time.TotalDays);
                else if(_time.TotalHours >= 1) ChatScrollNodes[i].transform.Find("Date").GetComponent<Text>().text = string.Format(Language.Str.CHAT_FORMAT_HOURAGO, _time.TotalHours);
                else ChatScrollNodes[i].transform.Find("Date").GetComponent<Text>().text = string.Format(Language.Str.CHAT_FORMAT_MINAGO, _time.TotalMinutes);
            }
        }
    }

    private void ActivateChatScroll()
    {
        ChatScroll.enabled = true;
    }

    //-----------------------------------------------

    private void SetSettingInfos()
    {
        SettingLabelUID.text = Language.Str.SETTING_USERNUM + NetworkManager.Instance.mUserData.memberUID;

        SettingNodes[0].transform.GetChild(0).GetComponent<Image>().sprite =
            SoundManager.isOnBGM() ? Source_ImgBGMOn : Source_ImgBGMOff;

        SettingNodes[1].transform.GetChild(0).GetComponent<Image>().sprite =
            SoundManager.isOnEffect() ? Source_ImgEffectOn : Source_ImgEFfectOff;

        //SettingCurrentLanguageName.text = Language.GetLanguageName(Language.Code);
        //SettingCurrentLanguageIcon.sprite = MainScene.Instance.GetCityImgs(Language.GetLanguageCountryID(Language.Code));
    }

    private void ActivateSettingLanguageScroll()
    {
        SettingLanguagePopupScroll.enabled = true;
    }

    public void LinkFacebook()
    {
        if (FBManager.Instance.isLogin())
        {
            FBManager.Instance.LogOut();
            PopupManager.Instance.SetPopup("Facebook Logout.");
            MainScene.Instance.Invoke("SetMYInfos", 0.5f);
            if (MainScene.Instance.PlayerInfoPopup.activeSelf) MainScene.Instance.Invoke("OpenPlayerInfo",0.5f);
        }
        else
        {
            FBManager.Instance.Login(delegate
            {
                OnFacebookLogin();
            });
        }
    }

    private void OnFacebookLogin()
    {
        MainScene.Instance.SetMYInfos();
        if (MainScene.Instance.PlayerInfoPopup.activeSelf) MainScene.Instance.OpenPlayerInfo();

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestFacebookLogin(
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();
                if (BaseRank.activeSelf && RankCategory == 1) Invoke("SetRankCategoryOnFacebookLogin", 0.1f);
            },
            delegate (int _code, string _message)
            {
                Debug.Log("on fail : " + _code + " / " + _message);
                PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
            },

            delegate (string _message)
            {
                Debug.Log("on error");
                PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
            });
    }

    private void SetRankCategoryOnFacebookLogin()
    {
        SetRankCategory(1);
    }

    //-----------------------------------------------

    public void OpenOfferwall()
    {
        //open tapjoy offerwall
#if UNITY_EDITOR
#else
        TapjoyManager.Instance.OpenOfferwall();
#endif
    }
}
