﻿using UnityEngine;
using System.Collections;

public class NetworkHandler : MonoBehaviour {

	public delegate void CallbackDelegateHandler(string result, byte[] result_array);
	public CallbackDelegateHandler ResultDelegate;
	public delegate void ErrorDelegateHandler(string result, byte[] result_array);
	public ErrorDelegateHandler ErrorDelegate;

    private static NetworkHandler _instance = null;
    public static NetworkHandler self
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType(typeof(NetworkHandler)) as NetworkHandler;
                if(_instance == null)
                {
                    _instance = (new GameObject("NetworkHandler")).AddComponent<NetworkHandler>();
                }
            }
            return _instance;
        }
    }

	void Awake()
	{
        DontDestroyOnLoad(this);
        isOnRequest = false;
	}

	// Use this for initialization
	void Start () {
		// to keep this file accessible throughout the application
		DontDestroyOnLoad(this.gameObject);
	}
	
	// To start a coroutine to request for a web URL / API
	public void ServerRequest(string url, byte[] post)
	{
		StartCoroutine(DoCall(url, post));
	}

    // To process the request from coroutine
    public bool isOnRequest { get; private set; }
	private IEnumerator DoCall(string url, byte[] post)
	{
        isOnRequest = true;

        WWW w;
		w = new WWW(url, post);

        bool _isfailed = false;
        float _timer = 0;
        while(!w.isDone)
        {
            if(_timer > 30)
            {
                _isfailed = true;
                break;
            }
            _timer += Time.deltaTime;
            yield return null;
        }

        if (_isfailed)
        {
            string _result = NetworkManager.VALUE_RESULT_MESSAGE_TIMEOUT;
            ErrorDelegate(_result, System.Text.Encoding.UTF8.GetBytes(_result));
        }
        else
        {
            if (w.error != null)
            {
                print("Error: " + w.error);
                if (ErrorDelegate != null) ErrorDelegate(w.text, w.bytes);
            }
            else
            {

#if UNITY_EDITOR
                print("Result: " + w.text.Substring(0, Mathf.Min(w.text.Length, 5000)));
#endif
                ResultDelegate(w.text, w.bytes);

                w.Dispose();
            }
        }

        isOnRequest = false;
	}
}
