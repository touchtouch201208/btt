﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SimpleJSON;
using IgaworksUnityAOS;

public class IGAWorksManager : MonoBehaviour {

    private static string userID = "";

	public static IGAWorksManager Instance = null;
	// Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(this);

		Instance = this;

        IgaworksUnityPluginAOS.InitPlugin();
        IgaworksUnityPluginAOS.Common.startApplication();

        IgaworksUnityPluginAOS.LiveOps.initialize();
        IgaworksUnityPluginAOS.LiveOps.enableService((PlayerPrefs.GetInt("UsePush") == 0));
        
		IgaworksUnityPluginAOS.OnReceiveDeeplinkData = mOnReceiveDeeplinkData;
    }
    
    void Start()
    {
        IgaworksUnityPluginAOS.Common.startSession();
		IgaworksUnityPluginAOS.LiveOps.requestPopupResource ();
    }

    //---------------------------------------------

    //---------------------------------------------

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus) IgaworksUnityPluginAOS.Common.endSession();
        else
        {
            IgaworksUnityPluginAOS.Common.startSession();
            IgaworksUnityPluginAOS.LiveOps.resume();
        }
    }

    void OnApplicationQuit() {
        IgaworksUnityPluginAOS.Common.endSession(); 
    }
    
    //---------------------------------------------
    
    public static void onLoginComplete()
    {
        userID = NetworkManager.Instance.mUserData.memberUID.ToString();

        IgaworksUnityPluginAOS.Adbrix.firstTimeExperience("Login");
        IgaworksUnityPluginAOS.Common.setUserId(userID);
    }

    public static void onTutorialComplete()
    {
        IgaworksUnityPluginAOS.Adbrix.firstTimeExperience("PassTutorial");
    }
    
    public static void onRetention(string _key)
    {
        IgaworksUnityPluginAOS.Adbrix.retention(_key);
    }

    public static void CheckCoupon()
    {
        IgaworksUnityPluginAOS.Coupon.showCouponDialog(true);
    }

    public static void AddCouponSuccessDelegate(IgaworksUnityPluginAOS.onSendCouponSucceed _onsuccess)
    {
        IgaworksUnityPluginAOS.OnSendCouponSucceed = _onsuccess;
    }

    //---------------------------------------------
    
    public static void SetLevel(int _level)
    {
        IgaworksUnityPluginAOS.Adbrix.setCustomCohort(IgaworksUnityPluginAOS.CohortVariable.COHORT_1, "level_" + _level);
    }

    public static void SetAssistant(int _id)
    {
        IgaworksUnityPluginAOS.Adbrix.setCustomCohort(IgaworksUnityPluginAOS.CohortVariable.COHORT_1, "assistant_" + _id);
    }

    //통계 -------------------------------------------------------------------------

    //public static void onBuyMenu3_DoubleCash() { IgaworksUnityPluginAOS.Adbrix.retention("buydoublecash"); }

    //------------------------------------------------
    
	//------------------------------------------------
	//딥링크
	private void mOnReceiveDeeplinkData(string deeplink)
	{
		JSONNode _data = JSONNode.Parse(deeplink);
        Debug.Log(_data);
	}

	//-----------------
}
