﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TapjoyUnity;
using SimpleJSON;

public class TapjoyManager : MonoBehaviour {

    TJPlacement offerwallPlacement;

    public static TapjoyManager Instance = null;

#if UNITY_ANDROID
    private const string sdkKey = "I4nqsWeiTOi-sOYVulFKsgECg4PrdmKU4ygmmUdI4zZx-3v--VmCPfjAFISi";
#elif UNITY_IOS
    private const string sdkKey = "q35iRyXTT-9wqj-MVNboAEBpyDVWquYxwEriRodsY9p9ocamI3vR0R7Jlvw";
#endif

    // Use this for initialization
    void Start () {

        // Placement Delegates
        TJPlacement.OnRequestSuccess += HandlePlacementRequestSuccess;
        TJPlacement.OnRequestFailure += HandlePlacementRequestFailure;
        TJPlacement.OnContentReady += HandlePlacementContentReady;
        TJPlacement.OnContentShow += HandlePlacementContentShow;
        TJPlacement.OnContentDismiss += HandlePlacementContentDismiss;
        TJPlacement.OnPurchaseRequest += HandleOnPurchaseRequest;
        TJPlacement.OnRewardRequest += HandleOnRewardRequest;

        // Currency Delegates
        Tapjoy.OnAwardCurrencyResponse += HandleAwardCurrencyResponse;
        Tapjoy.OnAwardCurrencyResponseFailure += HandleAwardCurrencyResponseFailure;
        Tapjoy.OnSpendCurrencyResponse += HandleSpendCurrencyResponse;
        Tapjoy.OnSpendCurrencyResponseFailure += HandleSpendCurrencyResponseFailure;
        Tapjoy.OnGetCurrencyBalanceResponse += HandleGetCurrencyBalanceResponse;
        Tapjoy.OnGetCurrencyBalanceResponseFailure += HandleGetCurrencyBalanceResponseFailure;
        Tapjoy.OnEarnedCurrency += HandleEarnedCurrency;

        if (offerwallPlacement == null)
        {
            offerwallPlacement = TJPlacement.CreatePlacement("offerwall_unit");
        }

        Tapjoy.OnConnectSuccess += delegate { Debug.Log("tapjoy on connect success"); };
        Tapjoy.OnConnectFailure += delegate { Debug.Log("tapjoy on connect failed"); };

        Tapjoy.Connect(sdkKey);

        Instance = this;

    }

    public void OnLogin()
    {
        Tapjoy.SetUserID(NetworkManager.Instance.mUserData.memberUID.ToString());
    }

    public void OpenOfferwall()
    {
        PopupManager.Instance.SetPopupWait();
        offerwallPlacement.RequestContent();
    }

    #region Tapjoy Delegate Handlers

    #region Placement Delegate Handlers
    public void HandlePlacementRequestSuccess(TJPlacement placement)
    {
        PopupManager.Instance.SetOffPopup();
        if (placement.IsContentAvailable())
        {
            if (placement.GetName() == "offerwall_unit")
            {
                // Show offerwall immediately
                placement.ShowContent();
            }
        }
        else
        {
            Debug.Log("C#: No content available for " + placement.GetName());
        }
    }

    public void HandlePlacementRequestFailure(TJPlacement placement, string error)
    {
        Debug.Log("C#: HandlePlacementRequestFailure");
        Debug.Log("C#: Request for " + placement.GetName() + " has failed because: " + error);
        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
    }

    public void HandlePlacementContentReady(TJPlacement placement)
    {
        Debug.Log("C#: HandlePlacementContentReady");
        if (placement.IsContentAvailable())
        {
            //placement.ShowContent();
        }
    }

    public void HandlePlacementContentShow(TJPlacement placement)
    {
        Debug.Log("C#: HandlePlacementContentShow");
    }

    public void HandlePlacementContentDismiss(TJPlacement placement)
    {
        Debug.Log("C#: HandlePlacementContentDismiss");

        if (placement.GetName() == "offerwall_unit")
        {
            // Show offerwall immediately
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestOfferwallRewardCheck
                (
                delegate (JSONNode N)
                {
                    int _reward = N["reward"].AsInt;
                    if (_reward > 0) PopupManager.Instance.SetPopup(string.Format("get {0} gems!", _reward));
                    else PopupManager.Instance.SetOffPopup();
                },
                delegate (int _code, string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                },
                delegate (string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );
        }
    }

    void HandleOnPurchaseRequest(TJPlacement placement, TJActionRequest request, string productId)
    {
        Debug.Log("C#: HandleOnPurchaseRequest");
        request.Completed();
    }

    void HandleOnRewardRequest(TJPlacement placement, TJActionRequest request, string itemId, int quantity)
    {
        Debug.Log("C#: HandleOnRewardRequest");
        request.Completed();
    }

    #endregion

    #region Currency Delegate Handlers
    public void HandleAwardCurrencyResponse(string currencyName, int balance)
    {
        Debug.Log("C#: HandleAwardCurrencySucceeded: currencyName: " + currencyName + ", balance: " + balance);
    }

    public void HandleAwardCurrencyResponseFailure(string error)
    {
        Debug.Log("C#: HandleAwardCurrencyResponseFailure: " + error);
    }

    public void HandleGetCurrencyBalanceResponse(string currencyName, int balance)
    {
        Debug.Log("C#: HandleGetCurrencyBalanceResponse: currencyName: " + currencyName + ", balance: " + balance);
    }

    public void HandleGetCurrencyBalanceResponseFailure(string error)
    {
        Debug.Log("C#: HandleGetCurrencyBalanceResponseFailure: " + error);
    }

    public void HandleSpendCurrencyResponse(string currencyName, int balance)
    {
        Debug.Log("C#: HandleSpendCurrencyResponse: currencyName: " + currencyName + ", balance: " + balance);
    }

    public void HandleSpendCurrencyResponseFailure(string error)
    {
        Debug.Log("C#: HandleSpendCurrencyResponseFailure: " + error);
    }

    public void HandleEarnedCurrency(string currencyName, int amount)
    {
        Debug.Log("C#: HandleEarnedCurrency: currencyName: " + currencyName + ", amount: " + amount);

        Tapjoy.ShowDefaultEarnedCurrencyAlert();
    }
    #endregion
#endregion
}
