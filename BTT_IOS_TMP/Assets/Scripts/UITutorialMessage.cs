﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITutorialMessage : MonoBehaviour {

    public void SetMessage(string _txt)
    {
        GetComponent<UnityEngine.UI.Text>().text = _txt;
    }
}
