﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssistantNode : MonoBehaviour {

    private UICityButtonNodes[] assistants;

    public void OnInit()
    {
        assistants = GetComponentsInChildren<UICityButtonNodes>(true);
        SetAssistant();
    }

    public void SetAssistant()
    {
        for(int i = 0; i<assistants.Length; i++)
        {
            assistants[i].gameObject.SetActive(assistants[i].CityID == NetworkManager.Instance.mUserData.selectedAssistantID);
        }
    }
}
