﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TipManager : MonoBehaviour {

    public Sprite Source_Img_PriceUp;
    public Sprite Source_Img_PriceDown;

    public Text TextTip;
    public Image ImageArrow;
    public Button TipBG;

    private float tip_timer = 0;
    private int MoveCityID = -1;

    public static TipManager Instance = null;

    void Awake()
    {
        Instance = this;

        TipBG.onClick.AddListener(delegate
        {
        if (MoveCityID != -1)
        {
                if (NetworkManager.Instance.mUserData.heart <= 0)
                {
                    //MarketManager.Instance.HeartBase.SetActive(true);
                    MainScene.Instance.OpenHeartPopup();
                }
                else
                {
                    if(MoveCityID != NetworkManager.Instance.mUserData.currentCityCode)
                    {
                        MarketManager.Instance.OpenTravel(MoveCityID);
                        if (UserData.Instance.CityInfos[MoveCityID] == UserData.AVAILABLE)
                            MarketManager.Instance.StartCoroutine(MarketManager.Instance.MoveToCityAnimation(MoveCityID));
                        else MarketManager.Instance.OpenTravelPopup(MoveCityID);
                    }
                    
                }
            }
        });

        if (Language.Code == Language.LanguageCode.KR) TextTip.lineSpacing = 1;
        else if (Language.Code == Language.LanguageCode.KR
            || Language.Code == Language.LanguageCode.KR
            || Language.Code == Language.LanguageCode.KR) TextTip.resizeTextForBestFit = false;
    }

	// Update is called once per frame
	void Update () {

        tip_timer -= Time.deltaTime;
        
        if(tip_timer <= 0)
        {
            SetMessage();
        }
	}

    public void SetMessage()
    {
        ImageArrow.gameObject.SetActive(false);
        TipBG.image.raycastTarget = false;

        int _id = NetworkManager.Instance.mUserData.selectedAssistantID;
        if (_id == 1) TextTip.text = Language.Str.tip_list_default[Random.Range(0, Language.Str.tip_list_default.Count)];
        else if (_id == 2) TextTip.text = Language.Str.tip_list_some_info[Random.Range(0, Language.Str.tip_list_some_info.Count)];
        else if (_id == 3)
        {
            TextTip.text = Language.Str.tip_list_better_info[Random.Range(0, Language.Str.tip_list_better_info.Count)];

        }
        else if (_id == 8)
        {
            int _state = Random.Range(0, 2);
            var _list = _state == 0 ? UserData.Instance.MarketBestLowPriceTip : UserData.Instance.MarketBestHighPriceTip;

            int _val = Random.Range(0, _list.Count);
            var _node = _list.ElementAt(_val);

            TextTip.text =
                string.Format(Language.Str.TIP_FORMAT_BEST_PRICE,
                _node.Name,
                _state == 0 ? Language.Str.TIP_LOWEST : Language.Str.TIP_HIGHEST,
                Language.Str.CITYNAME(_node.CityCode)
                );

            ImageArrow.sprite = _state == 1 ? Source_Img_PriceUp : Source_Img_PriceDown;
            ImageArrow.gameObject.SetActive(true);

            MoveCityID = _node.CityCode;
            TipBG.image.raycastTarget = true;
        }
        else // 4 ~ 7
        {
            var _list = UserData.Instance.MarketRandomPriceTip;

            int _val = Random.Range(0, _list.Count);
            var _node = _list.ElementAt(_val);

            TextTip.text =
                string.Format(Language.Str.TIP_FORMAT_REALTIME_PRICE,
                new Money(_node.CurrentPrice.ToString()).ToWon(),
                _node.Name,
                Language.Str.CITYNAME(_node.CityCode),
                _node.CurrentPrice > _node.OriginPrice ? Language.Str.TIP_SHORTAGE : Language.Str.TIP_SALE
                );

            ImageArrow.sprite = _node.CurrentPrice > _node.OriginPrice ? Source_Img_PriceUp : Source_Img_PriceDown;
            ImageArrow.gameObject.SetActive(true);

            MoveCityID = _node.CityCode;
            TipBG.image.raycastTarget = true;
        }

        tip_timer = 15;
    }

    private void SetEmptyTip(int _val)
    {
        TextTip.text = Language.Str.TIP_EMPTY;
        ImageArrow.gameObject.SetActive(false);
    }
}
