﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class FactoryList
{
    public class FactoryMember
    {
        public int itemID { get; private set; }
        public DateTime createStartTime { get; private set; }
        public int itemCount { get; private set; }
        public int level { get; private set; }
        public int managerLevel { get; private set; }

        public FactoryMember(JSONNode _node)
        {
            long _createStartTime = _node["createStartTime"].AsLong;
            createStartTime = _createStartTime == 0 ? DateTime.MinValue : NetworkManager.ServerBaseTime.AddMilliseconds(_createStartTime);
            itemID = _node["itemID"].AsInt;
            itemCount = _node["itemCount"].AsInt;
            level = _node["level"].AsInt;
            managerLevel = _node["managerLevel"].AsInt;
        }
    }
    
    public List<FactoryMember> Factorys = new List<FactoryMember>();

    private static FactoryList instance = null;
    public static FactoryList Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new FactoryList();
            }
            return instance;
        }
    }

    public FactoryList()
    {
    }

    public void InitList(JSONNode _json)
    {
        Factorys.Clear();

        for (int i = 0; i < _json.AsArray.Count; i++)
        {
            var _node = new FactoryMember(_json.AsArray[i]);
            Factorys.Add(_node);
        }
    }
}
