﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using SimpleJSON;

public class UserData {

    public class Product
    {
        public int ID { get; private set; }
        public string name
        {
            get
            {
                return Language.Str.ITEMNAME(ID);
            }
        }
        public int amount;
        public long totalPrice;
        public long AvgPrice
        {
            get { return totalPrice / amount; }
        }
        public long MedianPrice
        {
            get { return ItemDB.Instance.ItemDatas[ID].medianPrice; }
        }
        public long LowPrice
        {
            get { return ItemDB.Instance.ItemDatas[ID].lowPrice; }
        }
        public long HighPrice
        {
            get { return ItemDB.Instance.ItemDatas[ID].highPrice; }
        }
        public Sprite Img
        {
            get { return ItemDB.Instance.GetitemImg(ID); }
        }

        public Product(int _id, int _amount = 0)
        {
            ID = _id;
            amount = _amount;
            totalPrice = (int)(_amount * MedianPrice);
        }

        public Product(int _id, int _amount, long _price)
        {
            ID = _id;
            amount = _amount;
            totalPrice = _price;
        }

        public Product(Product _other)
        {
            ID = _other.ID;
            amount = _other.amount;
            totalPrice = _other.totalPrice;
        }
    }

    public class ProductInfo
    {
        public int ID { get; private set; }
        public int isAvailable;
        public int Trades;

        public ProductInfo(int _id, int _available, int _trades)
        {
            ID = _id;
            isAvailable = _available;
            Trades = _trades;
        }
    }

    public class MarketProduct
    {
        public int ProductID { get; private set; }
        public long CurrentPrice;
        public long BoostPrice
        {
            get
            {
                float _boost_val = 0;
                if (CurrentPrice > OriginPrice) _boost_val = (100 + Instance.GetProductBoost(ItemDB.Instance.ItemDatas[ProductID].itemGroupID)) / 100.0f;
                else _boost_val = (100 - Instance.GetProductBoost(ItemDB.Instance.ItemDatas[ProductID].itemGroupID)) / 100.0f;
                return (long)(CurrentPrice * _boost_val);
            }
        }
        public long OriginPrice { get; private set; }
        public int CityCode { get; private set; }
        public int Available;
        public string Name
        {
            get
            {
                return Language.Str.ITEMNAME(ProductID);
            }
        }
        public Sprite Img
        {
            get { return ItemDB.Instance.GetitemImg(ProductID); }
        }

        public MarketProduct(int _id, long _currentPrice, long _originPrice, int _CityCode, int _Available)
        {
            ProductID = _id;
            CurrentPrice = _currentPrice;
            OriginPrice = _originPrice;
            CityCode = _CityCode;
            Available = _Available;
        }
    }

    public Money TotalMoney = new Money("0");
    public int Level = 0;
    public int Exp = 0;

    public Dictionary<int, Product> WareHouseProducts = new Dictionary<int, Product>();
    public Dictionary<int, Product> InventoryProducts = new Dictionary<int, Product>();
    public Dictionary<int, ProductInfo> InventoryProductInfos = new Dictionary<int, ProductInfo>();

    public long MarketStepCode = -1;
    public List<MarketProduct> MarketBestHighPriceTip = new List<MarketProduct>();
    public List<MarketProduct> MarketBestLowPriceTip = new List<MarketProduct>();
    public List<MarketProduct> MarketRandomPriceTip = new List<MarketProduct>();
    public Dictionary<int, MarketProduct> CurrentMarketInfos = new Dictionary<int, MarketProduct>();
    public Dictionary<int, MarketProduct> CurrentMarketAllInfos = new Dictionary<int, MarketProduct>();

    public Dictionary<int, int> CityInfos = new Dictionary<int, int>();
    public List<int> Formulas = new List<int>();

    public Dictionary<int, int> Boosters = new Dictionary<int, int>();
    public Dictionary<int, int> Assistants = new Dictionary<int, int>();

    public List<int> Levels = new List<int>();

    public const int AVAILABLE = 0;
    public const int UNAVAILABLE = 1;

    public int WareHouseSize
    {
        get
        {
            return ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel].rewardSlot;
        }
    }
    public int InventorySize
    {
        get
        {
            return ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel].rewardSlot;
        }
    }

    private static UserData instance = null;
    public static UserData Instance
    {
        get
        {
            if (instance == null) instance = new UserData();
            return instance;
        }

        private set { }
    }
    
    public UserData()
    {
        //FileSavePath = Application.persistentDataPath + "/UserSaveData.dat"; // ver 0.0.3
        FileSavePath = Application.persistentDataPath + "/UserData.dat"; // after 0.0.13

        TotalMoney = new Money("0");

        //booster db를 돌면서 그룹별 항목을 리스트에 추가해야함
        for(int i = 0; i<BoosterDB.Instance.Boosters.Count; i++)
        {
            Boosters.Add(BoosterDB.Instance.Boosters.ElementAt(i).Key, 0);
        }
        
        for(int i = 0; i<AssistantDB.Instance.Assistants.Count; i++)
        {
            Assistants.Add(AssistantDB.Instance.Assistants.ElementAt(i).Key, i == 0 ? AVAILABLE : UNAVAILABLE);
        }

        Levels.Add(1);
    }

    public void AddProductWareHouse(Product _product)
    {
        /*
        if (_product.amount == 0) return;

        if (WareHouseProducts.ContainsKey(_product.ID)) WareHouseProducts[_product.ID].amount += _product.amount;
        else
        {
            WareHouseProducts.Add(_product.ID, new Product(_product.ID, _product.amount));
        }
        */
    }

    //both sub product ( _amout < 0 )
    public void AddProductWareHouse(int _id, int _amount)
    {
        /*
        if (_amount == 0) return;

        if (WareHouseProducts.ContainsKey(_id))
        {
            WareHouseProducts[_id].amount += _amount;
            if (WareHouseProducts[_id].amount <= 0) WareHouseProducts.Remove(_id);
        }
        else WareHouseProducts.Add(_id, new Product(_id, _amount));
        */
    }

    public void AddProductInventory(int _id, int _amount, long _price)
    {
        if (_amount == 0) return;

        if (InventoryProducts.ContainsKey(_id))
        {
            InventoryProducts[_id].amount += _amount;
            InventoryProducts[_id].totalPrice += _amount * _price;
        }
        else
        {
            InventoryProducts.Add(_id, new Product(_id, _amount));
            InventoryProducts[_id].totalPrice = _amount * _price;
        }
    }
    
    public void SubProductInventory(int _id, int _amount)
    {
        if (_amount == 0) return;

        if (InventoryProducts.ContainsKey(_id))
        {
            if(_amount >= InventoryProducts[_id].amount) InventoryProducts.Remove(_id);
            else
            {
                InventoryProducts[_id].totalPrice = (InventoryProducts[_id].totalPrice / InventoryProducts[_id].amount) * (InventoryProducts[_id].amount - _amount);
                InventoryProducts[_id].amount -= _amount;
            }
        }
        else return;
    }
    
    public int GetWareHouseCount()
    {
        /*
        int result = 0;
        for (int i = 0; i < WareHouseProducts.Count; i++)
        {
            result += WareHouseProducts.ElementAt(i).Value.amount;
        }

        return result;
        */

        return GetInventoryCount();
    }

    public int GetWareHouseLeftCount()
    {
        //return WareHouseSize - GetWareHouseCount();
        return GetInventoryLeftCount();
    }

    public bool isWareHouseFull(int _val = 0)
    {
        /*
        if(_val == 0) return GetWareHouseCount() >= WareHouseSize;
        else return GetWareHouseCount() + _val > WareHouseSize;
        */
        return isInventoryFull(_val);
    }

    public bool isExistItemWarehouse(int _id, int _amount)
    {
        /*
        if (!WareHouseProducts.ContainsKey(_id)) return false;
        return WareHouseProducts[_id].amount >= _amount;
        */

        return isExistItemInventory(_id, _amount);
    }

    public int GetInventoryCount()
    {
        int result = 0;
        for (int i = 0; i < InventoryProducts.Count; i++)
        {
            result += InventoryProducts.ElementAt(i).Value.amount;
        }

        return result;
    }

    public int GetInventoryLeftCount()
    {
        return InventorySize - GetInventoryCount();
    }

    public bool isInventoryFull(int _val = 0)
    {
        if (_val == 0) return GetInventoryCount() >= InventorySize;
        else return GetInventoryCount() + _val > InventorySize;
    }

    public bool isExistItemInventory(int _id, int _amount)
    {
        if (!InventoryProducts.ContainsKey(_id)) return false;
        return InventoryProducts[_id].amount >= _amount;
    }
    
    public void OnTradeItem(int _groupID)
    {
        var _info = GetCurrentUnlockItem(_groupID);
        if (_info == null) return;
        _info.Trades = Mathf.Clamp(_info.Trades + 1, 0, ItemDB.Instance.ItemDatas[_info.ID].trades);
    }

    public ProductInfo GetCurrentUnlockItem(int _groupID)
    {
        var group_infos = InventoryProductInfos.Where(x => ItemDB.Instance.ItemDatas[x.Value.ID].itemGroupID == _groupID).ToDictionary(k => k.Key, k => k.Value);

        for (int i = 0; i < group_infos.Count; i++)
        {
            var _node = group_infos.ElementAt(i).Value;
            if (ItemDB.Instance.ItemDatas[_node.ID].trades > 0
                && _node.isAvailable == UNAVAILABLE) return _node;
        }

        return null;
    }

    public bool isExistUpgradeableItem(int _groupID)
    {
        for (int i = 0; i < InventoryProducts.Count ; i++)
        {
            var _node = InventoryProducts.ElementAt(i).Value;
            if (ItemDB.Instance.ItemDatas[_node.ID].itemGroupID == _groupID
                && !FormulaDB.Instance.FormulaItmes.Contains(_node.ID)) return true;
        }

        return false;
    }

    public int GetProductBoost(int _groupID)
    {
        if (Boosters[_groupID] == 0) return 0;
        else return BoosterDB.Instance.Boosters[_groupID][Boosters[_groupID] - 1].percentage;
    }

    public float GetFactoryBoost()
    {
        float result = 1;
        
        if ((NetworkManager.Instance.mUserData.reserveTime2 - NetworkManager.Instance.GetServerNow()).TotalSeconds > 0)
            result *= 5;

        if ((NetworkManager.Instance.mUserData.reserveTime1 - NetworkManager.Instance.GetServerNow()).TotalSeconds > 0)
            result *= 2;

        if ((NetworkManager.Instance.mUserData.reserveTime5 - NetworkManager.Instance.GetServerNow()).TotalSeconds > 0)
            result *= 2;

        if (NetworkManager.Instance.mUserData.selectedAssistantID == 5
            || NetworkManager.Instance.mUserData.selectedAssistantID == 6
            || NetworkManager.Instance.mUserData.selectedAssistantID == 7)
            result *= 1.2f;
        
        return result;
    }

    public int GetFactoryTabBoost()
    {
        if ((NetworkManager.Instance.mUserData.reserveTime3 - NetworkManager.Instance.GetServerNow()).TotalSeconds > 0)
            return 5;
        else return 1;
    }

    public JSONClass GetJson()
    {
        JSONClass _result = new JSONClass();
        
        _result["listMemberCity"] = new JSONArray();
        for (int i = 0; i < CityInfos.Count; i++)
        {
            var _info = CityInfos.ElementAt(i).Value;
            JSONClass _node = new JSONClass();
            _node["cityCode"].AsInt = CityInfos.ElementAt(i).Key;
            _node["availability"].AsInt = CityInfos.ElementAt(i).Value; // 수정필요
            _result["listMemberCity"].AsArray.Add(_node);
        }

        _result["listMemberFormula"] = new JSONArray();
        for (int i = 0; i < Formulas.Count; i++)
        {
            var _info = Formulas[i];
            JSONClass _node = new JSONClass();
            _node["formulaID"].AsInt = _info;
            _node["amount"].AsInt = 1; // 수정필요
            _result["listMemberFormula"].AsArray.Add(_node);
        }

        _result["listMemberInventory"] = new JSONArray();
        for (int i = 0; i<InventoryProductInfos.Count; i++)
        {
            var _info = InventoryProductInfos.ElementAt(i).Value;
            JSONClass _node = new JSONClass();
            _node["itemID"].AsInt = _info.ID;
            _node["isAvailable"].AsInt = _info.isAvailable;
            _node["trades"].AsInt = _info.Trades;

            if(InventoryProducts.ContainsKey(_info.ID))
            {
                _node["amount"].AsInt = InventoryProducts[_info.ID].amount;
                _node["totalPrice"].AsLong = InventoryProducts[_info.ID].totalPrice;
            }
            else
            {
                _node["amount"].AsInt = 0;
                _node["totalPrice"].AsLong = 0;
            }
            
            _result["listMemberInventory"].AsArray.Add(_node);
        }

        /*
        _result["listMemberWareHouse"] = new JSONArray();
        for (int i = 0; i < WareHouseProducts.Count; i++)
        {
            var _info = WareHouseProducts.ElementAt(i).Value;
            JSONClass _node = new JSONClass();
            _node["itemID"].AsInt = _info.ID;
            _node["amount"].AsInt = _info.amount;
            _result["listMemberWareHouse"].AsArray.Add(_node);
        }
        */

        _result["listMemberBooster"] = new JSONArray();
        for (int i = 0; i < Boosters.Count; i++)
        {
            var _info = Boosters.ElementAt(i);
            JSONClass _node = new JSONClass();
            _node["itemGroupID"].AsInt = _info.Key;
            _node["boosterLevel"].AsInt = _info.Value;
            _result["listMemberBooster"].AsArray.Add(_node);
        }

        _result["listMemberAssistant"] = new JSONArray();
        for (int i = 0; i < Assistants.Count; i++)
        {
            var _info = Assistants.ElementAt(i);
            JSONClass _node = new JSONClass();
            _node["assistantID"].AsInt = _info.Key;
            _node["assistantLevel"].AsInt = _info.Value;
            _result["listMemberAssistant"].AsArray.Add(_node);
        }

        _result["listMemberQuest"] = new JSONArray();
        for (int i = 0; i < QuestList.Instance.Quests.Count; i++)
        {
            var _info = QuestList.Instance.Quests[i];
            JSONClass _node = new JSONClass();
            _node["memberUID"].AsLong = NetworkManager.Instance.mUserData.memberUID;
            _node["questID"].AsInt = _info.questID;
            _node["questState"].AsInt = _info.questState;
            _result["listMemberQuest"].AsArray.Add(_node);
        }

        return _result;
    }

    private string FileSavePath = "";
    private const string AES_KEY = "2bc7h1j8sz2is9bk";
    public long DataVersion = -1;

    public void SaveData()
    {
        DataVersion++;

        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);
        
        var _json = NetworkManager.Instance.UploadDatas(new JSONClass());
        _json["listMemberCity"] = GetJson()["listMemberCity"];
        _json["memberUID"].AsLong = NetworkManager.Instance.mUserData.memberUID;

        string _data = _json.ToString();

        /*
        int stream_size = 0;
        
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _data);

        byte[] _bytes = new byte[stream_size];
        int seek = 0;
        seek = NetworkManager.Instance.AddByteStreamData(_bytes, _data, seek);
        Debug.Log(seek);
        */

        byte[] _bytes = Encrypt(_data);

        b.Serialize(f, _bytes);
        f.Close();
    }
    
    public void LoadData()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            byte[] _data = (byte[])obj;

            //int seek = 0;
            //int _length = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            //string _json = Encoding.UTF8.GetString(_data, seek, _length); seek += _length;

            string _json = Descrypt(_data);
            
            var N = JSON.Parse(_json);
            NetworkManager.Instance.LoadDataFromJson(N);
        }
    }

    private byte[] Encrypt(string toEncrypt)
    {
        byte[] keyArray = UTF8Encoding.UTF8.GetBytes(AES_KEY);
        byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
        RijndaelManaged rDel = new RijndaelManaged();
        rDel.Key = keyArray;
        rDel.Mode = CipherMode.ECB;
        rDel.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = rDel.CreateEncryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
        //return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        return resultArray;
    }

    private string Descrypt(byte[] toDecrypt)
    {
        byte[] keyArray = UTF8Encoding.UTF8.GetBytes(AES_KEY);
        byte[] toEncryptArray = toDecrypt;
        RijndaelManaged rDel = new RijndaelManaged();
        rDel.Key = keyArray;
        rDel.Mode = CipherMode.ECB;
        rDel.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = rDel.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
        return UTF8Encoding.UTF8.GetString(resultArray);
    }
}
