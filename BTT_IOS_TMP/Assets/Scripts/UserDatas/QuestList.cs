﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class QuestList
{

    public class QuestMember
    {
        public int questID;
        public int questState;

        public QuestMember(JSONNode _node)
        {
            questID = _node["questID"].AsInt;
            questState = _node["questState"].AsInt;
        }
    }

    public Dictionary<int, QuestMember> QuestMap = new Dictionary<int, QuestMember>();
    public List<QuestMember> Quests = new List<QuestMember>();
    public List<QuestMember> QuestActivates;

    public const int QUEST_STATE_NONE = 0;
    public const int QUEST_STATE_ACTIVATE = 1;
    public const int QUEST_STATE_CLEAR = 2;
    public const int QUEST_STATE_CLEAR_WAIT = 3;

    private static QuestList instance = null;
    public static QuestList Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new QuestList();
            }
            return instance;
        }
    }

    public QuestList()
    {
    }

    public void InitList(JSONNode _json)
    {
        QuestMap.Clear();
        Quests.Clear();

        for (int i = 0; i < _json.AsArray.Count; i++)
        {
            var _node = new QuestMember(_json.AsArray[i]);
            QuestMap.Add(_node.questID, _node);
            Quests.Add(_node);
        }

        InitAcitveQuest();
    }

    private void InitAcitveQuest()
    {
        QuestActivates = Quests.Where(
            x =>
            x.questState == QUEST_STATE_ACTIVATE
            || x.questState == QUEST_STATE_CLEAR_WAIT
            ).ToList();
    }

    public void SetNewQuset(int _id) // 한번에 한번씩만
    {
        //for(int i = Quests.IndexOf(QuestMap[_id]); i<Quests.Count; i++)
        for (int i = 0; i < Quests.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[Quests[i].questID];
            if(_info.repeat == 1) // 모든 퀘스트를 완료하여 반복구간에 들어오면
            {
                var _repeats = Quests.Where(x => QuestDB.Instance.QuestDatas[x.questID].repeat == 1).ToList();
                while(true)
                {
                    int _idx = UnityEngine.Random.Range(0, _repeats.Count);
                    if (_repeats[_idx].questState != QUEST_STATE_ACTIVATE
                        && _repeats[_idx].questState != QUEST_STATE_CLEAR_WAIT)
                    {
                        _repeats[_idx].questState = QUEST_STATE_ACTIVATE;
                        InitAcitveQuest();
                        return;
                    }
                }
            }
            else
            {
                if (Quests[i].questState != QUEST_STATE_NONE) continue;
                
                if (_info.skipIf == 1)
                {
                    bool _isSkip = true;
                    if (_info.questType == QuestDB.QUESTTYPE_BUY_ASSISTANT)
                    {
                        for (int j = 0; j < UserData.Instance.Assistants.Count; j++)
                        {
                            if (UserData.Instance.Assistants.ElementAt(j).Value == 0)
                            {
                                _isSkip = false;
                                break;
                            }
                        }
                    }
                    else if (_info.questType == QuestDB.QUESTTYPE_BUY_MANAGER)
                    {
                        for (int j = 0; j < FactoryManager.Instance.Factorys.Count; j++)
                        {
                            var _factory = FactoryManager.Instance.Factorys.ElementAt(j).Value;
                            if (_factory.ManagerLavel < ManagerDB.Instance.Managers[_factory.FactoryID].Last().level)
                            {
                                _isSkip = false;
                                break;
                            }
                        }
                    }
                    else if (_info.questType == QuestDB.QUESTTYPE_TRAVEL_UNLOCK)
                    {
                        for (int j = 0; j < UserData.Instance.CityInfos.Count; j++)
                        {
                            if(UserData.Instance.CityInfos.ElementAt(j).Value == UserData.UNAVAILABLE)
                            {
                                _isSkip = false;
                                break;
                            }
                        }
                    }
                    else if (_info.questType == QuestDB.QUESTTYPE_RND_UNLOCK)
                    {
                        for (int j = 0; j < UserData.Instance.InventoryProductInfos.Count; j++)
                        {
                            if (UserData.Instance.InventoryProductInfos.ElementAt(j).Value.isAvailable == UserData.UNAVAILABLE)
                            {
                                _isSkip = false;
                                break;
                            }
                        }
                    }
                    else if(_info.questType == QuestDB.QUESTTYPE_INVENTORY_UPGRADE)
                    {
                        if(NetworkManager.Instance.mUserData.inventoryLevel
                            < ShopDB.Instance.Inventory.Keys.Max())
                        {
                            _isSkip = false;
                            break;
                        }
                    }

                    if (_isSkip) continue;
                }

                bool _overlap = false;
                for(int j = 0; j<QuestActivates.Count; j++)
                {
                    var _active_quest_info = QuestDB.Instance.QuestDatas[QuestActivates[j].questID];
                    if(_info.questType == _active_quest_info.questType
                        && _info.reserveInt1 == _active_quest_info.reserveInt1
                        && _info.reserveInt2 == _active_quest_info.reserveInt2)
                    {
                        _overlap = true;
                        break;
                    }
                }

                if (_overlap) continue;

                Quests[i].questState = QUEST_STATE_ACTIVATE;
                InitAcitveQuest();
                if (QuestDB.Instance.QuestDatas[Quests[i].questID].questType == QuestDB.QUESTTYPE_FACTORY_OPEN)
                {
                    for(int j = 0; j<FactoryManager.Instance.Factorys.Count; j++)
                    {
                        OnFactoryOpen(FactoryManager.Instance.Factorys.ElementAt(j).Key);
                    }
                }
                else if (QuestDB.Instance.QuestDatas[Quests[i].questID].questType == QuestDB.QUESTTYPE_TRAVEL_UNLOCK
                    && QuestDB.Instance.QuestDatas[Quests[i].questID].reserveInt2 != -1) OnTravelUnlock();
                return;
            }
        }
    }

    public void OnFactoryUpgarde()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for(int i = 0; i<QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if(_info.questType == QuestDB.QUESTTYPE_FACTORY_UPGRADE)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                MenuManager.Instance.SetQusetNotificate();
            }
        }
    }

    public void OnTravel(int _id, int _region)
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_TRAVEL)
            {
                if (_info.reserveInt1 == -1)
                {
                    if (_info.reserveInt2 == -1
                        || _info.reserveInt2 == _region)
                    {
                        QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                        MenuManager.Instance.SetQusetNotificate();
                    }
                }
                else if (_info.reserveInt1 == _id)
                {
                    QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                    MenuManager.Instance.SetQusetNotificate();
                }
            }
        }
    }

    public void OnBuyBoost()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_BUY_BOOST)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
            }
        }
    }

    public void OnBuyAssistant()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_BUY_ASSISTANT)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
            }
        }
    }

    public void OnMarketBuy(int _itmeid, int _groupid)
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_MARKET_BUY)
            {
                if (_info.reserveInt1 == -1)
                {
                    if (_info.reserveInt2 == -1
                        || _info.reserveInt2 == _groupid)
                    {
                        QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                        MenuManager.Instance.SetQusetNotificate();
                    }
                }
                else if (_info.reserveInt1 == _itmeid)
                {
                    QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                    MenuManager.Instance.SetQusetNotificate();
                }
            }
        }
    }

    public void OnMarketSell(int _itmeid, int _groupid)
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_MARKET_SELL)
            {
                if (_info.reserveInt1 == -1)
                {
                    if (_info.reserveInt2 == -1
                        || _info.reserveInt2 == _groupid)
                    {
                        QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                        MenuManager.Instance.SetQusetNotificate();
                    }
                }
                else if (_info.reserveInt1 == _itmeid)
                {
                    QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                    MenuManager.Instance.SetQusetNotificate();
                }
            }
        }
    }

    public void OnLevelUP()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_LEVELUP)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                MenuManager.Instance.SetQusetNotificate();
            }
        }
    }

    public void OnFactoryOpen(int _id)
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_FACTORY_OPEN)
            {
                if (_info.reserveInt1 == _id)
                {
                    QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                    MenuManager.Instance.SetQusetNotificate();
                }
            }
        }
    }

    public void OnBuyManager()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_BUY_MANAGER)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                MenuManager.Instance.SetQusetNotificate();
            }
        }
    }

    public void OnBuyInventory()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_INVENTORY_UPGRADE)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                MenuManager.Instance.SetQusetNotificate();
            }
        }
    }

    public void OnTravelUnlock()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_TRAVEL_UNLOCK)
            {
                if (_info.reserveInt2 == -1)
                {
                    QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                    MenuManager.Instance.SetQusetNotificate();
                }
                else
                {
                    bool _isClear = true;
                    for (int j = 0; j < UserData.Instance.CityInfos.Count; j++)
                    {
                        var _node = UserData.Instance.CityInfos.ElementAt(j);
                        if (CityDB.Instance.CityDatas[_node.Key].regionCode == _info.reserveInt2
                            && _node.Value == UserData.UNAVAILABLE)
                        {
                            _isClear = false;
                            break;
                        }
                    }
                    if (_isClear)
                    {
                        QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                        MenuManager.Instance.SetQusetNotificate();
                    }
                }
            }
        }
    }

    public void OnRNDUpgrade()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_RND_UPGRADE)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                MenuManager.Instance.SetQusetNotificate();
            }
        }
    }

    public void OnRNDUnlock()
    {
        if (TutorialManager.Instance.Step != -1) return;

        for (int i = 0; i < QuestActivates.Count; i++)
        {
            var _info = QuestDB.Instance.QuestDatas[QuestActivates[i].questID];
            if (_info.questType == QuestDB.QUESTTYPE_RND_UNLOCK)
            {
                QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
                MenuManager.Instance.SetQusetNotificate();
            }
        }
    }

    public void SetQuestFull()
    {
        if (QuestActivates.Count < 3) while (QuestActivates.Count < 3) SetNewQuset(1);
    }

    public void CheckClearQuest()
    {
        for (int i = 0; i < QuestActivates.Count; i++)
        {
            if(QuestActivates[i].questState == QUEST_STATE_CLEAR_WAIT)
                MenuManager.Instance.SetQusetNotificate();
        }
    }

#if UNITY_EDITOR

    public void SetQuestClear()
    {
        for(int i = 0; i<QuestActivates.Count; i++)
        {
            QuestActivates[i].questState = QUEST_STATE_CLEAR_WAIT;
        }
    }
#endif
}
