﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class RNDList
{
    public class RNDMember
    {
        public int RNDID;
        public int Availability;
        public int FormulaID;
        public int UnlockID; // for unlock
        public long totalPrice = 0;
        public DateTime createStartTime;
        public int itemCount;

        public RNDMember(JSONNode _node)
        {
            long _createStartTime = _node["createStartTime"].AsLong;
            createStartTime = NetworkManager.ServerBaseTime.AddMilliseconds(_createStartTime);
            RNDID = _node["centerID"].AsInt;
            Availability = _node["availability"].AsInt;
            FormulaID = _node["formulaID"].AsInt;
            UnlockID = _node["itemID"].AsInt;
            if (!string.IsNullOrEmpty(_node["totalPrice"].ToString())) totalPrice = _node["totalPrice"].AsLong;
            itemCount = _node["itemCount"].AsInt;
        }
    }

    public List<RNDMember> RNDs = new List<RNDMember>();
    public Dictionary<int, RNDMember> RNDMaps = new Dictionary<int, RNDMember>();

    private static RNDList instance = null;
    public static RNDList Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new RNDList();
            }
            return instance;
        }
    }

    public RNDList()
    {
    }

    public void InitList(JSONNode _json)
    {
        RNDs.Clear();
        RNDMaps.Clear();

        for (int i = 0; i < _json.AsArray.Count; i++)
        {
            var _node = new RNDMember(_json.AsArray[i]);
            RNDs.Add(_node);
            RNDMaps.Add(_node.RNDID, _node);
        }
    }
}
