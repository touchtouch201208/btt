﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class GiftList
{
    public class GiftMember
    {
        public long mailUID { get; private set; }
        public long recvMemberUID { get; private set; }
        public long sendMemberUID { get; private set; }
        public string sendNickname { get; private set; }
        public string message { get; private set; }
        public int giftType { get; private set; }
        public int amount { get; private set; }
        public int reserveInt { get; private set; }
        public long reserveLong { get; private set; }
        public string reserveString { get; private set; }
        public int expiredate { get; private set; }

        public GiftMember(JSONNode _node)
        {
            mailUID = _node["mailUID"].AsLong;
            recvMemberUID = _node["recvMemberUID"].AsLong;
            sendMemberUID = _node["sendMemberUID"].AsLong;
            sendNickname = _node["sendNickname"];
            message = _node["message"];
            giftType = _node["giftType"].AsInt;
            amount = _node["amount"].AsInt;
            reserveInt = _node["reserveInt"].AsInt;
            reserveLong = _node["reserveLong"].AsLong;
            reserveString = _node["reserveString"];
            expiredate = _node["expiredate"].AsInt;
        }

        public string GetGiftName()
        {
            string _name = "";
            switch(giftType)
            {
                case GIFT_TYPE_CASH: _name = "캐쉬"; break;
                case GIFT_TYPE_GOLD: _name = "전력"; break;
                case GIFT_TYPE_TOKEN: _name = "대결권"; break;
                case GIFT_TYPE_TICKET: _name = "뽑기권"; break;
                default: return "";
            }

            return string.Format("{0} x{1}", _name, amount);
        }
    }
    
    public const int GIFT_TYPE_CASH = 1;
    public const int GIFT_TYPE_GOLD = 2;
    public const int GIFT_TYPE_TOKEN = 3;
    public const int GIFT_TYPE_TICKET = 4;

    public List<GiftMember> Gifts = new List<GiftMember>();

    private static GiftList instance = null;
    public static GiftList Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GiftList();
            }
            return instance;
        }
    }

    public GiftList()
    {
    }

    public void InitList(JSONNode _json)
    {
        Gifts.Clear();

        for (int i = 0; i < _json.AsArray.Count; i++)
        {
            Gifts.Add(new GiftMember(_json.AsArray[i]));
        }
    }
}
