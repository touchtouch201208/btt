﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using SimpleJSON;

public class NetworkManager
{

    public const int PLATFORM_CODE_PC = 0; //PC
    public const int PLATFORM_CODE_AOS = 1; //Android
    public const int PLATFORM_CODE_IOS = 2; //IOS

    public const int ACCOUNT_TYPE_DJ_DEVICE = 10; //디바이스ID기반
    public const int ACCOUNT_TYPE_DJ_GOOGLE = 11; //구글계정기반
    public const int ACCOUNT_TYPE_DJ_FACEBOOK = 12; //페이스북계정기반
    public const int ACCOUNT_TYPE_DJ_KAKAO = 13; //카카오계정기반

    public const int ACCOUNT_TYPE_RZ_DEVICE = 20; //디바이스ID기반
    public const int ACCOUNT_TYPE_RZ_GOOGLE = 21; //구글계정기반
    public const int ACCOUNT_TYPE_RZ_FACEBOOK = 22; //페이스북계정기반
    public const int ACCOUNT_TYPE_RZ_KAKAO = 23; //카카오계정기반

    public const int VALUE_RESULT_CODE_SUCCESS = 1;  //프로토콜 결과 (성공)
    public const int VALUE_RESULT_CODE_ERROR = 0;    //프로토콜 결과 (실패)
    public const int VALUE_RESULT_CODE_NEED_UPDATE = 2;  //프로토콜 결과 (업데이트) << 로그
    public const string VALUE_RESULT_MESSAGE_TIMEOUT = "TIME OUT";
    
    public const string JSON_PROTOCOL_URL_TEST = "http://tradetycoon.rhyzengames.com/protocolTest.api";
    public const string JSON_PROTOCOL_URL_CRYPTO = "http://tradetycoon.rhyzengames.com/protocol.api";

    public static string JSON_PROTOCOL_URL = JSON_PROTOCOL_URL_CRYPTO;
    
    public static string UPDATE_URL = "";
    public static string APP_VERSION = "0.0.1";

    public const string VALUE_NICKNAME_NULL = "미입력";

    //----------
    //게임 내에서 사용되는 데이터

    public class UserDatas
    {
        public long memberUID = -1;
        public int memberGrade = 0; // 0 - 신규가입, 1 - 닉네임입력, 2 - 튜토리얼 클리어, 3 - 패키지상품 구입
        public string nickname = "";
        public string photoURL = "";
        private int mheart = 0;
        public int heart
        {
            get { return mheart; }
            set
            {
                mheart = value;
                heartLabel = value.ToString();
            }
        }
        public DateTime heartLastChargedTime;
        public DateTime securityFinishTime;
        public DateTime wareHouseFinishTime = DateTime.MinValue;
        private int mcash = 0;
        public int cash
        {
            get { return mcash; }
            set
            {
                mcash = value;
                cashLabel = value.ToString("#,###0");
            }
        }
        public int level = 0;
        public long exp = 0;
        public int currentCityCode = -1;
        public int selectedAssistantID = 1;
        public string moneyString = "";
        public int moneyLength = 0;
        public int wareHouseLevel = 0;
        public int inventoryLevel = 0;
        public int cityUnlockPoint = 0;
        public DateTime reserveTime1; // facfory boost 1
        public DateTime reserveTime2; // factory boost 2
        public DateTime reserveTime3; // factory tab boost
        public DateTime reserveTime4; // open box
        public DateTime reserveTime5; // ad boost

        public DateTime ADBoostDelayTime = DateTime.MinValue;
        public int ADBoostCount = 0;

        public string heartLabel { get; private set; }
        public string cashLabel { get; private set; }
    }
    public UserDatas mUserData = new UserDatas();

    public static DateTime ServerBaseTime = new DateTime(1970, 1, 1, 9, 0, 0, DateTimeKind.Unspecified); // to utc issue
    public TimeSpan ServerTimeDepth;

    //---------

    private static NetworkManager instance = null;

    public static NetworkManager Instance
    {
        get
        {
            if (instance == null) instance = new NetworkManager();

            return instance;
        }
    }

    public DateTime GetServerNow()
    {
        return (DateTime.Now + ServerTimeDepth);
    }

    private JSONNode GetResultJson(string _data)
    {
        if(JSON_PROTOCOL_URL == JSON_PROTOCOL_URL_CRYPTO)
        {
            string _result = Crypto.AesDecrypt(_data);
            return JSONNode.Parse(_result);
        }
        else return JSONNode.Parse(_data);
    }

    private void OnProtocolSuccess(string _data, byte[] _array)
    {
        try
        {
            var N = GetResultJson(_data);

            LoadDataFromJson(N);

            // --------------------------------------------------------
            if (!string.IsNullOrEmpty(N["marketPrice"].ToString()))
            {
                UserData.Instance.CurrentMarketInfos.Clear();
                UserData.Instance.CurrentMarketAllInfos.Clear();

                var _marektinfo = N["marketPrice"];
                int _key = _marektinfo["cityCode"].AsInt;
                var _infos = _marektinfo["cityMarketPrice"];
                for (int j = 0; j < _infos.Count; j++)
                {
                    int _id = _infos[j]["itemID"].AsInt;
                    int _available = _infos[j]["available"].AsInt;
                    var _node = new UserData.MarketProduct(
                        _id,
                        _infos[j]["currentPrice"].AsLong,
                        _infos[j]["medianPrice"].AsLong,
                        _key,
                        _available);

                    //if (_available == UserData.AVAILABLE) UserData.Instance.CurrentMarketInfos[_key].Add(_id, _node);
                    UserData.Instance.CurrentMarketAllInfos.Add(_id, _node);
                }
            }

            if (!string.IsNullOrEmpty(N["marketStepCode"].ToString()))
            {
                UserData.Instance.MarketStepCode = N["marketStepCode"].AsLong;
            }

            if (!string.IsNullOrEmpty(N["marketPriceTipLowest"].ToString()))
            {
                UserData.Instance.MarketBestLowPriceTip.Clear();

                var _infos = N["marketPriceTipLowest"].AsArray;
                for (int j = 0; j < _infos.Count; j++)
                {
                    int _id = _infos[j]["itemID"].AsInt;
                    UserData.Instance.MarketBestLowPriceTip.Add(
                    new UserData.MarketProduct(
                        _id,
                        _infos[j]["currentPrice"].AsLong,
                        _infos[j]["medianPrice"].AsLong,
                        _infos[j]["cityCode"].AsInt,
                        UserData.AVAILABLE
                        )
                        );
                }
            }

            if (!string.IsNullOrEmpty(N["marketPriceTipHighest"].ToString()))
            {
                UserData.Instance.MarketBestHighPriceTip.Clear();

                var _infos = N["marketPriceTipHighest"].AsArray;
                for (int j = 0; j < _infos.Count; j++)
                {
                    int _id = _infos[j]["itemID"].AsInt;
                    UserData.Instance.MarketBestHighPriceTip.Add(
                    new UserData.MarketProduct(
                        _id,
                        _infos[j]["currentPrice"].AsLong,
                        _infos[j]["medianPrice"].AsLong,
                        _infos[j]["cityCode"].AsInt,
                        UserData.AVAILABLE
                        )
                        );
                }
            }

            if (!string.IsNullOrEmpty(N["marketPriceTipRandomCity"].ToString()))
            {
                UserData.Instance.MarketRandomPriceTip.Clear();

                var _infos = N["marketPriceTipRandomCity"].AsArray;
                for (int j = 0; j < _infos.Count; j++)
                {
                    int _id = _infos[j]["itemID"].AsInt;
                    UserData.Instance.MarketRandomPriceTip.Add(
                    new UserData.MarketProduct(
                        _id,
                        _infos[j]["currentPrice"].AsLong,
                        _infos[j]["medianPrice"].AsLong,
                        _infos[j]["cityCode"].AsInt,
                        UserData.AVAILABLE
                        )
                        );
                }
            }

            // --------------------------------------------------------
            if (!string.IsNullOrEmpty(N["downLoadURL"].ToString()))
            {
                UPDATE_URL = N["downLoadURL"];
            }

            long server_time = N["currentTimeAtServer"].AsLong;
            DateTime _server_now = ServerBaseTime.AddMilliseconds(server_time);
            ServerTimeDepth = _server_now - DateTime.Now;

            int resultCode = N["resultCode"].AsInt;
            string resultMessage = N["resultMessage"];

            if (resultCode == VALUE_RESULT_CODE_SUCCESS)
            {
                OnProtocolSuccessCB(N);
            }
            else OnProtocolErrorCB(resultCode, resultMessage);

        }
        catch (Exception e)
        {
            Debug.Log(e);
            OnProtocolFailCB(e.Message);
        }

    }

    private void OnProtocolFail(string _data, byte[] _array)
    {
        OnProtocolFailCB(_data);
    }

    public void LoadDataFromJson(JSONNode N)
    {
        //DBs --------------------------------------------------------
        if (!string.IsNullOrEmpty(N["configDB"].ToString()))
        {
            ConfigDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["levelDB"].ToString()))
        {
            LevelDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["factoryDB"].ToString()))
        {
            FactoryDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["cityDB"].ToString()))
        {
            CityDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["regionDB"].ToString()))
        {
            RegionDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["itemDB"].ToString()))
        {
            ItemDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["itemGroupDB"].ToString()))
        {
            ItemGroupDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["formulaDB"].ToString()))
        {
            FormulaDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["shopDB"].ToString()))
        {
            ShopDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["managerDB"].ToString()))
        {
            ManagerDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["assistantDB"].ToString()))
        {
            AssistantDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["boosterDB"].ToString()))
        {
            BoosterDB.Instance.InitDB(N);
        }
        
        if (!string.IsNullOrEmpty(N["questDB"].ToString()))
        {
            QuestDB.Instance.InitDB(N);
        }

        if (!string.IsNullOrEmpty(N["memberData"].ToString()))
        {
            var _userData = JSONNode.Parse(N["memberData"].ToString());

            long _data_uid = mUserData.memberUID;
            long _data_version = UserData.Instance.DataVersion;

            bool _isNewData = true;
            if (N["responseProtocol"].Value != "protocolReVisitBoxU")
            {
                if (!string.IsNullOrEmpty(_userData["dataVersion"].ToString()))
                {
                    _data_uid = _userData["memberUID"].AsLong;
                    _data_version = _userData["dataVersion"].AsLong;

                    if (_data_version < UserData.Instance.DataVersion
                        && mUserData.memberUID == _data_uid) _isNewData = false;
                }
            }

            //server base data
            if (!string.IsNullOrEmpty(_userData["memberUID"].ToString())) mUserData.memberUID = _userData["memberUID"].AsLong;
            if (!string.IsNullOrEmpty(_userData["memberGrade"].ToString())) mUserData.memberGrade = _userData["memberGrade"].AsInt;
            if (!string.IsNullOrEmpty(_userData["nickname"].ToString())) mUserData.nickname = _userData["nickname"];
            if (!string.IsNullOrEmpty(_userData["photoURL"].ToString())) mUserData.photoURL = _userData["photoURL"];
            if (!string.IsNullOrEmpty(_userData["heart"].ToString())) mUserData.heart = _userData["heart"].AsInt;
            if (!string.IsNullOrEmpty(_userData["heartLastChargedTime"].ToString())) mUserData.heartLastChargedTime = ServerBaseTime.AddMilliseconds(_userData["heartLastChargedTime"].AsLong);
            if (!string.IsNullOrEmpty(_userData["securityFinishTime"].ToString())) mUserData.securityFinishTime = ServerBaseTime.AddMilliseconds(_userData["securityFinishTime"].AsLong);
            if (!string.IsNullOrEmpty(_userData["wareHouseFinishTime"].ToString()))
            {
                float _warehouseTime = _userData["wareHouseFinishTime"].AsLong;
                mUserData.wareHouseFinishTime = _warehouseTime == 0 ? DateTime.MinValue : ServerBaseTime.AddMilliseconds(_warehouseTime);
            }
            if (!string.IsNullOrEmpty(_userData["cash"].ToString())){ mUserData.cash = _userData["cash"].AsInt; }
            if (!string.IsNullOrEmpty(_userData["cityUnlockPoint"].ToString())) mUserData.cityUnlockPoint = _userData["cityUnlockPoint"].AsInt;
            if (!string.IsNullOrEmpty(_userData["reserveTime1"].ToString())) mUserData.reserveTime1 = ServerBaseTime.AddMilliseconds(_userData["reserveTime1"].AsLong);
            if (!string.IsNullOrEmpty(_userData["reserveTime2"].ToString())) mUserData.reserveTime2 = ServerBaseTime.AddMilliseconds(_userData["reserveTime2"].AsLong);
            if (!string.IsNullOrEmpty(_userData["reserveTime3"].ToString())) mUserData.reserveTime3 = ServerBaseTime.AddMilliseconds(_userData["reserveTime3"].AsLong);

            //local base data
            if (!_isNewData) return;

            if (!string.IsNullOrEmpty(_userData["reserveTime4"].ToString())) mUserData.reserveTime4 = ServerBaseTime.AddMilliseconds(_userData["reserveTime4"].AsLong);
            if (!string.IsNullOrEmpty(_userData["reserveTime5"].ToString())) mUserData.reserveTime5 = ServerBaseTime.AddMilliseconds(_userData["reserveTime5"].AsLong);

            if (!string.IsNullOrEmpty(_userData["ADBoostDelayTime"].ToString()))
            {
                mUserData.ADBoostDelayTime = _userData["ADBoostDelayTime"].AsLong == 0 ?
                    DateTime.MinValue : ServerBaseTime.AddMilliseconds(_userData["ADBoostDelayTime"].AsLong);
            }
            if (!string.IsNullOrEmpty(_userData["ADBoostCount"].ToString())) mUserData.ADBoostCount = _userData["ADBoostCount"].AsInt;

            bool _ismoneychange = false;
            if (!string.IsNullOrEmpty(_userData["level"].ToString())) mUserData.level = _userData["level"].AsInt;
            if (!string.IsNullOrEmpty(_userData["exp"].ToString())) mUserData.exp = _userData["exp"].AsLong;
            if (!string.IsNullOrEmpty(_userData["currentCityCode"].ToString())) mUserData.currentCityCode = _userData["currentCityCode"].AsInt;
            if (!string.IsNullOrEmpty(_userData["selectedAssistantID"].ToString())) mUserData.selectedAssistantID = _userData["selectedAssistantID"].AsInt;
            if (!string.IsNullOrEmpty(_userData["moneyString"].ToString()))
            {
                _ismoneychange = true;
                mUserData.moneyString = _userData["moneyString"];
            }
            if (!string.IsNullOrEmpty(_userData["moneyLength"].ToString()))
            {
                _ismoneychange = true;
                mUserData.moneyLength = _userData["moneyLength"].AsInt;
            }
            if (!string.IsNullOrEmpty(_userData["wareHouseLevel"].ToString())) mUserData.wareHouseLevel = _userData["wareHouseLevel"].AsInt;
            if (!string.IsNullOrEmpty(_userData["inventoryLevel"].ToString())) mUserData.inventoryLevel = _userData["inventoryLevel"].AsInt;
            UserData.Instance.DataVersion = _data_version;

            if(_ismoneychange) UserData.Instance.TotalMoney = new Money(mUserData.moneyString, mUserData.moneyLength);
        }

        //list data --------------------------------------------------------
        if (!string.IsNullOrEmpty(N["listMemberMail"].ToString()))
        {
            GiftList.Instance.InitList(N["listMemberMail"]);
        }

        if (!string.IsNullOrEmpty(N["listMemberFactory"].ToString()))
        {
            FactoryList.Instance.InitList(N["listMemberFactory"]);
        }

        if (!string.IsNullOrEmpty(N["listMemberCenter"].ToString()))
        {
            RNDList.Instance.InitList(N["listMemberCenter"]);
        }
        
        if (!string.IsNullOrEmpty(N["listMemberQuest"].ToString()))
        {
            QuestList.Instance.InitList(N["listMemberQuest"]);
        }

        if (!string.IsNullOrEmpty(N["listMemberInventory"].ToString()))
        {
            UserData.Instance.InventoryProducts.Clear();

            var _inven = N["listMemberInventory"].AsArray;
            
            for (int i = 0; i < _inven.Count; i++)
            {
                if (UserData.Instance.InventoryProductInfos.ContainsKey(_inven[i]["itemID"].AsInt))
                {
                    UserData.Instance.InventoryProductInfos[_inven[i]["itemID"].AsInt].isAvailable = _inven[i]["isAvailable"].AsInt;
                    UserData.Instance.InventoryProductInfos[_inven[i]["itemID"].AsInt].Trades = _inven[i]["trades"].AsInt;
                }
                else
                {
                    UserData.Instance.InventoryProductInfos.Add(
                        _inven[i]["itemID"].AsInt,
                        new UserData.ProductInfo(
                            _inven[i]["itemID"].AsInt,
                            _inven[i]["isAvailable"].AsInt,
                            _inven[i]["trades"].AsInt
                            ));
                }
                
                if (_inven[i]["amount"].AsInt <= 0) continue;
                UserData.Instance.InventoryProducts.Add(
                    _inven[i]["itemID"].AsInt,
                    new UserData.Product(
                        _inven[i]["itemID"].AsInt,
                        _inven[i]["amount"].AsInt,
                        _inven[i]["totalPrice"].AsLong
                        //_inven[i]["isAvailable"].AsInt,
                        //_inven[i]["trades"].AsInt
                        ));
            }
        }

        if (!string.IsNullOrEmpty(N["listMemberWareHouse"].ToString()))
        {
            UserData.Instance.WareHouseProducts.Clear();

            var _list = N["listMemberWareHouse"].AsArray;
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i]["amount"].AsInt <= 0) continue;
                UserData.Instance.WareHouseProducts.Add(
                    _list[i]["itemID"].AsInt,
                    new UserData.Product(_list[i]["itemID"].AsInt, _list[i]["amount"].AsInt));
            }
        }

        if (!string.IsNullOrEmpty(N["listMemberCity"].ToString()))
        {
            UserData.Instance.CityInfos.Clear();

            var _list = N["listMemberCity"].AsArray;
            for (int i = 0; i < _list.Count; i++)
            {
                UserData.Instance.CityInfos.Add(
                    _list[i]["cityCode"].AsInt,
                    _list[i]["availability"].AsInt);
            }
        }

        if (!string.IsNullOrEmpty(N["listMemberFormula"].ToString()))
        {
            UserData.Instance.Formulas.Clear();

            var _list = N["listMemberFormula"].AsArray;
            for (int i = 0; i < _list.Count; i++)
            {
                for (int j = 0; j < _list[i]["amount"].AsInt; j++)
                {
                    UserData.Instance.Formulas.Add(_list[i]["formulaID"].AsInt);
                }
            }
        }
        
        if (!string.IsNullOrEmpty(N["listMemberBooster"].ToString()))
        {
            UserData.Instance.Boosters.Clear();

            var _list = N["listMemberBooster"].AsArray;
            for (int i = 0; i < _list.Count; i++)
            {
                if(BoosterDB.Instance.Boosters.ContainsKey(_list[i]["itemGroupID"].AsInt))
                    UserData.Instance.Boosters.Add(_list[i]["itemGroupID"].AsInt, _list[i]["boosterLevel"].AsInt);
            }
        }

        if (!string.IsNullOrEmpty(N["listMemberAssistant"].ToString()))
        {
            UserData.Instance.Assistants.Clear();

            var _list = N["listMemberAssistant"].AsArray;
            for (int i = 0; i < _list.Count; i++)
            {
                UserData.Instance.Assistants.Add(_list[i]["assistantID"].AsInt, i == 0 ? 1 : _list[i]["assistantLevel"].AsInt);
            }
        }

        if (!string.IsNullOrEmpty(N["listMemberLevel"].ToString()))
        {
            UserData.Instance.Levels.Clear();

            var _list = N["listMemberLevel"].AsArray;
            for (int i = 0; i < _list.Count; i++)
            {
                UserData.Instance.Levels.Add(_list[i]["level"].AsInt);
            }
        }
    }

    private JSONClass CommonParams()
    {
        JSONClass jsonParam = new JSONClass();
        jsonParam["deviceID"] = SystemInfo.deviceUniqueIdentifier;
        jsonParam["apiVersion"] = "0.0.1"; // 서버 로직 버전
        jsonParam["localCode"] = Application.systemLanguage.ToString();

        JSONClass config_db = new JSONClass();
        config_db["dataNo"].AsInt = 1;
        config_db["dataVersion"].AsInt = ConfigDB.Instance.Version;

        JSONClass level_db = new JSONClass();
        level_db["dataNo"].AsInt = 2;
        level_db["dataVersion"].AsInt = LevelDB.Instance.Version;

        JSONClass factory_db = new JSONClass();
        factory_db["dataNo"].AsInt = 3;
        factory_db["dataVersion"].AsInt = FactoryDB.Instance.Version;

        JSONClass city_db = new JSONClass();
        city_db["dataNo"].AsInt = 4;
        city_db["dataVersion"].AsInt = CityDB.Instance.Version;

        JSONClass region_db = new JSONClass();
        region_db["dataNo"].AsInt = 5;
        region_db["dataVersion"].AsInt = RegionDB.Instance.Version;

        JSONClass item_db = new JSONClass();
        item_db["dataNo"].AsInt = 6;
        item_db["dataVersion"].AsInt = ItemDB.Instance.Version;

        JSONClass itemgroup_db = new JSONClass();
        itemgroup_db["dataNo"].AsInt = 7;
        itemgroup_db["dataVersion"].AsInt = ItemGroupDB.Instance.Version;

        JSONClass formula_db = new JSONClass();
        formula_db["dataNo"].AsInt = 10;
        formula_db["dataVersion"].AsInt = FormulaDB.Instance.Version;

        JSONClass shop_db = new JSONClass();
        shop_db["dataNo"].AsInt = 11;
        shop_db["dataVersion"].AsInt = ShopDB.Instance.Version;

        JSONClass manager_db = new JSONClass();
        manager_db["dataNo"].AsInt = 12;
        manager_db["dataVersion"].AsInt = ManagerDB.Instance.Version;

        JSONClass assistant_db = new JSONClass();
        assistant_db["dataNo"].AsInt = 13;
        assistant_db["dataVersion"].AsInt = AssistantDB.Instance.Version;

        JSONClass booster_db = new JSONClass();
        booster_db["dataNo"].AsInt = 14;
        booster_db["dataVersion"].AsInt = BoosterDB.Instance.Version;

        JSONClass quest_db = new JSONClass();
        quest_db["dataNo"].AsInt = 16;
        quest_db["dataVersion"].AsInt = QuestDB.Instance.Version;

        jsonParam["dataVersion"].AsArray.Add(config_db);
        jsonParam["dataVersion"].AsArray.Add(level_db);
        jsonParam["dataVersion"].AsArray.Add(factory_db);
        jsonParam["dataVersion"].AsArray.Add(city_db);
        jsonParam["dataVersion"].AsArray.Add(region_db);
        jsonParam["dataVersion"].AsArray.Add(item_db);
        jsonParam["dataVersion"].AsArray.Add(itemgroup_db);
        jsonParam["dataVersion"].AsArray.Add(formula_db);
        jsonParam["dataVersion"].AsArray.Add(shop_db);
        jsonParam["dataVersion"].AsArray.Add(manager_db);
        jsonParam["dataVersion"].AsArray.Add(assistant_db);
        jsonParam["dataVersion"].AsArray.Add(booster_db);
        jsonParam["dataVersion"].AsArray.Add(quest_db);

        return jsonParam;
    }

    public JSONClass UploadDatas(JSONClass _root)
    {
        JSONClass _userData = new JSONClass();

        mUserData.moneyString = UserData.Instance.TotalMoney.number;
        mUserData.moneyLength = UserData.Instance.TotalMoney.length;

        _userData["memberUID"].AsLong = mUserData.memberUID;
        _userData["memberGrade"].AsInt = mUserData.memberGrade;
        _userData["nickname"] = mUserData.nickname;
        _userData["photoURL"] = mUserData.photoURL;
        _userData["heart"].AsInt = mUserData.heart;
        _userData["heartLastChargedTime"].AsLong = (long)(mUserData.heartLastChargedTime - ServerBaseTime).TotalMilliseconds;
        _userData["securityFinishTime"].AsLong = (long)(mUserData.securityFinishTime - ServerBaseTime).TotalMilliseconds;
        _userData["wareHouseFinishTime"].AsLong =
            WareHouseManager.Instance.WareHouseUpgradeTime == DateTime.MinValue ? 0 :
            (long)(WareHouseManager.Instance.WareHouseUpgradeTime - ServerBaseTime).TotalMilliseconds;
        _userData["cash"].AsInt = mUserData.cash;
        _userData["level"].AsInt = mUserData.level;
        _userData["exp"].AsLong = mUserData.exp;
        _userData["currentCityCode"].AsInt = mUserData.currentCityCode;
        _userData["selectedAssistantID"].AsInt = mUserData.selectedAssistantID;
        _userData["moneyString"] = mUserData.moneyString;
        _userData["moneyLength"].AsInt = mUserData.moneyLength;
        _userData["wareHouseLevel"].AsInt = mUserData.wareHouseLevel;
        _userData["inventoryLevel"].AsInt = mUserData.inventoryLevel;
        _userData["cityUnlockPoint"].AsInt = mUserData.cityUnlockPoint;
        _userData["reserveTime1"].AsLong = (long)(mUserData.reserveTime1 - ServerBaseTime).TotalMilliseconds;
        _userData["reserveTime2"].AsLong = (long)(mUserData.reserveTime2 - ServerBaseTime).TotalMilliseconds;
        _userData["reserveTime3"].AsLong = (long)(mUserData.reserveTime3 - ServerBaseTime).TotalMilliseconds;
        _userData["reserveTime4"].AsLong = (long)(mUserData.reserveTime4 - ServerBaseTime).TotalMilliseconds;
        _userData["reserveTime5"].AsLong = (long)(mUserData.reserveTime5 - ServerBaseTime).TotalMilliseconds;
        _userData["ADBoostDelayTime"].AsLong = 
            mUserData.ADBoostDelayTime == DateTime.MinValue ? 0 :
            (long)(mUserData.ADBoostDelayTime - ServerBaseTime).TotalMilliseconds;
        _userData["ADBoostCount"].AsInt = mUserData.ADBoostCount;
        _userData["dataVersion"].AsLong = UserData.Instance.DataVersion;

        _root["memberData"] = _userData;
        _root["listMemberFormula"] = UserData.Instance.GetJson()["listMemberFormula"];
        _root["listMemberInventory"] = UserData.Instance.GetJson()["listMemberInventory"];
        //_root["listMemberWareHouse"] = UserData.Instance.GetJson()["listMemberWareHouse"];
        _root["listMemberBooster"] = UserData.Instance.GetJson()["listMemberBooster"];
        _root["listMemberAssistant"] = UserData.Instance.GetJson()["listMemberAssistant"];
        _root["listMemberFactory"] = FactoryManager.Instance.GetJson()["listMemberFactory"];
        _root["listMemberCenter"] = RNDCenterManager.Instance.GetJson()["listMemberCenter"];
        _root["listMemberQuest"] = UserData.Instance.GetJson()["listMemberQuest"];

        return _root;
    }

    private byte[] GetParamsString(JSONClass _params)
    {
        if (JSON_PROTOCOL_URL == JSON_PROTOCOL_URL_CRYPTO)
        {
            /*
            byte[] bytes = Encoding.Default.GetBytes(_params.ToString());
            string _myString = Encoding.UTF8.GetString(bytes);
            string _result = Crypto.AesEncrypt(_myString);
            return Encoding.UTF8.GetBytes(_result);
            */

            string _result = Crypto.AesEncrypt(_params.ToString());
            return Encoding.UTF8.GetBytes(_result);
        }
        else return Encoding.UTF8.GetBytes(_params.ToString());
    }

    private Action<JSONNode> OnProtocolSuccessCB;
    private Action<int, string> OnProtocolErrorCB;
    private Action<string> OnProtocolFailCB;

    public void Login(
        string _acountID,
        int _type,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail)
    {
        if (NetworkHandler.self.isOnRequest) return;

        //Debug.Log("package name : " + Application.identifier);

        int _platformcode = -1;
#if UNITY_EDITOR
        _platformcode = PLATFORM_CODE_PC;
#elif UNITY_ANDROID
        _platformcode = PLATFORM_CODE_AOS;
#elif UNITY_IOS
        _platformcode = PLATFORM_CODE_IOS;
#endif
        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolLogin";
        jsonParam["platformCode"].AsInt = _platformcode;
        jsonParam["accountType"].AsInt = _type;
        jsonParam["accountID"] = _acountID;
        jsonParam["deviceCtn"].AsLong = GetCtn();
        jsonParam["deviceModel"] = SystemInfo.deviceModel;
        jsonParam["osVersion"] = SystemInfo.operatingSystem;
        jsonParam["appVersion"] = APP_VERSION; // 어플리케이션 버전

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));

        Debug.Log("on login");
    }

    public void RequestDataAll
        (
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberDataR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void SetNickname(
        string _nickname,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail)
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberDataNicknameU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["nickname"] = _nickname;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void ShopBuy
        (int _id,
         Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail)
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolShopBuyTemp";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["amount"].AsInt = _id;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }
    
    public void RequestGiftList
        (
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberMailListR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestReceiveGfit
        (
        long _giftUID,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberMailU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["mailUID"].AsLong = _giftUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestReceiveGfitAll
        (
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberMailAllU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }
    
    public void RequestCompleteTutorial(
        int _select_city_code,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberDataTutorialClearU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["cityCode"].AsInt = _select_city_code;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestTravel(
        int _select_city_code,
        long _market_step_code,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        mUserData.currentCityCode = _select_city_code;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolTravel";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["cityCode"].AsInt = _select_city_code;
        jsonParam["marketStepCode"].AsLong = _market_step_code;
        jsonParam = UploadDatas(jsonParam);
        
        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestBuyShopItem(
        int _item_id,
        int _price_type,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolShopBuyU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["shopID"].AsLong = _item_id;
        jsonParam["priceType"].AsInt = _price_type;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestUnlockCity(
        int _item_id,
        int _price_type,
        int _unlock_city,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolShopBuyU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["shopID"].AsLong = _item_id;
        jsonParam["priceType"].AsInt = _price_type;
        jsonParam["cityCode"].AsInt = _unlock_city;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestLevelUp(
        int _level,
        int _city_code,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberLevelC";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["level"].AsLong = _level;
        jsonParam["cityCode"].AsInt = _city_code;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestChatList(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberChatListR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }
    
    public void RequestSendChatMessage(
        string _message,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberChatC";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["message"] = _message;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestRankList(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolRankListR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestRankListFacebook(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolFaceBookFriendsRankListR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["listFaceBookID"] = new JSONArray();

        for (int i = 0; i < FBManager.Instance.FriendDataList.Count + 1; i++)
        {
            JSONClass _node = new JSONClass();
            if (i == 0) _node["faceBookID"] = FBManager.Instance.MyID;
            else _node["faceBookID"] = FBManager.Instance.FriendDataList[i - 1]["id"];
            jsonParam["listFaceBookID"].AsArray.Add(_node);
        }

        Debug.Log(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestTutorialTravel(
        int _cityCode,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolTutorialTravel";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["cityCode"].AsInt = _cityCode;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestUnlockCity(
        int _cityCode,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolCityUnlockByPoint";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["cityCode"].AsInt = _cityCode;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestDataKeywordMoney(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberDataKeywordR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["listKeyword"] = new JSONArray();
        var _str = new JSONClass(); _str["keyword"] = "moneyString";
        var _length = new JSONClass(); _length["keyword"] = "moneyLength";
        jsonParam["listKeyword"].Add(_str);
        jsonParam["listKeyword"].Add(_length);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestDataKeywordCash(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberDataKeywordR";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["keyword"] = "cash";

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }
    
    public void RequestRevisitOpenBox(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolReVisitBoxU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestQuestClear
        (
        int _id,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        if (NetworkHandler.self.isOnRequest) return;

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberQuestRewardU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["questID"].AsInt = _id;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestPurchaseStart
        (
        ShopDB.ShopMember _node,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        var _info = BillingManager.Instance.InfoList[_node.itemKey];

        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberBillingC";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["itemKey"] = _info.itemkey;
#if UNITY_ANDROID
        jsonParam["platformCode"].AsInt = PLATFORM_CODE_AOS;
#elif UNITY_IOS
        jsonParam["platformCode"].AsInt = PLATFORM_CODE_IOS;
#endif
        jsonParam["price"] = _info.PriceKRW;
        jsonParam["currency"] = "KRW";

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestPurchaseUpdateAOS
        (
        string _signedData,
        string _signature,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberBillingU";
        jsonParam["orderNo"].AsLong = BillingManager.Instance.OrderNo;
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
#if UNITY_ANDROID
        jsonParam["platformCode"].AsLong = PLATFORM_CODE_AOS;
#elif UNITY_IOS
        jsonParam["platformCode"].AsLong = PLATFORM_CODE_IOS;
#endif
        jsonParam["signedData"] = _signedData;
        jsonParam["signature"] = _signature;
        jsonParam = UploadDatas(jsonParam);

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestOfferwallRewardCheck(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolRewardCheckRU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;

        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    public void RequestFacebookLogin(
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail
        )
    {
        JSONClass jsonParam = CommonParams();
        jsonParam["requestProtocol"] = "protocolMemberFaceBookCU";
        jsonParam["memberUID"].AsLong = mUserData.memberUID;
        jsonParam["faceBookID"] = FBManager.Instance.MyID;
        jsonParam["nickname"] = FBManager.Instance.MyName;
        
        OnProtocolSuccessCB = _onsuccess;
        OnProtocolErrorCB = _onerror;
        OnProtocolFailCB = _onfail;
        NetworkHandler.self.ResultDelegate = OnProtocolSuccess;
        NetworkHandler.self.ErrorDelegate = OnProtocolFail;
        NetworkHandler.self.ServerRequest(JSON_PROTOCOL_URL, GetParamsString(jsonParam));
    }

    /****************************************************************
    Util Method
    ****************************************************************/
    public int AddbyteStreamSize(int _size, bool _data)
    {
        _size += sizeof(bool); return _size;
    }

    public int AddbyteStreamSize(int _size, int _data)
    {
        _size += sizeof(int); return _size;
    }

    public int AddbyteStreamSize(int _size, short _data)
    {
        _size += sizeof(short); return _size;
    }

    public int AddbyteStreamSize(int _size, long _data)
    {
        _size += sizeof(long); return _size;
    }

    public int AddbyteStreamSize(int _size, float _data)
    {
        _size += sizeof(float); return _size;
    }

    public int AddbyteStreamSize(int _size, string _data)
    {
        _size += sizeof(int);
        _size += Encoding.UTF8.GetByteCount(_data);
        return _size;
    }

    public int AddbyteStreamSize(int _size, byte[] _data)
    {
        _size += sizeof(int);
        _size += _data.Length;
        return _size;
    }

    //-----------
    public int AddByteStreamData(byte[] bytes, bool _data, int _seek)
    {
        Buffer.BlockCopy(BitConverter.GetBytes(_data), 0, bytes, _seek, sizeof(bool));
        _seek += sizeof(bool);

        return _seek;
    }

    public int AddByteStreamData(byte[] bytes, int _data, int _seek)
    {
        Buffer.BlockCopy(BitConverter.GetBytes(_data), 0, bytes, _seek, sizeof(int));
        _seek += sizeof(int);

        return _seek;
    }

    public int AddByteStreamData(byte[] bytes, short _data, int _seek)
    {
        Buffer.BlockCopy(BitConverter.GetBytes(_data), 0, bytes, _seek, sizeof(short));
        _seek += sizeof(short);

        return _seek;
    }

    public int AddByteStreamData(byte[] bytes, long _data, int _seek)
    {
        Buffer.BlockCopy(BitConverter.GetBytes(_data), 0, bytes, _seek, sizeof(long));
        _seek += sizeof(long);

        return _seek;
    }

    public int AddByteStreamData(byte[] bytes, float _data, int _seek)
    {
        Buffer.BlockCopy(BitConverter.GetBytes(_data), 0, bytes, _seek, sizeof(float));
        _seek += sizeof(float);

        return _seek;
    }

    public int AddByteStreamData(byte[] bytes, string _data, int _seek)
    {
        int _str_byte_count = Encoding.UTF8.GetByteCount(_data);
        Buffer.BlockCopy(BitConverter.GetBytes(_str_byte_count), 0, bytes, _seek, sizeof(int));
        _seek += sizeof(int);
        Buffer.BlockCopy(Encoding.UTF8.GetBytes(_data), 0, bytes, _seek, _str_byte_count);
        _seek += _str_byte_count;

        return _seek;
    }

    public int AddByteStreamData(byte[] bytes, byte[] _data, int _seek)
    {
        int _str_byte_count = _data.Length;
        Buffer.BlockCopy(BitConverter.GetBytes(_str_byte_count), 0, bytes, _seek, sizeof(int));
        _seek += sizeof(int);
        Buffer.BlockCopy(_data, 0, bytes, _seek, _str_byte_count);
        _seek += _str_byte_count;

        return _seek;
    }

    private long GetCtn()
    {
#if UNITY_EDITOR
        return -1;
#elif UNITY_ANDROID
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
        using (AndroidJavaObject telephonyManager = activity.Call<AndroidJavaObject>("getSystemService", activity.GetStatic<string>("TELEPHONY_SERVICE")))
        {
            string phoneNum = telephonyManager.Call<string>("getLine1Number");
            if (!string.IsNullOrEmpty(phoneNum))
            {
                return long.Parse(phoneNum);
            }
        }

        return -1;
#else
        return -1;
#endif
    }

}
