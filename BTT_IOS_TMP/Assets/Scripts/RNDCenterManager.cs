﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class RNDCenterManager : MonoBehaviour {

    public class RNDNode
    {
        public int RNDID { get; private set; }
        public int GroupID { get; private set; }
        public int FormulaID = -1;
        public int ItemID
        {
            get
            {
                if (Level == UserData.UNAVAILABLE) return -1;
                if (FormulaID == -1) return -1;
                return FormulaDB.Instance.Formulas[FormulaID].itemID;
            }
        }
        public int UnlockID = -1;
        public long TotalPrice = 0; // for upgrade item
        public int Level;
        public int Amount;
        public float BuildSeconds
        {
            get
            {
                if (Level == UserData.UNAVAILABLE) return -1;
                if (FormulaID != -1) return FormulaDB.Instance.Formulas[FormulaID].time * 60;
                else if (UnlockID != -1) return ItemDB.Instance.ItemDatas[UnlockID].time * 60;
                else return 0;
            }
        }
        public DateTime BuildTime = DateTime.MinValue;
        public float BuildAddTime = 0;
        public long BuildCost
        {
            get
            {
                return ShopDB.Instance.RND[RNDID].priceMoney;
            }
        }
        private Money CurrentBuildCost;

        public RNDNode(int _id, int _groupid)
        {
            RNDID = _id;
            GroupID = _groupid;
        }

        public void OnInit()
        {
            if (Level == UserData.UNAVAILABLE)
            {
                Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(false);
                Instance.RNDUIMaps[RNDID].Gauge.fillAmount = 0;
                Instance.RNDUIMaps[RNDID].Timer.text = "-";

                Instance.RNDUIMaps[RNDID].BtnTab.image.color = Color.gray;
                Instance.RNDUIMaps[RNDID].LockBase.SetActive(true);
                Instance.RNDUIMaps[RNDID].RestBase.SetActive(false);
                Instance.RNDUIMaps[RNDID].WorkBase.SetActive(false);
                Instance.RNDUIMaps[RNDID].FinishBase.SetActive(false);

                CurrentBuildCost = new Money(BuildCost.ToString());
                Instance.RNDUIMaps[RNDID].BtnBuy.transform.Find("Price").GetComponent<Text>().text =
                Instance.RNDUIMaps[RNDID].BtnBuyLock.transform.Find("Price").GetComponent<Text>().text =
                    CurrentBuildCost.ToWon();
                Instance.RNDUIMaps[RNDID].BtnBuyGem.transform.Find("Price").GetComponent<Text>().text =
                    ShopDB.Instance.RND[RNDID].priceGem.ToString();
            }
            else
            {
                if(FormulaID != -1)
                {
                    Instance.RNDUIMaps[RNDID].WorkBase.transform.Find("Process").Find("Label").GetComponent<Text>().text =
                        string.Format(Language.Str.RND_FORMAT_INUSE, ItemDB.Instance.ItemDatas[FormulaDB.Instance.Formulas[FormulaID].reciptItemID].itemName, Amount);
                }
                else if(UnlockID != -1)
                {
                    Instance.RNDUIMaps[RNDID].WorkBase.transform.Find("Process").Find("Label").GetComponent<Text>().text =
                        string.Format(Language.Str.RND_FORMAT_INUSE_UNLOCK, ItemDB.Instance.ItemDatas[UnlockID].itemName);
                }

                bool _isend = (FormulaID != -1 || UnlockID != -1);
                if(_isend)
                {
                    _isend = BuildTime == DateTime.MinValue;
                    if (!_isend) _isend = (BuildTime.AddSeconds(BuildSeconds) - NetworkManager.Instance.GetServerNow()).TotalSeconds < 0;
                }

                if (_isend)
                {
                    Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(true);
                    Instance.RNDUIMaps[RNDID].Amount.text = "!"; //Amount.ToString();
                    Instance.RNDUIMaps[RNDID].Gauge.fillAmount = 1;
                    Instance.RNDUIMaps[RNDID].Timer.text = "-";
                    Instance.RNDUIMaps[RNDID].BtnComplete.transform.Find("Price").GetComponent<Text>().text = "-";
                }
                else
                {
                    Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(false);
                    Instance.RNDUIMaps[RNDID].Gauge.fillAmount = 0;
                    Instance.RNDUIMaps[RNDID].Timer.text =
                        BuildSeconds > 3600 ?
                        string.Format(
                            "{0:00}h{1:00}m",
                            Math.Floor(BuildSeconds / 3600),
                            Math.Floor((BuildSeconds % 3600) / 60)) :
                        string.Format(
                            "{0:00}m{1:00}s",
                            Math.Floor(BuildSeconds / 60),
                            Math.Floor(BuildSeconds % 60));
                }

                InitUnlockBtnUI();
                SetUpgradeBtnActive();

                Instance.RNDUIMaps[RNDID].BtnTab.image.color = Color.white;
                Instance.RNDUIMaps[RNDID].LockBase.SetActive(false);
                Instance.RNDUIMaps[RNDID].RestBase.SetActive(!(FormulaID != -1 || UnlockID != -1));
                Instance.RNDUIMaps[RNDID].WorkBase.SetActive((FormulaID != -1 || UnlockID != -1));
                Instance.RNDUIMaps[RNDID].FinishBase.SetActive(_isend);
            }
        }

        public void InitUnlockBtnUI()
        {
            var _node = UserData.Instance.GetCurrentUnlockItem(GroupID);
            if(_node == null)
            {
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Img").GetComponent<Image>().fillAmount = 0;
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Label").GetComponent<Text>().text = Language.Str.RND_UNLOCK_ALL;
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Block").gameObject.SetActive(false);
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Price").gameObject.SetActive(false);
            }
            else
            {
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Img").GetComponent<Image>().fillAmount = 
                    (float)(_node.Trades) / ItemDB.Instance.ItemDatas[_node.ID].trades;
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Label").GetComponent<Text>().text = Language.Str.RND_UNLOCK;
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Block").gameObject.SetActive(_node.Trades < ItemDB.Instance.ItemDatas[_node.ID].trades);
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Price").gameObject.SetActive(true);
                Instance.RNDUIMaps[RNDID].BtnUnlockItem.transform.Find("Price").GetComponent<Text>().text = 
                    "$" + new Money(ItemDB.Instance.ItemDatas[_node.ID].price.ToString()).ToWon();
            }
        }

        public void SetUpgradeBtnActive()
        {
            bool _isActive = UserData.Instance.isExistUpgradeableItem(GroupID);
            Instance.RNDUIMaps[RNDID].BtnUpgradeItem.interactable = _isActive;
            //Instance.RNDUIMaps[RNDID].BtnUpgradeItem.image.color = _isActive ? Color.white : Color.white * 0.5f;
        }

        public void OnUpdate()
        {
            if (Level == UserData.UNAVAILABLE)
            {
                bool _active = UserData.Instance.TotalMoney.CompareMoney(CurrentBuildCost);
                Instance.RNDUIMaps[RNDID].BtnBuy.gameObject.SetActive(_active);
                Instance.RNDUIMaps[RNDID].BtnBuyLock.gameObject.SetActive(!_active);
                return;
            }
            
            if (BuildTime != DateTime.MinValue && (FormulaID != -1 || UnlockID != -1))
            {
                //BuildAddTime += Time.deltaTime;

                //float _left_time = (float)((BuildTime.AddSeconds(BuildSeconds - BuildAddTime) - BuildTime).TotalSeconds);
                float _left_time = (float)((BuildTime.AddSeconds(BuildSeconds) - NetworkManager.Instance.GetServerNow().AddSeconds(BuildAddTime)).TotalSeconds);
                Instance.RNDUIMaps[RNDID].Timer.text =
                    _left_time > 3600 ?
                        string.Format(
                            "{0:00}h{1:00}m",
                            Math.Floor(_left_time / 3600),
                            Math.Floor((_left_time % 3600) / 60)) :
                        string.Format(
                            "{0:00}m{1:00}s",
                            Math.Floor(_left_time / 60),
                            Math.Floor(_left_time % 60));
                Instance.RNDUIMaps[RNDID].Gauge.fillAmount = (BuildSeconds - _left_time) / BuildSeconds;

                Instance.RNDUIMaps[RNDID].BtnComplete.transform.Find("Price").GetComponent<Text>().text =
                    (Mathf.Ceil(_left_time / 300) * ShopDB.Instance.RND[RNDID].completeGem).ToString("0");

                if (_left_time < 0)
                {
                    //Amount += FormulaDB.Instance.Formulas[FormulaID].amount;

                    Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(true);
                    Instance.RNDUIMaps[RNDID].Amount.text = "!";
                    Instance.RNDUIMaps[RNDID].Timer.text = "-";
                    Instance.RNDUIMaps[RNDID].BtnComplete.transform.Find("Price").GetComponent<Text>().text = "-";
                    Instance.RNDUIMaps[RNDID].FinishBase.SetActive(true);

                    BuildAddTime = 0;
                    BuildTime = DateTime.MinValue;
                    
                    if (UnlockID != -1)
                    {
                        UserData.Instance.GetCurrentUnlockItem(GroupID).isAvailable = UserData.AVAILABLE;
                        if (ItemDB.Instance.ItemDatas.ContainsKey(ItemDB.Instance.ItemDatas[UnlockID].levelRemove))
                            UserData.Instance.InventoryProductInfos[ItemDB.Instance.ItemDatas[UnlockID].levelRemove].isAvailable = 2; // 마켓에서도 보이지않게됨

                        Amount = 0;
                        Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(false);
                        Instance.RNDUIMaps[RNDID].Gauge.fillAmount = 0;
                        Instance.RNDUIMaps[RNDID].Timer.text = "-";

                        UnlockID = -1;

                        Instance.RNDUIMaps[RNDID].RestBase.SetActive(UnlockID == -1);
                        Instance.RNDUIMaps[RNDID].WorkBase.SetActive(UnlockID != -1);
                        Instance.RNDUIMaps[RNDID].FinishBase.SetActive(UnlockID != -1);

                        OnInit();
                        QuestList.Instance.OnRNDUnlock();

                        //PopupManager.Instance.SetPopup("Item unlocked!");
                    }
                }
            }
        }

        public void OnBuildTab()
        {
            if (Level == UserData.UNAVAILABLE) return;

            if (BuildTime != DateTime.MinValue &&
                (FormulaID != -1 || UnlockID != -1))
            {
                float _add_time = 5.0f * UserData.Instance.GetFactoryTabBoost();
                BuildAddTime += _add_time;

                /*
                UITextAppearManager.Instance.SetAppear(
                    Instance.RNDUIMaps[RNDID].Timer.transform.parent,
                    "-1 sec",
                    Instance.RNDUIMaps[RNDID].Timer.fontSize,
                    Instance.RNDUIMaps[RNDID].Timer.color,
                    Color.white
                    );
                    */

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_FACTORY);
            }
        }

        public void OnCollect()
        {
            if (Level == UserData.UNAVAILABLE) return;
            
            if(FormulaID != -1)
            {
                int _success_count = 0;
                float _success_rate = FormulaDB.Instance.Formulas[FormulaID].successRate;
                if (NetworkManager.Instance.mUserData.selectedAssistantID >= 6) _success_rate += 20;
                for (int i = 0; i < Amount; i++)
                    if (UnityEngine.Random.Range(0, 100.0f) < FormulaDB.Instance.Formulas[FormulaID].successRate) _success_count++;

                if (_success_count == 0)
                {
                    Instance.OpenUpgradeResultPopup(FormulaID, 0, 0, Amount);
                }
                else
                {
                    long _total_price = TotalPrice / _success_count;
                    UserData.Instance.AddProductInventory(ItemID, _success_count, _total_price);

                    /*
                    UIEffectManager.Instance.SetEffect(
                                _success_count,
                                Instance.RNDUIMaps[RNDID].BtnTab.transform,
                                WareHouseManager.Instance.InventoryImgRoot.transform,
                                ItemID);
                    */

                    Instance.OpenUpgradeResultPopup(FormulaID, _success_count, _total_price, Amount);
                }

                Amount = 0;
                Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(false);
                Instance.RNDUIMaps[RNDID].Gauge.fillAmount = 0;
                Instance.RNDUIMaps[RNDID].Timer.text = "-";

                FormulaID = -1;

                Instance.RNDUIMaps[RNDID].RestBase.SetActive(FormulaID == -1);
                Instance.RNDUIMaps[RNDID].WorkBase.SetActive(FormulaID != -1);
                Instance.RNDUIMaps[RNDID].FinishBase.SetActive(false);

                OnInit();
                WareHouseManager.Instance.UpdateMainAmount();
                QuestList.Instance.OnRNDUpgrade();

                SoundManager.Instance.PlayEffects(
                    _success_count <= 0
                    ? SoundManager.EFFECT_ERROR
                    : SoundManager.EFFECT_RND_FINISH);
            }
            else if (UnlockID != -1)
            {
                UserData.Instance.GetCurrentUnlockItem(GroupID).isAvailable = UserData.AVAILABLE;
                if (ItemDB.Instance.ItemDatas.ContainsKey(ItemDB.Instance.ItemDatas[UnlockID].levelRemove))
                    UserData.Instance.InventoryProductInfos[ItemDB.Instance.ItemDatas[UnlockID].levelRemove].isAvailable = 2; // 마켓에서도 보이지않게됨

                Amount = 0;
                Instance.RNDUIMaps[RNDID].Amount.transform.parent.gameObject.SetActive(false);
                Instance.RNDUIMaps[RNDID].Gauge.fillAmount = 0;
                Instance.RNDUIMaps[RNDID].Timer.text = "-";

                UnlockID = -1;

                Instance.RNDUIMaps[RNDID].RestBase.SetActive(true);
                Instance.RNDUIMaps[RNDID].WorkBase.SetActive(false);
                Instance.RNDUIMaps[RNDID].FinishBase.SetActive(false);

                OnInit();
                QuestList.Instance.OnRNDUnlock();

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_RND_FINISH);
            }
        }
    }

    [Serializable]
    public struct RNDUINode
    {
        public int ID;
        public Transform Root;

        [HideInInspector]
        public Image IconImg;

        [HideInInspector]
        public GameObject LockBase;
        [HideInInspector]
        public GameObject RestBase;
        [HideInInspector]
        public GameObject WorkBase;
        [HideInInspector]
        public GameObject FinishBase;

        [HideInInspector]
        public Text Amount;
        [HideInInspector]
        public Image Gauge;
        [HideInInspector]
        public Text Timer;

        [HideInInspector]
        public Button BtnBuy;
        [HideInInspector]
        public GameObject BtnBuyLock;
        [HideInInspector]
        public Button BtnBuyGem;

        [HideInInspector]
        public Button BtnTab;
        [HideInInspector]
        public Button BtnUnlockItem;
        [HideInInspector]
        public Button BtnUpgradeItem;
        [HideInInspector]
        public Button BtnComplete;
        [HideInInspector]
        public Button BtnFinish;
    }
    
    public RNDUINode[] RNDUINodes;
    private Dictionary<int, RNDUINode> RNDUIMaps = new Dictionary<int, RNDUINode>();

    private Dictionary<int, RNDNode> RNDNodes = new Dictionary<int, RNDNode>();

    public static RNDCenterManager Instance = null;
    
    [Header("RND Upgrade UI")]
    public GameObject UpgradeBase;
    public Text UpgradeTitle;
    public Image UpgradeIconImg;
    public RectTransform UpgradeScrollRoot;
    private UIScrollNodeProduct[] UpgradeScrollNodes;
    private Dictionary<int, FormulaDB.FormulaMember> UpgradeScrollList;
    private int UpgradeScrollPage = 0;

    [Header("RND Upgrade Amount Popup")]
    public GameObject UpgradePopup;
    public Slider UpgradeAmountSlide;
    public Text UpgradeLabelCurrent;
    public Text UpgradeLabelPrice;
    public UIItemIcon UpgradeItemRootIcon;
    public Text UpgradeItemRootName;
    public UIItemIcon UpgradeItemTargetIcon;
    public Text UpgradeItemTargetName;
    public Text UpgradeLabelUpgradePrice;
    public Text UpgradeLabelUpgradeTime;
    public Text UpgradeLabelUpgradeResult;
    public Text UpgradeLabelSuccessRate;
    public Button UpgradeBtnConform;
    public Button UpgradeBtnCancel;

    [Header("RND Upgrade Result Popup")]
    public GameObject UpgradeResultPopup;
    public GameObject UpgradeResultSuccess;
    public GameObject UpgradeResultFailed;
    public UIItemIcon UpgradeResultItemRootIcon;
    public Text UpgradeResultItemRootName;
    public UIItemIcon UpgradeResultItemTargetIcon;
    public Text UpgradeResultItemTargetName;
    public Text UpgradeResultAmount;
    public Text UpgradeResultPrice;

    private int CurrentRNDID = -1;
    private int CurrentFormulaID = 0;

    void Awake()
    {
        Instance = this;

        RNDNodes.Add(1, new RNDNode(1, 0));
        RNDNodes.Add(2, new RNDNode(2, 1));
        RNDNodes.Add(3, new RNDNode(3, 2));
        RNDNodes.Add(4, new RNDNode(4, 3));
        RNDNodes.Add(5, new RNDNode(5, 4));
        RNDNodes.Add(6, new RNDNode(6, 5));
        RNDNodes.Add(7, new RNDNode(7, 6));
        RNDNodes.Add(8, new RNDNode(8, 7));

        for (int i = 0; i < RNDUINodes.Length; i++)
        {
            RNDUINodes[i].IconImg = RNDUINodes[i].Root.Find("IconImg").GetComponent<Image>();
            RNDUINodes[i].LockBase = RNDUINodes[i].Root.Find("LockedBase").gameObject;
            RNDUINodes[i].RestBase = RNDUINodes[i].Root.Find("RestBase").gameObject;
            RNDUINodes[i].WorkBase = RNDUINodes[i].Root.Find("WorkBase").gameObject;
            RNDUINodes[i].FinishBase = RNDUINodes[i].Root.Find("FinishBase").gameObject;
            RNDUINodes[i].Gauge = RNDUINodes[i].WorkBase.transform.Find("Process").Find("Gauge").GetComponent<Image>();
            RNDUINodes[i].Amount = RNDUINodes[i].Root.Find("AmountBG").GetChild(0).GetComponent<Text>();
            RNDUINodes[i].Timer = RNDUINodes[i].WorkBase.transform.Find("TimeBG").Find("Time").GetComponent<Text>();
            RNDUINodes[i].BtnBuy = RNDUINodes[i].LockBase.transform.Find("BtnUnlock").GetComponent<Button>();
            RNDUINodes[i].BtnBuyLock = RNDUINodes[i].LockBase.transform.Find("BtnLock").gameObject;
            RNDUINodes[i].BtnBuyGem = RNDUINodes[i].LockBase.transform.Find("BtnUnlockGem").GetComponent<Button>();
            RNDUINodes[i].BtnTab = RNDUINodes[i].Root.Find("Icon").GetComponent<Button>();
            RNDUINodes[i].BtnUnlockItem = RNDUINodes[i].RestBase.transform.Find("BtnUnlock").GetComponent<Button>();
            RNDUINodes[i].BtnUpgradeItem = RNDUINodes[i].RestBase.transform.Find("BtnUpgrade").GetComponent<Button>();
            RNDUINodes[i].BtnComplete = RNDUINodes[i].WorkBase.transform.Find("BtnComplete").GetComponent<Button>();
            RNDUINodes[i].BtnFinish = RNDUINodes[i].FinishBase.transform.GetChild(0).GetComponent<Button>();

            RNDUIMaps.Add(RNDUINodes[i].ID, RNDUINodes[i]);
            var _node = RNDNodes[RNDUINodes[i].ID];

            RNDUINodes[i].Root.Find("Name").GetComponent<Text>().text = string.Format(Language.Str.RND_FORMAT_NAME, Language.Str.ITEMGROUPNAME(_node.GroupID));

            RNDUINodes[i].BtnBuy.onClick.AddListener(delegate
            {
                Money _price = new Money(_node.BuildCost.ToString());
                if (UserData.Instance.TotalMoney.CompareMoney(_price))
                {
                    MainScene.Instance.OnUseMoney(_price);
                    _node.Level = UserData.AVAILABLE;
                    _node.OnInit();
                }
                else MainScene.Instance.SetPopupToShop(ShopDB.PRICE_TYPE_MONEY);
            });

            RNDUINodes[i].BtnBuyGem.onClick.AddListener(delegate
            {
                PopupManager.Instance.SetPopupWait();
                if(!MainScene.Instance.CheckPrice(ShopDB.Instance.RND[_node.RNDID].priceGem))
                {
                    NetworkManager.Instance.RequestBuyShopItem(
                    ShopDB.Instance.RND[_node.RNDID].shopID,
                    ShopDB.PRICE_TYPE_GEM,
                    delegate (JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();
                        _node.Level = RNDList.Instance.RNDMaps[_node.RNDID].Availability;
                        _node.Amount = RNDList.Instance.RNDMaps[_node.RNDID].itemCount;
                        _node.BuildTime = RNDList.Instance.RNDMaps[_node.RNDID].createStartTime;
                        _node.OnInit();

                        IGAWorksManager.onRetention("UseCashCompleteRND");
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_RND_INACTIVE_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_RND_INACTIVE_FAIL);
                    }
                    );
                }
            });

            RNDUINodes[i].BtnUpgradeItem.onClick.AddListener(delegate
            {
                if (_node.Level == UserData.UNAVAILABLE) return;

                if (_node.FormulaID == -1) OpenRNDCenter(_node.RNDID);
            });

            RNDUINodes[i].BtnUnlockItem.onClick.AddListener(delegate
            {
                var _info = UserData.Instance.GetCurrentUnlockItem(_node.GroupID);
                if (_info.Trades >= ItemDB.Instance.ItemDatas[_info.ID].trades)
                {
                    Money _price = new Money(ItemDB.Instance.ItemDatas[_info.ID].price.ToString());
                    if (!MainScene.Instance.CheckPrice(_price))
                    {
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_RND_UNLOCKSTART);
                        PopupManager.Instance.SetPopup(
                            string.Format(Language.Str.POPUP_RND_FORMAT_UNLOCK, ItemDB.Instance.ItemDatas[_info.ID].itemName),
                            delegate
                            {
                                MainScene.Instance.OnUseMoney(_price);

                                RNDUIMaps[_node.RNDID].WorkBase.transform.Find("Process").Find("Label").GetComponent<Text>().text =
                                string.Format(Language.Str.RND_FORMAT_INUSE_UNLOCK, ItemDB.Instance.ItemDatas[_info.ID].itemName);

                                _node.UnlockID = _info.ID;
                                _node.Amount = 1;
                                _node.BuildTime = NetworkManager.Instance.GetServerNow();
                                _node.OnInit();

                                IGAWorksManager.onRetention("UnlockRND" + _node.RNDID);
                                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
                            });
                    }
                }
            });

            RNDUINodes[i].BtnTab.onClick.AddListener(delegate
            {
                if (_node.Level == UserData.UNAVAILABLE) return;

                if (_node.BuildTime == DateTime.MinValue && (_node.FormulaID != -1 || _node.UnlockID != 1)) _node.OnCollect();
                else
                {
                    if (_node.UnlockID != -1) _node.OnBuildTab();
                    else if (_node.FormulaID == -1) OpenRNDCenter(_node.RNDID);
                    else _node.OnBuildTab();
                }
            });
            
            RNDUINodes[i].BtnFinish.onClick.AddListener(delegate
            {
                if (_node.Level == UserData.UNAVAILABLE) return;

                if (_node.BuildTime == DateTime.MinValue && (_node.FormulaID != -1 || _node.UnlockID != 1)) _node.OnCollect();
            });

            RNDUINodes[i].BtnComplete.onClick.AddListener(delegate
            {
                PopupManager.Instance.SetPopupWait();
                NetworkManager.Instance.RequestBuyShopItem(
                    ShopDB.Instance.RND[_node.RNDID].shopID,
                    ShopDB.PRICE_TYPE_COMPLETE_GEM,
                    delegate (JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();

                        //_node.Level = RNDList.Instance.RNDMaps[_node.RNDID].Availability;
                        //_node.Amount = RNDList.Instance.RNDMaps[_node.RNDID].itemCount;
                        _node.BuildTime = RNDList.Instance.RNDMaps[_node.RNDID].createStartTime;
                        _node.OnInit();

                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_RND_FINISH);
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    }
                    );
            });

            _node.FormulaID = RNDList.Instance.RNDMaps[_node.RNDID].FormulaID;
            _node.UnlockID = RNDList.Instance.RNDMaps[_node.RNDID].UnlockID;
            _node.TotalPrice = RNDList.Instance.RNDMaps[_node.RNDID].totalPrice;
            _node.Level = RNDList.Instance.RNDMaps[_node.RNDID].Availability;
            _node.Amount = RNDList.Instance.RNDMaps[_node.RNDID].itemCount;
            _node.BuildTime = RNDList.Instance.RNDMaps[_node.RNDID].createStartTime;
            _node.OnInit();
        }

        //------------------------------

        UpgradeScrollNodes = UpgradeScrollRoot.GetComponentsInChildren<UIScrollNodeProduct>();
        for(int i = 0; i<UpgradeScrollNodes.Length; i++)
        {
            int _idx = i;
            UpgradeScrollNodes[i].transform.Find("BtnUpgrade").GetComponent<Button>().onClick.AddListener(delegate
            {
                int _id = UpgradeScrollNodes[_idx].ProductID;
                OpenUpgradeAmountPopup(_id);
            });
        }

        //------------------------------
        
        //CheckBuildTimes();

        UpgradeAmountSlide.onValueChanged.AddListener(delegate
        {
            OnUpgradeSlideValueChange();
        });

        UpgradeBtnConform.onClick.AddListener(delegate
        {
            ActivateUpgradeItem();
        });

        UpgradeBtnCancel.onClick.AddListener(delegate
        {
            UpgradePopup.SetActive(false);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
        });
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < RNDNodes.Count; i++)
        {
            RNDNodes[RNDUINodes[i].ID].OnUpdate();
        }
    }

    public void OpenRNDCenter(int _id)
    {
        CurrentRNDID = _id;

        UpgradeIconImg.sprite = RNDUIMaps[_id].IconImg.sprite;
        UpgradeTitle.text = string.Format(Language.Str.RND_FORMAT_NAME, Language.Str.ITEMGROUPNAME(RNDNodes[_id].GroupID));
        UpgradeScrollList =
            FormulaDB.Instance.Formulas.Where(
                x =>
                ItemDB.Instance.ItemDatas[FormulaDB.Instance.Formulas[x.Key].itemID].itemGroupID == RNDNodes[_id].GroupID
                && UserData.Instance.isExistItemInventory(FormulaDB.Instance.Formulas[x.Key].reciptItemID, 1)
                ).ToDictionary(
                k => k.Key, k => FormulaDB.Instance.Formulas[k.Key]);

        UpgradeBase.SetActive(true);
        UpgradePopup.SetActive(false);
        SetUpgradeScroll();
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }
    
    private void SetUpgradeScroll()
    {
        UpgradeScrollRoot.transform.parent.parent.GetComponent<ScrollRect>().enabled = false;

        UpgradeScrollRoot.anchoredPosition = new Vector2(0, UpgradeScrollPage * 120);

        UpgradeScrollRoot.sizeDelta =
            new Vector2(
                UpgradeScrollRoot.sizeDelta.x,
                510 + ((UpgradeScrollList.Count - 3) * 120)
                );

        SetUpgradeScrollNodes();
        Invoke("ActiveUpgradeScroll", 0.33f);
    }

    private void ActiveUpgradeScroll()
    {
        UpgradeScrollRoot.transform.parent.parent.GetComponent<ScrollRect>().enabled = true;
    }

    private void SetUpgradeScrollNodes()
    {
        for (int i = 0; i < UpgradeScrollNodes.Length; i++)
        {
            var _rect = UpgradeScrollNodes[i].GetComponent<RectTransform>();
            _rect.anchoredPosition =
                new Vector2(
                    _rect.anchoredPosition.x,
                    (UpgradeScrollPage * -120)
                    + (i * -120));

            int _node_idx = i + UpgradeScrollPage;
            if (_node_idx >= UpgradeScrollList.Count)
            {
                _rect.gameObject.SetActive(false);
            }
            else
            {
                var _node = UpgradeScrollList.ElementAt(_node_idx).Value;
                var _inven = UserData.Instance.InventoryProducts[_node.reciptItemID];

                UpgradeScrollNodes[i].ProductID = _node.formulaID;
                _rect.Find("Icon").GetComponent<Image>().sprite = ItemDB.Instance.GetitemImg(_node.itemID);
                _rect.Find("Name").GetComponent<Text>().text = ItemDB.Instance.ItemDatas[_node.itemID].itemName;
                _rect.Find("AvgCost").GetComponent<Text>().text = string.Format(Language.Str.AVG_FORMAT, new Money(_inven.AvgPrice.ToString()).ToWon());
                _rect.Find("Amount").GetComponent<Text>().text = _inven.amount.ToString();

                _rect.gameObject.SetActive(true);
            }
        }
    }

    public void ExitRNDCenter()
    {
        UpgradeBase.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }
    
    private void OnUpgradeSlideValueChange()
    {
        Money _active_price = new Money(FormulaDB.Instance.Formulas[CurrentFormulaID].priceMoney.ToString());
        _active_price.MulMoney(UpgradeAmountSlide.value);
        UpgradeLabelPrice.text = "$" + _active_price.ToMoneyWon();
        UpgradeLabelPrice.color =
            UserData.Instance.TotalMoney.CompareMoney(_active_price) ? Color.black : Color.red;

        UpgradeLabelCurrent.text = UpgradeAmountSlide.value.ToString("0");
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SLIDE);
    }

    public void OpenUpgradeAmountPopup(int _id)
    {
        var _node = FormulaDB.Instance.Formulas[_id];
        
        if (!UserData.Instance.isExistItemInventory(_node.reciptItemID, 1)) return;

        CurrentFormulaID = _id;
        UpgradeItemRootName.text = ItemDB.Instance.ItemDatas[_node.reciptItemID].itemName;
        UpgradeItemRootIcon.SetItem(_node.reciptItemID);
        UpgradeItemTargetName.text = ItemDB.Instance.ItemDatas[_node.itemID].itemName;
        UpgradeItemTargetIcon.SetItem(_node.itemID);

        UpgradeLabelUpgradePrice.text = "$" + new Money(_node.priceMoney.ToString()).ToWon();
        float _build_time = _node.time * 60;
        UpgradeLabelUpgradeTime.text =
            _build_time > 3600 ?
                string.Format(
                    "{0:00}h {1:00}m",
                    Math.Floor(_build_time / 3600),
                    Math.Floor((_build_time % 3600) / 60)) :
                string.Format(
                    "{0:00}m {1:00}s",
                    Math.Floor(_build_time / 60),
                    Math.Floor(_build_time % 60));
        UpgradeLabelSuccessRate.text = string.Format(Language.Str.RND_FORMAT_SUCCESS_RATE, _node.successRate);
        //UpgradeLabelUpgradeResult.text = _node. //new Money(ItemDB.Instance.ItemDatas[_node.itemID].medianPrice.ToString()).ToWon();

        int _left_count = UserData.Instance.InventoryProducts[_node.reciptItemID].amount;
        long _buy_max_tmp = UserData.Instance.TotalMoney.GetInt64() / _node.priceMoney;
        int _buy_max = _buy_max_tmp > int.MaxValue ? int.MaxValue : (int)_buy_max_tmp;
        UpgradeAmountSlide.maxValue = _left_count < _buy_max ? _left_count : _buy_max;
        UpgradeAmountSlide.value = 0;
        OnUpgradeSlideValueChange();

        UpgradePopup.SetActive(true);
    }

    private void ActivateUpgradeItem()
    {
        var _rnd = RNDNodes[CurrentRNDID];
        var _formula = FormulaDB.Instance.Formulas[CurrentFormulaID];

        if (!UserData.Instance.isExistItemInventory(_formula.reciptItemID, 1)) return;

        int _active_amount = (int)UpgradeAmountSlide.value;
        if (_active_amount <= 0) return;

        Money _active_money = new Money(_formula.priceMoney.ToString());
        _active_money.MulMoney(_active_amount);

        if (MainScene.Instance.CheckPrice(_active_money)) return;

        //RND센터 작동
        RNDUIMaps[CurrentRNDID].WorkBase.transform.Find("Process").Find("Label").GetComponent<Text>().text =
        //string.Format("In Use\n{0} ({1})", ItemDB.Instance.ItemDatas[_node.itemID].itemName, _node.amount);
        string.Format(Language.Str.RND_FORMAT_INUSE, ItemDB.Instance.ItemDatas[_formula.reciptItemID].itemName, _active_amount);

        //돈을 제거
        MainScene.Instance.OnUseMoney(_active_money);

        //완성된 아이템의 원가 설정을 위해, 강화비용에 재료아이템의 원가를 더함
        long _total_price = UserData.Instance.InventoryProducts[_formula.reciptItemID].AvgPrice * _active_amount;
        _active_money.AddMoney(new Money(_total_price.ToString()));

        //RND센터 작동 이미지 반영
        _rnd.FormulaID = _formula.formulaID;
        _rnd.Amount = _active_amount;
        _rnd.TotalPrice = _active_money.GetInt64();
        _rnd.BuildTime = NetworkManager.Instance.GetServerNow();
        _rnd.OnInit();

        //원가계산 후 재료를 제거
        UserData.Instance.SubProductInventory(_formula.reciptItemID, _active_amount);

        IGAWorksManager.onRetention("UpgradeRND" + CurrentRNDID);

        CurrentFormulaID = -1;
        WareHouseManager.Instance.UpdateMainAmount();
        UpgradeBase.SetActive(false);
        UpgradePopup.SetActive(false);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SELL);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
    }
    
    private void OpenUpgradeResultPopup(int _id, int _amount, long _total_price, int _max_amount)
    {
        UpgradeResultPopup.SetActive(true);

        var _formula = FormulaDB.Instance.Formulas[_id];

        UpgradeResultItemRootIcon.SetItem(_formula.reciptItemID);
        UpgradeResultItemTargetIcon.SetItem(_formula.itemID);
        UpgradeResultItemRootName.text = ItemDB.Instance.ItemDatas[_formula.reciptItemID].itemName;
        UpgradeResultItemTargetName.text = ItemDB.Instance.ItemDatas[_formula.itemID].itemName;

        if (_amount == 0)
        {
            UpgradeResultSuccess.SetActive(false);
            UpgradeResultFailed.SetActive(true);
        }
        else
        {
            UpgradeResultSuccess.SetActive(true);
            UpgradeResultFailed.SetActive(false);

            Money _avg_cost = new Money(_total_price.ToString());
            UpgradeResultAmount.text = string.Format("{0}/{1}", _amount, _max_amount);
            UpgradeResultPrice.text = string.Format(Language.Str.AVG_FORMAT, _avg_cost.ToWon());
        }
    }

    public void ExitUpgradeResultPopup()
    {
        UpgradeResultPopup.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void SetUnlockItemBtnUI(int _group)
    {
        for (int i = 0; i < RNDNodes.Count; i++)
        {
            var _node = RNDNodes.ElementAt(i).Value;
            if (_node.GroupID == _group)
            {
                _node.InitUnlockBtnUI();
                _node.SetUpgradeBtnActive();
                return;
            }
        }
    }

    /*
    private void CheckBuildTimes()
    {
        return;

        for(int i = RNDNodes.Count-1; i>=0 ; i--)
        {
            var _node = RNDNodes.ElementAt(i).Value;
            if (_node.Level == UserData.UNAVAILABLE
                || _node.FormulaID == -1) continue;

            float _over_time = (float)(NetworkManager.Instance.GetServerNow() - _node.BuildTime.AddSeconds(_node.BuildAddTime)).TotalSeconds;
            _node.BuildAddTime += _over_time;
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        //if (!pauseStatus) CheckBuildTimes();
    }
    */

    public JSONClass GetJson()
    {
        JSONClass _result = new JSONClass();

        for (int i = 0; i < RNDNodes.Count; i++)
        {
            var _info = RNDNodes.ElementAt(i).Value;
            JSONClass _node = new JSONClass();
            _node["centerID"].AsInt = _info.RNDID;
            _node["availability"].AsInt = _info.Level;
            _node["formulaID"].AsInt = _info.FormulaID;
            _node["itemID"].AsInt = _info.UnlockID;
            _node["itemCount"].AsInt = _info.Amount;
            _node["totalPrice"].AsLong = _info.TotalPrice;
            _node["createStartTime"].AsLong = (long)(_info.BuildTime.AddSeconds(-_info.BuildAddTime) - NetworkManager.ServerBaseTime).TotalMilliseconds;
            _result["listMemberCenter"].AsArray.Add(_node);
        }

        return _result;
    }
}
