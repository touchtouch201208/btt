﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProductMoveEffect : MonoBehaviour {
    
    private Transform RootPos;
    private Transform TargetPos = null;
    private Vector3 _dir;
    private float _dis = 0;
    private float _spd = 0;
    private Image ImgIcon;
    private ParticleSystem particle;

    // Use this for initialization
    void Awake()
    {
        particle = GetComponent<ParticleSystem>();
        ImgIcon = transform.GetChild(0).GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if(TargetPos != null)
        {
            if (Vector3.Distance(TargetPos.position, transform.position) > 0.2f)
            {
                transform.position += _dir * (_spd + (_dis - Vector3.Distance(TargetPos.position, transform.position))) * Time.deltaTime;
                _dir = Vector3.Lerp(_dir, ((TargetPos.position) - transform.position).normalized, 0.05f);
            }
            else
            {
                ImgIcon.gameObject.SetActive(false);
                particle.Stop();

                var _ani = TargetPos.GetComponent<Animation>();
                if (_ani != null)
                {
                    _ani.Stop();
                    _ani.Play("PopupAnimation2");
                }
                TargetPos = null;
            }
        }
    }

    public void Setup(
        Transform _root_pos,
        Transform _target_pos,
        int _product_id)
    {
        RootPos = _root_pos;
        TargetPos = _target_pos;
        transform.SetParent(_target_pos.transform);
        transform.localScale = Vector3.one;
        transform.position = RootPos.position;

        _dir = ((TargetPos.position - RootPos.position) * -1).normalized;
        _dir = Quaternion.Euler(0, 0, Random.Range(30, 150) * (Random.Range(0, 2) == 0 ? 1 : -1)) * _dir;
        _dis = Vector3.Distance(TargetPos.position, RootPos.position);
        _spd = Random.Range(4.0f, 7.0f);
        
        ImgIcon.sprite = ItemDB.Instance.GetitemImg(_product_id);
        ImgIcon.gameObject.SetActive(true);
        particle.Play();
    }
}
