﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using SimpleJSON;

public class TitleScene : MonoBehaviour {

    private AndroidPermission permissionCheck;

    private const int SINGLE_PERMISSION = 0;
    private const int MULTIPLE_PERMISSION = 1;

    public string[] permissions;
    private string[] deninedPermissions;

    private void InitPermissionCheck()
    {
        if (permissionCheck == null)
        {
            permissionCheck = new AndroidPermission();
            permissionCheck.Init();

            permissionCheck.OnCheckExplainAction = OnCheckExplain;
            permissionCheck.OnCheckNonExplainAction = OnCheckNonExplain;
            permissionCheck.OnCheckAlreadyAction = OnCheckAlready;
            permissionCheck.OnCheckFailedAction = OnCheckFailed;

            permissionCheck.OnResultAction = OnRequestResult;
        }
    }

    private void RequestPermissions()
    {
        PopupManager.Instance.SetPopup(
            "Please check permissions and continue.",
            delegate
            {
                PopupManager.Instance.SetPopupWait();
                permissionCheck.RequestPermissions(deninedPermissions, MULTIPLE_PERMISSION);
            });
    }

    public bool CheckPermissions()
    {
        if (permissionCheck != null && permissions != null)
        {
            deninedPermissions = permissionCheck.DeninedPermissions(permissions);

            if (deninedPermissions != null)
            {
                if (deninedPermissions.Length <= 0) return true;

                RequestPermissions();
                return false;
            }
            else return true;
        }

        return false;
    }

    public void OnCheckExplain(CheckEventArgs args)
    {
        // Show Explain Dialog
        if (permissionCheck != null)
        {
            permissionCheck.ShowDialog(args.permission, 0, "Request Permission", "Need allow permission.");
        }
    }

    public void OnCheckNonExplain(CheckEventArgs args)
    {
        if (permissionCheck != null)
        {
            permissionCheck.RequestPermission(args.permission, 0);
        }
    }

    public void OnCheckAlready(CheckEventArgs args)
    {
    }

    public void OnCheckFailed(ErrorEventArgs errArgs)
    {
    }

    public void OnRequestResult(ResultEventArgs args)
    {
        if (args.denined.Length > 0)
        {
            bool forceQuit = false;

            for (int i = 0; i < args.denined.Length; i++)
            {
                for (int j = 0; j < permissions.Length; j++)
                {
                    if (args.denined[i] == permissions[j])
                    {
                        forceQuit = true;
                        break;
                    }
                }

                if (forceQuit) break;
            }

            if (forceQuit)
            {
                RequestPermissions();
            }
            else
            {
                PopupManager.Instance.SetOffPopup();
                OnLogin();
            }
        }
        else
        {
            PopupManager.Instance.SetOffPopup();
            OnLogin();
        }
    }

    //-----------------------------

    public UnityEngine.UI.Text Version_Label;
    public UnityEngine.UI.Image LoadingImg_BG;
    public UnityEngine.UI.Image LoadingImg;

    void Awake()
    {
        Version_Label.text = "Ver " + NetworkManager.APP_VERSION;

        CityDB.Instance.InitCityPriceInfo(Resources.Load<TextAsset>("city_infos").text);
    }

    void Start()
    {
        InitPermissionCheck();

        Debug.Log(Language.Str);
        StartCoroutine(StartGame());
    }
    
    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(2.0f);

        if(CheckPermissions()) OnLogin();
        //OnLogin();
    }
	
	// Update is called once per frame
	void Update () {
        
        /*
        if(!PopupManager.Instance.isOnPopup()
            && Input.GetMouseButtonDown(0))
        {
            StartCoroutine(StartGame());
        }
        */
    }

    public void OnLogin()
    {
#if UNITY_EDITOR

        UserData.Instance.LoadData();

        int _type = -1;
        if (Application.identifier == "com.digitaljackie.tradetycoon") _type = NetworkManager.ACCOUNT_TYPE_DJ_DEVICE;
        else if (Application.identifier == "com.rhyzengames.tradetycoon") _type = NetworkManager.ACCOUNT_TYPE_DJ_DEVICE;
        
        NetworkManager.Instance.Login
                (
                "TEST05",
                _type,
                delegate (JSONNode N)
                {
                    IGAWorksManager.onLoginComplete();
                    TapjoyManager.Instance.OnLogin();

                    StartCoroutine(LoadMainScene());
                },

                delegate (int _code, string _message)
                {
                    PopupManager.Instance.SetPopup(_message);
                },

                delegate (string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );
#else
        GPGManager.Instance.Login(
            delegate
            {
                UserData.Instance.LoadData();
                LoginGoogle();
            },
            delegate
            {
                UserData.Instance.LoadData();
                LoginDevice();
            }
            );

        /*
        UserData.Instance.LoadData();
        LoginDevice();
        */
#endif
    }

    private void LoginDevice()
    {
        int _type = -1;
        if (Application.identifier == "com.digitaljackie.tradetycoon") _type = NetworkManager.ACCOUNT_TYPE_DJ_DEVICE;
        else if (Application.identifier == "com.rhyzengames.tradetycoon") _type = NetworkManager.ACCOUNT_TYPE_DJ_DEVICE;
        
        NetworkManager.Instance.Login
                (
                SystemInfo.deviceUniqueIdentifier,
                _type,
                delegate (JSONNode N)
                {
                    IGAWorksManager.onLoginComplete();
                    TapjoyManager.Instance.OnLogin();

                    StartCoroutine(LoadMainScene());
                },

                delegate (int _code, string _message)
                {
                    PopupManager.Instance.SetPopup(_message);
                },

                delegate (string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );
    }

    private void LoginGoogle()
    {
        int _type = -1;
        if (Application.identifier == "com.digitaljackie.tradetycoon") _type = NetworkManager.ACCOUNT_TYPE_DJ_GOOGLE;
        else if (Application.identifier == "com.rhyzengames.tradetycoon") _type = NetworkManager.ACCOUNT_TYPE_DJ_GOOGLE;
        
        NetworkManager.Instance.Login
                    (
                    GPGManager.Instance.GetUserID(),
                    _type,
                    delegate (JSONNode N)
                    {
                        IGAWorksManager.onLoginComplete();
                        TapjoyManager.Instance.OnLogin();
                        
                        StartCoroutine(LoadMainScene());
                    },

                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(_message);
                    },

                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_PERMISSION);
                    }
               );
    }

    private void PassTutorialOnLogin()
    {
        NetworkManager.Instance.RequestCompleteTutorial
            (
            1,
            delegate (JSONNode N2)
            {
                StartCoroutine(LoadMainScene());
            },

            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(_message);
            },

            delegate (string _message)
            {
                PopupManager.Instance.SetPopup("Please check your internet connection and restart game.");
            }
            );
    }

    IEnumerator LoadMainScene()
    {
        LoadingImg_BG.gameObject.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync("MainScene", LoadSceneMode.Single);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f)
        {
            LoadingImg.fillAmount = async.progress;
            yield return null;
        }

        LoadingImg.fillAmount = 1.0f;
        async.allowSceneActivation = true;

        /*
        AsyncOperation async = SceneManager.LoadSceneAsync("MainScene", LoadSceneMode.Single);
        while (!async.isDone) yield return null;
        */
    }
}
