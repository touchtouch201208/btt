﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class FactoryManager : MonoBehaviour {
    
    public class FactoryNode
    {
        public int FactoryID { get; private set; }
        public int ItemID { get; private set; }
        public int Level;
        public int ManagerLavel = 0;
        public int Amount;
        public Money ProductPrice
        {
            get
            {
                Money _return = new Money(FactoryDB.Instance.FactoryDatas[FactoryID][Level].value.ToString());
                float _manager_bonus = 1;
                if (ManagerLavel == 2) _manager_bonus = 1.1f;
                else if (ManagerLavel == 3) _manager_bonus = 1.25f;
                else if (ManagerLavel == 4) _manager_bonus = 1.5f;
                else if (ManagerLavel >= 5) _manager_bonus = 2;
                _return.MulMoney(_manager_bonus);
                _return.MulMoney(UserData.Instance.GetFactoryBoost());

                return _return;
            }
        }
        public float BuildSeconds
        {
            get
            {
                return FactoryDB.Instance.FactoryDatas[FactoryID][Level].time * 60;
            }
        }
        public DateTime BuildTime = DateTime.MinValue;
        public float BuildAddTime = 0;
        public long BuildCost
        {
            get
            {
                if (Level >= FactoryDB.Instance.FactoryMaxLevel) return 0;
                return FactoryDB.Instance.FactoryDatas[FactoryID][Level + 1].buildCost;
            }
        }
        private string Name
        {
            get
            {
                return Language.Str.ITEMNAME(ItemID) + Language.Str.FACTORY;
            }
        }

        private string LevelStr
        {
            get
            {
                return string.Format("{0}", Level.ToString());
            }
        }
        private Money FirstBuildCost;
        private Money CurrentBuildCost;

        public FactoryNode(int _id, int _itemid)
        {
            FactoryID = _id;
            ItemID = _itemid;

            FirstBuildCost = new Money(((long)FactoryDB.Instance.FactoryDatas[FactoryID][1].buildCost).ToString());
        }

        public void OnInit()
        {
            if(Level <= 0)
            {
                Instance.FactoryUINodeMap[FactoryID].Level.text = "";
                Instance.FactoryUINodeMap[FactoryID].Amount.transform.parent.gameObject.SetActive(false);
                Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount = 0;
                Instance.FactoryUINodeMap[FactoryID].Timer.text = "-";

                Instance.FactoryUINodeMap[FactoryID].IconImg.color = Color.black;
                Instance.FactoryUINodeMap[FactoryID].BtnTab.image.color = Color.gray;
                //Instance.FactoryUINodeMap[FactoryID].BtnTab.transform.GetChild(0).GetComponent<Animation>().Stop();
                //Instance.FactoryUINodeMap[FactoryID].BtnTab.transform.GetChild(0).GetComponent<Animation>().playAutomatically = false;
                Instance.FactoryUINodeMap[FactoryID].Base.gameObject.SetActive(false);
                Instance.FactoryUINodeMap[FactoryID].ImgBlock.SetActive(true);
                Instance.FactoryUINodeMap[FactoryID].ImgBlock.transform.GetChild(0).GetComponent<Text>().text =
                    string.Format("{0}\n${1}", Language.Str.FACTORYNAME(FactoryID), FirstBuildCost.ToWon());
            }
            else
            {
                CurrentBuildCost = new Money(BuildCost.ToString());
                Instance.FactoryUINodeMap[FactoryID].Level.text = LevelStr;
                Instance.FactoryUINodeMap[FactoryID].LevelGauge.fillAmount =
                    Level >= FactoryDB.Instance.FactoryMaxLevel ? 1.0f :
                    (float)FactoryDB.Instance.FactoryDatas[FactoryID][Level].step / FactoryDB.Instance.FactoryDatas[FactoryID][Level].maxStep;
                Instance.FactoryUINodeMap[FactoryID].UpgradePrice.text = "$" + CurrentBuildCost.ToWon();
                Instance.FactoryUINodeMap[FactoryID].UpgradeBlockPrice.text = "$" + CurrentBuildCost.ToWon();
                Instance.FactoryUINodeMap[FactoryID].Amount.transform.parent.gameObject.SetActive(Amount > 0);
                Instance.FactoryUINodeMap[FactoryID].Amount.text = Amount.ToString();
                Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount = Amount >= 10 ? 1.0f : 0;
                //Instance.FactoryUINodeMap[FactoryID].Gauge.transform.GetChild(0).gameObject.SetActive(BuildSeconds < 1.0f);
                Instance.FactoryUINodeMap[FactoryID].Timer.text =
                    Amount >= 10 ? "-" : string.Format(
                        "{0:00}:{1:00}",
                        Math.Floor((BuildSeconds % 3600) / 60),
                        Math.Floor(BuildSeconds % 60));

                Instance.FactoryUINodeMap[FactoryID].IconImg.color = Color.white;
                Instance.FactoryUINodeMap[FactoryID].BtnTab.image.color = Color.white;
                //Instance.FactoryUINodeMap[FactoryID].BtnTab.transform.GetChild(0).GetComponent<Animation>().Play();
                //Instance.FactoryUINodeMap[FactoryID].BtnTab.transform.GetChild(0).GetComponent<Animation>().playAutomatically = true;
                Instance.FactoryUINodeMap[FactoryID].ImgBlock.SetActive(false);
                Instance.FactoryUINodeMap[FactoryID].Base.gameObject.SetActive(true);

                SetMaxLevel();
            }

            Instance.FactoryUINodeMap[FactoryID].Manager.gameObject.SetActive(false);

            /*
            if(ManagerLavel > 0)
            {
                Instance.FactoryUINodeMap[FactoryID].Manager.gameObject.SetActive(true);
                Instance.FactoryUINodeMap[FactoryID].Manager.sprite = ManagerDB.Instance.Managers[FactoryID][ManagerLavel - 1].Img;
                Instance.FactoryUINodeMap[FactoryID].ManagerLabel.text = "";
            }
            else Instance.FactoryUINodeMap[FactoryID].Manager.gameObject.SetActive(false);
            */
        }

        public void OnUpdate()
        {
            if (Level <= 0)
            {
                Instance.FactoryUINodeMap[FactoryID].ImgBlock.GetComponent<Image>().sprite =
                    UserData.Instance.TotalMoney.CompareMoney(FirstBuildCost) ? Instance.Img_Source_Btn_Unlock : Instance.Img_Source_Btn_Block;
                return;
            }

            OnGetMoney();
            Instance.FactoryUINodeMap[FactoryID].Value.text = "$" + ProductPrice.ToWon();

            if (BuildTime != DateTime.MinValue && Amount < 10)
            {
                //BuildAddTime += Time.deltaTime;

                //float _left_time = (float)((BuildTime.AddSeconds(BuildSeconds - BuildAddTime) - BuildTime).TotalSeconds);
                float _left_time = (float)((BuildTime.AddSeconds(BuildSeconds) - NetworkManager.Instance.GetServerNow().AddSeconds(BuildAddTime)).TotalSeconds);
                Instance.FactoryUINodeMap[FactoryID].Timer.text
                    = string.Format(
                        "{0:00}:{1:00}",
                        Math.Floor((_left_time % 3600) / 60),
                        Math.Floor(_left_time % 60));
                //Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount = 
                float _gauge_val = BuildSeconds < 0.8f ? 1.0f : (BuildSeconds - _left_time) / BuildSeconds;
                Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount =
                    Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount > _gauge_val ? 0 :
                    Mathf.Lerp(Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount, _gauge_val, 0.25f);

                if (_left_time < 0)
                {
                    if (_left_time < -86400) _left_time = -86400;
                    int _amount_count = Mathf.Abs((int)(_left_time / BuildSeconds)) + 1;

                    if (ManagerLavel >= 1)
                    {
                        Money _sell_price = new Money(ProductPrice);
                        _sell_price.MulMoney(_amount_count);
                        
                        MainScene.Instance.OnGetMoney(_sell_price);

                        /*
                        UITextAppearManager.Instance.SetAppear(
                            Instance.FactoryUINodeMap[FactoryID].Level.transform,
                            string.Format("+${0}", _sell_price.ToWon()),
                            32,
                            Color.green,
                            Color.black
                            );
                            */

                        Instance.FactoryUINodeMap[FactoryID].Amount.transform.parent.gameObject.SetActive(Amount > 0);
                        Instance.FactoryUINodeMap[FactoryID].Gauge.fillAmount =
                            BuildSeconds < 0.8f ? 1.0f : 0;
                        Instance.FactoryUINodeMap[FactoryID].Timer.text =
                            Instance.FactoryUINodeMap[FactoryID].Timer.text
                            = string.Format(
                                "{0:00}:{1:00}",
                                Math.Floor((BuildSeconds % 3600) / 60),
                                Math.Floor(BuildSeconds % 60));

                        WareHouseManager.Instance.UpdateMainAmount();
                    }
                    // 매니저 0레벨, 공장에 상품 누적
                    else
                    {
                        Amount += _amount_count;
                        if (Amount >= 10) Amount = 10;

                        Instance.FactoryUINodeMap[FactoryID].Amount.transform.parent.gameObject.SetActive(true);
                        Instance.FactoryUINodeMap[FactoryID].Amount.text = Amount.ToString();
                        Instance.FactoryUINodeMap[FactoryID].Timer.text = "-";
                    }

                    _left_time += _amount_count * BuildSeconds;
                    BuildAddTime = Amount >= 10 ? 0 : BuildSeconds - _left_time;
                    BuildTime = Amount >= 10 ? DateTime.MinValue : NetworkManager.Instance.GetServerNow();
                }
            }
        }

        public void OnBuildTab()
        {
            if (Level <= 0) return;

            if (BuildTime == DateTime.MinValue)
            {
                BuildTime = NetworkManager.Instance.GetServerNow();
                BuildAddTime = 0;
            }
            else
            {
                float _add_time = 5.0f * UserData.Instance.GetFactoryTabBoost();
                BuildAddTime += _add_time;
                
                /*
                UITextAppearManager.Instance.SetAppear(
                    Instance.FactoryUINodeMap[FactoryID].Timer.transform.parent,
                    string.Format("-{0} sec", _add_time),
                    Instance.FactoryUINodeMap[FactoryID].Timer.fontSize,
                    Instance.FactoryUINodeMap[FactoryID].Timer.color,
                    Color.white
                    );
                    */

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_FACTORY);
            }
        }

        public void OnCollect()
        {
            if (Level <= 0) return;
            if (Amount <= 0) return;

            Money _sell_price = new Money(ProductPrice);
            _sell_price.MulMoney(Amount);
            MainScene.Instance.OnGetMoney(_sell_price);
                
            var _ui = Instance.FactoryUINodeMap[FactoryID];
                
            UIEffectManager.Instance.SetEffect(
                Amount,
                _ui.BtnTab.transform,
                MainScene.Instance.Txt_TotalMoney.transform,
                -1);

            /*
            UITextAppearManager.Instance.SetAppear(
                _ui.Level.transform,
                string.Format("+${0}", _sell_price.ToWon()),
                32,
                Color.green,
                Color.black
                );
                */

            Amount = 0;
            _ui.Amount.transform.parent.gameObject.SetActive(false);
            
            WareHouseManager.Instance.UpdateAllWareHouse();

            if (BuildTime == DateTime.MinValue)
            {
                BuildTime = NetworkManager.Instance.GetServerNow();
                BuildAddTime = 0;
            }
        }

        public void OnUpgrade()
        {
            if (UserData.Instance.TotalMoney.CompareMoney(CurrentBuildCost))
            {
                MainScene.Instance.OnUseMoney(CurrentBuildCost);
                Level++;

                //효과증가 UI 표시
                if (FactoryDB.Instance.FactoryDatas[FactoryID][Level].step == 0)
                {
                    Instance.ToastMessage.SetActive(false);
                    Instance.ToastMessage.SetActive(true);
                    QuestList.Instance.OnFactoryUpgarde();
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
                }

                //Instance.FactoryUINodeMap[FactoryID].Name.text = Name;
                Instance.FactoryUINodeMap[FactoryID].Level.text = LevelStr;
                Instance.FactoryUINodeMap[FactoryID].LevelGauge.fillAmount =
                    Level >= FactoryDB.Instance.FactoryMaxLevel ? 1.0f : 
                    (float)FactoryDB.Instance.FactoryDatas[FactoryID][Level].step / FactoryDB.Instance.FactoryDatas[FactoryID][Level].maxStep;

                //Instance.FactoryUINodeMap[FactoryID].Gauge.transform.GetChild(0).gameObject.SetActive(BuildSeconds < 1.0f);

                UITextAppearManager.Instance.SetAppear(
                    Instance.FactoryUINodeMap[FactoryID].LevelGauge.transform,
                    "- $" + CurrentBuildCost.ToWon(),
                    Instance.FactoryUINodeMap[FactoryID].UpgradePrice.fontSize,
                    Instance.FactoryUINodeMap[FactoryID].UpgradePrice.color,
                    Color.red);

                Instance.FactoryUINodeMap[FactoryID].Level.GetComponent<Animation>().Stop();
                Instance.FactoryUINodeMap[FactoryID].Level.GetComponent<Animation>().Play();

                CurrentBuildCost = new Money(BuildCost.ToString());
                Instance.FactoryUINodeMap[FactoryID].UpgradePrice.text = "$" + CurrentBuildCost.ToWon();
                Instance.FactoryUINodeMap[FactoryID].UpgradeBlockPrice.text = "$" + CurrentBuildCost.ToWon();

                SetMaxLevel();

                /* 다음공장 자동해제 : 생략
                if (FactoryID != Instance.FactoryUINodes[Instance.FactoryUINodes.Length - 1].ID
                    && Instance.Factorys[FactoryID + 1].Level == 0
                    && Level >= 10)
                {
                    Instance.Factorys[FactoryID + 1].Level = 1;
                    Instance.Factorys[FactoryID + 1].BuildTime = NetworkManager.Instance.GetServerNow();
                    Instance.Factorys[FactoryID + 1].OnInit();
                }
                */

                //SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_FACTORY_UPGRADE);
            }
            else SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);  
        }

        private void SetMaxLevel()
        {
            if (Level >= FactoryDB.Instance.FactoryMaxLevel)
            {
                Instance.FactoryUINodeMap[FactoryID].BtnUpgrade.gameObject.SetActive(false);
                Instance.FactoryUINodeMap[FactoryID].ImgUpgradeBlock.SetActive(false);
                Instance.FactoryUINodeMap[FactoryID].ImgUpgradeMAX.SetActive(true);
            }
        }

        public void OnUnlock()
        {
            if(UserData.Instance.TotalMoney.CompareMoney(FirstBuildCost))
            {
                MainScene.Instance.OnUseMoney(FirstBuildCost);
                Instance.Factorys[FactoryID].Level = 1;
                Instance.Factorys[FactoryID].BuildTime = NetworkManager.Instance.GetServerNow();
                Instance.Factorys[FactoryID].OnInit();

                QuestList.Instance.OnFactoryOpen(FactoryID);

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_FACTORY_UPGRADE);
            }
            else SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
        }

        public void OnGetMoney()
        {
            if (Level >= FactoryDB.Instance.FactoryMaxLevel) return;

            if(Level > 0)
            {
                bool _active = UserData.Instance.TotalMoney.CompareMoney(CurrentBuildCost);
                Instance.FactoryUINodeMap[FactoryID].BtnUpgrade.gameObject.SetActive(_active);
                Instance.FactoryUINodeMap[FactoryID].ImgUpgradeBlock.SetActive(!_active);
            }
        }
    }

    public Dictionary<int, FactoryNode> Factorys = new Dictionary<int, FactoryNode>();

    [Serializable]
    public struct FactoryUINode
    {
        public int ID;
        public Transform Root;
        public Image IconImg;

        [HideInInspector]
        public Transform Base;
        [HideInInspector]
        public Text Name;
        [HideInInspector]
        public Text Level;
        [HideInInspector]
        public Image LevelGauge;
        [HideInInspector]
        public Text Amount;
        [HideInInspector]
        public Image Gauge;
        [HideInInspector]
        public Text Value;
        [HideInInspector]
        public Text Timer;
        [HideInInspector]
        public Text UpgradePrice;
        [HideInInspector]
        public Text UpgradeBlockPrice;

        [HideInInspector]
        public Button BtnTab;
        [HideInInspector]
        public Button BtnUpgrade;
        [HideInInspector]
        public GameObject ImgUpgradeBlock;
        [HideInInspector]
        public GameObject ImgUpgradeMAX;
        [HideInInspector]
        public GameObject ImgBlock;
        [HideInInspector]
        public Image Manager;
        [HideInInspector]
        public Text ManagerLabel;
    }

    public GameObject ToastMessage;
    public Sprite Img_Source_Btn_Block;
    public Sprite Img_Source_Btn_Unlock;
    public FactoryUINode[] FactoryUINodes;
    private Dictionary<int, FactoryUINode> FactoryUINodeMap = new Dictionary<int, FactoryUINode>();

    public static FactoryManager Instance = null;

    void Awake()
    {
        Instance = this;

        Factorys.Add(1, new FactoryNode(1, 1));
        Factorys.Add(2, new FactoryNode(2, 2));
        Factorys.Add(3, new FactoryNode(3, 3));
        Factorys.Add(4, new FactoryNode(4, 4));
        Factorys.Add(5, new FactoryNode(5, 5));
        Factorys.Add(6, new FactoryNode(6, 6));
        Factorys.Add(7, new FactoryNode(7, 7));
        Factorys.Add(8, new FactoryNode(8, 8));
        Factorys.Add(9, new FactoryNode(9, 9));
        Factorys.Add(10, new FactoryNode(10, 10));

        for (int i = 0; i<FactoryUINodes.Length; i++)
        {
            FactoryUINodes[i].Base = FactoryUINodes[i].Root.Find("Base");
            FactoryUINodes[i].Name = FactoryUINodes[i].Base.Find("Name").GetComponent<Text>();
            FactoryUINodes[i].LevelGauge = FactoryUINodes[i].Base.Find("LevelBG").GetChild(0).GetComponent<Image>();
            FactoryUINodes[i].Level = FactoryUINodes[i].Base.Find("LevelBG").GetChild(1).GetComponent<Text>();
            FactoryUINodes[i].Amount = FactoryUINodes[i].Base.Find("AmountBG").GetChild(0).GetComponent<Text>();
            FactoryUINodes[i].Gauge = FactoryUINodes[i].Base.Find("Gauge").GetComponent<Image>();
            FactoryUINodes[i].Value = FactoryUINodes[i].Base.Find("Value").GetComponent<Text>();
            FactoryUINodes[i].Timer = FactoryUINodes[i].Base.Find("TimeBG").GetChild(0).GetComponent<Text>();
            FactoryUINodes[i].BtnUpgrade = FactoryUINodes[i].Base.Find("UpgradeButton").GetComponent<Button>();
            FactoryUINodes[i].UpgradePrice = FactoryUINodes[i].BtnUpgrade.transform.Find("Price").GetComponent<Text>();
            FactoryUINodes[i].ImgUpgradeBlock = FactoryUINodes[i].Base.Find("LockButton").gameObject;
            FactoryUINodes[i].UpgradeBlockPrice = FactoryUINodes[i].ImgUpgradeBlock.transform.Find("Price").GetComponent<Text>();
            FactoryUINodes[i].ImgUpgradeMAX = FactoryUINodes[i].Base.Find("UpgradeMaxLevel").gameObject;
            FactoryUINodes[i].BtnTab = FactoryUINodes[i].Base.Find("Icon").GetComponent<Button>();
            FactoryUINodes[i].ImgBlock = FactoryUINodes[i].Root.Find("Block").gameObject;
            FactoryUINodes[i].Manager = FactoryUINodes[i].Base.Find("Manager").GetComponent<Image>();
            FactoryUINodes[i].ManagerLabel = FactoryUINodes[i].Manager.transform.GetChild(0).GetComponent<Text>();

            FactoryUINodes[i].Level.GetComponent<RectTransform>().pivot = Vector2.one / 2.0f;

            FactoryUINodeMap.Add(FactoryUINodes[i].ID, FactoryUINodes[i]);
            var _node = Factorys[FactoryUINodes[i].ID];

            FactoryUINodes[i].BtnTab.onClick.AddListener(delegate
            {
                if (_node.Amount > 0) _node.OnCollect();
                else _node.OnBuildTab();
            });

            FactoryUINodes[i].BtnUpgrade.onClick.AddListener(delegate
            {
                _node.OnUpgrade();
            });

            FactoryUINodes[i].ImgBlock.GetComponent<Button>().onClick.AddListener(delegate
            {
                _node.OnUnlock();
            });

            _node.Level = FactoryList.Instance.Factorys[i].level;
            _node.ManagerLavel = FactoryList.Instance.Factorys[i].managerLevel;
            _node.Amount = FactoryList.Instance.Factorys[i].itemCount;
            _node.BuildTime = FactoryList.Instance.Factorys[i].createStartTime;
            _node.OnInit();
        }

        //CheckBuildTimes();
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        
        UpdateFactorys();
	}

    public void UpdateFactorys()
    {
        if (TutorialManager.Instance.Step > -1
            && TutorialManager.Instance.Step <= 1) return;

        for (int i = 0; i < FactoryUINodes.Length; i++)
        {
            Factorys[FactoryUINodes[i].ID].OnUpdate();
        }
    }

    private void CheckBuildTimes()
    {
        return;

        /*
        for (int i = Factorys.Count - 1; i >= 0; i--)
        {
            var _node = Factorys.ElementAt(i).Value;
            if (_node.Level <= 0
                || _node.BuildTime == DateTime.MinValue
                || _node.Amount >= 10) continue;

            float _over_time = (float)(NetworkManager.Instance.GetServerNow() - _node.BuildTime.AddSeconds(_node.BuildAddTime)).TotalSeconds;
            _node.BuildAddTime += _over_time;
        }
        */
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus) CheckBuildTimes();
    }

    public JSONClass GetJson()
    {
        JSONClass _result = new JSONClass();

        for(int i  =0; i<Factorys.Count; i++)
        {
            var _info = Factorys.ElementAt(i).Value;
            JSONClass _node = new JSONClass();
            _node["itemID"].AsInt = _info.ItemID;
            _node["createStartTime"].AsLong = (long)(_info.BuildTime.AddSeconds(-_info.BuildAddTime) - NetworkManager.ServerBaseTime).TotalMilliseconds;
            _node["itemCount"].AsInt = _info.Amount;
            _node["level"].AsInt = _info.Level;
            _node["managerLevel"].AsInt = _info.ManagerLavel;
            _node["upgradeStartTime"].AsInt = 0;
            _result["listMemberFactory"].AsArray.Add(_node);
        }

        return _result;
    }
}
