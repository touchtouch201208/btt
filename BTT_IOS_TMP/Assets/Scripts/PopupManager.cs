﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PopupManager : MonoBehaviour
{
    public Canvas CanvasRoot;
    public GameObject PopupBase;
    public GameObject PopupIndicator;
    public Text PopupMessage;
    public Button PopupOkButton;
    public Text PopupOKLabel;
    public Button PopupCloseButton;
    public Text PopupCloseLabel;
    private int BtnFontRootSize;

    public static PopupManager Instance = null;

    // Use this for initialization
    void Awake()
    {
        Instance = this;
        
        PopupOkButton.onClick.AddListener(delegate
        {
            SetOffPopup();
            if (PopupProcess != null) PopupProcess();
        });
        
        PopupCloseButton.onClick.AddListener(delegate 
        {
            SetOffPopup();
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
        });

        BtnFontRootSize = PopupOKLabel.fontSize;

        transform.localScale = Vector3.one;
        gameObject.SetActive(false);
    }

    private Action PopupProcess = null;
    public void SetPopup(
        string _message,
        Action _process = null,
        string _ok_label = "",
        string _close_label = ""
        )
    {
        PopupBase.SetActive(true);
        PopupIndicator.SetActive(false);
        PopupProcess = _process;
        PopupMessage.text = _message;

        if (_ok_label == "") _ok_label = Language.Str.CONFIRM;
        if (_close_label== "") _close_label = Language.Str.CANCEL;
        PopupOKLabel.text = _ok_label;
        PopupCloseLabel.text = _close_label;

        if (_process == null)
        {
            PopupOkButton.gameObject.SetActive(true);
            PopupCloseButton.gameObject.SetActive(false);

            PopupOKLabel.fontSize = BtnFontRootSize;
            PopupOKLabel.resizeTextForBestFit = true;
            PopupOkButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -100);
        }
        else
        {
            PopupOkButton.gameObject.SetActive(true);
            PopupCloseButton.gameObject.SetActive(true);

            PopupOKLabel.fontSize = BtnFontRootSize;
            PopupCloseLabel.fontSize = BtnFontRootSize;
            PopupOKLabel.resizeTextForBestFit = true;
            PopupCloseLabel.resizeTextForBestFit = true;
            Invoke("SetPopupBtnFontSize", 0.1f);

            PopupOkButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(-132, -100);
            PopupCloseButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(132, -100);
        }
    }

    private void SetPopupBtnFontSize()
    {
        float _size_ok = PopupOKLabel.cachedTextGenerator.fontSizeUsedForBestFit * (1 / CanvasRoot.scaleFactor);
        float _size_close = PopupCloseLabel.cachedTextGenerator.fontSizeUsedForBestFit * (1 / CanvasRoot.scaleFactor);
        float _size = Mathf.Min(_size_ok, _size_close);

        PopupOKLabel.resizeTextForBestFit = false;
        PopupCloseLabel.resizeTextForBestFit = false;
        PopupOKLabel.fontSize = (int)_size;
        PopupCloseLabel.fontSize = (int)_size;
    }
    
    public void SetPopupWait()
    {
        PopupBase.SetActive(true);
        PopupIndicator.SetActive(true);
        PopupMessage.rectTransform.sizeDelta = new Vector2(435, 173);
        PopupMessage.text = Language.Str.POPUP_MESSAGE_WAIT;

        PopupOkButton.gameObject.SetActive(false);
        PopupCloseButton.gameObject.SetActive(false);
    }

    public void SetOffPopup() { PopupBase.SetActive(false); }
    public bool isOnPopup() { return PopupBase.activeSelf; }
    public void OnEscape() { if (PopupOkButton.gameObject.activeSelf) SetOffPopup(); }
}
