﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LanguagePackBase
{
    //base
    public string GEM;
    public string MONEYUNIT;
    public string BUY;
    public string SELL;
    public string YES;
    public string NO;
    public string OK;
    public string CANCEL;
    public string CLOSE;
    public string REQUIRE;
    public string CONFIRM;
    public string MAX;
    public string MAX_LEVEL; // MAX LEVEL
    public string NONE;
    public string TOTAL;

    public string LEVEL_FORMAT; // Lvl {0}
    public string FACTORY;
    public string BOOST;
    public string BONUS;
    public string OFF;
    public string EA;
    public string AVG;
    public string AVG_FORMAT; // "Avg Cost ${0}"
    public string LOCK;
    public string UNLOCK;
    public string SALE_FORMAT; // "{0} More"
    public string WATCH_AD; // "WATCH AD"
    public string WATCH_AD_2; // "WATCH\nAD"
    public string UPGRADE; // UPGRADE
    public string LATER;
    public string HIRE;
    public string OKAY;
    public string SECURITY;
    public string FINISHED;
    public string PRICES = ""; // PRICES

    public string LEVELUP_TITLE; // LEVEL UP!

    public string POPUP_MESSAGE_WAIT; //
    public string POPUP_MESSAGE_PERMISSION; // Please check permissions and restart game.
    public string POPUP_MESSAGE_EXITGAME; // "Are you sure to quit game?"
    public string POPUP_MESSAGE_NETWORKERROR; // "Network error."
    public string POPUP_MESSAGE_NETWORKFAIL; // "Network error. Please play game in better network."
    public string POPUP_MESSAGE_NOTENOUGH_MONEY; // "Not enough cash!\n"
    public string POPUP_MESSAGE_NOTENOUGH_GEM; // "Not enough gems!\n"
    public string POPUP_MESSAGE_NOTENOUGH_MESSAGE; // "Would you like to go to the Shop to get some?"
    public string POPUP_HEART_CONFIRMED; // "Heart refill confirmed."
    public string POPUP_HEART_FAILED; // "Heart refill failed."
    public string POPUP_SECURITY_FORMAT_BUY_MONEY; // "Are you sure you want\n{0} of security\nfor ${1}"
    public string POPUP_SECURITY_FORMAT_BUY_GEM; //"Are you sure you want {0}\nof security for {1} Gem"
    public string POPUP_SECURITY_FAILED; // "Buy security failed."
    public string POPUP_INVENTORY_FULL; // "Inventory is Full."
    public string POPUP_INVENTORY_MAX; // "Inventory level is max."
    public string POPUP_TRAVEL_UNLOCK_ALL; // "All citys is opend."
    public string POPUP_TRAVEL_UNLOCK_ERROR; // "unlock city error"
    public string POPUP_TRAVEL_UNLOCK_FAIL; // "unlock city fail. try again."
    public string POPUP_TRAVEL_BURGLARY_FORMAT_MONEY; // "Bad luck, boss! You were robbed.\nShould have gotten that bodyguard.\nThe bad guys got away with\n${0}!"
    public string POPUP_TRAVEL_BURGLARY_FORMAT_ITEM; // "Bad luck, boss! You were robbed.\nShould have gotten that bodyguard.\nThe bad guys got away with\n${0} and {2} {1}!"
    public string POPUP_TRAVEL_ERROR;
    public string POPUP_TRAVEL_FAIL;
    public string POPUP_QUEST_FAIL;
    public string POPUP_RANK_FAIL;
    public string POPUP_CHAT_SEND_FAIL; // "send chat message fail. try again."
    public string POPUP_CHAT_GET_FAIL; // "get chat list fail. try again."
    public string POPUP_RND_INACTIVE_ERROR; // "inactive R&D center error."
    public string POPUP_RND_INACTIVE_FAIL; // "inactive R&D center fail."
    public string POPUP_RND_FORMAT_UNLOCK; //"Activate R&D Center to unlock {0}?"
    public string POPUP_PURCHASE_GEM_SUCCESS; //"Gem purchase confirmed."
    public string POPUP_PURCHASE_MONEY_SUCCESS; //"Purchase money confirmd."
    public string POPUP_BOOSTAD_EMPTY_COUNT; // 

    public string UNLOCKCITY_TITLE; //  "unlock this city?"

    public string SECURITY_1HOUR; // 1 hour
    public string SECURITY_3HOURS; // 3 hours
    public string SECURITY_1DAY; // 1 day

    public string HEART_MESSAGE_MORE_DEFAULT; // "I've added 1 heart for you.\ni have more videos available if you would like more hearts."
    public string HEART_MESSAGE_MORE_GRATEOFFER; // "I've added 2 hearts for you.\ni have more videos available if you would like more hearts."
    public string HEART_TITLE_BASE; // Need\nmore\nhearts?
    public string HEART_TITLE_GREAT_OFFER; // I have a great offer for you.
    public string HEART_MESSAGE_GREAT_OFFER; // Looks like you could use a few extra hearts.  I can give you 2 hearts for watching a short message.  Sound okay?
    public string HEART_TITLE_NEXT_HEART; // Next Heart
    public string HEART_TITLE_LEFTTIME;
    public string HEART_TITLE_REFILL; // Refill         for
    public string HEART_INVITE_FRIENDS; // INVITE\nFRIENDS
    public string HERAT_THANKS; // SURE, THANKS!

    public string RETURNING_MESSAGE_ROOT1; // "You earned\n\n\nwhile you were gone.\nWatch an ad to\n<color=#B0fa40>DOUBLE EARNINGS!</color>"
    public string RETURNING_MESSAGE_ROOT2;
    public string RETURNING_MESSAGE_DOUBLE1; //"You made\n\n\nwhile you were gone!\n\n";
    public string RETURNING_MESSAGE_DOUBLE2;
    public string RETURNING_FORMAT_TITLE = "";
    public string RETURNING_TITLE_CONFIRMED; //ENJOY YOUR\nx2 EARNINGS!
    public string RETURNING_NOTHANKS; // NO, THANKS!
    public string RETURNING_THANKS; // OK, THANKS!

    public string TRAVEL_FORMAT_UNLOCK_TITLE; // "{0}\nhas not been\nunlocked."
    public string TRAVEL_UNLOCK_NOW; //UNLOCK NOW
    public string TRAVEL_SELECT_DESTINATION; // Select Destination
    public string TRAVEL_UNLOCK_CITY; // UNLOCK CITY
    public string TRAVEL_SELECT_UNLOCK_CITY; // SELECT UNLOCK CITY

    public string RANK_MY; //"MY RANK"
    public string RANK_TITLE;
    public string RANK_TAB_TOTAL; // TOTAL
    public string RANK_TAB_FRIENDS; // FRIENDS
    public string RANK_TAB_DAILY; // Daily
    public string RANK_TAB_WEEKLY; // Weekly
    public string RANK_TAB_MONTHLY; // Monthly
    public string QUEST_TITLE; //"QUESTS"

    public string CHAT_FORMAT_DAYAGO; // "{0:0} day ago"
    public string CHAT_FORMAT_HOURAGO; // "{0:0} hour ago"
    public string CHAT_FORMAT_MINAGO; // "{0:0} min ago"
    public string CHAT_MESSAGE_GUIDE; // Tap here to say something
    public string CHAT_SEND; // SEND

    public string SETTING_USERNUM; // "User Number : "

    public string RND_FORMAT_NAME; //"{0} R&D CENTER"
    public string RND_FORMAT_INUSE; // "In Use\n{0} ({1})"
    public string RND_FORMAT_INUSE_UNLOCK; // "In Use\nUnlock {0}"
    public string RND_FORMAT_SUCCESS_RATE; // "SUCESS RATE : {0}%"
    public string RND_UNLOCK; // "ITEM UNLOCK"
    public string RND_UNLOCK_ALL; // "ALL ITEM UNLOCKED"
    public string RND_FINISH_NOW; //"FINISH NOW"
    public string RND_ITEM_UNLOCK; // "ITEM UNLOCK"
    public string RND_ITEM_UPGRADE; // "ITEM UPGRADE";
    public string RND_TITLE_BASE; //Which item would\nyou like to upgrade?
    public string RND_TITLE_UPGRADE; // How many would you like to upgrade?
    public string RND_ACTIVATE; // ACTIVATE
    public string RND_Investment; //Investment
    public string RND_TIME; // Time
    public string RND_RETURN; // RETURN
    public string RND_UPGRADE_TITLE_SUCCESS; // SUCCESS!
    public string RND_UPGRADE_MESSAGE_SUCCESS; // UPGRADE COMPLETED!
    public string RND_UPGRADE_TITLE_FAIL; // FAILED
    public string RND_UPGRADE_MESSAGE_FAIL; // R&D FAILED!

    public string SHOP_BOOST_FORAMT_TIME; // "now on active - {0:00}:{1:00}:{2:00}"
    public string SHOP_BOOST_FORMAT_LEVEL; // "{0} Booster\nLv.{1}"
    public string SHOP_BOOST_FORMAT_LEVEL_MAX; // "{0} Booster\nLv.MAX"
    public string SHOP_BOOST_FORMAT_EXPLAIN; // "Increase discount/bonus to {0}% on all {1}"
    public string SHOP_TITLE_BASE; // MENU
    public string SHOP_TITLE_GEMS;
    public string SHOP_TITLE_MANAGERS;
    public string SHOP_TITLE_BOOSTS;
    public string SHOP_TITLE_ASSISTANTS;
    public string SHOP_TITLE_BOXES;
    public string SHOP_TITLE_CASH;
    public string SHoP_MANAGER_EMPTY = ""; // All managers are hired
    public string SHOP_BOOAST_CASH_EXPLAIN_1; // Increase production rate of all factories by 5x for 1 day
    public string SHOP_BOOAST_CASH_EXPLAIN_2; // Increase production rate of all factories by 2x for 1 day 
    public string SHOP_BOOAST_CASH_EXPLAIN_3; // Increase tap power by 5x for 1 day 
    public string SHOP_BOX_BRONZE_NAME; // BRONZE CHEST
    public string SHOP_BOX_BRONZE_EXPLAIN; // Cash, gems, and a random item
    public string SHOP_BOX_SILVER_NAME; // SILVER CHEST
    public string SHOP_BOX_SILVER_EXPLAIN; // Cash, gems, and a random item
    public string SHOP_BOX_GOLD_NAME; // GOLD CHEST
    public string SHOP_BOX_GOLD_EXPLAIN; // More cash, gems, and an even better item.
    public string SHOP_PACKAGE_FORMAT_EXPLAIN; // 150 Gems\nLarge Bag(50)\n$10,000

    public string MENU_TITLE_BASE; // MENU
    public string MENU_TITLE_CHATS; // CHATS
    public string MENU_TITLE_QUESTS; // QUESTS
    public string MENU_TITLE_RANKINGS; // RANKINGS
    public string MENU_TITLE_OFFERWALL; // FREE GEMS
    public string MENU_TITLE_SETTINGS; // SETTINGS
    public string MENU_SETTING_BGM; // BGM
    public string MENU_SETTING_EFFECT; // SOUND EFFECTS
    public string MENU_SETTING_NOTICE; // NOTICE
    public string MENU_SETTING_CONTACT; // CONTACT US
    public string MENU_SETTING_INFO; // GAME INFO
    public string MENU_SETTING_LANGUAGE; // LANGUAGE
    public string MENU_SETTING_RESTORE; // RESTORE
    public string MENU_SETTING_QUIT; // QUIT GAME

    public string TIP_FORMAT_BEST_PRICE; // "{0} prices {1} in {2}!"
    public string TIP_LOWEST; // "lowest"
    public string TIP_HIGHEST; // "hightest"
    public string TIP_FORMAT_REALTIME_PRICE; // "${0} {1} {3} in {2}!"
    public string TIP_SHORTAGE; // "shortage"
    public string TIP_SALE; // "sale"
    public string TIP_EMPTY; // "Good day, Boss!";

    public string MARKET_LOCKED; // "R&D Req"
    public string MARKET_ALL; // "ALL"
    public string MARKET_SALE; // "SALE"
    public string MARKET_FILTER; // "FILTER:"
    public string MARKET_SORT_GROUP; // "BY\nGROUP"
    public string MARKET_SORT_PRICE; // "BY\nPRICE"
    public string MARKET_SORT_UNLOCK; // "UNLOCK"
    public string MARKET_TITLE_BUY; // How many would you like to buy?
    public string MARKET_TITLE_SELL; // How many would you like to sell?
    public string BURGLARY_TITLE; // OH NO!

    public string BOOSTAD_TITLE_ROOT; // Looking for\na quick boost?
    public string BOOSTAD_TITLE_MORE; // Want some more\nquick boost?
    public string BOOSTAD_TITLE_END; // Enjoy your x2 boost!
    public string BOOSTAD_MESSAGE_ROOT; // Watch this fine quality ad for x2 profit boost to all your factories!\n<color=#B0fa40><size=24>(For 4 hours)</size></color>
    public string BOOSTAD_MESSAGE_MORE; // Watch this fine quality ad for x2 profit boost to all your factories!\n<color=#B0fa40><size=24>(Max 16 hours)</size></color>
    public string BOOSTAD_MESSAGE_END; // Boost filled for today!  Watch ads tomorrow to refresh your timer.
    public string BOOSTAD_WATCH_MORE; // WATCH MORE
    public string BOOSTAD_FORMAT_COUNT = ""; // YOU HAVE <color=yellow>6 MORE</color> AD BONUSES THAT YOU CAN USE TODAY!


    public string INVENTORY_MESSAGE_FORMAT_UPGRADE; // "Upgrade to\n{0} slots?"
    public string INVENTORY_TITLE; // MY INVENTORY

    public string FACTORY_RATEBOOST_LABEL; // FACTORY PRODUCTION RATE
    public string FACTORY_RATEBOOST_LABEL_EX; //  2x!

    public string WAREHOUSE;
    public string INVENTORY;

    public Dictionary<int, string> TUTORIALMESSAGES;
    public string TUTORIAL_POPUP_MESSAGE;
    public string TUTORIAL_POPUP_INFO;

    public List<string> MONEY_BIG_UNIT;
    protected Dictionary<int, string> item_names;
    protected Dictionary<int, string> item_group_names;
    protected Dictionary<int, string> city_names;
    protected Dictionary<int, string> factory_names;
    protected Dictionary<int, string> assistant_names;
    protected Dictionary<int, string> assistant_labels;
    protected Dictionary<int, string> quest_names;
    public List<string> tip_list_default;
    public List<string> tip_list_some_info;
    public List<string> tip_list_better_info;
    public Dictionary<int, Dictionary<int, string>> manager_info;

    public string ITEMNAME(int _id)
    {
        return item_names.ContainsKey(_id) ? item_names[_id] : "";
    }

    public string ITEMGROUPNAME(int _id)
    {
        return item_group_names.ContainsKey(_id) ? item_group_names[_id] : "";
    }

    public string CITYNAME(int _id)
    {
        return city_names.ContainsKey(_id) ? city_names[_id] : "";
    }

    public string FACTORYNAME(int _id)
    {
        return factory_names.ContainsKey(_id) ? factory_names[_id] : FACTORY;
    }

    public string ASSISTANTNAME(int _id)
    {
        return assistant_names.ContainsKey(_id) ? assistant_names[_id] : "";
    }

    public string ASSISTANTLABEL(int _id)
    {
        return assistant_labels.ContainsKey(_id) ? assistant_labels[_id] : "";
    }

    public string QUESTNAMES(int _id)
    {
        return quest_names.ContainsKey(_id) ? quest_names[_id] : "";
    }

    public string MANAGERINFO(int _id, int _level)
    {
        if (!manager_info.ContainsKey(_id)) return "";
        return manager_info[_id].ContainsKey(_level) ? manager_info[_id][_level] : "";
    }

    protected string FILE_ITEM_NAMES;
    protected string FILE_ITEM_GROUP_NAMES;
    protected string FILE_CITY_NAMES;
    protected string FILE_ASSISTANT_TXT;
    protected string FILE_TIP_DEFAULT;
    protected string FILE_TIP_SOME;
    protected string FILE_TIP_BETTER;
    protected string FILE_MANAGER_INFO;
    protected string FILE_FACTORY_NAMES;
    protected string FILE_QUEST_NAMES;
    protected void InitList()
    {
        item_names = new Dictionary<int, string>();
        string[] _items = Resources.Load<TextAsset>(FILE_ITEM_NAMES).text.Split('\n');
        for (int i = 0; i < _items.Length; i++)
        {
            if (_items[i].Length <= 0) continue;
            string[] _node = _items[i].Split('\t');
            item_names.Add(System.Convert.ToInt32(_node[0]), _node[1]);
        }

        item_group_names = new Dictionary<int, string>();
        string[] _groups = Resources.Load<TextAsset>(FILE_ITEM_GROUP_NAMES).text.Split('\n');
        for (int i = 0; i < _groups.Length; i++)
        {
            if (_groups[i].Length <= 0) continue;
            string[] _node = _groups[i].Split('\t');
            item_group_names.Add(System.Convert.ToInt32(_node[0]), _node[1]);
        }

        city_names = new Dictionary<int, string>();
        string[] _citys = Resources.Load<TextAsset>(FILE_CITY_NAMES).text.Split('\n');
        for (int i = 0; i < _citys.Length; i++)
        {
            if (_citys[i].Length <= 0) continue;
            string[] _node = _citys[i].Split('\t');
            city_names.Add(System.Convert.ToInt32(_node[0]), _node[1]);
        }

        factory_names = new Dictionary<int, string>();
        string[] _factorys = Resources.Load<TextAsset>(FILE_FACTORY_NAMES).text.Split('\n');
        for (int i = 0; i < _factorys.Length; i++)
        {
            if (_factorys[i].Length <= 0) continue;
            string[] _node = _factorys[i].Split('\t');
            factory_names.Add(System.Convert.ToInt32(_node[0]), _node[1]);
        }

        quest_names = new Dictionary<int, string>();
        string[] _quests = Resources.Load<TextAsset>(FILE_QUEST_NAMES).text.Split('\n');
        for (int i = 0; i < _quests.Length; i++)
        {
            if (_quests[i].Length <= 0) continue;
            string[] _node = _quests[i].Split('\t');
            quest_names.Add(System.Convert.ToInt32(_node[0]), _node[1]);
        }

        assistant_names = new Dictionary<int, string>();
        assistant_labels = new Dictionary<int, string>();
        string[] _assistants = Resources.Load<TextAsset>(FILE_ASSISTANT_TXT).text.Split('\n');
        for (int i = 0; i < _assistants.Length; i++)
        {
            if (_assistants[i].Length <= 0) continue;
            string[] _node = _assistants[i].Split('\t');
            assistant_names.Add(System.Convert.ToInt32(_node[0]), _node[1]);
            assistant_labels.Add(System.Convert.ToInt32(_node[0]), _node[2]);
        }

        tip_list_default = new List<string>();
        string[] _tip_1 = Resources.Load<TextAsset>(FILE_TIP_DEFAULT).text.Split('\n');
        for (int i = 0; i < _tip_1.Length; i++)
        {
            if (_tip_1[i].Length <= 0) continue;
            string[] _node = _tip_1[i].Split('\t');
            tip_list_default.Add(_node[1]);
        }

        tip_list_some_info = new List<string>();
        string[] _tip_2 = Resources.Load<TextAsset>(FILE_TIP_SOME).text.Split('\n');
        for (int i = 0; i < _tip_2.Length; i++)
        {
            if (_tip_2[i].Length <= 0) continue;
            string[] _node = _tip_2[i].Split('\t');
            tip_list_some_info.Add(_node[1]);
        }

        tip_list_better_info = new List<string>();
        string[] _tip_3 = Resources.Load<TextAsset>(FILE_TIP_BETTER).text.Split('\n');
        for (int i = 0; i < _tip_3.Length; i++)
        {
            if (_tip_3[i].Length <= 0) continue;
            string[] _node = _tip_3[i].Split('\t');
            tip_list_better_info.Add(_node[1]);
        }

        manager_info = new Dictionary<int, Dictionary<int, string>>();
        string[] _manager = Resources.Load<TextAsset>(FILE_MANAGER_INFO).text.Split('\n');
        for (int i = 0; i < _manager.Length; i++)
        {
            if (_manager[i].Length <= 0) continue;
            string[] _node = _manager[i].Split('\t');
            int _id = int.Parse(_node[0]);
            int _level = int.Parse(_node[1]);
            if (!manager_info.ContainsKey(_id)) manager_info.Add(_id, new Dictionary<int, string>());
            manager_info[_id].Add(_level, _node[2]);
        }
    }
}
