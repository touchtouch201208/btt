﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Money
{
	public string number = "";
	public int length = 0;

	public Money(string _number){

		number = _number.Substring(0, Mathf.Clamp(_number.Length, 0, 12));
		length = _number.Length;

		SetNumberFull ();
	}

	public Money(string _number, int _length)
	{
		number = _number; length = _length;

		SetNumberFull ();
	}

	public Money(Money _money)
	{
		number = _money.number; length = _money.length;

		SetNumberFull ();
	}

	private void SetNumberFull()
	{
        //실제 길이가 크고, 표현부 숫자가 작을 때
        //표현부 숫자의 부족한 자리를 0으로 최대치까지 채운다
		if(length >= 12 && number.Length < 12)
		{
			while(number.Length < 12) number += "0";
		}

        //표현부 숫자가 최대치이고, 실제 길이가 표현부보다 작을때
        //실제 길이를 표현부 최대치로 맞춘다
		if(number.Length >= 12 && length < 12) length = 12;

        //표현부 숫자가 최대치보다 길 때
        //표현부 숫자를 최대치로 맞춘다
		if(number.Length > 12) number = number.Substring(0,12);
	}

	public bool CompareMoney(Money _money)
	{
		if (length > _money.length) return true;
		else if (length < _money.length) return false;
		else return CompareStr(number, _money.number);
	}

	public void AddMoney(Money _money)
	{
		if(length - _money.length >= 12) return; // 내가 매우 크면 아무것도 안함
		else if(_money.length - length >= 12) // 내가 매우 작으면 덮어씀
		{
			number = _money.number;
			length = _money.length;
			return;
		}

		string num1 = number;
		string num2 = _money.number;
		int before_length = Mathf.Max (length, _money.length);

		//자리수의 차이를 숫자부에 반영함
		//(작은쪽을 자리수 차이만큼 줄임)
		int root_gap = Mathf.Abs (num1.Length - num2.Length); // 12자리 이상과 이하의 수 덧셈에서 자리수 차이를 메꾸기 위한 계산
		if(length > _money.length)
		{
			num2 = num2.Substring(0, num2.Length + root_gap - (length - _money.length));
		}
		else if(length < _money.length)
		{
			num1 = num1.Substring(0, num1.Length + root_gap - (_money.length - length));
		}

		string result_num = AddNumString (num1, num2);
		int result_length = before_length;

		//11자리와 11자리 숫자의 합이 12일때만 별도처리
		//11자리와 12자리, 혹은 12자리끼리의 합의 경우 : before_length가 12이므로 상관없음
		//12자리끼리의 합이 13일때 : > 12 조건으로 넘어감
		//11자리 아래에서의 합일때 : < 12 조건으로 넘어감
		if(num1.Length <= 11 && num2.Length <= 11 && result_num.Length == 12)
		{
			result_length = 12;
		}
		else if(result_num.Length > 12) // 20자리 이상에서, 같은 자리의 두 값이 더해져 자리수가 늘어날 때
		{
			result_length += result_num.Length - 12;
			result_num = result_num.Substring(0, 12);
		}
		else if(result_num.Length < 12)
		{
			result_length = result_num.Length;
		}

		number = result_num; length = result_length;
	}

	public void SubMoney(Money _money)
	{
		if(!CompareMoney(_money)) return;
		if(length - _money.length >= 12) return;

		string num1 = number;
		string num2 = _money.number;
		int before_length = length;

		if(num1.Length == 12 && num2.Length == 12)
		{
			num2 = num2.Substring(0, num2.Length - (length - _money.length));
		}

		string result_num = SubNumString (num1, num2);
		if (result_num == "0") 
		{ 
			number = "0";
			length = 1;
			return;
		}

		int result_length = before_length;
		if(result_num.Length < num1.Length) result_length = before_length - (num1.Length - result_num.Length);
		if(result_length >= 12 && result_num.Length < 12)
		{
			while(result_num.Length < 12) result_num += "0";
		}
		
		 number = result_num; length = result_length;
	}

	public void MulMoney(float _val)
	{
		if(_val == 0)
		{ 
			number = "0";
			length = 1;
			return;
		}

        if (_val == 1) return;

		string result_num = MulNumString (number, _val);
		int result_length = length + (result_num.Length - number.Length);

		number = result_num; length = result_length;
        SetNumberFull();

		if (number == "0" && length == 1) number = "1";
	}

	public void MulMoney(Money _val)
	{
		if(_val.number == "0")
		{
			number = "0";
			length = 1;
			return;
		}

		int add_length = _val.length - _val.number.Length;
		if(add_length > 0) length += add_length;
		MulMoney(System.Convert.ToSingle(_val.number));
	}

	public void AddMoney(string _val){ AddMoney (new Money (_val)); }
	public void SubMoney(string _val){ SubMoney (new Money (_val)); }

	public string GetUnit(ref int calc_length, string _unit)
	{
		string unit = _unit;

        /*
		int maxunit_count = 0;
		
		while(calc_length > 78)
		{
			calc_length -= 78;
			maxunit_count++;
        }
		
		if(maxunit_count != 0)
		{
			unit = unit.Insert(0, Language.Str.MONEY_BIG_UNIT[25] + ((maxunit_count > 1) ? maxunit_count.ToString() : ""));
		}

        for(int i = 25; i>0; i--)
        {
            if (calc_length > i * 3)
            {
                calc_length -= i * 3; unit = unit.Insert(0, Language.Str.MONEY_BIG_UNIT[i - 1]);
                break;
            }
        }
        */
        
        if(calc_length > 6)
        {
            for (int i = 30; i > 0; i--)
            {
                if (calc_length > i * 3)
                {
                    calc_length -= i * 3; unit = unit.Insert(0, Language.Str.MONEY_BIG_UNIT[i - 1]);
                    break;
                }
            }
        }

        /*
        if (calc_length > 5)
        {
            for (int i = 30; i > 0; i--)
            {
                if (calc_length > (i * 3) + 1)
                {
                    calc_length -= i * 3; unit = unit.Insert(0, Language.Str.MONEY_BIG_UNIT[i - 1]);
                    break;
                }
            }
        }
        */

        return unit;
	}
	
	public string ToWon(string _unit = "") // return : 1234.56{unit}
	{
        //if (_unit == "") _unit = Language.Str.MONEYUNIT;

        /*
		int calc_length = length;
		string unit = GetUnit (ref calc_length, _unit);

        if (length <= 6) return number + unit;
        else
		{
			string result = number.Substring (0, calc_length + 2);
			if (result [result.Length - 1] == '0' && result [result.Length - 2] == '0')
				result = result.Substring (0, result.Length - 2);
			else
				result = result.Insert (result.Length - 2, ".");
			
			result += unit;
			
			return result;
		}
        */

        return ToMoneyWon(_unit);
    }
	
	public string ToMoneyWon(string _unit = "") // return : 1,234 {unit}
	{
        //if (_unit == "") _unit = Language.Str.MONEYUNIT;

		int calc_length = length;
		string unit = GetUnit (ref calc_length, _unit);
        
		if(length <= 6) return NumToMoney(number) + unit;
		else
		{
			string result = number.Substring(0, calc_length);
            string _decimal = number.Substring(0, calc_length + 2);
            if (_decimal[result.Length - 1] == '0' && _decimal[result.Length - 2] == '0')
                _decimal = "";
            else
            {
                _decimal = _decimal.Substring(calc_length, 2);
                _decimal = _decimal.Insert(0, ".");
            }

            return NumToMoney(result) + _decimal + unit;
		}

        /*
        string result = number;

        if (length > 5) result = number.Substring(0, calc_length);

        return NumToMoney(result) + unit;
        */
	}

    public string ToMoneyNumber() // return : 1,234,567,890 or 1,234 {unit}
    {
        if (length <= 12) return NumToMoney(number);
        else return ToMoneyWon();
    }

    public long GetInt64()
    {
        if (length <= 12) return Int64.Parse(number);
        else if (length >= 19) return long.MaxValue;
        else
        {
            string _result = number;
            while (_result.Length < length) _result += "0";
            return Int64.Parse(_result);
        }
    }

	//----------------------------------------------

	public static string AddNumString(string _val1, string _val2)
	{
		
		char[] addval, addval2;
		if (_val1.Length > _val2.Length)
		{
			addval = _val1.ToCharArray();
			addval2 = _val2.ToCharArray();
		}
		else
		{
			addval = _val2.ToCharArray();
			addval2 = _val1.ToCharArray();
		}
		
		for (int i = 0; i < Mathf.Min(_val1.Length, _val2.Length); i++)
		{
			// 끝자리에서 첫자리로 이동, 각 자리수의 합을 계산
			int addnum = Convert.ToInt32(addval[addval.Length - 1 - i] - 48) +
				Convert.ToInt32(addval2[addval2.Length - 1 - i] - 48);
			
			//합이 10보다 클때
			if (addnum >= 10)
			{
				addval[addval.Length - 1 - i] = Convert.ToChar(addnum % 10 + 48);
				addval = AddNumPass(addval, addval.Length - 2 - i);
			}
			//합이 10보다 작을때
			else addval[addval.Length - 1 - i] = Convert.ToChar(addnum + 48);
		}
		
		return new string(addval);
	}
	
	static char[] AddNumPass(char[] _str, int _index)
	{
		if (_index < 0)
		{
			string str = new string(_str);
			return str.Insert(0, "1").ToCharArray();
		}
		
		char temp = _str[_index];
		if (++temp > '9')
		{
			_str[_index] = '0';
			return AddNumPass(_str, --_index);
		}
		else
		{
			_str[_index] = temp;
			return _str;
		}
	}
	
	//합과 달리 반드시 큰쪽이 val1, 작은쪽이 val2에 위치
	public static string SubNumString(string _val1, string _val2)
	{
		if (!CompareStr(_val1, _val2))
		{
			return _val1;
		}
		
		char[] addval, addval2;
		addval = _val1.ToCharArray();
		addval2 = _val2.ToCharArray();
		
		for (int i = 0; i < Mathf.Min(_val1.Length, _val2.Length); i++)
		{
			// 끝자리에서 첫자리로 이동, 각 자리수의 차를 계산
			int subnum = Convert.ToInt32(addval[addval.Length - 1 - i] - 48) -
				Convert.ToInt32(addval2[addval2.Length - 1 - i] - 48);
			
			//차가 0보다 작을때
			if (subnum < 0)
			{
				addval[addval.Length - 1 - i] = Convert.ToChar(10 - Mathf.Abs(subnum) + 48);
				int subnumpass_index = addval.Length - 2 - i;
				while (true)
				{
					char temp = addval[subnumpass_index];
					if (--temp < '0')
					{
						addval[subnumpass_index--] = '9';
					}
					else
					{
						addval[subnumpass_index] = temp;
						break;
					}
				}
			}
			//차가 0보다 클때
			else addval[addval.Length - 1 - i] = Convert.ToChar(subnum + 48);
		}
		
		//결과의 앞자리가 0이면 맨 앞이 0이 아닐때까지 지운다(0001234 -> 1234)
		string result = new string(addval);
		
		int num_zero_index = 0;
		for (int j = 0; j < result.Length; j++, num_zero_index++) if (result[j] != '0') break;
		if (num_zero_index != 0) result = result.Substring(num_zero_index, result.Length - num_zero_index);
		
		if (result == "") result = "0";
		
		return result;
	}
	
	public static string MulNumString(string _val, float _mulval)
	{
		if (_mulval == 1) return _val;
		else if (_mulval == 0) return "0";
		
		string result = "0";
		
		string num1 = "0";
		string num2 = "0";
		string num3 = "0";
        string num4 = "0";
		
		//100보다 크면, 횟수만큼 반복증가를 하는게 아니라
		//각 자리수별로 단위를 증가시키고 더함
		// - 1 * 20800 = (1*20000) + (1*800)
		// - 여기서 (1*20000)은 10000번 반복이 아닌
		// - 0 4개를 이어붙이고 맨 앞숫자인 2만큼 누적
		if (_mulval >= 100)
		{
			string integer_val = _mulval.ToString("0");
			for (int i = 0; i < integer_val.Length; i++)
			{
				if (integer_val[i] == '0') continue;
				string temp = _val;
				for (int j = 1; j < integer_val.Length - i; j++) temp = temp.Insert(temp.Length, "0");
				for (int k = 0; k < Convert.ToInt32(integer_val[i] - 48); k++) num1 = AddNumString(num1, temp);
			}
		}
		else for (int i = 0; i < (int)_mulval; i++) { num1 = AddNumString(num1, _val); }


        float decimal_val = _mulval % 1.0f; // xxx.xx... -> 0.xxx...
        decimal_val = Mathf.Round(decimal_val * 1000); // 0.xxx... -> xxx.0
        if (decimal_val == 0) return num1;

        for (int i = 0; i < (int)(decimal_val / 100); i++) { num2 = AddNumString(num2, _val); } // 0.1자리 합
        for (int i = 0; i < (int)(((int)(decimal_val % 100)) / 10); i++) { num3 = AddNumString(num3, _val); } // 0.01자리 합
        for (int i = 0; i < (int)(decimal_val % 10); i++) { num4 = AddNumString(num4, _val); } // 0.001자리 합

        result = AddNumString(result, num1);
        if (num2.Length >= 2) result = AddNumString(result, num2.Substring(0, num2.Length - 1));
        if (num3.Length >= 3) result = AddNumString(result, num3.Substring(0, num3.Length - 2));
        if (num4.Length >= 4) result = AddNumString(result, num4.Substring(0, num4.Length - 3));

        return result;
	}

	public static Money AddNumString(Money _val1, Money _val2){ Money result = new Money (_val1); result.AddMoney (_val2); return result; }
	public static Money SubNumString(Money _val1, Money _val2){ Money result = new Money (_val1); result.SubMoney (_val2); return result; }
	public static Money MulNumString(Money _val1, float _val2){ Money result = new Money (_val1); result.MulMoney (_val2); return result; }
	public static Money MulNumString(Money _val1, Money _val2){ Money result = new Money (_val1); result.MulMoney (_val2); return result; }
	
	public static Money AddNumString(Money _val1, string _val2){ Money result = new Money (_val1); result.AddMoney (new Money(_val2)); return result; }
	public static Money SubNumString(Money _val1, string _val2){ Money result = new Money (_val1); result.SubMoney (new Money(_val2)); return result; }

	//val1이 val2보다 크거나 같다면 true를 반환
	public static bool CompareStr(string _val1, string _val2)
	{
		if (_val1.Length > _val2.Length) return true;
		else if (_val1.Length < _val2.Length) return false;
		else
		{
			for (int i = 0; i < _val1.Length; i++)
			{
				if (Convert.ToInt32(_val1[i]) > Convert.ToInt32(_val2[i])) return true;
				else if (Convert.ToInt32(_val1[i]) < Convert.ToInt32(_val2[i])) return false;
			}
		}
		
		return true;
	}
	
	public static bool CompareStr(Money _money, string _val)
	{
		return _money.CompareMoney (new Money (_val));
	}
	
	public static bool CompareStr(Money _val1, Money _val2)
	{
		return _val1.CompareMoney (_val2);
	}

	public static string NumToMoney(string _val)
	{
		for (int i = _val.Length - 3; i > 0; i -= 3) _val = _val.Insert(i, ",");
		return _val;
	}
}
