﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class AssistantManager : MonoBehaviour {

    public static AssistantManager Instance = null;

    public AssistantNode[] AssistantObjects;

	// Use this for initialization
	void Awake () {

        Instance = this;
        for (int i = 0; i < AssistantObjects.Length; i++) AssistantObjects[i].OnInit();
    }

    public void SetAssistant()
    {
        for(int i = 0; i<AssistantObjects.Length; i++) AssistantObjects[i].SetAssistant();
    }
}
