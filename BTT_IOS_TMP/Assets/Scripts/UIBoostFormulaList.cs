﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBoostFormulaList : MonoBehaviour {

    public Text[] ListLabel;
    public Image[] ListImage;

    public void SetFormula(string _formula = "")
    {
        for (int i = 0; i < ListLabel.Length; i++) ListLabel[i].gameObject.SetActive(false);
        for (int i = 0; i < ListImage.Length; i++) ListImage[i].gameObject.SetActive(false);

        if (!string.IsNullOrEmpty(_formula))
        {
            string[] _recipt = _formula.Split('+');

            int _id = 0;
            int _amount = 0;

            for (int i = 0; i < _recipt.Length; i++)
            {
                if (i >= ListLabel.Length) break;

                string[] _recipt_node = _recipt[i].Split(':');
                _id = int.Parse(_recipt_node[0]);
                ListLabel[i].text = _recipt_node[1];
                ListLabel[i].color =
                    (UserData.Instance.isExistItemWarehouse(_id, _amount) ||
                    UserData.Instance.isExistItemInventory(_id, _amount))
                    ? Color.white : Color.red;
                ListImage[i].sprite = ItemDB.Instance.GetitemImg(int.Parse(_recipt_node[0]));
                ListLabel[i].gameObject.SetActive(true);
                ListImage[i].gameObject.SetActive(true);
            }
        }
    }
}
