﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class TutorialManager : MonoBehaviour {
    
    public static TutorialManager Instance = null;
    [HideInInspector]
    public int Step = -1;

    public GameObject InputBlock;
    private UIScrollNodeProduct[] StepObjects;

    [Header("For Select Home")]
    public RectTransform CanvasRoot;
    public RectTransform BaseSelectHomeCityMap;
    public RectTransform SelectHomeCityMapImg;
    private UICityButtonNodes[] SelectHomeCityButtons;
    private int SelectHomeCityNowPage = 0;
    private Vector3 SelectHomeCityScrollTouchRoot = Vector3.zero;
    private float SelectHomeCityScrollVal = 0;
    private Vector2[] SelectHomeCityAreaPositions = new Vector2[3];
    private Vector2 SelectHomeCityScrollPageDepth;

    [Header("For Select Home Animation")]
    public GameObject SelectHomeAnimationBase;
    public RectTransform SelectHomeAniAirplain;
    public RectTransform SelectHomeAniAirplainFrom;
    public RectTransform SelectHomeAniAirplainTo;
    public RectTransform SelectHomeAniFrom;
    public RectTransform SelectHomeAniTo;
    public Image SelectHomeAniToImg;
    public Text SelectHomeAniToName;

    private int HomeCityID = -1;

    private float _step_delay_timer = 0;

    // Use this for initialization
    void Awake () {

        Instance = this;
        transform.localScale = Vector3.one;

        StepObjects = GetComponentsInChildren<UIScrollNodeProduct>(true).OrderBy(x => x.name).ToArray();
        
        SelectHomeCityButtons = BaseSelectHomeCityMap.GetComponentsInChildren<UICityButtonNodes>(true);
        for (int i = 0; i < SelectHomeCityButtons.Length; i++)
        {
            int _idx = i;
            SelectHomeCityButtons[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                //if(!MoveProcess) StartCoroutine(MoveToCityAnimation(SelectHomeCityButtons[_idx].CityID));
                
                int _city = SelectHomeCityButtons[_idx].CityID;
                HomeCityID = _city;

                PopupManager.Instance.SetPopupWait();
                NetworkManager.Instance.RequestTutorialTravel(
                   _city,
                   delegate (JSONNode N)
                   {
                       PopupManager.Instance.SetOffPopup();

                       UserData.Instance.CurrentMarketAllInfos[1].CurrentPrice = 750;
                       UserData.Instance.CurrentMarketAllInfos[8].CurrentPrice = 825;
                       UserData.Instance.CurrentMarketAllInfos[8].Available = UserData.AVAILABLE;

                       for(int j = 0; j<UserData.Instance.CurrentMarketAllInfos.Count; j++)
                       {
                           var _product = UserData.Instance.CurrentMarketAllInfos.ElementAt(j).Value;
                           if (_product.OriginPrice - _product.CurrentPrice >= ItemDB.Instance.ItemDatas[_product.ProductID].stdDev
                           && _product.CurrentPrice < 825) _product.Available = UserData.UNAVAILABLE;
                       }

                       MainScene.Instance.OnCityChange(_city);
                       SetNextStep();
                   },

                   delegate (int _code, string _message)
                   {
                       PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                   },

                   delegate (string _message)
                   {
                       PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                   }
                   );
            }
            );
        }

        SelectHomeCityAreaPositions[0] = new Vector2(1130, -155); // america
        SelectHomeCityAreaPositions[1] = new Vector2(-120, -350); // europe
        SelectHomeCityAreaPositions[2] = new Vector2(-1195, 5); // asia

        if (NetworkManager.Instance.mUserData.memberGrade >= 2)
        {
            gameObject.SetActive(false);
            return;
        }

        for (int i = 0; i < StepObjects.Length; i++)
        {
            var _message = StepObjects[i].transform.GetComponentInChildren<UITutorialMessage>();
            if (_message != null) _message.SetMessage(Language.Str.TUTORIALMESSAGES[i]);
        }

        InputBlock.SetActive(true);
        SetNextStep();
    }

    void Start()
    {
        SelectHomeCityScrollPageDepth = new Vector2(-BaseSelectHomeCityMap.sizeDelta.x * BaseSelectHomeCityMap.localScale.x, 0);
    }

    // Update is called once per frame
    void Update () {
        
        switch (Step)
        {
            case 2:
                {
                    if (FactoryManager.Instance.Factorys[1].Amount > 0) SetNextStep();
                }
                break;
                
            case 3:
                {
                    if (FactoryManager.Instance.Factorys[1].Amount == 0) SetNextStep();
                }
                break;

            case 4:
                {
                    if (FactoryManager.Instance.Factorys[1].Level > 1) SetNextStep();
                }
                break;

            case 5:
                {
                    if (FactoryManager.Instance.Factorys[2].Level >= 1) SetNextStep();
                }
                break;

            case 6:
                if (Input.GetMouseButtonDown(0))
                {
                    var _btn = ShopManager.Instance.NotificateIcon.transform.parent.GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(_btn, Input.mousePosition, Camera.main))
                    {
                        SetNextStep();
                    }
                }
                break;

            case 7:
                if (Input.GetMouseButtonDown(0))
                {
                    var _btn = ShopManager.Instance.NotificateManager.transform.parent.GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(_btn, Input.mousePosition, Camera.main))
                    {
                        SetNextStep();
                    }
                }
                break;

            case 8:
                if (FactoryManager.Instance.Factorys.ElementAt(0).Value.ManagerLavel > 0) SetNextStep();
                break;

            case 9:
                if (Input.GetMouseButtonDown(0))
                {
                    var _btn = ShopManager.Instance.BaseRoot.transform.Find("ButtonBack").GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(_btn, Input.mousePosition, Camera.main))
                    {
                        SetNextStep();
                    }
                }
                break;

            case 10:
                if (Input.GetMouseButtonDown(0))
                {
                    var _btn = MarketManager.Instance.BtnTravel.GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(_btn, Input.mousePosition, Camera.main))
                    {
                        SetNextStep();
                    }
                }
                break;
                
            case 11:
                {
                    if (Input.GetMouseButtonDown(0)) SelectHomeCityScrollTouchRoot = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    if (SelectHomeCityScrollTouchRoot != Vector3.zero)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            SelectHomeCityScrollVal = (Camera.main.ScreenToViewportPoint(Input.mousePosition) - SelectHomeCityScrollTouchRoot).x * CanvasRoot.sizeDelta.x;
                            if (Mathf.Abs(SelectHomeCityScrollVal) >= 160)
                            {
                                if (SelectHomeCityScrollVal < 0) SetNextSelectHomeCityPage();
                                else SetPrevSelectHomeCityPage();
                                SelectHomeCityScrollTouchRoot = Vector3.zero;
                                SelectHomeCityScrollVal = 0;
                            }
                        }
                        else if (Input.GetMouseButtonUp(0))
                        {
                            SelectHomeCityScrollTouchRoot = Vector3.zero;
                            SelectHomeCityScrollVal = 0;
                        }
                    }

                    if (SelectHomeCityNowPage < 0)
                    {
                        int _pos_idx = 3 + (3 * ((SelectHomeCityNowPage + 1) / -3)) + SelectHomeCityNowPage;
                        BaseSelectHomeCityMap.anchoredPosition =
                            Vector2.Lerp(BaseSelectHomeCityMap.anchoredPosition,
                            SelectHomeCityAreaPositions[_pos_idx]
                            + (Vector2.right * SelectHomeCityScrollVal)
                            + (SelectHomeCityScrollPageDepth * ((SelectHomeCityNowPage - 2) / 3)),
                            0.1f);
                    }
                    else
                    {
                        BaseSelectHomeCityMap.anchoredPosition =
                            Vector2.Lerp(BaseSelectHomeCityMap.anchoredPosition,
                            SelectHomeCityAreaPositions[SelectHomeCityNowPage % 3]
                            + (Vector2.right * SelectHomeCityScrollVal)
                            + (SelectHomeCityScrollPageDepth * (SelectHomeCityNowPage / 3)),
                            0.1f);
                    }
                }
                break;


            case 12:
                {
                    if (MarketManager.Instance.SellPopup.activeSelf) SetNextStep();
                }
                break;

            case 13:
                if (!MarketManager.Instance.SellPopup.activeSelf)
                {
                    if (UserData.Instance.GetInventoryCount() != 0) Step = 12;
                    SetNextStep();
                }
                break;

            case 14:
                if (Input.GetMouseButtonDown(0))
                {
                    var _btn = MarketManager.Instance.MarketHomeBtnSale.GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(_btn, Input.mousePosition, Camera.main))
                    {
                        MarketManager.Instance.SetMarketHomeSortState(MarketManager.MARKET_HOME_SORT_SALE);
                        SetNextStep();
                    }
                }
                break;

            case 15:
                if (MarketManager.Instance.BuyPopup.activeSelf) SetNextStep();
                break;

            case 16:
                if (!MarketManager.Instance.BuyPopup.activeSelf)
                {
                    if (!UserData.Instance.isInventoryFull()) Step = 14;
                    SetNextStep();
                }
                break;

            case 17:
                if (Input.GetMouseButtonDown(0))
                {
                    var _btn = MarketManager.Instance.BtnMarketExit.GetComponent<RectTransform>();
                    if (RectTransformUtility.RectangleContainsScreenPoint(_btn, Input.mousePosition, Camera.main))
                    {
                        MarketManager.Instance.ExitMarket();
                        SetNextStep();
                    }
                }
                break;

            default: break;
        }
    }

    public void SetNextStep()
    {
        Step++;
        ActivateStepObject();

        switch (Step)
        {
            case 0:
                Invoke("SetNextStep", 1.0f);
                break;

            case 2:
                UserData.Instance.TotalMoney.AddMoney("10000");
                UserData.Instance.AddProductInventory(1, 10, 500);
                WareHouseManager.Instance.UpdateMainAmount();
                FactoryManager.Instance.Factorys[1].BuildTime = NetworkManager.Instance.GetServerNow();
                FactoryManager.Instance.Factorys[1].BuildAddTime = 0;
                InputBlock.SetActive(false);
                break;

            case 6:
                InputBlock.SetActive(true);
                break;

            case 7:
                ShopManager.Instance.SetBase(ShopManager.Instance.BaseMenu);
                InputBlock.SetActive(false);
                break;

            case 8:
                ShopManager.Instance.SetBase(ShopManager.Instance.BaseManager);
                ShopManager.Instance.BaseManager.GetComponentInChildren<ScrollRect>().enabled = false;
                break;

            case 9:
                InputBlock.SetActive(true);
                break;

            case 10:
                ShopManager.Instance.ExitShop();
                ShopManager.Instance.ExitShop();
                break;

            case 11:
                OpenSelectCity();
                InputBlock.SetActive(false);
                break;

            case 12:
                MarketManager.Instance.SetMarketScrollPage(0);
                InputBlock.SetActive(false);
                MarketManager.Instance.OpenMarket();
                MarketManager.Instance.MarketBase.GetComponent<ScrollRect>().enabled = false;

                StepObjects[Step].gameObject.SetActive(false);
                Invoke("ActivateStepObject", 1.0f);
                break;

            case 13:
                break;

            case 14:
                InputBlock.SetActive(true);
                break;

            case 15:
                InputBlock.SetActive(false);
                break;

            case 16:
                MarketManager.Instance.BuyAmountSlide.value = 10;
                break;

            case 17:
                InputBlock.SetActive(true);
                break;

            case 18:
                RequestCompleteTutorial();
                break;
        }
    }

    private void RequestCompleteTutorial()
    {
        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestCompleteTutorial(
                    HomeCityID,
                    delegate (JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();
                        MarketManager.Instance.MarketBase.GetComponent<ScrollRect>().enabled = true;
                        ShopManager.Instance.BaseManager.GetComponentInChildren<ScrollRect>().enabled = true;
                        Step = -1;
                        gameObject.SetActive(false);

                        IGAWorksManager.onTutorialComplete();
                    },

                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(_message,
                            delegate { RequestCompleteTutorial(); });
                    },

                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL,
                            delegate { RequestCompleteTutorial(); });
                    }
                    );
    }

    private void ActivateStepObject() {
        for (int i = 0; i < StepObjects.Length; i++) StepObjects[i].gameObject.SetActive(false);
        if(Step < StepObjects.Length) StepObjects[Step].gameObject.SetActive(true);
    }

    IEnumerator WaitForLevelUp()
    {
        while (
            PopupManager.Instance.isOnPopup()
            || MainScene.Instance.BaseLevelUp.activeSelf
            || BoxRewardManager.Instance.gameObject.activeSelf) yield return null;
        MarketManager.Instance.ExitMarket();
        InputBlock.SetActive(true);
        ActivateStepObject();
    }

    //------------------

    private void OpenSelectCity()
    {
        SelectHomeCityNowPage = 0;
        BaseSelectHomeCityMap.anchoredPosition = SelectHomeCityAreaPositions[SelectHomeCityNowPage];
        SetSelectHomeCityMapPage();
    }

    public void SetNextSelectHomeCityPage()
    {
        SelectHomeCityNowPage++;
        SetSelectHomeCityMapPage();
    }

    public void SetPrevSelectHomeCityPage()
    {
        SelectHomeCityNowPage--;
        SetSelectHomeCityMapPage();
    }

    private void SetSelectHomeCityMapPage()
    {
        int _val = SelectHomeCityNowPage;
        if (_val < 0) _val = _val - 2;
        SelectHomeCityMapImg.anchoredPosition = Vector2.right * (BaseSelectHomeCityMap.sizeDelta.x * (_val / 3));
    }

    private bool MoveProcess = false;
    IEnumerator MoveToCityAnimation(int _id)
    {
        MoveProcess = true;
        HomeCityID = _id;

        SelectHomeAniToImg.sprite = MainScene.Instance.GetCityImgs(_id);
        SelectHomeAniToName.text = CityDB.Instance.CityDatas[_id].cityName.ToUpper();

        Vector2 _from_target = new Vector2(-1000, 0);
        Vector2 _to_target = new Vector2(25, 0);

        SelectHomeAniFrom.anchoredPosition = new Vector2(-25, 0);
        SelectHomeAniTo.anchoredPosition = new Vector2(1000, 0);

        SelectHomeAniAirplain.position = SelectHomeAniAirplainFrom.position;

        SelectHomeAnimationBase.SetActive(true);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_AIRPLANE);

        float _timer = 0;
        while (_timer < 3.0f)
        {
            _timer += Time.deltaTime;

            if (_timer > 1.0f)
            {
                SelectHomeAniAirplain.position = Vector3.Lerp(SelectHomeAniAirplain.position, SelectHomeAniAirplainTo.position, 0.1f);
                SelectHomeAniFrom.anchoredPosition = Vector2.Lerp(SelectHomeAniFrom.anchoredPosition, _from_target, 0.1f);
                SelectHomeAniTo.anchoredPosition = Vector2.Lerp(SelectHomeAniTo.anchoredPosition, _to_target, 0.1f);
            }

            yield return null;
        }

        MainScene.Instance.OnCityChange(HomeCityID);
        SelectHomeAnimationBase.SetActive(false);
        MoveProcess = false;
        SetNextStep();
    }

    public bool isOnLevelup() { return Step == -1 || Step == 18; }
}
