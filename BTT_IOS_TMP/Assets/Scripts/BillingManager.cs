﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;

public class BillingManager : MonoBehaviour, IStoreListener
{
    private IStoreController m_Controller;
    private IAppleExtensions m_AppleExtensions;
    private ISamsungAppsExtensions m_SamsungExtensions;

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_Controller = controller;
        m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
        m_SamsungExtensions = extensions.GetExtension<ISamsungAppsExtensions>();
        
        // On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
        // On non-Apple platforms this will have no effect; OnDeferred will never be called.
        m_AppleExtensions.RegisterPurchaseDeferredListener(OnDeferred);
    }

    /// <summary>
    /// This will be called when a purchase completes.
    /// </summary>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        Debug.Log("Purchase OK: " + e.purchasedProduct.definition.id);
        Debug.Log("Receipt: " + e.purchasedProduct.receipt);

        // You should unlock the content here.
        var N = JSONNode.Parse(e.purchasedProduct.receipt);
        var N_Payload = JSONNode.Parse(N["Payload"]);
        string m_SignedData = N_Payload["json"];
        string m_Signature = N_Payload["signature"];

        //ApplicationManager.Instance.memberBillingU(e.purchasedProduct.definition.id, m_SignedData, m_Signature);
        NetworkManager.Instance.RequestPurchaseUpdateAOS(m_SignedData, m_Signature, OnPurchaseSuccess, OnPurchaseError, OnPurchaseFail);

        // Indicate we have handled this purchase, we will not be informed of it again.x
        return PurchaseProcessingResult.Complete;
    }

    /// <summary>
    /// This will be called is an attempted purchase fails.
    /// </summary>
    public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
    {
        Debug.Log("Purchase failed: " + item.definition.id);
        Debug.Log(r);

        OnPurchaseExit();
        //ApplicationManager.Instance.purchaseCallBackFailure(r.ToString());
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("Billing failed to initialize!");
        switch (error)
        {
            case InitializationFailureReason.AppNotKnown:
                Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                break;
            case InitializationFailureReason.PurchasingUnavailable:
                // Ask the user if billing is disabled in device settings.
                Debug.Log("Billing disabled!");
                break;
            case InitializationFailureReason.NoProductsAvailable:
                // Developer configuration error; check product metadata.
                Debug.Log("No products available for purchase!");
                break;
        }
    }

    /// <summary>
	/// This will be called after a call to IAppleExtensions.RestoreTransactions().
	/// </summary>
	private void OnTransactionsRestored(bool success)
    {
        Debug.Log("Transactions restored.");
    }

    /// <summary>
    /// iOS Specific.
    /// This is called as part of Apple's 'Ask to buy' functionality,
    /// when a purchase is requested by a minor and referred to a parent
    /// for approval.
    /// 
    /// When the purchase is approved or rejected, the normal purchase events
    /// will fire.
    /// </summary>
    /// <param name="item">Item.</param>
    private void OnDeferred(Product item)
    {
        Debug.Log("Purchase deferred: " + item.definition.id);
    }

    //----------------------------------------------------------------
    // Custom method
    //----------------------------------------------------------------

    private static BillingManager _instance = null;
    public static BillingManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType(typeof(BillingManager)) as BillingManager;
                if (_instance == null)
                {
                    _instance = (new GameObject("BillingManager")).AddComponent<BillingManager>();
                }
            }
            return _instance;
        }
    }

    [HideInInspector]
    public int OrderNo = -1;

    public class PurchaseInfoNode
    {
        public string itemkey;
        public string aosBillCode;
        public string iosBillCode;
        public string PriceKRW;
        public string PriceUSD;

        public PurchaseInfoNode(
            string _key,
            string _aos,
            string _ios,
            string _priceKRW,
            string _priceUSD)
        {
            itemkey = _key;
            aosBillCode = _aos;
            iosBillCode = _ios;
            PriceKRW = _priceKRW;
            PriceUSD = _priceUSD;
        }
    }
    public Dictionary<string, PurchaseInfoNode> InfoList = new Dictionary<string, PurchaseInfoNode>();

    // Use this for initialization
    void Awake () {

        DontDestroyOnLoad(this);

        var _text = Resources.Load<TextAsset>("productList");
        var N = JSONNode.Parse(_text.text);
        var _list = N["productList"];
        for (int i = 0; i < _list.Count; i++)
        {
            string _key = _list[i]["itemKey"];
            InfoList.Add(_key,
                new PurchaseInfoNode(
                    _key,
                    _list[i]["aosBillCode"],
                    _list[i]["iosBillCode"],
                    _list[i]["priceKRW"],
                    _list[i]["priceUSD"]
                    ));
        }

        var module = StandardPurchasingModule.Instance();

        // The FakeStore supports: no-ui (always succeeding), basic ui (purchase pass/fail), and 
        // developer ui (initialization, purchase, failure code setting). These correspond to 
        // the FakeStoreUIMode Enum values passed into StandardPurchasingModule.useFakeStoreUIMode.
        module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

        var builder = ConfigurationBuilder.Instance(module);
        // This enables the Microsoft IAP simulator for local testing.
        // You would remove this before building your release package.
        builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;

        //

        builder.AddProduct("bundleofgems", ProductType.Consumable, null);
        builder.AddProduct("chestofgems", ProductType.Consumable, null);
        builder.AddProduct("handfulofgems", ProductType.Consumable, null);
        builder.AddProduct("pileofgems", ProductType.Consumable, null);
        builder.AddProduct("sackofgems", ProductType.Consumable, null);
        builder.AddProduct("starterpack", ProductType.Consumable, null);

        //builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);
        //builder.Configure<ISamsungAppsConfiguration>().SetMode(SamsungAppsMode.AlwaysSucceed);
        //m_IsSamsungAppsStoreSelected = module.androidStore == AndroidStore.SamsungApps;
        //builder.Configure<ITizenStoreConfiguration>().SetGroupId("100000085616");


#if RECEIPT_VALIDATION
		validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.bundleIdentifier);
#endif

        // Now we're ready to initialize Unity IAP.
        UnityPurchasing.Initialize(this, builder);
    }

    Action<JSONNode> OnPurchaseSuccess;
    Action<int, string> OnPurchaseError;
    Action<string> OnPurchaseFail;
    Action OnPurchaseExit;
    public void Purchase(
        string _item_key,
        Action<JSONNode> _onsuccess,
        Action<int, string> _onerror,
        Action<string> _onfail,
        Action _onexit
        )
    {
        OnPurchaseSuccess = _onsuccess;
        OnPurchaseError = _onerror;
        OnPurchaseFail = _onfail;
        OnPurchaseExit = _onexit;

        if (m_Controller.products.WithID(_item_key) != null)
            m_Controller.InitiatePurchase(m_Controller.products.WithID(_item_key));
    }
}
