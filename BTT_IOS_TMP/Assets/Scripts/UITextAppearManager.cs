﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextAppearManager : MonoBehaviour {

    private Rigidbody[] AppearTexts;
    private int AppearIdx = 0;

    public static UITextAppearManager Instance = null;

	// Use this for initialization
	void Awake () {

        Instance = this;

        AppearTexts = GetComponentsInChildren<Rigidbody>(true);

        Invoke("SetAwakeWait", 0.5f);
	}

    private void SetAwakeWait() { _awake_wait = false; }

    private bool _awake_wait = true;
    public void SetAppear(
        Transform _pos,
        string _txt,
        int _size,
        Color _color,
        Color _outline)
    {
        if (_awake_wait) return;

        AppearTexts[AppearIdx].gameObject.SetActive(true);
        AppearTexts[AppearIdx].transform.SetParent(_pos);
        AppearTexts[AppearIdx].transform.localPosition = new Vector3(Random.Range(-20, 20), Random.Range(20, 50), 0);

        AppearTexts[AppearIdx].velocity = Vector3.zero;
        AppearTexts[AppearIdx].AddForce(Vector3.up * Random.Range(125, 175));

        Text _text = AppearTexts[AppearIdx].GetComponent<Text>();
        _text.text = _txt;
        _text.fontSize = _size;
        _text.color = _color;
        _text.GetComponent<Canvas>().sortingOrder = _pos.GetComponentInParent<Canvas>().sortingOrder;
        AppearTexts[AppearIdx].GetComponent<Outline>().effectColor = _outline;
        //AppearTexts[AppearIdx].GetComponent<Outline>().effectColor = new Color(0, 0, 0, 0);

        AppearTexts[AppearIdx].GetComponent<Animation>().Stop();
        AppearTexts[AppearIdx].GetComponent<Animation>().Play();

        if (++AppearIdx >= AppearTexts.Length) AppearIdx = 0;
    }
}
