﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEffectManager : MonoBehaviour {

    private UIProductMoveEffect[] Effects;
    private int Effect_id = 0;

    public static UIEffectManager Instance = null;
    
	// Use this for initialization
	void Awake () {
        Instance = this;

        Effects = GetComponentsInChildren<UIProductMoveEffect>();
	}

    public void SetEffect(int _amount, Transform _root, Transform _target, int _id)
    {
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_PRODUCT);
        for (int i = 0; i < Mathf.Clamp(_amount, 0, 10); i++)
        {
            Effects[Effect_id].Setup(_root, _target, _id);
            if (++Effect_id >= Effects.Length) Effect_id = 0;
        }
    }
}
