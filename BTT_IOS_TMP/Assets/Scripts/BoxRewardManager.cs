﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class BoxRewardManager : MonoBehaviour {

    public static BoxRewardManager Instance = null;

    void Awake()
    {
        Instance = this;

        transform.localScale = Vector3.one;
        gameObject.SetActive(false);
    }

    public const int BOX_ID_BRONZE_BOX = 0;
    public const int BOX_ID_SILVER_BOX = 1;
    public const int BOX_ID_GOLD_BOX = 2;
    public const int BOX_ID_RND_CARD_BOX = 3;

    private int NowRewardIndex = -1;
    private Money RewardMoney;
    private int RewardGem;
    private int RewardBoxType;
    private JSONNode RewardList;

    public GameObject OpenBase;
    public GameObject OpenBoxBronze;
    public GameObject OpenBoxSilver;
    public GameObject OpenBoxGold;
    public GameObject OpenBoxRND;
    public GameObject RewardBase;
    public GameObject RewardMoneyIcon;
    public GameObject RewardGemIcon;
    public GameObject RewardItemIcon;
    public GameObject RewardCardIcon;
    public GameObject OkButton;
    public Text RewardNameLabel;

    public void SetReward(JSONNode N)
    {
        if (string.IsNullOrEmpty(N["rewardMoney"]))
        {
            NowRewardIndex = 2;
        }
        else
        {
            RewardMoney = new Money(N["rewardMoney"]);
            NowRewardIndex = 0;
        }
        RewardGem = N["rewardGem"].AsInt;
        RewardBoxType = N["boxID"].AsInt;
        RewardList = N["reward"].AsArray;

        gameObject.SetActive(true);
        //ShowReward();
        StartShowReward();
    }

    private void StartShowReward()
    {
        RewardBase.SetActive(false);
        OpenBase.SetActive(false);
        OpenBoxBronze.SetActive(false);
        OpenBoxSilver.SetActive(false);
        OpenBoxGold.SetActive(false);
        OpenBoxRND.SetActive(false);
        OkButton.SetActive(false);

        OpenBase.SetActive(true);
        switch (RewardBoxType)
        {
            case BOX_ID_BRONZE_BOX: OpenBoxBronze.SetActive(true); break;
            case BOX_ID_GOLD_BOX: OpenBoxGold.SetActive(true); break;
            case BOX_ID_RND_CARD_BOX: OpenBoxRND.SetActive(true); break;
            case BOX_ID_SILVER_BOX:
            default: OpenBoxSilver.SetActive(true); break;
        }
        Invoke("ShowReward", 1.0f);
    }

    private void ShowReward()
    {
        RewardBase.SetActive(false);

        RewardMoneyIcon.SetActive(false);
        RewardGemIcon.SetActive(false);
        RewardItemIcon.SetActive(false);
        RewardCardIcon.SetActive(false);

        if (NowRewardIndex == 0)
        {
            RewardMoneyIcon.SetActive(true);
            RewardNameLabel.text = "$" + RewardMoney.ToMoneyWon();
        }
        else if(NowRewardIndex == 1)
        {
            RewardGemIcon.SetActive(true);
            RewardNameLabel.text = string.Format("{0} {1}", RewardGem, 
                RewardGem > 1 ? Language.Str.SHOP_TITLE_GEMS : Language.Str.GEM);
        }
        else
        {
            int _idx = NowRewardIndex - 2;
            int _type = int.Parse(RewardList[_idx]["rewardType"]);
            int _id = int.Parse(RewardList[_idx]["rewardID"]);

            if(_type == ShopDB.REWARD_TYPE_ITEM)
            {
                RewardItemIcon.SetActive(true);
                RewardItemIcon.transform.GetChild(0).GetComponent<UIItemIcon>().SetItem(_id);
                RewardNameLabel.text = ItemDB.Instance.ItemDatas[_id].itemName;

                RNDCenterManager.Instance.SetUnlockItemBtnUI(ItemDB.Instance.ItemDatas[_id].itemGroupID);
                if (MarketManager.Instance.MarketBase.activeSelf) MarketManager.Instance.SetMarketProductAvailableList();
            }
            else
            {
                RewardCardIcon.SetActive(true);
                RewardCardIcon.transform.GetChild(0).GetComponent<Image>().sprite =
                    ItemDB.Instance.GetitemImg(FormulaDB.Instance.Formulas[_id].itemID);
                RewardNameLabel.text = "R&D Card\n" + ItemDB.Instance.ItemDatas[FormulaDB.Instance.Formulas[_id].itemID].itemName;
            }
        }

        RewardBase.SetActive(true);
        OkButton.SetActive(true);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_REWARD);
    }

    public void SetNextReward()
    {
        NowRewardIndex++;

        if (NowRewardIndex - 2 >= RewardList.Count)
        {
            WareHouseManager.Instance.UpdateMainAmount();
            gameObject.SetActive(false);
        }
        else
        {
            if(RewardGemIcon.activeSelf)
            {
                NetworkManager.Instance.RequestDataKeywordCash
                (
                delegate (JSONNode N)
                {
                    ShowReward();
                },

                delegate (int _code, string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                },

                delegate (string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );
            }
            else if(RewardMoneyIcon.activeSelf)
            {
                UserData.Instance.TotalMoney.AddMoney(RewardMoney);
                NetworkManager.Instance.mUserData.moneyString = UserData.Instance.TotalMoney.number;
                NetworkManager.Instance.mUserData.moneyLength = UserData.Instance.TotalMoney.length;
                ShowReward();
            }
            else ShowReward();
        }
    }
}
