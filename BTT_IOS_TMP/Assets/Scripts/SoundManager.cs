﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance = null;

    public const int BGM_TITLE = 1;
    public const int BGM_GAME = 2;
    public const int BGM_TRAVEL = 3;
    public AudioSource BGM_TITLE_SOURCE;
    public AudioSource BGM_GAME_SOURCE;
    public AudioSource BGM_TRAVEL_SOURCE;

    public const int EFFECT_BUTTON = 1;
    public const int EFFECT_BUTTON_OK = 2;
    public const int EFFECT_BUTTON_CLOSE = 3;
    public const int EFFECT_BUTTON_FACTORY = 4;
    public const int EFFECT_PRODUCT = 5;
    public const int EFFECT_POPUP = 6;
    public const int EFFECT_AIRPLANE = 7;
    public const int EFFECT_BUY = 8;
    public const int EFFECT_SELL = 9;
    public const int EFFECT_LEVELUP = 10;
    public const int EFFECT_REWARD = 11;
    public const int EFFECT_ERROR = 12;
    public const int EFFECT_BURGLER = 13;
    public const int EFFECT_SLIDE = 14;
    public const int EFFECT_RND_UNLOCKSTART= 16;
    public const int EFFECT_RND_FINISH = 17;
    public const int EFFECT_CITY_UNLOCK = 18;
    public const int EFFECT_CITY_SELECT = 19;
    public const int EFFECT_MARKET_SELECT = 20;
    public const int EFFECT_MARKET_EXIT = 21;
    public const int EFFECT_TAP1 = 24;
    public const int EFFECT_TAP2 = 25;
    public const int EFFECT_FACTORY_UPGRADE = 26;
    public const int EFFECT_QUEST_CLEAR = 27;


    public AudioSource EFFECT_BUTTON_SOURCE;
    public AudioSource EFFECT_BUTTON_OK_SOURCE;
    public AudioSource EFFECT_BUTTON_CLOSE_SOURCE;
    public AudioSource EFFECT_BUTTON_FACTORY_SOURCE;
    public AudioSource EFFECT_PRODUCT_SOURCE;
    public AudioSource EFFECT_POPUP_SOURCE;
    public AudioSource EFFECT_AIRPLANE_SOURCE;
    public AudioSource EFFECT_BUY_SOURCE;
    public AudioSource EFFECT_SELL_SOURCE;
    public AudioSource EFFECT_LEVELUP_SOURCE;
    public AudioSource EFFECT_REWARD_SOURCE;
    public AudioSource EFFECT_ERROR_SOURCE;
    public AudioSource EFFECT_BURGLER_SOURCE;
    public AudioSource EFFECT_SLIDE_SOURCE;
    public AudioSource EFFECT_TOCITY_SOURCE;
    public AudioSource EFFECT_RND_UNLOCKSTART_SOURCE;
    public AudioSource EFFECT_RND_FINISH_SOURCE;
    public AudioSource EFFECT_CITY_UNLOCK_SOURCE;
    public AudioSource EFFECT_CITY_SELECT_SOURCE;
    public AudioSource EFFECT_MARKET_SELECT_SOURCE;
    public AudioSource EFFECT_MARKET_EXIT_SOURCE;
    public AudioSource EFFECT_TAP1_SOURCE;
    public AudioSource EFFECT_TAP2_SOURCE;
    public AudioSource EFFECT_FACTORY_UPGRADE_SOURCE;
    public AudioSource EFFECT_QUEST_CLEAR_SOURCE;

    private Dictionary<int, AudioSource> Effects = new Dictionary<int, AudioSource>();
    private Dictionary<int, AudioSource> BGMs = new Dictionary<int, AudioSource>();

    private int NowBGMKey = -1;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        //
        BGMs.Add(BGM_TITLE, BGM_TITLE_SOURCE);
        BGMs.Add(BGM_GAME, BGM_GAME_SOURCE);
        BGMs.Add(BGM_TRAVEL, BGM_TRAVEL_SOURCE);

        //
        Effects.Add(EFFECT_BUTTON, EFFECT_BUTTON_SOURCE);
        Effects.Add(EFFECT_BUTTON_OK, EFFECT_BUTTON_OK_SOURCE);
        Effects.Add(EFFECT_BUTTON_CLOSE, EFFECT_BUTTON_CLOSE_SOURCE);
        Effects.Add(EFFECT_BUTTON_FACTORY, EFFECT_BUTTON_FACTORY_SOURCE);
        Effects.Add(EFFECT_PRODUCT, EFFECT_PRODUCT_SOURCE);
        Effects.Add(EFFECT_POPUP, EFFECT_POPUP_SOURCE);
        Effects.Add(EFFECT_AIRPLANE, EFFECT_AIRPLANE_SOURCE);
        Effects.Add(EFFECT_BUY, EFFECT_BUY_SOURCE);
        Effects.Add(EFFECT_SELL, EFFECT_SELL_SOURCE);
        Effects.Add(EFFECT_LEVELUP, EFFECT_LEVELUP_SOURCE);
        Effects.Add(EFFECT_REWARD, EFFECT_REWARD_SOURCE);
        Effects.Add(EFFECT_ERROR, EFFECT_ERROR_SOURCE);
        Effects.Add(EFFECT_BURGLER, EFFECT_BURGLER_SOURCE);
        Effects.Add(EFFECT_SLIDE, EFFECT_SLIDE_SOURCE);
        Effects.Add(EFFECT_RND_UNLOCKSTART, EFFECT_RND_UNLOCKSTART_SOURCE);
        Effects.Add(EFFECT_RND_FINISH, EFFECT_RND_FINISH_SOURCE);
        Effects.Add(EFFECT_CITY_UNLOCK, EFFECT_CITY_UNLOCK_SOURCE);
        Effects.Add(EFFECT_CITY_SELECT, EFFECT_CITY_SELECT_SOURCE);
        Effects.Add(EFFECT_MARKET_SELECT, EFFECT_MARKET_SELECT_SOURCE);
        Effects.Add(EFFECT_MARKET_EXIT, EFFECT_MARKET_EXIT_SOURCE);
        Effects.Add(EFFECT_TAP1, EFFECT_TAP1_SOURCE);
        Effects.Add(EFFECT_TAP2, EFFECT_TAP2_SOURCE);
        Effects.Add(EFFECT_FACTORY_UPGRADE, EFFECT_FACTORY_UPGRADE_SOURCE);
        Effects.Add(EFFECT_QUEST_CLEAR, EFFECT_QUEST_CLEAR_SOURCE);
    }

    public void PlayEffects(int _key)
    {
        if (!isOnEffect()) return;
        if (Effects.ContainsKey(_key)) Effects[_key].PlayOneShot(Effects[_key].clip);
    }

    public void PlayBGM(int _key = -1)
    {
        if(!isOnBGM())
        {
            if (BGMs.ContainsKey(NowBGMKey)) BGMs[NowBGMKey].Stop();
            if (_key != -1) NowBGMKey = _key;
        }
        else
        {
            if (_key == -1)
            {
                if (BGMs.ContainsKey(NowBGMKey)) BGMs[NowBGMKey].Play();
            }
            else
            {
                if (NowBGMKey == _key) return;

                if (BGMs.ContainsKey(NowBGMKey)) BGMs[NowBGMKey].Stop();
                NowBGMKey = _key;
                if (BGMs.ContainsKey(NowBGMKey)) BGMs[NowBGMKey].Play();
            }
        }
    }

    private const string EffectKey = "Effect";
    private const string BGMKey = "BGM";

    public void SetEffect(bool _val)
    {
        PlayerPrefs.SetInt(EffectKey, _val ? 0 : 1);
    }

    public void SetBGM(bool _val)
    {
        PlayerPrefs.SetInt(BGMKey, _val ? 0 : 1);
        PlayBGM();
    }

    public static bool isOnEffect() { return PlayerPrefs.GetInt(EffectKey) == 0; }
    public static bool isOnBGM() { return PlayerPrefs.GetInt(BGMKey) == 0; }
}
