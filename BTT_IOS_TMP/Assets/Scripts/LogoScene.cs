﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoScene : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Invoke("LoadScene", 2.5f);
	}
	
	private void LoadScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
