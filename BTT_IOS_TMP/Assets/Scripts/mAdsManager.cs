﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using System;
using SimpleJSON;
using GoogleMobileAds.Api;

public class mAdsManager : MonoBehaviour
{

    private static mAdsManager _instance = null;
    public static mAdsManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(mAdsManager)) as mAdsManager;
                if (_instance == null)
                {
                    _instance = (new GameObject("mAdsManager")).AddComponent<mAdsManager>();
                }
            }
            return _instance;
        }
    }

    // Update is called once per frame
    void Awake()
    {

        DontDestroyOnLoad(this);

        InitUnityAD();
        InitAdmob();
    }

#if UNITY_ANDROID
    private const string UnityAD = "1744447";
    private const string AdmobID = "ca-app-pub-4979777715149491~9805822932";
    private const string AdmobVideoAdID = "ca-app-pub-4979777715149491/6445162423";
#elif UNITY_IOS
    private const string UnityAD = "1744451";
    private const string AdmobID = "ca-app-pub-4979777715149491~7277584712";
    private const string AdmobVideoAdID = "ca-app-pub-4979777715149491/1067399586";
#endif

    private void InitUnityAD()
    {
        try
        {
            Advertisement.Initialize(UnityAD, false);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private RewardBasedVideoAd admobVideoAD = null;
    private Action onAdmobAdClosed;

    private void InitAdmob()
    {
        MobileAds.Initialize(AdmobID);

        admobVideoAD = RewardBasedVideoAd.Instance;
        admobVideoAD.OnAdRewarded += delegate (object _sender, Reward _reward) 
        {
            onAdmobAdClosed();
        };

        AdRequest.Builder builder = new AdRequest.Builder();
        AdRequest request = builder.Build();

        admobVideoAD.LoadAd(request, AdmobVideoAdID);
        admobVideoAD.OnAdClosed += delegate (object _sender, System.EventArgs _args)
        {
            _isOnAD = false;
            admobVideoAD.LoadAd(request, AdmobVideoAdID);
        };
    }

    private void showUnityAd(Action m_finishedVideoAD)
    {
        Advertisement.Show("rewardedVideo",
            new ShowOptions
            {
                resultCallback = result =>
                {
                    _isOnAD = false;

                    if (result == ShowResult.Finished)
                    {
                        m_finishedVideoAD();
                    }
                }
            }
            );
    }

    private void showAdmobAD(Action m_finishedVideoAD)
    {
        onAdmobAdClosed = m_finishedVideoAD;

        admobVideoAD.Show();
    }

    public bool isCanShowUnityAd()
    {
        return Advertisement.IsReady("rewardedVideo");
    }

    public bool isCanShowAdmob()
    {
        return admobVideoAD.IsLoaded();
    }

    private bool _isOnAD = false;
    public void ShowAD(Action _act)
    {
        if (_isOnAD) return;

        _isOnAD = true;

#if UNITY_EDITOR
        if (isCanShowUnityAd()) showUnityAd(_act);
        else
        {
            _isOnAD = false;
            PopupManager.Instance.SetPopup("Please retry later.");
        }
#else
        if (isCanShowAdmob()) showAdmobAD(_act);
        else if (isCanShowUnityAd()) showUnityAd(_act);
        else
        {
            _isOnAD = false;
            PopupManager.Instance.SetPopup("Please retry later.");
        }
#endif
    }
}
