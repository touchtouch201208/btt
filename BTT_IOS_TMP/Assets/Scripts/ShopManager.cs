﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class ShopManager : MonoBehaviour {

    public GameObject BaseRoot;
    public GameObject BaseMenu;
    public GameObject BaseGem;
    public GameObject BaseManager;
    public GameObject BaseBoost;
    public GameObject BaseBox;
    public GameObject BaseAssistant;
    public GameObject BaseMoney;

    public Text LabelMoney;
    public Text LabelGem;

    public GameObject NotificateIcon;
    public GameObject NotificateManager;
    public GameObject NotificateBooster;
    public GameObject NotificateAssistant;
    public GameObject NotificateBox;

    public GameObject ManagerScrollRoot;
    public GameObject ManagerEmptyRoot;

    public ScrollRect GemScroll;
    public RectTransform GemScrollRoot;

    private UICityButtonNodes[] GemNodes;
    private UIScrollNodeProduct[] ManagerNodes;
    private UIScrollNodeProduct[] BoosterNodes;
    private UICityButtonNodes[] BoosterCashNodes;
    private UIScrollNodeProduct[] AssistantNodes;
    private UICityButtonNodes[] BoxNodes;
    private UICityButtonNodes[] MoneyNodes;

    public static ShopManager Instance = null;

    void Awake()
    {
        Instance = this;

        //--------------
        GemNodes = BaseGem.GetComponentsInChildren<UICityButtonNodes>(true);
        var _gem_first = ShopDB.Instance.Gems[0].rewardGem
            / float.Parse(BillingManager.Instance.InfoList[ShopDB.Instance.Gems[0].itemKey].PriceUSD);
        for (int i = 0; i<GemNodes.Length; i++)
        {
            int _idx = i;
            int _id = GemNodes[i].CityID;

            var _node = ShopDB.Instance.Gems[_id];
            var _info = BillingManager.Instance.InfoList[_node.itemKey];

            if (_id < 5)
            {
                GemNodes[i].transform.Find("Amount").GetComponent<Text>().text = _node.rewardGem.ToString();
                GemNodes[i].transform.Find("Price").GetComponent<Text>().text = "$" + _info.PriceUSD.ToString();
                if (_id >= 1) GemNodes[i].transform.Find("Sale").GetComponent<Text>().text =
                        string.Format(Language.Str.SALE_FORMAT, ((_node.rewardGem / (_gem_first * float.Parse(_info.PriceUSD))) - 1.0f).ToString("0%"));
            }
            else if (_id == 5)
            {
                GemNodes[i].transform.Find("Explain2").GetComponent<Text>().text =
                    string.Format(Language.Str.SHOP_PACKAGE_FORMAT_EXPLAIN, _node.rewardGem, Language.Str.SHOP_TITLE_GEMS, new Money(_node.rewardMoney.ToString()).ToWon());
            }

            GemNodes[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyGem(_id);
            });
        }

        //--------------
        ManagerNodes = BaseManager.GetComponentsInChildren<UIScrollNodeProduct>(true).OrderBy(x=>x.name).ToArray();
        for(int i = 0; i<ManagerNodes.Length; i++)
        {
            ManagerNodes[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -30 + (-100 * i));

            int _idx = i;
            ManagerNodes[i].transform.Find("BuyButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyManager(ManagerNodes[_idx].ProductID, _idx);
            }
            );
            
            ManagerNodes[i].transform.Find("Factory").GetComponent<Text>().text = "";
        }

        BoosterNodes = BaseBoost.GetComponentsInChildren<UIScrollNodeProduct>().OrderBy(x => x.name).ToArray();
        for (int i = 0; i<BoosterNodes.Length; i++)
        {
            int _key = BoosterDB.Instance.Boosters.ElementAt(i).Key;
            int _idx = i;
            BoosterNodes[i].ProductID = _key;
            BoosterNodes[i].transform.Find("BuyButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyBooster(_idx);
            }
            );
        }

        BoosterCashNodes = BaseBoost.GetComponentsInChildren<UICityButtonNodes>();
        for(int i = 0; i<BoosterCashNodes.Length; i++)
        {
            BoosterCashNodes[i].transform.Find("Explain").Find("Price").GetComponent<Text>().text = ShopDB.Instance.Booster[BoosterCashNodes[i].CityID].priceGem.ToString();

            int _idx = i;
            BoosterCashNodes[i].transform.Find("BuyButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyBoosterCash(BoosterCashNodes[_idx].CityID);
            }
            );
        }

        AssistantNodes = BaseAssistant.GetComponentsInChildren<UIScrollNodeProduct>().OrderBy(x => x.name).ToArray();
        for(int i = 0; i<AssistantNodes.Length; i++)
        {
            int _id = AssistantDB.Instance.Assistants.ElementAt(i).Key;
            int _idx = i;
            AssistantNodes[i].ProductID = _id;

            AssistantNodes[i].transform.Find("Explain").GetComponent<Text>().text = Language.Str.ASSISTANTLABEL(_id);
            AssistantNodes[i].transform.Find("Explain").Find("Name").GetComponent<Text>().text = Language.Str.ASSISTANTNAME(_id);
            
            AssistantNodes[i].transform.Find("BuyButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyAssistant(_idx);
            }
            );

            AssistantNodes[i].transform.Find("BuyGemButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyAssistantGem(_idx - 1);
            }
            );

            AssistantNodes[i].transform.Find("UseButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                UseAssistant(_id);
            }
            );
        }

        BoxNodes = BaseBox.GetComponentsInChildren<UICityButtonNodes>().OrderBy(x => x.name).ToArray();
        for (int i = 0; i < BoxNodes.Length; i++)
        {
            int _idx = BoxNodes[i].CityID;
            if(BoxNodes[i].transform.Find("BuyButton") != null)
                BoxNodes[i].transform.Find("BuyButton").GetComponent<Button>().onClick.AddListener(delegate
                {
                    BuyBoxMoney(_idx);
                }
                );

            if (BoxNodes[i].transform.Find("BuyGemButton") != null)
                BoxNodes[i].transform.Find("BuyGemButton").GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyBoxGem(_idx);
            }
            );

            var _ad = BoxNodes[i].transform.Find("GetADButton");
            if(_ad != null) _ad.GetComponent<Button>().onClick.AddListener(delegate
            {
                GetBoxAD();
            }
            );
        }

        //--------------
        MoneyNodes = BaseMoney.GetComponentsInChildren<UICityButtonNodes>(true);
        var _money_first = (float)ShopDB.Instance.Moneys[0].rewardMoney / ShopDB.Instance.Moneys[0].priceGem;
        for (int i = 0; i < MoneyNodes.Length; i++)
        {
            int _idx = i;
            int _id = MoneyNodes[i].CityID;

            var _node = ShopDB.Instance.Moneys[_id];

            MoneyNodes[i].transform.Find("Amount").GetComponent<Text>().text = "$" + new Money(_node.rewardMoney.ToString()).ToWon();
            MoneyNodes[i].transform.Find("Price").GetComponent<Text>().text = _node.priceGem.ToString();

            if (i > 0)
            {
                MoneyNodes[i].transform.Find("Sale").GetComponent<Text>().text =
                string.Format(Language.Str.SALE_FORMAT, ((_node.rewardMoney / (_money_first * _node.priceGem)) - 1.0f).ToString("0%"));
            }

            MoneyNodes[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                BuyMoney(_id);
            });
        }

        OnNewItemCheck();
        BaseRoot.SetActive(false);
    }

	// Use this for initialization
	void Start () {
	
	}

    private float notice_timer = 99;
	// Update is called once per frame
	void Update () {
	
        if(BaseRoot.activeSelf)
        {
            LabelMoney.text = "$" + UserData.Instance.TotalMoney.ToWon();
            LabelGem.text = NetworkManager.Instance.mUserData.cashLabel;
        }
        
        if (BaseBoost.activeSelf)
        {
            for (int i = 0; i < BoosterCashNodes.Length; i++)
            {
                if (BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").gameObject.activeSelf)
                {
                    System.DateTime _time = System.DateTime.MinValue;
                    switch (BoosterCashNodes[i].CityID)
                    {
                        case 0: _time = NetworkManager.Instance.mUserData.reserveTime1; break;
                        case 1: _time = NetworkManager.Instance.mUserData.reserveTime2; break;
                        case 2: _time = NetworkManager.Instance.mUserData.reserveTime3; break;
                    }

                    float _left_time = (float)(_time - NetworkManager.Instance.GetServerNow()).TotalSeconds;
                    if (_left_time > 0)
                    {
                        BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").GetComponent<Text>().text = string.Format(
                            Language.Str.SHOP_BOOST_FORAMT_TIME,
                            Mathf.Floor(_left_time / 3600),
                            Mathf.Floor((_left_time % 3600) / 60),
                            Mathf.Floor(_left_time % 60)
                            );

                        BoosterCashNodes[i].transform.Find("BuyButton").Find("Label").gameObject.SetActive(false);
                        BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").gameObject.SetActive(true);
                    }
                    else
                    {
                        BoosterCashNodes[i].transform.Find("BuyButton").Find("Label").gameObject.SetActive(true);
                        BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").gameObject.SetActive(false);
                    }
                }
            }
        }
        else if(BaseBox.activeSelf)
        {
            float _left_time = (float)(NetworkManager.Instance.mUserData.reserveTime4 - NetworkManager.Instance.GetServerNow()).TotalSeconds;
            if (_left_time > 0)
            {
                BoxNodes[0].transform.Find("GetADButton").Find("Label").GetComponent<Text>().text = string.Format(
                    "{0:00}:{1:00}:{2:00}",
                    Mathf.Floor(_left_time / 3600),
                    Mathf.Floor((_left_time % 3600) / 60),
                    Mathf.Floor(_left_time % 60)
                    );
            }
            else
            {
                BoxNodes[0].transform.Find("GetADButton").Find("Label").GetComponent<Text>().text = Language.Str.WATCH_AD_2;
            }
        }

        notice_timer += Time.deltaTime;
        if(notice_timer > 1)
        {
            float _left_time = (float)(NetworkManager.Instance.mUserData.reserveTime4 - NetworkManager.Instance.GetServerNow()).TotalSeconds;
            if (_left_time > 0) NotificateBox.SetActive(false);
            else
            {
                NotificateIcon.SetActive(true);
                NotificateBox.SetActive(true);
            }

            notice_timer = 0;
        }
	}

    private GameObject BeforeBase = null;
    public void SetBase(GameObject _base)
    {
        if (BaseRoot.activeSelf) BeforeBase = BaseRoot;
        else BeforeBase = null;

        BaseRoot.SetActive(true);
        //SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON);

        BaseMenu.SetActive(false);
        BaseGem.SetActive(false);
        BaseManager.SetActive(false);
        BaseBoost.SetActive(false);
        BaseBox.SetActive(false);
        BaseAssistant.SetActive(false);
        BaseMoney.SetActive(false);

        _base.SetActive(true);

        if(_base == BaseMenu)
        {
        }
        if(_base == BaseManager)
        {
            NotificateManager.SetActive(false);
            SetManagerInfos();
        }
        else if(_base == BaseBoost)
        {
            NotificateBooster.SetActive(false);
            SetBoosterInfos();
        }
        else if(_base == BaseAssistant)
        {
            NotificateAssistant.SetActive(false);
            SetAssistantInfos();
        }
        else if(_base == BaseBox)
        {
            NotificateBox.SetActive(false);
            SetBoxInfos();
        }
        else if(_base == BaseGem
            || _base == BaseMoney)
        {
            SetGemInfos();
        }
    }

    public void ExitShop()
    {
        if (BaseMenu.activeSelf)
        {
            BaseRoot.SetActive(false);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
        }
        else if (BeforeBase == BaseRoot)
        {
            if (BaseManager.activeSelf)
            {
                CheckPriceManager = new Money(UserData.Instance.TotalMoney);
                OnNewItemCheck();
            }
            if (BaseAssistant.activeSelf)
            {
                CheckPriceAssistant = new Money(UserData.Instance.TotalMoney);
                OnNewItemCheck();
            }
            if (BaseBoost.activeSelf)
            {
                CheckPriceBooster = new Money(UserData.Instance.TotalMoney);
                OnNewItemCheck();
            }
            SetBase(BaseMenu);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_TAP1);
        }
        else
        {
            BaseRoot.SetActive(false);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
        }
    }

    //-----------------------------------------------

    private void SetGemInfos()
    {
        if (NetworkManager.Instance.mUserData.memberGrade == 3)
        {
            for(int i = 0; i<GemNodes.Length; i++)
            {
                if(GemNodes[i].CityID == 5)
                {
                    GemNodes[i].gameObject.SetActive(false);
                    break;
                }
            }

            GemScroll.enabled = false;
            GemScrollRoot.anchoredPosition = Vector2.up * 355;
        }
        else
        {
            GemScroll.enabled = true;
            GemScrollRoot.anchoredPosition = Vector2.zero;
            for (int i = 0; i < GemNodes.Length; i++)
            {
                if (GemNodes[i].CityID == 5)
                {
                    GemNodes[i].gameObject.SetActive(true);
                    break;
                }
            }
        }
    }

    public void BuyGem(int _idx)
    {
#if UNITY_EDITOR

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.ShopBuy(
                    ShopDB.Instance.Gems[_idx].rewardGem,
                    delegate (SimpleJSON.JSONNode N)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_PURCHASE_GEM_SUCCESS);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
                        SetGemInfos();
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    }
                    );
#else
        var _node = ShopDB.Instance.Gems[_idx];

        NetworkManager.Instance.RequestPurchaseStart(
            _node,
            delegate (SimpleJSON.JSONNode N)
            {
                var _info = BillingManager.Instance.InfoList[_node.itemKey];

                BillingManager.Instance.Purchase(
                    BillingManager.Instance.InfoList[_node.itemKey].aosBillCode,
                    delegate (SimpleJSON.JSONNode N2)
                    {
                        if (N2["resultCode"].AsInt == 1)
                            PopupManager.Instance.SetPopup("Purchase gems confirmd.");
                        else PopupManager.Instance.SetPopup(N2["resultMessage"]);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
                        SetGemInfos();
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup("Error : " + _message);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup("Please try again.");
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate
                    {
                        PopupManager.Instance.SetOffPopup();
                    }
                    );
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup("Error : " + _message);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup("Please try again.");
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            }
        );
#endif

    }

    public void BuyMoney(int _idx)
    {
        if(!MainScene.Instance.CheckPrice(ShopDB.Instance.Moneys[_idx].priceGem))
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestBuyShopItem(
                        ShopDB.Instance.Moneys[_idx].shopID,
                        ShopDB.PRICE_TYPE_GEM,
                        delegate (SimpleJSON.JSONNode N)
                        {
                            PopupManager.Instance.SetOffPopup();
                            PopupManager.Instance.SetPopup(Language.Str.POPUP_PURCHASE_MONEY_SUCCESS);
                            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                            IGAWorksManager.onRetention("UseCashBuyMoney" + _idx);
                        },
                        delegate (int _code, string _message)
                        {
                            PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                        },
                        delegate (string _message)
                        {
                            PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                        }
                        );
        }
    }

    //-----------------------------------------------

    private void SetManagerInfos()
    {
        var _list = ManagerDB.Instance.Managerlist.Where(x => x.level > FactoryManager.Instance.Factorys[x.factoryID].ManagerLavel).ToList();
        _list = _list.OrderBy(x => x.level).ThenBy(x => x.factoryID).ToList();

        //모든 매니저를 구입하면
        if (_list.Count == 0)
        {
            ManagerEmptyRoot.SetActive(true);
            ManagerScrollRoot.SetActive(false);
        }
        else // 아니면
        {
            ManagerEmptyRoot.SetActive(false);
            ManagerScrollRoot.SetActive(true);

            int active_count = 0;
            for (int i = 0; i < ManagerNodes.Length; i++)
            {
                if (i >= _list.Count) ManagerNodes[i].gameObject.SetActive(false);
                else
                {
                    active_count++;

                    var _node = _list[i];
                    int _id = ManagerDB.Instance.Managerlist.IndexOf(_node);

                    ManagerNodes[i].gameObject.SetActive(true);

                    ManagerNodes[i].ProductID = _id;

                    ManagerNodes[i].transform.Find("Explain").GetComponent<Text>().text = Language.Str.MANAGERINFO(_node.factoryID, _node.level);

                    ManagerNodes[i].transform.Find("IconImg").GetComponent<Image>().sprite = _node.Img;

                    var _price = new Money(_node.priceMoney.ToString());
                    ManagerNodes[i].transform.Find("Explain").Find("Price").GetComponent<Text>().text =
                    "$" + _price.ToWon();

                    bool _active = UserData.Instance.TotalMoney.CompareMoney(_price);
                    ManagerNodes[i].transform.Find("BuyButton").gameObject.SetActive(_active);
                    ManagerNodes[i].transform.Find("LockButton").gameObject.SetActive(!_active);
                }
            }

            ManagerNodes[0].transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(
                ManagerNodes[0].transform.parent.GetComponent<RectTransform>().sizeDelta.x, 60 + (active_count * 100));
        }
    }

    public void BuyManager(int _idx, int _ui_idx)
    {
        var _node = ManagerDB.Instance.Managerlist[_idx];

        Money _price = new Money(_node.priceMoney.ToString());

        if (UserData.Instance.TotalMoney.CompareMoney(_price))
        {
            MainScene.Instance.OnUseMoney(_price);
            FactoryManager.Instance.Factorys[_node.factoryID].ManagerLavel = _node.level;
            FactoryManager.Instance.Factorys[_node.factoryID].OnCollect();
            FactoryManager.Instance.Factorys[_node.factoryID].OnInit();

            var _txt = ManagerNodes[_ui_idx].transform.Find("BuyButton").Find("Label").GetComponent<Text>();
            UITextAppearManager.Instance.SetAppear(
                    //_txt.transform.parent.parent,
                    ManagerNodes[_ui_idx].transform.Find("BG"),
                    "- $" + _price.ToWon(),
                    _txt.fontSize,
                    Color.green * 0.5f,
                    Color.red);

            SetManagerInfos();
            QuestList.Instance.OnBuyManager();

            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
        }
        else SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
    }

    //-----------------------------------------------

    private void SetBoosterInfos()
    {
        //set boost cash ------------------------------------------------------------
        for (int i = 0; i<BoosterCashNodes.Length; i++)
        {
            var _node = ShopDB.Instance.Booster[BoosterCashNodes[i].CityID];

            System.DateTime _time = System.DateTime.MinValue;
            switch(BoosterCashNodes[i].CityID)
            {
                case 0: _time = NetworkManager.Instance.mUserData.reserveTime1; break;
                case 1: _time = NetworkManager.Instance.mUserData.reserveTime2; break;
                case 2: _time = NetworkManager.Instance.mUserData.reserveTime3; break;
            }
            
            float _left_time = (float)(_time - NetworkManager.Instance.GetServerNow()).TotalSeconds;
            if(_left_time > 0)
            {
                BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").GetComponent<Text>().text = string.Format(
                    Language.Str.SHOP_BOOST_FORAMT_TIME,
                    Mathf.Floor(_left_time / 3600),
                    Mathf.Floor((_left_time % 3600) / 60),
                    Mathf.Floor(_left_time % 60)
                    );

                BoosterCashNodes[i].transform.Find("BuyButton").Find("Label").gameObject.SetActive(false);
                BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").gameObject.SetActive(true);
            }
            else
            {
                BoosterCashNodes[i].transform.Find("BuyButton").Find("Label").gameObject.SetActive(true);
                BoosterCashNodes[i].transform.Find("BuyButton").Find("Time").gameObject.SetActive(false);
            }
        }

        //set boost node ------------------------------------------------------------
        for (int i = 0; i < BoosterNodes.Length; i++)
        {
            var _node = UserData.Instance.Boosters.ElementAt(i);
            var _max_info = BoosterDB.Instance.Boosters[_node.Key].Last();

            if (_node.Value >= _max_info.level)
            {
                /*
                BoosterNodes[i].transform.Find("Name").GetComponent<Text>().text =
                    string.Format(Language.Str.SHOP_BOOST_FORMAT_LEVEL_MAX, Language.Str.ITEMGROUPNAME(_node.Key));
                    */

                BoosterNodes[i].transform.Find("Explain").GetComponent<Text>().text = 
                    string.Format(Language.Str.SHOP_BOOST_FORMAT_EXPLAIN,
                    _max_info.percentage, Language.Str.ITEMGROUPNAME(_node.Key));

                BoosterNodes[i].transform.Find("Explain").Find("Price").GetComponent<Text>().text =Language.Str.MAX_LEVEL;

                /*
                BoosterNodes[i].transform.Find("IconLabel").GetComponent<Text>().text =
                    _max_info.percentage + "%";
                    */

                BoosterNodes[i].transform.Find("BuyButton").gameObject.SetActive(false);
                BoosterNodes[i].transform.Find("LockButton").gameObject.SetActive(false);
                BoosterNodes[i].transform.Find("Max").gameObject.SetActive(true);
            }
            else
            {
                var _info = BoosterDB.Instance.Boosters[_node.Key][_node.Value];
                
                /*
                BoosterNodes[i].transform.Find("Name").GetComponent<Text>().text =
                string.Format("{0} Booster\nLv.{1}",
                    ItemGroupDB.Instance.ItemGroups[_node.Key],
                    _node.Value + 1
                    );
                    */

                BoosterNodes[i].transform.Find("Explain").GetComponent<Text>().text =
                    string.Format(Language.Str.SHOP_BOOST_FORMAT_EXPLAIN,
                    _info.percentage, Language.Str.ITEMGROUPNAME(_node.Key));

                /*
                BoosterNodes[i].transform.Find("IconLabel").GetComponent<Text>().text =
                    _info.percentage + "%";
                    */

                //BoosterNodes[i].GetComponentInChildren<UIBoostFormulaList>().SetFormula(_info.formula);

                var _price = new Money(_info.priceMoney.ToString());
                BoosterNodes[i].transform.Find("Explain").Find("Price").GetComponent<Text>().text =
                    "$" + _price.ToWon();

                bool _active = UserData.Instance.TotalMoney.CompareMoney(_price);
                BoosterNodes[i].transform.Find("BuyButton").gameObject.SetActive(_active);
                BoosterNodes[i].transform.Find("LockButton").gameObject.SetActive(!_active);
                BoosterNodes[i].transform.Find("Max").gameObject.SetActive(false);
            }
        }
    }

    public void BuyBooster(int _idx)
    {
        int _key = BoosterNodes[_idx].ProductID;

        int _level = UserData.Instance.Boosters[_key];

        if (_level >= BoosterDB.Instance.Boosters[_key].Last().level) return;

        var _info = BoosterDB.Instance.Boosters[_key][_level];
        
        Money _price = new Money(_info.priceMoney.ToString());

        int _id = 0; int _amount = 0;
        if (UserData.Instance.TotalMoney.CompareMoney(_price))
        {
            if (_info.formula != "")
            {
                string[] _recipt = _info.formula.Split('+');

                for (int i = 0; i < _recipt.Length; i++)
                {
                    string[] _recipt_node = _recipt[i].Split(':');
                    _id = int.Parse(_recipt_node[0]);
                    _amount = int.Parse(_recipt_node[1]);
                    if (!UserData.Instance.isExistItemWarehouse(_id, _amount)
                        && !UserData.Instance.isExistItemInventory(_id, _amount))
                    {
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                        return;
                    }
                }

                for (int i = 0; i < _recipt.Length; i++)
                {
                    string[] _recipt_node = _recipt[i].Split(':');
                    _id = int.Parse(_recipt_node[0]);
                    _amount = int.Parse(_recipt_node[1]);
                    if (UserData.Instance.isExistItemInventory(_id, _amount)) UserData.Instance.SubProductInventory(_id, _amount);
                }
            }

            MainScene.Instance.OnUseMoney(_price);
            UserData.Instance.Boosters[_key]++;

            var _txt = BoosterNodes[_idx].transform.Find("BuyButton").Find("Price").GetComponent<Text>();
            UITextAppearManager.Instance.SetAppear(
                    //_txt.transform.parent.parent,
                    BoosterNodes[_idx].transform.Find("BG"),
                    "- $" + _price.ToWon(),
                    _txt.fontSize,
                    Color.green * 0.5f,
                    Color.red);

            SetBoosterInfos();
            QuestList.Instance.OnBuyBoost();

            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
        }
        else SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
    }

    public void BuyBoosterCash(int _idx)
    {
        System.DateTime _time = System.DateTime.MinValue;
        switch (_idx)
        {
            case 0: _time = NetworkManager.Instance.mUserData.reserveTime1; break;
            case 1: _time = NetworkManager.Instance.mUserData.reserveTime2; break;
            case 2: _time = NetworkManager.Instance.mUserData.reserveTime3; break;
        }

        float _left_time = (float)(_time - NetworkManager.Instance.GetServerNow()).TotalSeconds;
        if (_left_time > 0) return;

        if (MainScene.Instance.CheckPrice(ShopDB.Instance.Booster[_idx].priceGem)) return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
                    ShopDB.Instance.Booster[_idx].shopID,
                    ShopDB.PRICE_TYPE_GEM,
                    delegate (SimpleJSON.JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();
                        SetBoosterInfos();
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                        IGAWorksManager.onRetention("UseCashBooster" + _idx);
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    }
                    );
    }

    //-----------------------------------------------
    
    private void SetAssistantInfos()
    {
        for (int i = 0; i < AssistantNodes.Length; i++)
        {
            var _node = AssistantDB.Instance.Assistants.ElementAt(i);

            if(UserData.Instance.Assistants[_node.Key] == 1)
            {
                AssistantNodes[i].transform.Find("BuyButton").gameObject.SetActive(false);
                AssistantNodes[i].transform.Find("LockButton").gameObject.SetActive(false);
                AssistantNodes[i].transform.Find("BuyGemButton").gameObject.SetActive(false);
                AssistantNodes[i].transform.Find("UseButton").gameObject.SetActive(_node.Key != NetworkManager.Instance.mUserData.selectedAssistantID);
            }
            else
            {
                var _price = ShopDB.Instance.Assistatn[i - 1];
                var _money = new Money(_price.priceMoney.ToString());
                AssistantNodes[i].transform.Find("BuyButton").Find("Price").GetComponent<Text>().text =
                AssistantNodes[i].transform.Find("LockButton").Find("Price").GetComponent<Text>().text =
                    "$" + _money.ToWon();

                AssistantNodes[i].transform.Find("BuyGemButton").Find("Price").GetComponent<Text>().text
                    = _price.priceGem.ToString();

                bool _active = UserData.Instance.TotalMoney.CompareMoney(_money);
                AssistantNodes[i].transform.Find("BuyButton").gameObject.SetActive(_active);
                AssistantNodes[i].transform.Find("LockButton").gameObject.SetActive(!_active);
                AssistantNodes[i].transform.Find("BuyGemButton").gameObject.SetActive(true);
                AssistantNodes[i].transform.Find("UseButton").gameObject.SetActive(false);
            }
        }
    }

    public void BuyAssistant(int _idx)
    {
        int _id = AssistantNodes[_idx].ProductID;
        if (UserData.Instance.Assistants[_id] == 1) return;

        Money _price = new Money(AssistantDB.Instance.Assistants[_id].priceMoney.ToString());
        Debug.Log(_price.ToWon());
        if (MainScene.Instance.CheckPrice(_price)) return;
        
        MainScene.Instance.OnUseMoney(_price);
        UserData.Instance.Assistants[_id] = 1;

        var _txt = AssistantNodes[_idx].transform.Find("BuyButton").Find("Price").GetComponent<Text>();
        UITextAppearManager.Instance.SetAppear(
                AssistantNodes[_idx].transform.Find("BG"),
                "- $" + _price.ToWon(),
                _txt.fontSize,
                Color.green * 0.5f,
                Color.red);
        
        UseAssistant(_id);

        SetAssistantInfos();
        QuestList.Instance.OnBuyAssistant();

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
    }

    public void BuyAssistantGem(int _idx)
    {
        if (MainScene.Instance.CheckPrice(ShopDB.Instance.Assistatn[_idx].priceGem)) return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
                    ShopDB.Instance.Assistatn[_idx].shopID,
                    ShopDB.PRICE_TYPE_GEM,
                    delegate (SimpleJSON.JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();

                        UseAssistant(ShopDB.Instance.Assistatn[_idx].level);

                        SetAssistantInfos();
                        AssistantManager.Instance.SetAssistant();
                        QuestList.Instance.OnBuyAssistant();

                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                        IGAWorksManager.onRetention("UseCashBuyAssistant" + _idx);
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    }
                    );
    }

    public void UseAssistant(int _id)
    {
        if (UserData.Instance.Assistants[_id] != 1) return;

        NetworkManager.Instance.mUserData.selectedAssistantID = _id;
        AssistantManager.Instance.SetAssistant();
        TipManager.Instance.SetMessage();

        SetAssistantInfos();

        IGAWorksManager.SetAssistant(NetworkManager.Instance.mUserData.selectedAssistantID);
    }

    //-----------------------------------------------
    
    private void SetBoxInfos()
    {
        for (int i = 0; i < BoxNodes.Length; i++)
        {
            var _price = ShopDB.Instance.Box[BoxNodes[i].CityID];

            if(BoxNodes[i].transform.Find("GetADButton") == null)
            {
                Money _money = new Money(_price.priceMoney.ToString());
                BoxNodes[i].transform.Find("BuyButton").Find("Price").GetComponent<Text>().text =
                BoxNodes[i].transform.Find("LockButton").Find("Price").GetComponent<Text>().text =
                        "$" + _money.ToWon();

                BoxNodes[i].transform.Find("BuyGemButton").Find("Price").GetComponent<Text>().text
                        = _price.priceGem.ToString();

                bool _acitve = UserData.Instance.TotalMoney.CompareMoney(_money);
                BoxNodes[i].transform.Find("BuyButton").gameObject.SetActive(_acitve);
                BoxNodes[i].transform.Find("LockButton").gameObject.SetActive(!_acitve);
            }
        }
    }

    public void BuyBoxMoney(int _id)
    {
        if (MainScene.Instance.CheckPrice(new Money(ShopDB.Instance.Box[_id].priceMoney.ToString()))) return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
                    ShopDB.Instance.Box[_id].shopID,
                    ShopDB.PRICE_TYPE_MONEY,
                    delegate (SimpleJSON.JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();

                        MainScene.Instance.OnUseMoney(new Money(ShopDB.Instance.Box[_id].priceMoney.ToString()));
                        BoxRewardManager.Instance.SetReward(N);
                        SetBoxInfos();
                        OnNewItemCheck();
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    }
                    );

    }

    public void BuyBoxGem(int _id)
    {
        if (MainScene.Instance.CheckPrice(ShopDB.Instance.Box[_id].priceGem)) return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
                    ShopDB.Instance.Box[_id].shopID,
                    ShopDB.PRICE_TYPE_GEM,
                    delegate (SimpleJSON.JSONNode N)
                    {
                        PopupManager.Instance.SetOffPopup();

                        BoxRewardManager.Instance.SetReward(N);
                        SetBoxInfos();
                        OnNewItemCheck();

                        IGAWorksManager.onRetention("UseCashBuyBox" + _id);
                    },
                    delegate (int _code, string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    },
                    delegate (string _message)
                    {
                        PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                    }
                    );
    }

    public void GetBoxAD()
    {
        float _left_time = (float)(NetworkManager.Instance.mUserData.reserveTime4 - NetworkManager.Instance.GetServerNow()).TotalSeconds;
        if (_left_time > 0) return;

        mAdsManager.Instance.ShowAD(delegate
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestRevisitOpenBox(
                        delegate (SimpleJSON.JSONNode N)
                        {
                            PopupManager.Instance.SetOffPopup();
                            NetworkManager.Instance.mUserData.reserveTime4 = NetworkManager.Instance.GetServerNow().AddHours(2);
                            NotificateIcon.SetActive(false);
                            OnNewItemCheck();

                            BoxRewardManager.Instance.SetReward(N);

                            IGAWorksManager.onRetention("ADBronzeBox");
                        },
                        delegate (int _code, string _message)
                        {
                            PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                        },
                        delegate (string _message)
                        {
                            PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                        }
                        );
        });
    }

    //-------------------------------

    private Money CheckPriceManager = new Money("0");
    private Money CheckPriceBooster = new Money("0");
    private Money CheckPriceAssistant = new Money("0");

    public void OnNewItemCheck()
    {
        if (CheckPriceManager.CompareMoney(UserData.Instance.TotalMoney)) CheckPriceManager = new Money(UserData.Instance.TotalMoney);
        if (CheckPriceBooster.CompareMoney(UserData.Instance.TotalMoney)) CheckPriceBooster = new Money(UserData.Instance.TotalMoney);
        if (CheckPriceAssistant.CompareMoney(UserData.Instance.TotalMoney)) CheckPriceAssistant = new Money(UserData.Instance.TotalMoney);

        //also on get money

        if (BaseManager.activeSelf)
        {
            var _list = ManagerDB.Instance.Managerlist.Where(x => x.level > FactoryManager.Instance.Factorys[x.factoryID].ManagerLavel).ToList();
            _list = _list.OrderBy(x => x.level).ThenBy(x => x.factoryID).ToList();

            for (int i = 0; i < ManagerNodes.Length; i++)
            {
                if (i < _list.Count)
                {
                    var _node = _list[i];
                    var _price = new Money(_node.priceMoney.ToString());
                    bool _active = UserData.Instance.TotalMoney.CompareMoney(_price);
                    ManagerNodes[i].transform.Find("BuyButton").gameObject.SetActive(_active);
                    ManagerNodes[i].transform.Find("LockButton").gameObject.SetActive(!_active);
                }
            }
        }
        else if(BaseBoost.activeSelf)
        {
            for (int i = 0; i < BoosterNodes.Length; i++)
            {
                var _node = UserData.Instance.Boosters.ElementAt(i);
                var _max_info = BoosterDB.Instance.Boosters[_node.Key].Last();

                if (_node.Value < _max_info.level)
                {
                    var _info = BoosterDB.Instance.Boosters[_node.Key][_node.Value];
                    var _price = new Money(_info.priceMoney.ToString());
                    bool _active = UserData.Instance.TotalMoney.CompareMoney(_price);
                    BoosterNodes[i].transform.Find("BuyButton").gameObject.SetActive(_active);
                    BoosterNodes[i].transform.Find("LockButton").gameObject.SetActive(!_active);
                }
            }
        }
        else if(BaseAssistant.activeSelf)
        {
            for (int i = 0; i < AssistantNodes.Length; i++)
            {
                var _node = AssistantDB.Instance.Assistants.ElementAt(i);

                if (UserData.Instance.Assistants[_node.Key] != 1)
                {
                    Money _money = new Money(ShopDB.Instance.Assistatn[i - 1].priceMoney.ToString());
                    bool _active = UserData.Instance.TotalMoney.CompareMoney(_money);
                    AssistantNodes[i].transform.Find("BuyButton").gameObject.SetActive(_active);
                    AssistantNodes[i].transform.Find("LockButton").gameObject.SetActive(!_active);
                }
            }
        }
        else if(BaseBox.activeSelf)
        {
            for (int i = 1; i < BoxNodes.Length; i++)
            {
                Money _money = new Money(ShopDB.Instance.Box[BoxNodes[i].CityID].priceMoney.ToString());
                bool _acitve = UserData.Instance.TotalMoney.CompareMoney(_money);
                BoxNodes[i].transform.Find("BuyButton").gameObject.SetActive(_acitve);
                BoxNodes[i].transform.Find("LockButton").gameObject.SetActive(!_acitve);
            }
        }

        WareHouseManager.Instance.OnCheckNotificateInventoryPrice();

        //if (NotificateIcon.activeSelf) return;

        bool _is_on_new_item = false;
        bool _is_on_manager = false;
        bool _is_on_booster = false;
        bool _is_on_assistant = false;
        
        for(int i = 0; i<ManagerDB.Instance.Managerlist.Count; i++)
        {
            var _node = ManagerDB.Instance.Managerlist[i];
            if (FactoryManager.Instance.Factorys[_node.factoryID].ManagerLavel >= _node.level) continue;
            Money _price = new Money(_node.priceMoney.ToString());
            if (_price.CompareMoney(CheckPriceManager) && UserData.Instance.TotalMoney.CompareMoney(_price))
            //if (UserData.Instance.TotalMoney.CompareMoney(_price))
            {
                _is_on_new_item = true;
                _is_on_manager = true;
                break;
            }
        }

        for(int i = 0; i<BoosterNodes.Length; i++)
        {
            int _key = BoosterNodes[i].ProductID;
            int _level = UserData.Instance.Boosters[_key];
            if (_level >= BoosterDB.Instance.Boosters[_key].Last().level) continue;

            var _info = BoosterDB.Instance.Boosters[_key][_level];
            Money _price = new Money(_info.priceMoney.ToString());
            if (_price.CompareMoney(CheckPriceBooster) && UserData.Instance.TotalMoney.CompareMoney(_price))
            //if (UserData.Instance.TotalMoney.CompareMoney(_price))
            {
                _is_on_new_item = true;
                _is_on_booster = true;
                break;
            }
        }

        for (int i = 0; i < AssistantNodes.Length; i++)
        {
            int _id = AssistantNodes[i].ProductID;
            if (UserData.Instance.Assistants[_id] == 1) continue;
            Money _price = new Money(AssistantDB.Instance.Assistants[_id].priceMoney.ToString());
            if (_price.CompareMoney(CheckPriceAssistant) && UserData.Instance.TotalMoney.CompareMoney(_price))
            //if (UserData.Instance.TotalMoney.CompareMoney(_price))
            {
                _is_on_new_item = true;
                _is_on_assistant = true;
                break;
            }
        }

        if (!NotificateBox.activeSelf) NotificateIcon.SetActive(_is_on_new_item);
        NotificateManager.SetActive(_is_on_manager);
        NotificateBooster.SetActive(_is_on_booster);
        NotificateAssistant.SetActive(_is_on_assistant);

        //if(!NotificateIcon.activeSelf) NotificateIcon.SetActive(_is_on_new_item);
        //if(!NotificateManager.activeSelf) NotificateManager.SetActive(_is_on_manager);
        //if (!NotificateBooster.activeSelf) NotificateBooster.SetActive(_is_on_booster);
        //if (!NotificateAssistant.activeSelf) NotificateAssistant.SetActive(_is_on_assistant);
    }
}
