﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using SimpleJSON;

public class MainScene : MonoBehaviour {

    public Transform UIRoot;
    
    [Header("For Main Infos")]
    public TMPro.TextMeshProUGUI Txt_TotalMoney;
    //public TMPro.TextMeshProUGUI Txt_TotalMoney2;
    public Image Img_City;
    public Image Img_City_BG;
    public Text Txt_CityName;
    public Text Txt_Level;
    public Image Img_Exp_Gauge;

    public Text Txt_HeartCount;
    public Text Txt_HeartTimer;
    public Image Img_HeartTimer;
    public Text Txt_SecurityTimer;
    public Text Txt_CashCount;
    public Text Txt_BoostTimer;
    public GameObject OBj_ADBoostActive;

    [Header("For Main Scroll")]
    public RectTransform MainScroll;
    public GameObject MainToRndBtn;
    public GameObject MainToFactoryBtn;
    private Vector2 MainScrollPosFactory = Vector2.zero;
    private Vector2 MainScrollPosRND = new Vector2(-640, 0);
    private int MainScrollNowPos = 0;

    [Header("For My Infos")]
    public Image Img_MyPic;
    public Text Txt_MyName;
    public Sprite Source_Img_NonePic;

    [Header("For Heart")]
    public GameObject BaseHeart;
    public GameObject BaseHeartRoot;
    public GameObject BaseHeartMore;
    public Text HeartPopup_Txt_Timer;
    public Text HeartPopup_Txt_Price;
    public Text HeartMore_Txt_Label;
    public Button HeartMore_Btn;

    [Header("For Security")]
    public GameObject BaseSecurity;
    public TMPro.TextMeshProUGUI SecurityPopup_Txt_Money;
    public Text SecurityPopup_Txt_Gem;

    [Header("For LevelUp")]
    public GameObject BaseLevelUp;
    public Text LevelUp_Txt_Level;
    public GameObject BaseUnlockCity;
    public RectTransform CanvasRoot;
    public RectTransform BaseUnlockCityMap;
    public RectTransform UnlockCityMapImg;
    private UICityButtonNodes[] UnlockCityButtons;
    private int UnlockCityNowPage = 0;
    private Vector3 UnlockCityScrollTouchRoot = Vector3.zero;
    private float UnlockCityScrollVal = 0;
    private Vector2[] UnlockCityAreaPositions = new Vector2[3];
    private Vector2 UnlockCityScrollPageDepth;

    [Header("For Unlock City Popup")]
    public GameObject UnlockPopupBase;
    public Image UnlockPopupCityImg;
    public Text UnlockPopupName;
    public Text UnlockPopupMessage;
    public Text UnlockPopupInfoHigh;
    public Text UnlockPopupInfoLow;
    public Button UnlockPopupBtnOK;
    public Button UnlockPopupBtnCancel;

    [Header("For Return")]
    public GameObject ReturningPopup;
    public GameObject ReturningBaseRoot;
    public GameObject ReturningBaseSuccess;
    public Text ReturningMessageWelcome;
    public Text ReturningMessageRoot1;
    public Text ReturningMessageRoot2;
    public Text ReturningMessageRootPrice;
    public Text ReturningMessageSuccess1;
    public Text ReturningMessageSuccess2;
    public Text ReturningMessageSuccessPrice;
    public Button ReturningBtnWatchAD;
    public Button ReturningBtnClose;
    public Button ReturningBtnOK;
    public static bool isRetuningOnLoad = true;

    [Header("For AD Boost")]
    public GameObject ADBoostPopup;
    public GameObject ADBoostBaseRoot;
    public GameObject ADBoostBaseMore;
    public GameObject ADBoostBaseEnd;
    public Text ADBoostLabelEndTimer;

    [Header("Player info Popup")]
    public GameObject PlayerInfoPopup;
    public Image PlayerInfoImgPic;
    public InputField PlayerInfoInputName;
    public GameObject PlayerInfoBtnSetPic;
    public GameObject PlayerInfoBtnSetName;
    public Button PlayerInfoBtnFacebook;
    private Sprite CurrentPlayerPic;
    private string PlayerInfoPicPath;
    [SerializeField]
    private Kakera.Unimgpicker ImgPicker;

    public Dictionary<int, Sprite> city_images = new Dictionary<int, Sprite>();
    public Dictionary<int, Sprite> city_bgs = new Dictionary<int, Sprite>();

    [Header("For Font")]
    public Font FontDefault1;
    public Font FontDefault2;
    public Font FontKR1;
    public Font FontKR2;
    public Font FontRU1;
    public Font FontRU2;

    private UIScrollNodeProduct[] TouchEffects;
    private int touch_idx = 0;

    public static MainScene Instance = null;

    void Awake()
    {
        Instance = this;

        //------------------

        var _security_btns = BaseSecurity.GetComponentsInChildren<UICityButtonNodes>();
        for(int i= 0; i<_security_btns.Length; i++)
        {
            var _node = ShopDB.Instance.Security[_security_btns[i].CityID];
            var _label = _security_btns[i].transform.Find("Label").GetComponent<Text>();

            if (_security_btns[i].name.Contains("Money"))
            {
                _label.text = "$" + new Money(_node.priceMoney.ToString()).ToWon();
            }
            else if (_security_btns[i].name.Contains("Gem"))
            {
                _label.text = _node.priceGem.ToString();
            }
        }

        //------------------
        UnlockCityButtons = BaseUnlockCityMap.GetComponentsInChildren<UICityButtonNodes>(true);
        for(int i = 0; i<UnlockCityButtons.Length; i++)
        {
            int _idx = i;
            UnlockCityButtons[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                OpenUnlockCityPopup(UnlockCityButtons[_idx].CityID);
            }
            );
        }
        
        UnlockPopupBtnOK.onClick.AddListener(delegate 
        {
            //_request_unlock_city = _unlock_popup_city;

            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestUnlockCity(
                _unlock_popup_city,
                delegate (JSONNode N)
                {
                    PopupManager.Instance.SetOffPopup();

                    ExitUnlockCityPopup();
                    BaseUnlockCity.SetActive(false);
                    QuestList.Instance.OnTravelUnlock();
                    MarketManager.Instance.OpenTravel();

                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_CITY_UNLOCK);
                },
                delegate (int _code, string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                },
                delegate (string _message)
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );
        });
        UnlockPopupBtnCancel.onClick.AddListener(delegate { ExitUnlockCityPopup(); });
        
        TouchEffects = GetComponentsInChildren<UIScrollNodeProduct>();

        OnCityChange(NetworkManager.Instance.mUserData.currentCityCode);

        SoundManager.Instance.PlayBGM(SoundManager.BGM_GAME);

        UnlockCityAreaPositions[0] = new Vector2(1130, -155); // america
        UnlockCityAreaPositions[1] = new Vector2(-120, -350); // europe
        UnlockCityAreaPositions[2] = new Vector2(-1195, 5); // asia

        var _texts = UIRoot.GetComponentsInChildren<UIText>(true);
        for (int i = 0; i < _texts.Length; i++) _texts[i].SetText();

        //------------------
        ImgPicker.Completed += delegate (string _path)
        {
            StartCoroutine(LoadProfilPic(_path));
        };

        PlayerInfoInputName.onEndEdit.AddListener(delegate (string _txt)
        {
            if (_txt.Length <= 0) PlayerInfoInputName.text = NetworkManager.Instance.mUserData.nickname;
            if(string.Compare(PlayerInfoInputName.text, NetworkManager.Instance.mUserData.nickname) != 0)
            {
                SetPlayerNickname();
            }
        });
    }

    public Sprite GetCityImgs(int _id)
    {
        if (!city_images.ContainsKey(_id)) city_images.Add(_id, Resources.Load<Sprite>("UI/Citys/city_" + _id));
        return city_images[_id];
    }

    public Sprite GetCityBg(int _id)
    {
        if (!city_bgs.ContainsKey(_id)) city_bgs.Add(_id, Resources.Load<Sprite>("UI/BGs/bg_" + _id));
        return city_bgs[_id];
    }

    private bool isNumberText(string _txt)
    {
        _txt = _txt.Replace(" ", "");
        _txt = _txt.Replace(":", "");
        _txt = _txt.Replace(",", "");
        _txt = _txt.Replace(".", "");
        _txt = _txt.Replace("/", "");
        _txt = _txt.Replace("-", "");
        _txt = _txt.Replace("$", "");
        _txt = _txt.Replace("M", "");
        _txt = _txt.Replace("B", "");
        _txt = _txt.Replace("T", "");

        try
        {
            long var = long.Parse(_txt);
            return true;
        }
        catch
        {
            return false;
        }
    }

	// Use this for initialization
	void Start () {
        
        //------------------
        if (Language.Code == Language.LanguageCode.KR
            || Language.Code == Language.LanguageCode.RU)
        {
            var _kr_txts = UIRoot.GetComponentsInChildren<Text>(true);
            for (int i = 0; i < _kr_txts.Length; i++)
            {
                if (isNumberText(_kr_txts[i].text)) continue;

                if (Language.Code == Language.LanguageCode.KR)
                {
                    if (_kr_txts[i].font == FontDefault1) _kr_txts[i].font = FontKR1;
                    else if (_kr_txts[i].font == FontDefault2) _kr_txts[i].font = FontKR2;
                }
                else if (Language.Code == Language.LanguageCode.RU)
                {
                    if (_kr_txts[i].font == FontDefault1) _kr_txts[i].font = FontRU1;
                    else if (_kr_txts[i].font == FontDefault2) _kr_txts[i].font = FontRU2;
                }
                _kr_txts[i].resizeTextForBestFit = true;
                _kr_txts[i].resizeTextMaxSize = _kr_txts[i].fontSize;
            }
        }

        //-------------------------
        UnlockCityScrollPageDepth = new Vector2(-BaseUnlockCityMap.sizeDelta.x * BaseUnlockCityMap.localScale.x, 0);

        IGAWorksManager.SetLevel(NetworkManager.Instance.mUserData.level);
        IGAWorksManager.SetAssistant(NetworkManager.Instance.mUserData.selectedAssistantID);

        SetExpGauge();
        QuestList.Instance.SetQuestFull();
        StartCoroutine(StartGameProcess());

        PlayerInfoPicPath = string.Format("{0}/PlayerPic{1}.png", Application.persistentDataPath, NetworkManager.Instance.mUserData.memberUID);
        CurrentPlayerPic = Source_Img_NonePic;
        StartCoroutine(LoadProfilPic(""));
        Invoke("SetMYInfos", 0.1f);
    }
	
	// Update is called once per frame
	void Update () {

        //-------------------------

        if(MarketManager.Instance.MainBase.activeSelf)
        {
            MainScreenScroll();

            MainScroll.anchoredPosition = Vector2.Lerp(
                MainScroll.anchoredPosition,
                MainScrollNowPos == 0 ? MainScrollPosFactory : MainScrollPosRND,
                0.1f);
        }

        //-------------------------
        //total money
        //Txt_TotalMoney.text = "$" + SetTotalMoneyLabel();
        Txt_TotalMoney.text = SetTotalMoneyLabel();

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.KeypadPlus)) OnGetMoney(Money.MulNumString(UserData.Instance.TotalMoney, 9));
        if (Input.GetKeyDown(KeyCode.KeypadMinus) && Money.CompareStr(UserData.Instance.TotalMoney, "10")) { UserData.Instance.TotalMoney.MulMoney(0.1f); OnGetMoney(new Money("0")); }
        if (Input.GetKey(KeyCode.KeypadEnter))
        {
            //GetEXP(1000000);
            QuestList.Instance.SetQuestClear();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) SoundManager.Instance.PlayBGM();
        if (Input.GetKeyDown(KeyCode.Alpha2))  SoundManager.Instance.PlayBGM(SoundManager.BGM_GAME);

        if (Input.GetMouseButtonDown(0))
        {
            TouchEffects[touch_idx].gameObject.SetActive(false);
            Vector3 _pos = Camera.main.ScreenToWorldPoint(Input.mousePosition); _pos.z = 0;
            TouchEffects[touch_idx].transform.position = _pos;
            TouchEffects[touch_idx].gameObject.SetActive(true);
            if (++touch_idx >= TouchEffects.Length) touch_idx = 0;
        }
#endif

        if (Input.touchCount > 0)
        {
            for(int i = 0; i<Input.touchCount; i++)
            {
                if(Input.touches[i].phase == TouchPhase.Began)
                {
                    TouchEffects[touch_idx].gameObject.SetActive(false);
                    Vector3 _pos = Camera.main.ScreenToWorldPoint(Input.touches[i].position); _pos.z = 0;
                    TouchEffects[touch_idx].transform.position = _pos;
                    TouchEffects[touch_idx].gameObject.SetActive(true);
                    if (++touch_idx >= TouchEffects.Length) touch_idx = 0;
                }
            }
        }

        //-------------------------
        //heart
        if (NetworkManager.Instance.mUserData.heart < 10)
        {
            float _heart_time =
                (float)
                (NetworkManager.Instance.mUserData.heartLastChargedTime.AddMilliseconds(ConfigDB.Instance.Configs[ConfigDB.CONFIG_HEART_RECHARGE_TIME].configInt)
                - NetworkManager.Instance.GetServerNow()
                ).TotalSeconds;

            if (_heart_time < 0)
            {
                _heart_time = 0;
                NetworkManager.Instance.mUserData.heart += 1;
                NetworkManager.Instance.mUserData.heartLastChargedTime = NetworkManager.Instance.GetServerNow();
            }

            Txt_HeartTimer.text =
                string.Format(
                    "{0:00}:{1:00}",
                    Math.Floor((_heart_time % 3600) / 60),
                    Math.Floor(_heart_time % 60));

            Img_HeartTimer.color = Color.white;

            if (BaseHeart.activeSelf) HeartPopup_Txt_Timer.text = Txt_HeartTimer.text;
        }
        else
        {
            Txt_HeartTimer.text = Language.Str.MAX;
            Img_HeartTimer.color = Color.gray;
            BaseHeart.SetActive(false);
        }

        Txt_HeartCount.text = NetworkManager.Instance.mUserData.heartLabel;

        //-------------------------
        //security
        float _security_time =
        (float)
        (NetworkManager.Instance.mUserData.securityFinishTime
            - NetworkManager.Instance.GetServerNow()
            ).TotalSeconds;
        if (_security_time > 0)
        {
            if (_security_time > 86400)
            {
                Txt_SecurityTimer.text =
                    string.Format(
                        "{0}d {1:00}h",
                        Math.Floor(_security_time / 86400),
                        Math.Floor((_security_time % 86400) / 3600));
            }
            else if (_security_time > 3600)
            {
                Txt_SecurityTimer.text =
                    string.Format(
                        "{0:00}h {1:00}m",
                        Mathf.Floor(_security_time / 3600),
                        Mathf.Floor((_security_time % 3600) / 60));
            }
            else
            {
                Txt_SecurityTimer.text =
                    string.Format(
                        "{0:00}:{1:00}",
                        Mathf.Floor(_security_time / 60),
                        Mathf.Floor(_security_time % 60));
            }
        }
        else Txt_SecurityTimer.text = Language.Str.NONE;
        //if(NetworkManager.Instance.mUserData.level < 10)
        //{
        //    Txt_SecurityTimer.text = "SAFETY";
        //}

        if (BaseSecurity.activeSelf)
        {
            SecurityPopup_Txt_Money.text = Txt_TotalMoney.text;
            SecurityPopup_Txt_Gem.text = Txt_CashCount.text;
        }

        Txt_CashCount.text = NetworkManager.Instance.mUserData.cashLabel;
        Txt_Level.text = string.Format(Language.Str.LEVEL_FORMAT, NetworkManager.Instance.mUserData.level.ToString());
        
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(TutorialManager.Instance.Step == -1
                && !BoxRewardManager.Instance.gameObject.activeSelf
                && !BaseLevelUp.activeSelf
                && !ReturningPopup.activeSelf)
            {
                if (PopupManager.Instance.isOnPopup()) PopupManager.Instance.OnEscape();
                else if (BaseUnlockCity.activeSelf) { if (UnlockPopupBase.activeSelf) ExitUnlockCityPopup(); }
                else if (RNDCenterManager.Instance.UpgradeBase.activeSelf) RNDCenterManager.Instance.ExitRNDCenter();
                else if (RNDCenterManager.Instance.UpgradeResultPopup.activeSelf) RNDCenterManager.Instance.ExitUpgradeResultPopup();
                else if (ShopManager.Instance.BaseRoot.activeSelf) ShopManager.Instance.ExitShop();
                else if (MenuManager.Instance.BaseRoot.activeSelf) MenuManager.Instance.ExitMenu();
                else if (BaseHeart.activeSelf) ExitHeartPopup();
                else if (BaseSecurity.activeSelf) ExitSecurity();
                else if (ADBoostPopup.activeSelf) ExitADBoost();
                else if (PlayerInfoPopup.activeSelf) ExitPlayerInfo();
                else if (WareHouseManager.Instance.WareHouseBase.activeSelf
                    || WareHouseManager.Instance.InventoryBase.activeSelf)
                    WareHouseManager.Instance.OnEscape();
                else if (MarketManager.Instance.MarketBase.activeSelf
                    || MarketManager.Instance.TravelBase.activeSelf
                    || MarketManager.Instance.BurglaryPopup.activeSelf)
                    MarketManager.Instance.OnEscape();
                else OnExitGame();
            }
        }

        //-------------------------
        //AD Boost
        float _boost_time =
        (float)
        (NetworkManager.Instance.mUserData.reserveTime5
            - NetworkManager.Instance.GetServerNow()
            ).TotalSeconds;
        if (_boost_time > 0)
        {
            Txt_BoostTimer.text =
                string.Format(
                    "{0:00}:{1:00}:{2:00}",
                    Math.Floor(_boost_time / 3600),
                    Math.Floor((_boost_time % 3600) / 60),
                    Math.Floor(_boost_time % 60));
            OBj_ADBoostActive.SetActive(true);
        }
        else
        {
            Txt_BoostTimer.text = "00:00:00";
            OBj_ADBoostActive.SetActive(false);
        }

        if (ADBoostBaseEnd.activeSelf) ADBoostLabelEndTimer.text = Txt_BoostTimer.text;

        //-------------------------
        //unlock city
        if (BaseUnlockCity.activeSelf
            && !UnlockPopupBase.activeSelf)
        {
            if (Input.GetMouseButtonDown(0)) UnlockCityScrollTouchRoot = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            if (UnlockCityScrollTouchRoot != Vector3.zero)
            {
                if (Input.GetMouseButton(0))
                {
                    UnlockCityScrollVal = (Camera.main.ScreenToViewportPoint(Input.mousePosition) - UnlockCityScrollTouchRoot).x * CanvasRoot.sizeDelta.x;
                    if (Mathf.Abs(UnlockCityScrollVal) >= 160)
                    {
                        if (UnlockCityScrollVal < 0) SetNextUnlockCityPage();
                        else SetPrevUnlockCityPage();
                        UnlockCityScrollTouchRoot = Vector3.zero;
                        UnlockCityScrollVal = 0;
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    UnlockCityScrollTouchRoot = Vector3.zero;
                    UnlockCityScrollVal = 0;
                }
            }
            
            if(UnlockCityNowPage < 0)
            {
                int _pos_idx = 3 + (3 * ((UnlockCityNowPage + 1) / -3)) + UnlockCityNowPage;
                BaseUnlockCityMap.anchoredPosition =
                    Vector2.Lerp(BaseUnlockCityMap.anchoredPosition,
                    UnlockCityAreaPositions[_pos_idx]
                    + (Vector2.right * UnlockCityScrollVal)
                    + (UnlockCityScrollPageDepth * ((UnlockCityNowPage - 2) / 3)),
                    0.1f);
            }
            else
            {
                BaseUnlockCityMap.anchoredPosition =
                    Vector2.Lerp(BaseUnlockCityMap.anchoredPosition,
                    UnlockCityAreaPositions[UnlockCityNowPage % 3]
                    + (Vector2.right * UnlockCityScrollVal)
                    + (UnlockCityScrollPageDepth * (UnlockCityNowPage / 3)),
                    0.1f);
            }
        }
    }

    private string SetTotalMoneyLabel(string _unit = "")
    {
        Money _money = UserData.Instance.TotalMoney;

        int calc_length = _money.length;
        string unit = GetUnit(ref calc_length, _unit);

        string result = _money.number;
        if (_money.length > 9) result = result.Substring(0, calc_length);

        return "<mspace=25>$" + Money.NumToMoney(result) + "</mspace> " + unit;
    }

    private string GetUnit(ref int calc_length, string _unit)
    {
        string unit = _unit;
        
        if (calc_length > 9)
        {
            for (int i = 30; i > 1; i--)
            {
                if (calc_length > 3 + (i * 3))
                {
                    calc_length -= i * 3; unit = unit.Insert(0, Language.Str.MONEY_BIG_UNIT[i - 1]);
                    break;
                }
            }
        }
        
        return unit;
    }

    public void SetMYInfos()
    {
        if(FBManager.Instance.isLogin())
        {
            Txt_MyName.text = FBManager.Instance.MyName;
            Img_MyPic.sprite = Source_Img_NonePic;
            PlayerInfoImgPic.sprite = Source_Img_NonePic;
            FBManager.Instance.SetPicture(FBManager.Instance.MyID, Img_MyPic);
            FBManager.Instance.SetPicture(FBManager.Instance.MyID, PlayerInfoImgPic);
        }
        else
        {
            Txt_MyName.text = NetworkManager.Instance.mUserData.nickname;
            Img_MyPic.sprite = CurrentPlayerPic;
        }
    }

    private Vector2 scroll_main_pos_root = Vector2.zero;
    private Vector2 scroll_main_pos = Vector2.zero;
    private float scroll_main_timer = 0;
    public void MainScreenScrollStart()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(MainScroll.parent.GetComponent<RectTransform>(), Input.mousePosition, Camera.main, out scroll_main_pos_root);
        scroll_main_timer = 0.2f;
    }

    private void MainScreenScroll()
    {
        if (scroll_main_pos_root != Vector2.zero
            && scroll_main_timer > 0
            && Input.GetMouseButton(0))
        {
            if (scroll_main_timer > 0)
            {
                scroll_main_timer -= Time.deltaTime;
                if (scroll_main_timer < 0) scroll_main_timer = 0;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(MainScroll.parent.GetComponent<RectTransform>(), Input.mousePosition, Camera.main, out scroll_main_pos);
            if (MainScrollNowPos == 0)
            {
                if (scroll_main_pos_root.x - scroll_main_pos.x > 200)
                {
                    scroll_main_pos_root = Vector2.zero;
                    ToRND();
                    return;
                }
            }
            else if (MainScrollNowPos == 1)
            {
                if (scroll_main_pos.x - scroll_main_pos_root.x > 200)
                {
                    scroll_main_pos_root = Vector2.zero;
                    ToFactory();
                    return;
                }
            }
        }
        else if (Input.GetMouseButtonUp(0)) scroll_main_pos_root = Vector2.zero;
    }
    
    public void OnExitGame()
    {
        PopupManager.Instance.SetPopup(
            Language.Str.POPUP_MESSAGE_EXITGAME,
            delegate
            {
                Application.Quit();
            });
    }

    //-------------------------
    public void ToFactory()
    {
        if (MainScroll.transform.parent.gameObject.activeSelf)
        {
            MainScrollNowPos = 0;
            MainToRndBtn.SetActive(true);
            MainToFactoryBtn.SetActive(false);
            //SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON);
        }
    }
    public void ToRND()
    {
        if (MainScroll.transform.parent.gameObject.activeSelf)
        {
            MainScrollNowPos = 1;
            MainToRndBtn.SetActive(false);
            MainToFactoryBtn.SetActive(true);
            //SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON);
        }
    }

    //-------------------------

    public void OnGetMoney(Money _money)
    {
        UserData.Instance.TotalMoney.AddMoney(_money);

        ShopManager.Instance.OnNewItemCheck();
        //FactoryManager.Instance.OnGetMoney();
    }

    public void OnUseMoney(Money _money)
    {
        if(UserData.Instance.TotalMoney.CompareMoney(_money))
        {
            UserData.Instance.TotalMoney.SubMoney(_money);
            ShopManager.Instance.OnNewItemCheck();
        }
    }

    //-------------------------

    public void OpenHeartPopup()
    {
        if (NetworkManager.Instance.mUserData.heart >= 10) return;

        BaseHeart.SetActive(true);
        BaseHeartRoot.SetActive(true);
        BaseHeartMore.SetActive(false);
        HeartPopup_Txt_Price.text = ShopDB.Instance.Heart[2].priceGem.ToString();
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    public void ExitHeartPopup()
    {
        BaseHeart.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void OpenHeartMore()
    {
        BaseHeart.SetActive(true);
        BaseHeartRoot.SetActive(false);
        BaseHeartMore.SetActive(true);

        HeartMore_Txt_Label.text = Language.Str.HEART_MESSAGE_MORE_DEFAULT;

        HeartMore_Btn.onClick.RemoveAllListeners();
        HeartMore_Btn.onClick.AddListener(delegate
        {
            GetHeartAD();
        });
    }

    public void OpenHeartMoreGreatOffer()
    {
        BaseHeart.SetActive(true);
        BaseHeartRoot.SetActive(false);
        BaseHeartMore.SetActive(true);

        HeartMore_Txt_Label.text = Language.Str.HEART_MESSAGE_MORE_GRATEOFFER;

        HeartMore_Btn.onClick.RemoveAllListeners();
        HeartMore_Btn.onClick.AddListener(delegate
        {
            MarketManager.Instance.GetGreatOfferAD();
        });
    }

    public void OnBuyHeartGem()
    {
        if (NetworkManager.Instance.mUserData.heart >= 10) return;

        PopupManager.Instance.SetPopupWait();

        if(NetworkManager.Instance.mUserData.cash >= ShopDB.Instance.Heart[2].priceGem)
        {
            NetworkManager.Instance.RequestBuyShopItem(
            ShopDB.Instance.Heart[2].shopID,
            ShopDB.PRICE_TYPE_GEM,
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_CONFIRMED);
                BaseHeart.SetActive(false);

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                MarketManager.Instance.ClearOffer();

                IGAWorksManager.onRetention("UseCashBuyHeart");
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            }
            );
        }
        else SetPopupToShop(ShopDB.PRICE_TYPE_GEM);
    }
    
    public void GetHeartAD()
    {
        mAdsManager.Instance.ShowAD(delegate
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestBuyShopItem(
            ShopDB.Instance.Heart[0].shopID,
            ShopDB.PRICE_TYPE_AD,
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();
                if (NetworkManager.Instance.mUserData.heart < 10) OpenHeartMore();
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                IGAWorksManager.onRetention("ADHeart1");
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            }
            );
        });
    }

    public void InviteFriend()
    {
        FBManager.Instance.InviteFriend(delegate
        {
            PopupManager.Instance.SetPopup("invite friend test");
        });
    }

    //-------------------------

    public void OpenSecurity()
    {
        BaseSecurity.SetActive(true);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    public void ExitSecurity()
    {
        BaseSecurity.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void OpenSecurityBuyPopupMoney(int _idx)
    {
        int __idx = _idx;
        OnBuySecurityAction = delegate
        {
            OnBuySecurityMoney(__idx);
        };

        string _name = "";
        switch(_idx)
        {
            case 0: _name = Language.Str.SECURITY_1HOUR; break;
            case 1: _name = Language.Str.SECURITY_3HOURS; break;
            case 2: _name = Language.Str.SECURITY_1DAY; break;
            default: break;
        }

        Money _price = new Money(ShopDB.Instance.Security[_idx].priceMoney.ToString());
        if(!CheckPrice(_price))
        {
            PopupManager.Instance.SetPopup(
                string.Format(Language.Str.POPUP_SECURITY_FORMAT_BUY_MONEY, _name, _price.ToWon()),
                OnBuySecurityAction);
        }
    }

    public void OpenSecurityBuyPopupGem(int _idx)
    {
        int __idx = _idx;
        OnBuySecurityAction = delegate
        {
            OnBuySecurityGem(__idx);
        };

        string _name = "";
        switch (_idx)
        {
            case 0: _name = Language.Str.SECURITY_1HOUR; break;
            case 1: _name = Language.Str.SECURITY_3HOURS; break;
            case 2: _name = Language.Str.SECURITY_1DAY; break;
            default: break;
        }

        int _price = ShopDB.Instance.Security[_idx].priceGem;
        if(!CheckPrice(_price))
        {
            PopupManager.Instance.SetPopup(
                string.Format(Language.Str.POPUP_SECURITY_FORMAT_BUY_GEM, _name, ShopDB.Instance.Security[_idx].priceGem),
                OnBuySecurityAction);
        }
    }
    
    private Action OnBuySecurityAction;
    public void OnBuySecurity()
    {
        OnBuySecurityAction();
    }

    private void OnBuySecurityMoney(int _idx)
    {
        //if (NetworkManager.Instance.mUserData.level < 10) return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
            ShopDB.Instance.Security[_idx].shopID,
            ShopDB.PRICE_TYPE_MONEY,
            delegate (JSONNode N)
            {
                //PopupManager.Instance.SetPopup("Buy security confirmd.");
                PopupManager.Instance.SetOffPopup();
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
                BaseSecurity.SetActive(false);
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_SECURITY_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_SECURITY_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            }
            );
    }

    private void OnBuySecurityGem(int _idx)
    {
        //if (NetworkManager.Instance.mUserData.level < 10) return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
            ShopDB.Instance.Security[_idx].shopID,
            ShopDB.PRICE_TYPE_GEM,
            delegate (JSONNode N)
            {
                //PopupManager.Instance.SetPopup("Buy security confirmd.");
                PopupManager.Instance.SetOffPopup();
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                IGAWorksManager.onRetention("UseCashBuySecurity" + _idx);
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_SECURITY_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_SECURITY_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            }
            );
    }

    //-------------------------

    public void OpenADBoost()
    {
        if(NetworkManager.Instance.mUserData.ADBoostDelayTime == DateTime.MinValue
            || NetworkManager.Instance.mUserData.ADBoostDelayTime.Day != NetworkManager.Instance.GetServerNow().Day)
        {
            NetworkManager.Instance.mUserData.ADBoostDelayTime = NetworkManager.Instance.GetServerNow();
            NetworkManager.Instance.mUserData.ADBoostCount = 0;
        }

        if (NetworkManager.Instance.mUserData.ADBoostCount >= 6)
        {
            OpenADBoostEnd();
        }
        else
        {
            ADBoostPopup.SetActive(true);
            ADBoostBaseRoot.SetActive(true);
            ADBoostBaseMore.SetActive(false);
            ADBoostBaseEnd.SetActive(false);
            ADBoostBaseRoot.transform.Find("Count").GetComponent<Text>().text = string.Format(Language.Str.BOOSTAD_FORMAT_COUNT, Mathf.Clamp(6 - NetworkManager.Instance.mUserData.ADBoostCount, 0, int.MaxValue));
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_TAP1);
        }
    }
    
    private void OpenADBoostEnd()
    {
        ADBoostPopup.SetActive(false);
        ADBoostPopup.SetActive(true);
        ADBoostBaseRoot.SetActive(false);
        ADBoostBaseMore.SetActive(false);
        ADBoostBaseEnd.SetActive(true);
        //ADBoostBaseEnd.transform.Find("Count").GetComponent<Text>().text = string.Format(Language.Str.BOOSTAD_FORMAT_COUNT, Mathf.Clamp(6 - NetworkManager.Instance.mUserData.ADBoostCount, 0, int.MaxValue));
    }

    public void ExitADBoost()
    {
        ADBoostPopup.SetActive(false);
        ADBoostBaseRoot.SetActive(false);
        ADBoostBaseMore.SetActive(false);
        ADBoostBaseEnd.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void GetADBoost()
    {
        if (NetworkManager.Instance.mUserData.ADBoostCount >= 6) return;

        mAdsManager.Instance.ShowAD(delegate
        {
            //광고시간 적용
            var _max_time = NetworkManager.Instance.GetServerNow().AddHours(24);
            if (DateTime.Compare(NetworkManager.Instance.GetServerNow(), NetworkManager.Instance.mUserData.reserveTime5) > 0)
                NetworkManager.Instance.mUserData.reserveTime5 = NetworkManager.Instance.GetServerNow();
            NetworkManager.Instance.mUserData.reserveTime5 = NetworkManager.Instance.mUserData.reserveTime5.AddHours(4);
            if ((_max_time - NetworkManager.Instance.mUserData.reserveTime5).TotalSeconds < 0)
                NetworkManager.Instance.mUserData.reserveTime5 = _max_time;

            //날짜가 지나면 광고 카운트를 초기화 (로컬기반)
            if (NetworkManager.Instance.mUserData.ADBoostDelayTime == DateTime.MinValue
            || NetworkManager.Instance.mUserData.ADBoostDelayTime.Day != NetworkManager.Instance.GetServerNow().Day)
            {
                NetworkManager.Instance.mUserData.ADBoostDelayTime = NetworkManager.Instance.GetServerNow();
                NetworkManager.Instance.mUserData.ADBoostCount = 0;
            }
            else NetworkManager.Instance.mUserData.ADBoostCount++; // 날짜카운트를 증가
            
            if(NetworkManager.Instance.mUserData.ADBoostCount >= 6)
            {
                OpenADBoostEnd();
            }
            else
            {
                ADBoostPopup.SetActive(false);
                ADBoostPopup.SetActive(true);
                ADBoostBaseRoot.SetActive(false);
                ADBoostBaseMore.SetActive(true);
                ADBoostBaseEnd.SetActive(false);
                ADBoostBaseMore.transform.Find("Count").GetComponent<Text>().text = string.Format(Language.Str.BOOSTAD_FORMAT_COUNT, Mathf.Clamp(6 - NetworkManager.Instance.mUserData.ADBoostCount, 0, int.MaxValue));
            }

            IGAWorksManager.onRetention("ADProductBoost");
        });
    }

    //--------------------------------------------

    public void OpenPlayerInfo()
    {
        PlayerInfoPopup.SetActive(true);

        if(FBManager.Instance.isLogin())
        {
            FBManager.Instance.SetPicture(FBManager.Instance.MyID, PlayerInfoImgPic);
            PlayerInfoInputName.text = FBManager.Instance.MyName;
            PlayerInfoBtnSetName.SetActive(false);
            PlayerInfoBtnSetPic.SetActive(false);
        }
        else
        {
            PlayerInfoImgPic.sprite = CurrentPlayerPic;
            PlayerInfoInputName.text = NetworkManager.Instance.mUserData.nickname;
            PlayerInfoBtnSetName.SetActive(true);
            PlayerInfoBtnSetPic.SetActive(true);
        }

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    public void ExitPlayerInfo()
    {
        PlayerInfoPopup.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    IEnumerator LoadProfilPic(string _path)
    {
        bool _isOnLocal = false;
        if(_path == "")
        {
            if(File.Exists(PlayerInfoPicPath))
            {
                try
                {
                    var _load_texture = new Texture2D(4, 4, TextureFormat.ARGB32, false);
                    byte[] bytes = File.ReadAllBytes(PlayerInfoPicPath);
                    if (_load_texture.LoadImage(bytes))
                    {
                        Rect _sprite_rect;
                        if (_load_texture.width > _load_texture.height) _sprite_rect = new Rect((_load_texture.width / 2) - (_load_texture.height / 2), 0, _load_texture.height, _load_texture.height);
                        else _sprite_rect = new Rect(0, (_load_texture.height / 2) - (_load_texture.width / 2), _load_texture.width, _load_texture.width);
                        CurrentPlayerPic = Sprite.Create(
                            _load_texture,
                            _sprite_rect,
                            Vector2.one / 2.0f);

                        _isOnLocal = true;
                    }
                }
                catch (Exception e) { Debug.LogError(e); }
            }
        }

        if (!_isOnLocal)
        {
            PlayerInfoBtnFacebook.interactable = false;

            var url = "file://" + _path;
            var www = new WWW(url);
            yield return www;

            if (www.error == null)
            {
                var _texture = www.texture;
                if (_texture == null)
                {
                    CurrentPlayerPic = Source_Img_NonePic;
                }
                else
                {
                    Rect _sprite_rect;
                    if (_texture.width > _texture.height) _sprite_rect = new Rect((_texture.width / 2) - (_texture.height / 2), 0, _texture.height, _texture.height);
                    else _sprite_rect = new Rect(0, (_texture.height / 2) - (_texture.width / 2), _texture.width, _texture.width);
                    CurrentPlayerPic = Sprite.Create(
                        _texture,
                        _sprite_rect,
                        Vector2.one / 2.0f);

                    try
                    {
                        byte[] bytes = _texture.EncodeToPNG();
                        File.WriteAllBytes(PlayerInfoPicPath, bytes);
                    }
                    catch (Exception e) { Debug.LogError(e); }
                }
            }
            else CurrentPlayerPic = Source_Img_NonePic;

            PlayerInfoBtnFacebook.interactable = true;
        }

        Img_MyPic.sprite = CurrentPlayerPic;
        PlayerInfoImgPic.sprite = CurrentPlayerPic;
    }

    public void SetPlayerNickname()
    {
        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.SetNickname(
            PlayerInfoInputName.text,
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();
                PlayerInfoInputName.text = NetworkManager.Instance.mUserData.nickname;
                Txt_MyName.text = NetworkManager.Instance.mUserData.nickname;
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(_message);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
            }
            );
    }

    public void SetPlayerInfoPic()
    {
        ImgPicker.Show("Select Image", "unimgpicker", 1024);
    }

    //-------------------------

    public void OnCityChange(int _id)
    {
        if(CityDB.Instance.CityDatas.ContainsKey(_id))
        {
            Txt_CityName.text = CityDB.Instance.CityDatas[_id].FullName.ToUpper();
            Img_City.gameObject.SetActive(true);
            Img_City.sprite = GetCityImgs(_id);
            Img_City_BG.sprite = GetCityBg(_id);
        }
        else
        {
            Txt_CityName.text = "";
            Img_City.gameObject.SetActive(false);
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if(TutorialManager.Instance.Step == -1) UserData.Instance.SaveData();
    }

    void OnApplicationQuit()
    {
        if (TutorialManager.Instance.Step == -1) UserData.Instance.SaveData();
    }

    //-------------------------

    private void SetExpGauge()
    {
        Img_Exp_Gauge.fillAmount = //((NetworkManager.Instance.mUserData.exp * 100) / LevelDB.Instance.Levels[NetworkManager.Instance.mUserData.level].maxExp) / 100.0f;
            (float)NetworkManager.Instance.mUserData.exp  / LevelDB.Instance.Levels[NetworkManager.Instance.mUserData.level].maxExp;
    }

    private Money MaxEXPMoney = new Money(long.MaxValue.ToString());
    public void GetEXP(Money _money)
    {
        if (_money.CompareMoney(MaxEXPMoney)) GetEXP(long.MaxValue);
        else GetEXP(_money.GetInt64());
    }
    
    public void GetEXP(long _val)
    {
        if (NetworkManager.Instance.mUserData.level >= LevelDB.Instance.LastLevel) return;
        if (!TutorialManager.Instance.isOnLevelup()) return;

        NetworkManager.Instance.mUserData.exp += _val;
        SetExpGauge();

        if (_levelup_process) return;

        if (NetworkManager.Instance.mUserData.exp >= LevelDB.Instance.Levels[NetworkManager.Instance.mUserData.level].maxExp)
        {
            StartCoroutine(LevelupProcess());
        }
    }

    public bool OpenUnlockCity()
    {
        bool _isUnlockable = false; // 언락할 수 있으면 = 남은 도시가 있으면

        for (int i = 0; i < UnlockCityButtons.Length; i++)
        {
            int _id = UnlockCityButtons[i].CityID;
            bool _isActive = UserData.Instance.CityInfos.ContainsKey(_id)
            && UserData.Instance.CityInfos[_id] == UserData.AVAILABLE;
            UnlockCityButtons[i].gameObject.SetActive(!_isActive);
            UnlockCityButtons[i].GetComponent<Button>().transform.GetChild(0).gameObject.SetActive(false);
            if (_isActive == false) _isUnlockable = true; // 현재 활성화되지 않은 도시가 있으면 = 언락할 수 있으면
        }
        
        _request_unlock_city = -1;

        if (!_isUnlockable) return false; // 언락할 수 없으면 종료

        switch (CityDB.Instance.CityDatas[NetworkManager.Instance.mUserData.currentCityCode].regionCode)
        {
            case 1: UnlockCityNowPage = 0; break; // 아메리카
            case 2: UnlockCityNowPage = 2; break; // 아시아
            case 3: UnlockCityNowPage = 1; break; // 유럽
        }
        BaseUnlockCityMap.anchoredPosition = UnlockCityAreaPositions[UnlockCityNowPage];
        SetUnlockCityMapPage();

        UnlockPopupBase.SetActive(false);
        BaseUnlockCity.SetActive(true);

        return true;
    }

    public void SetNextUnlockCityPage()
    {
        UnlockCityNowPage++;
        SetUnlockCityMapPage();
    }

    public void SetPrevUnlockCityPage()
    {
        UnlockCityNowPage--;
        SetUnlockCityMapPage();
    }

    private void SetUnlockCityMapPage()
    {
        int _val = UnlockCityNowPage;
        if (_val < 0) _val = _val - 2;
        UnlockCityMapImg.anchoredPosition = Vector2.right * (BaseUnlockCityMap.sizeDelta.x * (_val / 3));
    }

    private int _unlock_popup_city = -1;
    private void OpenUnlockCityPopup(int _city)
    {
        _unlock_popup_city = _city;

        UnlockPopupCityImg.sprite = GetCityImgs(_city);
        UnlockPopupName.text = CityDB.Instance.CityDatas[_city].FullName.ToUpper();
        UnlockPopupMessage.text =Language.Str.UNLOCKCITY_TITLE; //string.Format("{0}\nunlock this city?", CityDB.Instance.CityDatas[_city].cityName);
        UnlockPopupInfoLow.text = string.Format("{0} {1}", ItemDB.Instance.GetGroupName(CityDB.Instance.CityPriceInfoLow[_city]), Language.Str.PRICES);
        UnlockPopupInfoHigh.text = string.Format("{0} {1}", ItemDB.Instance.GetGroupName(CityDB.Instance.CityPriceInfoHigh[_city]), Language.Str.PRICES);

        UnlockPopupBase.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    public void ExitUnlockCityPopup()
    {
        UnlockPopupBase.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    private bool _levelup_process = false;
    private int _request_unlock_city = -1;
    IEnumerator LevelupProcess()
    {
        while (ReturningPopup.activeSelf
            || PopupManager.Instance.isOnPopup()) yield return null;

        _levelup_process = true;

        while (NetworkManager.Instance.mUserData.exp >= LevelDB.Instance.Levels[NetworkManager.Instance.mUserData.level].maxExp)
        {
            NetworkManager.Instance.mUserData.exp -= LevelDB.Instance.Levels[NetworkManager.Instance.mUserData.level].maxExp;
            NetworkManager.Instance.mUserData.level += 1;
            SetExpGauge();

            BaseLevelUp.SetActive(false);
            LevelUp_Txt_Level.text = NetworkManager.Instance.mUserData.level.ToString();
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_LEVELUP);
            BaseLevelUp.SetActive(true);

            yield return new WaitForSeconds(2.0f);

            /*
            if (LevelDB.Instance.Levels[NetworkManager.Instance.mUserData.level].unlockCity == 1)
            {
                if(OpenUnlockCity())
                {
                    BaseLevelUp.SetActive(false);

                    while (_request_unlock_city == -1) yield return null;
                }
            }
            */
            
            PopupManager.Instance.SetPopupWait();

            bool _isfail = false;
            NetworkManager.Instance.RequestLevelUp(
                NetworkManager.Instance.mUserData.level,
                //_request_unlock_city,
                -1,
                delegate (JSONNode N)
                {
                    PopupManager.Instance.SetOffPopup();

                    BaseLevelUp.SetActive(false);
                    BaseUnlockCity.SetActive(false);
                    UnlockPopupBase.SetActive(false);
                    BoxRewardManager.Instance.SetReward(N);
                    QuestList.Instance.OnLevelUP();

                    ShopManager.Instance.OnNewItemCheck();
                    IGAWorksManager.SetLevel(NetworkManager.Instance.mUserData.level);
                },
                delegate (int _code, string _message)
                {
                    BaseLevelUp.SetActive(false);
                    BaseUnlockCity.SetActive(false);
                    UnlockPopupBase.SetActive(false);
                    _isfail = true;
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
                },
                delegate (string _message)
                {
                    BaseLevelUp.SetActive(false);
                    BaseUnlockCity.SetActive(false);
                    UnlockPopupBase.SetActive(false);
                    _isfail = true;
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
                }
                );

            if (_isfail) break;

            while (PopupManager.Instance.isOnPopup() ||
                BoxRewardManager.Instance.gameObject.activeSelf) yield return null;

            BaseLevelUp.SetActive(false);
        }

        _levelup_process = false;
    }

    IEnumerator ReturningProcess(Money _before_money, Money _after_money)
    {
        while (_levelup_process) yield return null;

        //if (DateTime.Compare(NetworkManager.Instance.GetServerNow(), NetworkManager.Instance.mUserData.reserveTime4) > 0)
        {
            /*
            if (NetworkManager.Instance.mUserData.level >= 10)
            {
            }
            else
            {
                ReturningPopup.SetActive(true);
                ReturningMessage.text = "Would you like to try your luck with a Silver Chest?";
                ReturningMessage.transform.GetChild(0).gameObject.SetActive(false);

                ReturningBtnWatchAD.onClick.RemoveAllListeners();
                ReturningBtnWatchAD.onClick.AddListener(delegate
                {
                    mAdsManager.Instance.ShowAD(delegate
                    {
                        PopupManager.Instance.SetPopupWait();
                        NetworkManager.Instance.RequestRevisitOpenBox(
                                    delegate (SimpleJSON.JSONNode N)
                                    {
                                        PopupManager.Instance.SetOffPopup();
                                        NetworkManager.Instance.mUserData.reserveTime4 = NetworkManager.Instance.GetServerNow().AddMinutes(15);

                                        ReturningPopup.SetActive(false);
                                        BoxRewardManager.Instance.SetReward(N);
                                    },
                                    delegate (int _code, string _message)
                                    {
                                        PopupManager.Instance.SetPopup("Error : " + _message);
                                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                                    },
                                    delegate (string _message)
                                    {
                                        PopupManager.Instance.SetPopup("Please try again.");
                                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                                    }
                                    );
                    });
                });
            }
            */

            if (!new Money("0").CompareMoney(_after_money))
            {
                ReturningPopup.SetActive(true);
                ReturningBaseRoot.SetActive(true);
                ReturningBaseSuccess.SetActive(false);
                ReturningMessageWelcome.text = string.Format(Language.Str.RETURNING_FORMAT_TITLE, NetworkManager.Instance.mUserData.nickname);
                ReturningMessageRoot1.text = Language.Str.RETURNING_MESSAGE_ROOT1;
                ReturningMessageRoot2.text = Language.Str.RETURNING_MESSAGE_ROOT2;
                ReturningMessageRootPrice.text = "$" + _after_money.ToWon();

                ReturningBtnWatchAD.onClick.RemoveAllListeners();
                ReturningBtnWatchAD.onClick.AddListener(delegate
                {
                    mAdsManager.Instance.ShowAD(delegate
                    {
                        _after_money.MulMoney(2);
                        OpenReturningSuccess(_after_money);

                        IGAWorksManager.onRetention("ADReturning");
                    });
                });

                ReturningBtnClose.onClick.RemoveAllListeners();
                ReturningBtnClose.onClick.AddListener(delegate
                {
                    UserData.Instance.TotalMoney.AddMoney(_after_money);
                    ReturningPopup.SetActive(false);

                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
                });
            }
        }
    }

    private void OpenReturningSuccess(Money _get_money)
    {
        ReturningPopup.SetActive(true);
        ReturningBaseRoot.SetActive(false);
        ReturningBaseSuccess.SetActive(true);

        ReturningMessageSuccess1.text = Language.Str.RETURNING_MESSAGE_DOUBLE1;
        ReturningMessageSuccess2.text = Language.Str.RETURNING_MESSAGE_DOUBLE2;
        ReturningMessageSuccessPrice.text = "$" + _get_money.ToWon();

        ReturningBtnOK.onClick.RemoveAllListeners();
        ReturningBtnOK.onClick.AddListener(delegate
        {
            UserData.Instance.TotalMoney.AddMoney(_get_money);
            ReturningPopup.SetActive(false);
        });
    }
    
    IEnumerator StartGameProcess()
    {
        while (TutorialManager.Instance.Step != -1) yield return null;

        if (isRetuningOnLoad)
        {
            Money _before_money = new Money(UserData.Instance.TotalMoney);
            FactoryManager.Instance.UpdateFactorys();
            Money _after_money = Money.SubNumString(UserData.Instance.TotalMoney, _before_money);
            UserData.Instance.TotalMoney = new Money(_before_money);

            StartCoroutine(ReturningProcess(_before_money, _after_money));
        }
        else isRetuningOnLoad = true;

        StartCoroutine(LevelupProcess());
    }
    
    //--------------------------------------------
    public void SetPopupToShop(int _type)
    {
        string _case = "";
        if (_type == ShopDB.PRICE_TYPE_MONEY) _case = Language.Str.POPUP_MESSAGE_NOTENOUGH_MONEY;
        else if (_type == ShopDB.PRICE_TYPE_GEM) _case = Language.Str.POPUP_MESSAGE_NOTENOUGH_GEM;

        int _state = _type;
        PopupManager.Instance.SetPopup(_case + Language.Str.POPUP_MESSAGE_NOTENOUGH_MESSAGE,
            delegate
            {
                if(_state == ShopDB.PRICE_TYPE_GEM) ShopManager.Instance.SetBase(ShopManager.Instance.BaseGem);
                else if (_state == ShopDB.PRICE_TYPE_MONEY) ShopManager.Instance.SetBase(ShopManager.Instance.BaseMoney);
            });
    }

    //-----------------------------------------------------------------------
    //가격 비교 : 구입할 수 없어 팝업이 나오는 상태면 true를 반환
    public bool CheckPrice(int _gem)
    {
        if(NetworkManager.Instance.mUserData.cash < _gem)
        {
            SetPopupToShop(ShopDB.PRICE_TYPE_GEM);
            return true;
        }

        return false;
    }

    public bool CheckPrice(Money _money)
    {
        if (!UserData.Instance.TotalMoney.CompareMoney(_money))
        {
            SetPopupToShop(ShopDB.PRICE_TYPE_MONEY);
            return true;
        }

        return false;
    }

    //----

    public void PlayEffect(string _key)
    {
        int _effect = (int)typeof(SoundManager).GetField(_key).GetValue(null);
        SoundManager.Instance.PlayEffects(_effect);
    }
}
