﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using System;
using SimpleJSON;

public class FBManager : MonoBehaviour {

	public static FBManager Instance = null;

	// Use this for initialization
	void Awake () {
	
        if(Instance != null)
        {
            Destroy(this);
            return;
        }

		Instance = this;
        DontDestroyOnLoad(this);

		FB.Init 
        (
            delegate
            {
                if(FB.IsLoggedIn)
                {
                    LoadMe();
                }
            }
        );

        FriendDataList = new List<JSONNode>();
        FriendPictureMap = new Dictionary<string, Sprite>();
	}

    [HideInInspector]
    public string MyID = "";
    [HideInInspector]
    public string MyName = "";
    private string AppLink = "";

	public void Login(Action _onlogin = null)
	{
		FB.LogInWithReadPermissions
		(new List<string> (){"public_profile","user_friends" }, 
        delegate(ILoginResult _result)
        {
            if(_result.Error == null)
            {
                var node = JSONNode.Parse(_result.RawResult);
                LoadMe(_onlogin);   
            }
        }
        );
	}

	public void LogOut()
	{
        FB.LogOut ();
	}

    public bool isLogin() { return FB.IsLoggedIn; }

    [HideInInspector]
    public List<JSONNode> FriendDataList;
    [HideInInspector]
    public Dictionary<string, Sprite> FriendPictureMap;
    private Dictionary<string, List<Image>> PictureRequests = new Dictionary<string, List<Image>>();

    private void LoadAppLink()
    {
        FB.GetAppLink(delegate (IAppLinkResult _result)
        {
            AppLink = _result.Url;
            PopupManager.Instance.SetPopup(_result.RawResult);
        });
    }
    
    private void LoadMe(Action _onlogin = null)
    {
        FB.API(
            "/me",
            HttpMethod.GET,
            delegate (IGraphResult _result)
            {
                if (_result.Error == null)
                {
                    var node = JSONNode.Parse(_result.RawResult);
                    MyID = node["id"];
                    MyName = node["name"];
                    
                    LoadFriends(_onlogin);
                }
            }
        );
    }

    private void LoadFriends(Action _onlogin = null)
    {
        FriendDataList.Clear();
        FriendPictureMap.Clear();
        FB.API(
            "/me/friends?fields=id,name&limit=5000",
            HttpMethod.GET,
            delegate (IGraphResult _result)
            {
                if (_result.Error == null)
                {
                    var node = JSONNode.Parse(_result.RawResult);
                    for (int i = 0; i < node["data"].AsArray.Count; i++)
                    {
                        FriendDataList.Add(node["data"].AsArray[i]);
                    }

                    if (_onlogin != null) _onlogin();
                    StartCoroutine(DownLoadProfilePicture());
                }
            }
        );
    }

    public string GetFriendName(string _id)
    {
        for(int i = 0; i<FriendDataList.Count; i++)
        {
            if (FriendDataList[i]["id"].ToString().Replace("\"","") == _id)
            {
                return FriendDataList[i]["name"];
            }
        }

        return "";
    }
    
    IEnumerator DownLoadProfilePicture()
    {
        yield return null;

        FriendPictureMap.Clear();
        {
            WWW www = new WWW("https://graph.facebook.com/" + MyID + "/picture?width=100&height=100");
            yield return www;
            
            if (www.error == null)
            {
                FriendPictureMap.Add(MyID, Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.one / 2.0f));
                if (PictureRequests.ContainsKey(MyID))
                { 
                    for (int i = 0; i < PictureRequests[MyID].Count; i++)
                    {
                        PictureRequests[MyID][i].sprite = FriendPictureMap[MyID];
                    }
                    PictureRequests[MyID].Clear();
                }
            }

            www.Dispose();
        }

        for (int i = 0; i < FriendDataList.Count; i++)
        {
            WWW www = new WWW("https://graph.facebook.com/" + FriendDataList[i]["id"] + "/picture?width=100&height=100");
            yield return www;
            
            if(www.error == null)
            {
                string _id = FriendDataList[i]["id"];
                Sprite _sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.one / 2.0f);
                FriendPictureMap.Add(FriendDataList[i]["id"], _sprite);
                if (PictureRequests.ContainsKey(_id))
                {
                    for (int j = 0; j < PictureRequests[_id].Count; j++)
                    {
                        PictureRequests[_id][j].sprite = FriendPictureMap[_id];
                    }
                    PictureRequests[_id].Clear();
                }
            }
        }
    }

    public void SetPicture(string _id, Image _img)
    {
        if (FriendPictureMap.ContainsKey(_id))
        {
            _img.sprite = FriendPictureMap[_id];
        }
        else
        {
            if (!PictureRequests.ContainsKey(_id))
                PictureRequests.Add(_id, new List<Image>());

            PictureRequests[_id].Add(_img);
        }
    }

    public void InviteFriend(Action _act)
    {
        /*
        FB.Mobile.AppInvite(
            new Uri("https://play.google.com/store/apps/details?id=com.TouchTouch.SuddenRich_Kakao&hl=ko"),
            null,
            delegate(IAppInviteResult arg)
            {
                Debug.Log(arg);
                PopupManager.Instance.SetPopup(arg.RawResult);
            });
            */

        /*
        string data = string.Format("invite,{0}", MyID);
        FB.AppRequest("test", OGActionType.SEND, "", null, data, "invite test", delegate (IAppRequestResult _result)
        {
            PopupManager.Instance.SetPopup(_result.RawResult);
        });
        */
    }
}
