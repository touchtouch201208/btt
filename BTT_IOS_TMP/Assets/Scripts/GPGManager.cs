﻿using UnityEngine;
using System.Collections;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

using GooglePlayGames.BasicApi.SavedGame;
using System;

public class GPGManager : MonoBehaviour {

    private static GPGManager instance;
	public static GPGManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(GPGManager)) as GPGManager;
                if (instance == null)
                {
                    instance = (new GameObject("GPGManager")).AddComponent<GPGManager>();
                }
            }
            return instance;
        }
    }

    public static bool isLogin = false;

	// Use this for initialization
	void Awake () {

        DontDestroyOnLoad(this);

		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            //.EnableSavedGames()
			.Build();

		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
	}
    
	public void Login(
        System.Action _process,
        System.Action _onfail
        )
	{
		Social.localUser.Authenticate ((bool success) => {
            if (success)
            {
                isLogin = true;
                _process();

                /*
                if(AbilityManager.Instance != null)
                {
                    for(int i = 0; i<AbilityManager.Abilitys.Count; i++)
                    {
                        if (AbilityManager.Abilitys[i].isComplete()) CompleteAchievement(AbilityManager.Abilitys[i].AchievementKey);
                    }
                }
                */
            }
            else _onfail();
        });
	}

    public void LogOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    public string GetUserName() { return Social.localUser.userName; }

    public string GetUserID() { return Social.localUser.id; }

	public void ShowAchievement()
	{
		Social.ShowAchievementsUI ();
	}

	public void ShowLeaderboard()
	{
		Social.ShowLeaderboardUI ();
	}

    public void CompleteAchievement(string _key)
    {
        Social.ReportProgress(_key, 100, null);
    }

    private Action OnSaveComplete;
    private Action OnSaveFail;
    public void SaveData(Action _onsaved, Action _onfail )
    {
        OnSaveComplete = _onsaved;
        OnSaveFail = _onfail;

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution("GPGsaveData", DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened);
    }
    
    private void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            //DataManager.Instance.SaveData();

            // handle reading or writing of saved game.
            //SaveGame(game, DataManager.Instance.GetDataStream(), System.TimeSpan.FromSeconds(PlayInfoManager.PlayTime));
        }
        else
        {
            // handle error
            OnSaveFail();
        }
    }

    void SaveGame(ISavedGameMetadata game, byte[] savedData, System.TimeSpan totalPlaytime)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
        builder = builder
            .WithUpdatedPlayedTime(totalPlaytime)
            .WithUpdatedDescription("Saved game at " + System.DateTime.Now);

        SavedGameMetadataUpdate updatedMetadata = builder.Build();
        savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
    }

    private void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            OnSaveComplete();
        }
        else
        {
            // handle error
            OnSaveFail();
        }
    }

    private Action OnLoadComplete;
    private Action OnLoadFail;
    public void LoadData(Action _onload, Action _onfail)
    {
        OnLoadComplete = _onload;
        OnLoadFail = _onfail;

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution("GPGsaveData", DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnLoadGameOpened);
    }

    private void OnLoadGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            LoadGameData(game);
        }
        else
        {
            // handle error
            OnLoadFail();
        }
    }

    void LoadGameData(ISavedGameMetadata game)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    public void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle processing the byte array data
            //DataManager.Instance.LoadDataFromStream(data);

            OnLoadComplete();
        }
        else
        {
            // handle error
            OnLoadFail();
        }
    }

    public void ShowSaveDataUI()
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ShowSelectSavedGameUI(
            "Select saved game",
            1,
            false,
            true,
            delegate(SelectUIStatus _state, ISavedGameMetadata _data) { });
    }
}