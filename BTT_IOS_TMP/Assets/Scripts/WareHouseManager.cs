﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class WareHouseManager : MonoBehaviour {
    
    public static WareHouseManager Instance = null;

    public Text MainHouseSizeLabel;
    public Text MainInvenSizeLabel;

    public GameObject WareHouseMainBase;
    public GameObject WareHouseWorkBase;
    public Transform WareHouseImgRoot;
    public Image WareHouseImg;
    public Transform InventoryImgRoot;
    public Image InventoryImg;
    public Text WareHouseLabelUpgradePriceMoney;
    public Text WareHouseLabelUpgradePriceGem;
    public Text WareHouseLabelUpgradePriceComplete;
    public Image WareHouseUpgradeGauge;
    public Text WareHouseLabelUpgradeTime;
    public DateTime WareHouseUpgradeTime = DateTime.MinValue;
    private float WareHouseUpgradeAddTime = 0;
    private long WareHouseUpgradeSeconds
    {
        get { return ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1].time / 1000; }
    }
    private Dictionary<int, Sprite> WareHouseImgs = new Dictionary<int, Sprite>();

    public GameObject WareHouseBase;
    public GameObject WareHouseUpgradePopup;
    public RectTransform WareHouseScrollRoot;
    private UIScrollNodeProduct[] WareHouseScrollNodes;
    private int WareHouseScrollPage = 0;

    public GameObject InventoryBase;
    public RectTransform InventoryImgIcon;
    public GameObject InventoryUpgradeEffect;
    public GameObject InventoryUpgradePopup;
    public RectTransform InventoryScrollRoot;
    public GameObject NotificateInventory;
    public GameObject NotificateInventoryBtn;
    private UIScrollNodeProduct[] InventoryScrollNodes;
    private int InventoryScrollPage = 0;

    public GameObject ToInvenPopup;
    public Slider ToInvenAmountSlide;
    public Text ToInvenLabelCurrent;
    public Image ToInvenItemIcon;
    public Text ToInvenItemName;
    public Button ToInvenBtnSend;
    public Button ToInvenBtnCancel;
    private int ToInvenCurrentIdx = -1;

    public GameObject ToHousePopup;
    public Slider ToHouseAmountSlide;
    public Text ToHouseLabelCurrent;
    public Image ToHouseItemIcon;
    public Text ToHouseItemName;
    public Button ToHouseBtnSend;
    public Button ToHouseBtnCancel;
    private int ToHouseCurrentIdx = -1;

    void Awake()
    {
        Instance = this;

        for (int i = 0; i < ShopDB.Instance.Warehouse.Count; i++)
        {
            var _info = ShopDB.Instance.Warehouse.ElementAt(i).Value;
            WareHouseImgs.Add(_info.level, Resources.Load<Sprite>("UI/Warehouse/warehouse_" + _info.level));
        }

        WareHouseScrollNodes = WareHouseScrollRoot.GetComponentsInChildren<UIScrollNodeProduct>();
        InventoryScrollNodes = InventoryScrollRoot.GetComponentsInChildren<UIScrollNodeProduct>();

        for (int i = 0; i < WareHouseScrollNodes.Length; i++)
        {
            int idx = i;
            WareHouseScrollNodes[i].transform.Find("BtnToInven").GetComponent<Button>().onClick.AddListener(delegate
            {
                if (!UserData.Instance.isInventoryFull())
                {
                    SetToInvenPopup(idx);
                }
                else
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_INVENTORY_FULL);
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                }
            });
        }

        /*
        for (int i = 0; i < InventoryScrollNodes.Length; i++)
        {
            int idx = i;
            InventoryScrollNodes[i].transform.Find("BtnToHouse").GetComponent<Button>().onClick.AddListener(delegate
            {
                if (!UserData.Instance.isWareHouseFull())
                {
                    SetToHousePopup(idx);
                }
                else
                {
                    PopupManager.Instance.SetPopup("Warehouse is full.");
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                }
            });
        }
        */

        ToInvenAmountSlide.onValueChanged.AddListener(delegate 
        {
            ToInvenLabelCurrent.text = ToInvenAmountSlide.value.ToString();
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SLIDE);
        });
        ToHouseAmountSlide.onValueChanged.AddListener(delegate 
        {
            ToHouseLabelCurrent.text = ToHouseAmountSlide.value.ToString();
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SLIDE);
        });

        ToInvenBtnSend.onClick.AddListener(delegate
        {
            /*
            UserData.Product _product = UserData.Instance.WareHouseProducts[WareHouseScrollNodes[ToInvenCurrentIdx].ProductID];
            int _amount = (int)ToInvenAmountSlide.value;
            if (!UserData.Instance.isInventoryFull(_amount))
            {
                UIEffectManager.Instance.SetEffect(
                    _amount,
                    WareHouseImgRoot.transform,
                    InventoryImgRoot.transform,
                    _product.ID
                    );
                UserData.Instance.AddProductInventory(_product.ID, _amount);
                UserData.Instance.AddProductWareHouse(_product.ID, _amount * -1);
                UpdateWareHouseAmount();
                UpdateMainAmount();
                SetWareHouseProductListScroll();
                ToInvenPopup.SetActive(false);
            }
            */
        });
        ToInvenBtnCancel.onClick.AddListener(delegate { ExitToInvenPopup(); });

        ToHouseBtnSend.onClick.AddListener(delegate
        {
            /*
            UserData.Product _product = UserData.Instance.InventoryProducts[InventoryScrollNodes[ToHouseCurrentIdx].ProductID];
            int _amount = (int)ToHouseAmountSlide.value;
            if (!UserData.Instance.isWareHouseFull(_amount))
            {
                UIEffectManager.Instance.SetEffect(
                    _amount,
                    InventoryImgRoot.transform,
                    WareHouseImgRoot.transform,
                    _product.ID
                    );
                UserData.Instance.AddProductWareHouse(_product.ID, _amount);
                UserData.Instance.AddProductInventory(_product.ID, _amount * -1);
                UpdateInventoryAmount();
                UpdateMainAmount();
                SetInventoryProductListScroll();
                ToHousePopup.SetActive(false);
            }
            */
        });
        ToInvenBtnCancel.onClick.AddListener(delegate { ExitToHousePopup(); });

        WareHouseUpgradeTime = NetworkManager.Instance.mUserData.wareHouseFinishTime;

        UpdateMainAmount();
        UpdateWarehouseInfo();

        CheckBuildTimes();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (WareHouseUpgradeTime != DateTime.MinValue)
        {
            WareHouseUpgradeAddTime += Time.deltaTime;

            float _left_time =
                (float)((WareHouseUpgradeTime.AddSeconds(WareHouseUpgradeSeconds - WareHouseUpgradeAddTime)
                - WareHouseUpgradeTime).TotalSeconds);

            WareHouseLabelUpgradeTime.text
                = string.Format(
                    "{0:00}:{1:00}:{2:00}",
                    Math.Floor(_left_time / 3600),
                    Math.Floor((_left_time % 3600) / 60),
                    Math.Floor(_left_time % 60));
            WareHouseUpgradeGauge.fillAmount = (WareHouseUpgradeSeconds - _left_time) / WareHouseUpgradeSeconds;

            if (_left_time <= 0)
            {
                UpgradeWareHouseComplete();
            }
        }

        if (WareHouseBase.activeSelf)
        {
            int _now_page = (int)(WareHouseScrollRoot.anchoredPosition.y / 120);
            if (_now_page < 0) _now_page = 0;
            if (WareHouseScrollPage != _now_page)
            {
                WareHouseScrollPage = _now_page;
                SetWareHouseProductListScrollNodes();
            }
        }

        if (InventoryBase.activeSelf)
        {
            int _now_page = (int)(InventoryScrollRoot.anchoredPosition.y / 120);
            if (_now_page < 0) _now_page = 0;
            if (InventoryScrollPage != _now_page)
            {
                InventoryScrollPage = _now_page;
                SetInventoryProductListScrollNodes();
            }
        }
    }

    public void OnEscape()
    {
        if (WareHouseBase.activeSelf)
        {
            if (WareHouseUpgradePopup.activeSelf) ExitWareHouseUpgrade();
            else if (ToInvenPopup.activeSelf) ExitToInvenPopup();
            else ExitWareHouse();
        }
        else if (InventoryBase.activeSelf)
        {
            if (InventoryUpgradePopup.activeSelf) ExitInventoryUpgrade();
            else if (ToHousePopup.activeSelf) ExitToHousePopup();
            else ExitInventory();
        }
    }

    //------------------------------------------------------

    private void SetWareHouseProductListScroll()
    {
        WareHouseScrollRoot.transform.parent.parent.GetComponent<ScrollRect>().enabled = false;

        WareHouseScrollRoot.anchoredPosition = new Vector2(0, WareHouseScrollPage * 120);

        WareHouseScrollRoot.sizeDelta =
            new Vector2(
                WareHouseScrollRoot.sizeDelta.x,
                390 + ((UserData.Instance.WareHouseProducts.Count - 3) * 120)
                );

        SetWareHouseProductListScrollNodes();
        Invoke("ActiveWarehouseProductScroll", 0.33f);
    }

    private void ActiveWarehouseProductScroll()
    {
        WareHouseScrollRoot.transform.parent.parent.GetComponent<ScrollRect>().enabled = true;
    }

    private void SetWareHouseProductListScrollNodes()
    {
        for (int i = 0; i < WareHouseScrollNodes.Length; i++)
        {
            var _rect = WareHouseScrollNodes[i].GetComponent<RectTransform>();
            _rect.anchoredPosition =
                new Vector2(
                    _rect.anchoredPosition.x,
                    (WareHouseScrollPage * -120)
                    + (i * -120));

            int _node_idx = i + WareHouseScrollPage;
            if (_node_idx >= UserData.Instance.WareHouseProducts.Count)
            {
                _rect.gameObject.SetActive(false);
            }
            else
            {
                var _node = UserData.Instance.WareHouseProducts.ElementAt(_node_idx).Value;

                WareHouseScrollNodes[i].ProductID = _node.ID;
                _rect.Find("Icon").GetComponent<Image>().sprite = _node.Img;
                _rect.Find("Name").GetComponent<Text>().text = _node.name;
                _rect.Find("AvgCost").GetComponent<Text>().text = string.Format(Language.Str.AVG_FORMAT, ItemDB.Instance.ItemDatas[_node.ID]);
                _rect.Find("Amount").GetComponent<Text>().text = _node.amount.ToString();

                _rect.gameObject.SetActive(true);
            }
        }
    }

    private void SetInventoryProductListScroll()
    {
        InventoryScrollRoot.transform.parent.parent.GetComponent<ScrollRect>().enabled = false;

        InventoryScrollRoot.anchoredPosition = new Vector2(0, InventoryScrollPage * 120);

        InventoryScrollRoot.sizeDelta =
            new Vector2(
                InventoryScrollRoot.sizeDelta.x,
                510 + ((UserData.Instance.InventoryProducts.Count - 3) * 120)
                );

        SetInventoryProductListScrollNodes();
        Invoke("ActiveInventoryProductScroll", 0.33f);
    }

    private void ActiveInventoryProductScroll()
    {
        InventoryScrollRoot.transform.parent.parent.GetComponent<ScrollRect>().enabled = true;
    }

    private void SetInventoryProductListScrollNodes()
    {
        for (int i = 0; i < InventoryScrollNodes.Length; i++)
        {
            var _rect = InventoryScrollNodes[i].GetComponent<RectTransform>();
            _rect.anchoredPosition =
                new Vector2(
                    _rect.anchoredPosition.x,
                    (InventoryScrollPage * -120)
                    + (i * -120));

            int _node_idx = i + InventoryScrollPage;
            if (_node_idx >= UserData.Instance.InventoryProducts.Count)
            {
                _rect.gameObject.SetActive(false);
            }
            else
            {
                var _node = UserData.Instance.InventoryProducts.ElementAt(_node_idx).Value;

                InventoryScrollNodes[i].ProductID = _node.ID;
                _rect.Find("Icon").GetComponent<UIItemIcon>().SetItem(_node.ID);
                _rect.Find("Name").GetComponent<Text>().text = _node.name;
                _rect.Find("AvgCost").GetComponent<Text>().text =
                    string.Format(Language.Str.AVG_FORMAT,
                    new Money((_node.AvgPrice).ToString()).ToMoneyWon()
                    );
                _rect.Find("Amount").GetComponent<Text>().text = _node.amount.ToString();

                _rect.gameObject.SetActive(true);
            }
        }
    }

    //------------------------------------------------------

    private void SetToInvenPopup(int _idx)
    {
        UserData.Product _product = UserData.Instance.WareHouseProducts[WareHouseScrollNodes[_idx].ProductID];
        ToInvenItemIcon.sprite = _product.Img;
        ToInvenItemName.text = _product.name;
        int _amount = Mathf.Min(_product.amount, UserData.Instance.GetInventoryLeftCount());
        ToInvenAmountSlide.maxValue = _amount;
        ToInvenAmountSlide.value = _amount;
        ToInvenLabelCurrent.text = _amount.ToString();

        ToInvenCurrentIdx = _idx;
        ToInvenPopup.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    private void ExitToInvenPopup()
    {
        ToInvenPopup.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    private void SetToHousePopup(int _idx)
    {
        UserData.Product _product = UserData.Instance.InventoryProducts[InventoryScrollNodes[_idx].ProductID];
        ToHouseItemIcon.sprite = _product.Img;
        ToHouseItemName.text = _product.name;
        int _amount = Mathf.Min(_product.amount, UserData.Instance.GetWareHouseLeftCount());
        ToHouseAmountSlide.maxValue = _amount;
        ToHouseAmountSlide.value = _amount;
        ToHouseLabelCurrent.text = _amount.ToString();

        ToHouseCurrentIdx = _idx;
        ToHousePopup.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    private void ExitToHousePopup()
    {
        ToHousePopup.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    //------------------------------------------------------

    public void UpdateMainAmount()
    {
        MainHouseSizeLabel.text = string.Format("{0}/{1}",
            UserData.Instance.GetWareHouseCount(),
            UserData.Instance.WareHouseSize);

        MainInvenSizeLabel.text = string.Format("{0}/{1}",
            UserData.Instance.GetInventoryCount(),
            UserData.Instance.InventorySize);
    }

    public void UpdateWarehouseInfo()
    {
        if (NetworkManager.Instance.mUserData.wareHouseLevel
            >= ShopDB.Instance.Warehouse.Keys.Max())
        {
            WareHouseLabelUpgradePriceMoney.gameObject.SetActive(false);
            WareHouseLabelUpgradePriceMoney.text = Language.Str.MAX;
            WareHouseLabelUpgradePriceGem.text = Language.Str.MAX;
            WareHouseLabelUpgradePriceComplete.text = Language.Str.MAX;
        }
        else
        {
            var _info = ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1];
            WareHouseLabelUpgradePriceMoney.text = _info.priceMoney.ToString();
            WareHouseLabelUpgradePriceGem.text = _info.priceGem.ToString();
            WareHouseLabelUpgradePriceComplete.text = _info.completeGem.ToString();
        }

        //WareHouseImg.sprite = WareHouseImgs[NetworkManager.Instance.mUserData.wareHouseLevel];
        WareHouseMainBase.SetActive(WareHouseUpgradeTime == DateTime.MinValue);
        WareHouseWorkBase.SetActive(WareHouseUpgradeTime != DateTime.MinValue);
    }
    
    private void UpdateWareHouseAmount()
    {
        WareHouseBase.transform.Find("Base").Find("Amount").GetComponent<Text>().text = string.Format("{0}/{1}",
            UserData.Instance.GetWareHouseCount(), UserData.Instance.WareHouseSize);
    }

    private void UpdateInventoryAmount()
    {
        InventoryBase.transform.Find("Base").Find("Img").Find("Amount").GetComponent<Text>().text = string.Format("{0}/{1}",
            UserData.Instance.GetInventoryCount(), UserData.Instance.InventorySize);
    }

    public void UpdateAllWareHouse()
    {
        UpdateMainAmount();
        UpdateWareHouseAmount();
        SetWareHouseProductListScroll();
    }

    //------------------------------------------------------

    public void OpenWareHouse()
    {
        UpdateWareHouseAmount();
        SetWareHouseProductListScroll();
        WareHouseBase.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    public void ExitWareHouse()
    {
        WareHouseBase.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    private Vector2 InventoryImgPosNormal = new Vector2(-170, 226);
    private Vector2 InventoryImgPosMax = new Vector2(0, 226);
    public void OpenInventory()
    {
        UpdateInventoryAmount();
        SetInventoryProductListScroll();

        if (NetworkManager.Instance.mUserData.inventoryLevel
            >= ShopDB.Instance.Inventory.Keys.Max())
        {
            InventoryBase.transform.Find("Base").Find("Img").GetComponent<RectTransform>().anchoredPosition = InventoryImgPosMax;
            InventoryBase.transform.Find("Base").Find("UpgradeExplain").gameObject.SetActive(false);
            InventoryBase.transform.Find("Base").Find("BtnUpgradeMoney").gameObject.SetActive(false);
            InventoryBase.transform.Find("Base").Find("BtnUpgradeGem").gameObject.SetActive(false);
            NotificateInventory.SetActive(false);
        }
        else
        {
            //set display label
            var _info = ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1];

            InventoryBase.transform.Find("Base").Find("Img").GetComponent<RectTransform>().anchoredPosition = InventoryImgPosNormal;
            InventoryBase.transform.Find("Base").Find("UpgradeExplain").GetComponent<Text>().text = string.Format(Language.Str.INVENTORY_MESSAGE_FORMAT_UPGRADE, _info.rewardSlot);
            InventoryBase.transform.Find("Base").Find("BtnUpgradeMoney").Find("Label").GetComponent<Text>().text = "$" + new Money(_info.priceMoney.ToString()).ToWon();
            InventoryBase.transform.Find("Base").Find("BtnUpgradeGem").Find("Label").GetComponent<Text>().text = _info.priceGem.ToString();
        }

        InventoryUpgradeEffect.SetActive(false);
        InventoryBase.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_TAP2);
    }

    public void ExitInventory()
    {
        InventoryBase.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }
    
    public void OnCheckNotificateInventoryPrice()
    {
        if (NetworkManager.Instance.mUserData.inventoryLevel
            < ShopDB.Instance.Inventory.Keys.Max())
        {
            Money _price = new Money(ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1].priceMoney.ToString());
            if (UserData.Instance.TotalMoney.CompareMoney(_price))
            {
                NotificateInventory.SetActive(true);
                NotificateInventoryBtn.SetActive(true);
            }
            else
            {
                NotificateInventory.SetActive(false);
                NotificateInventoryBtn.SetActive(false);
            }
        }
    }

    //------------------------------------------------------

    public void OpenWareHouseUpgrade()
    {
        if (NetworkManager.Instance.mUserData.wareHouseLevel
            >= ShopDB.Instance.Warehouse.Keys.Max()) return;

        if(WareHouseUpgradeTime != DateTime.MinValue)
        {
            PopupManager.Instance.SetPopup("Warehouse is on upgrade.");
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            return;
        }

        //set display label
        var _info = ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1];
        WareHouseUpgradePopup.transform.Find("Base").Find("Amount").GetComponent<Text>().text = _info.rewardSlot.ToString();
        WareHouseUpgradePopup.transform.Find("Base").Find("BtnUpgradeMoney").Find("Label").GetComponent<Text>().text = "$" + new Money(_info.priceMoney.ToString()).ToWon();
        WareHouseUpgradePopup.transform.Find("Base").Find("BtnUpgradeGem").Find("Label").GetComponent<Text>().text = _info.priceGem.ToString();

        WareHouseUpgradePopup.SetActive(true);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }
    
    public void ExitWareHouseUpgrade()
    {
        WareHouseUpgradePopup.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void UpgradeWareHouseMoney()
    {
        //
        if (NetworkManager.Instance.mUserData.wareHouseLevel
            >= ShopDB.Instance.Warehouse.Keys.Max())
        {
            PopupManager.Instance.SetPopup("Warehouse level is max.");
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            return;
        }

        Money _price = new Money(ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1].priceMoney.ToString());
        if (UserData.Instance.TotalMoney.CompareMoney(_price))
        {
            MainScene.Instance.OnUseMoney(_price);
            WareHouseUpgradeTime = NetworkManager.Instance.GetServerNow();
            WareHouseUpgradePopup.SetActive(false);
            WareHouseBase.SetActive(false);
            UpdateMainAmount();
            UpdateWarehouseInfo();

            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
        }
        else MainScene.Instance.SetPopupToShop(ShopDB.PRICE_TYPE_MONEY);
    }

    public void UpgradeWareHouseGem()
    {
        if (NetworkManager.Instance.mUserData.wareHouseLevel
            >= ShopDB.Instance.Warehouse.Keys.Max()) return;

        if (ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1].priceGem == 0)
        {
            //gem price 0 : ad reward
            return;
        }

        if (MainScene.Instance.CheckPrice(ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1].priceGem))
            return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
           ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1].shopID,
           ShopDB.PRICE_TYPE_GEM,
           delegate (SimpleJSON.JSONNode N)
           {
               PopupManager.Instance.SetOffPopup();
               WareHouseUpgradePopup.SetActive(false);
               WareHouseBase.SetActive(false);
               UpdateMainAmount();
               UpdateWarehouseInfo();

               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

               IGAWorksManager.onRetention("UseCashUpgradeWarehouse");
           },
           delegate (int _code, string _message)
           {
               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
               PopupManager.Instance.SetPopup("upgrade warehouse error : " + _message);
           },
           delegate (string _message)
           {
               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
               PopupManager.Instance.SetPopup("upgrade warehouse failed.");
           }
           );
    }

    public void UpgradeWareHouseComplete()
    {
        if (NetworkManager.Instance.mUserData.wareHouseLevel
            >= ShopDB.Instance.Warehouse.Keys.Max()) return;

        WareHouseUpgradeAddTime = 0;
        WareHouseUpgradeTime = DateTime.MinValue;
        NetworkManager.Instance.mUserData.wareHouseLevel++;

        WareHouseLabelUpgradeTime.text = "-";
        WareHouseUpgradeGauge.fillAmount = 0.0f;

        UpdateMainAmount();
        UpdateWarehouseInfo();
    }

    public void UpgradeWareHouseCompleteGem()
    {
        if (NetworkManager.Instance.mUserData.wareHouseLevel
            >= ShopDB.Instance.Warehouse.Keys.Max()) return;
        
        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
           ShopDB.Instance.Warehouse[NetworkManager.Instance.mUserData.wareHouseLevel + 1].shopID,
           ShopDB.PRICE_TYPE_COMPLETE_GEM,
           delegate (SimpleJSON.JSONNode N)
           {
               PopupManager.Instance.SetOffPopup();

               WareHouseUpgradeAddTime = 0;
               WareHouseUpgradeTime = DateTime.MinValue;

               WareHouseLabelUpgradeTime.text = "-";
               WareHouseUpgradeGauge.fillAmount = 0.0f;

               UpdateMainAmount();
               UpdateWarehouseInfo();

               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
           },
           delegate (int _code, string _message)
           {
               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
               PopupManager.Instance.SetPopup("complete upgrade warehouse error : " + _message);
           },
           delegate (string _message)
           {
               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
               PopupManager.Instance.SetPopup("complete upgrade warehouse failed.");
           }
           );
    }

    private void CheckBuildTimes()
    {
        if (WareHouseUpgradeTime == DateTime.MinValue) return;

        float _over_time = (float)(NetworkManager.Instance.GetServerNow() - WareHouseUpgradeTime.AddSeconds(WareHouseUpgradeAddTime)).TotalSeconds;
        WareHouseUpgradeAddTime += _over_time;
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus) CheckBuildTimes();
    }

    //------------------------------------------------------

    public void OpenInventoryUpgrade()
    {
        if (NetworkManager.Instance.mUserData.inventoryLevel
            >= ShopDB.Instance.Inventory.Keys.Max())
        {
            PopupManager.Instance.SetPopup(Language.Str.POPUP_INVENTORY_MAX);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            return;
        }

        //set display label
        var _info = ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1];
        //InventoryUpgradePopup.transform.Find("Base").Find("Amount").GetComponent<Text>().text = "Inventory name\n" + _info.rewardSlot.ToString();
        InventoryUpgradePopup.transform.Find("Base").Find("Amount").GetComponent<Text>().text = _info.rewardSlot.ToString();
        InventoryUpgradePopup.transform.Find("Base").Find("BtnUpgradeMoney").Find("Label").GetComponent<Text>().text = "$" + new Money(_info.priceMoney.ToString()).ToWon();
        InventoryUpgradePopup.transform.Find("Base").Find("BtnUpgradeGem").Find("Label").GetComponent<Text>().text = _info.priceGem.ToString();

        InventoryUpgradePopup.SetActive(true);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    public void ExitInventoryUpgrade()
    {
        InventoryUpgradePopup.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void UpgradeInventoryMoney()
    {
        //
        Money _price = new Money(ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1].priceMoney.ToString());
        if (UserData.Instance.TotalMoney.CompareMoney(_price))
        {
            MainScene.Instance.OnUseMoney(_price);
            NetworkManager.Instance.mUserData.inventoryLevel++;

            UpdateMainAmount();
            OpenInventory();
            InventoryUpgradePopup.SetActive(false);

            InventoryUpgradeEffect.GetComponent<RectTransform>().anchoredPosition = InventoryImgIcon.anchoredPosition;
            InventoryUpgradeEffect.SetActive(true);
            QuestList.Instance.OnBuyInventory();

            OnCheckNotificateInventoryPrice();

            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);
        }
        else MainScene.Instance.SetPopupToShop(ShopDB.PRICE_TYPE_MONEY);
    }

    public void UpgradeInventoryGem()
    {
        if (NetworkManager.Instance.mUserData.inventoryLevel
            >= ShopDB.Instance.Inventory.Keys.Max()) return;

        if (ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1].priceGem == 0) return;

        if (MainScene.Instance.CheckPrice(ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1].priceGem))
            return;

        PopupManager.Instance.SetPopupWait();
        NetworkManager.Instance.RequestBuyShopItem(
           ShopDB.Instance.Inventory[NetworkManager.Instance.mUserData.inventoryLevel + 1].shopID,
           ShopDB.PRICE_TYPE_GEM,
           delegate (SimpleJSON.JSONNode N)
           {
               PopupManager.Instance.SetOffPopup();
               UpdateMainAmount();
               OpenInventory();
               InventoryUpgradePopup.SetActive(false);

               InventoryUpgradeEffect.SetActive(true);
               QuestList.Instance.OnBuyInventory();

               OnCheckNotificateInventoryPrice();

               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

               IGAWorksManager.onRetention("UseCashUpgradeInventory");
           },
           delegate (int _code, string _message)
           {
               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
               PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKERROR);
           },
           delegate (string _message)
           {
               SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
               PopupManager.Instance.SetPopup(Language.Str.POPUP_MESSAGE_NETWORKFAIL);
           }
           );
    }
}
