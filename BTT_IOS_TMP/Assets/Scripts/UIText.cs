﻿using UnityEngine;
using System.Collections;

public class UIText : MonoBehaviour {

    public string Text;
    public string Code;

	// Use this for initialization
	void Awake () {

        SetText();
	}

    public void SetText()
    {
        if (GetComponent<TextMesh>() != null)
        {
            string _label = Text + (string)typeof(LanguagePackBase).GetField(Code).GetValue(Language.Str);
            GetComponent<TextMesh>().text = _label;
        }
        else if (GetComponent<UnityEngine.UI.Text>() != null)
        {
            string _label = Text + (string)typeof(LanguagePackBase).GetField(Code).GetValue(Language.Str);
            GetComponent<UnityEngine.UI.Text>().text = _label;
        }
    }
}
