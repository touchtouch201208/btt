﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class CityDB
{
    public class CityMember
    {
        public int cityCode { get; private set; }
        public int regionCode { get; private set; }
        public int startCity1Code { get; private set; }
        public int startCity2Code { get; private set; }
        public int startCity3Code { get; private set; }
        public int startCity4Code { get; private set; }
        public string cityName
        {
            get
            {
                return Language.Str.CITYNAME(cityCode);
            }
        }
        public string FullName
        {
            get
            {
                //string _country = Language.Str.COUNTRYNAME(cityCode);
                //if (_country.Length == 0) return Language.Str.CITYNAME(cityCode);
                //else return string.Format("{0}, {1}", Language.Str.CITYNAME(cityCode), _country);
                //return string.Format("{0}, {1}", Language.Str.CITYNAME(cityCode), Language.Str.COUNTRYNAME(cityCode));
                return Language.Str.CITYNAME(cityCode);
            }
        }

        public CityMember
            (
            int _cityCode,
            int _regionCode,
            int _startCity1Code,
            int _startCity2Code,
            int _startCity3Code,
            int _startCity4Code
            )
        {
            cityCode = _cityCode;
            regionCode = _regionCode;
            startCity1Code = _startCity1Code;
            startCity2Code = _startCity2Code;
            startCity3Code = _startCity3Code;
            startCity4Code = _startCity4Code;
        }
    }

    public Dictionary<int, CityMember> CityDatas = new Dictionary<int, CityMember>();
    public int Version = -1;

    public const int LEVEL_UNLOCK_CITY = 1;

    public const int LEVEL_FREE_GIFT_SILVER = 1;
    public const int LEVEL_FREE_GIFT_GOLD = 2;

    //

    private static CityDB _instance = null;
    public static CityDB Instance
    {
        get
        {
            if (_instance == null) _instance = new CityDB();

            return _instance;
        }
    }

    public CityDB()
    {
        FileSavePath = Application.persistentDataPath + "/CityDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        CityDatas.Clear();

        Version = _json["dataVersion"].AsArray[3]["dataVersion"].AsInt;

        var _db = _json["cityDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            CityMember _info = new CityMember(
                _db.AsArray[i]["cityCode"].AsInt,
                _db.AsArray[i]["regionCode"].AsInt,
                _db.AsArray[i]["startCity1Code"].AsInt,
                _db.AsArray[i]["startCity2Code"].AsInt,
                _db.AsArray[i]["startCity3Code"].AsInt,
                _db.AsArray[i]["startCity4Code"].AsInt
                );

            CityDatas.Add(_info.cityCode, _info);
        }
        
        SaveFile();
        LogAll();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log city db : \n";
        for (int i = 0; i < CityDatas.Count; i++)
        {
            result += string.Format("{0}, {1}, {2}, {3}, {4}, {5}\n",
                CityDatas.ElementAt(i).Value.cityCode,
                CityDatas.ElementAt(i).Value.regionCode,
                CityDatas.ElementAt(i).Value.startCity1Code,
                CityDatas.ElementAt(i).Value.startCity2Code,
                CityDatas.ElementAt(i).Value.startCity3Code,
                CityDatas.ElementAt(i).Value.startCity4Code);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------

    public Dictionary<int, int> CityPriceInfoHigh = new Dictionary<int, int>();
    public Dictionary<int, int> CityPriceInfoLow = new Dictionary<int, int>();

    public void InitCityPriceInfo(string _data)
    {
        string[] _datas = _data.Split('\n');
        for(int i = 0; i<_datas.Length; i++)
        {
            string[] _node = _datas[i].Split('\t');
            CityPriceInfoLow.Add(int.Parse(_node[0]), int.Parse(_node[1]));
            CityPriceInfoHigh.Add(int.Parse(_node[0]), int.Parse(_node[2]));
        }
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _cityCode = 0;
        int _regionCode = 0;
        int _startCity1Code = 0;
        int _startCity2Code = 0;
        int _startCity3Code = 0;
        int _startCity4Code = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _cityCode = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _regionCode = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _startCity1Code = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _startCity2Code = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _startCity3Code = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _startCity4Code = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            CityDatas.Add(_cityCode,
                new CityMember(
                    _cityCode,
                    _regionCode,
                    _startCity1Code,
                    _startCity2Code,
                    _startCity3Code,
                    _startCity4Code)
                    );
        }
        
        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.Count);
        for (int i = 0; i < CityDatas.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.ElementAt(i).Value.cityCode);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.ElementAt(i).Value.regionCode);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.ElementAt(i).Value.startCity1Code);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.ElementAt(i).Value.startCity2Code);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.ElementAt(i).Value.startCity3Code);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, CityDatas.ElementAt(i).Value.startCity4Code);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.Count, seek);
        for (int i = 0; i < CityDatas.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.ElementAt(i).Value.cityCode, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.ElementAt(i).Value.regionCode, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.ElementAt(i).Value.startCity1Code, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.ElementAt(i).Value.startCity2Code, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.ElementAt(i).Value.startCity3Code, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, CityDatas.ElementAt(i).Value.startCity4Code, seek);
        }

        return result;
    }
}
