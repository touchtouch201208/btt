﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class BoosterDB
{
    public class BoosterMember
    {
        public int boosterID { get; private set; }
        public int itemGroupID { get; private set; }
        public int level { get; private set; }
        public int percentage { get; private set; } // 1 = 1%, 100 = 100%
        public long priceMoney { get; private set; }
        public string formula { get; private set; }
        
        public BoosterMember
            (int _boosterID,
            int _itemGroupID,
            int _level,
            int _percentage,
            long _priceMoney,
            string _formula
            )
        {
            boosterID = _boosterID;
            itemGroupID = _itemGroupID;
            level = _level;
            percentage = _percentage;
            priceMoney = _priceMoney;
            formula = _formula;
        }
    }

    public Dictionary<int, List<BoosterMember>> Boosters = new Dictionary<int, List<BoosterMember>>();
    public int Version = -1;

    private static BoosterDB _instance = null;
    public static BoosterDB Instance
    {
        get
        {
            if (_instance == null) _instance = new BoosterDB();

            return _instance;
        }
    }

    public BoosterDB()
    {
        FileSavePath = Application.persistentDataPath + "/BoosterDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Boosters.Clear();

        Version = _json["dataVersion"].AsArray[13]["dataVersion"].AsInt;

        var _db = _json["boosterDB"];

        int _boosterID = 0;
        int _itemGroupID = 0;
        int _level = 0;
        int _percentage = 0;
        long _priceMoney = 0;
        string _formula = "";

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            _boosterID= _db.AsArray[i]["boosterID"].AsInt;
            _itemGroupID = _db.AsArray[i]["itemGroupID"].AsInt;
            _level = _db.AsArray[i]["boosterLevel"].AsInt;
            _percentage = _db.AsArray[i]["percentage"].AsInt;
            _priceMoney = _db.AsArray[i]["priceMoney"].AsLong;
            _formula = "";
            if(!string.IsNullOrEmpty(_db.AsArray[i]["formula"])) _formula = _db.AsArray[i]["formula"];

            if (!Boosters.ContainsKey(_itemGroupID)) Boosters.Add(_itemGroupID, new List<BoosterMember>());
            Boosters[_itemGroupID].Add(
                new BoosterMember(
                    _boosterID,
                    _itemGroupID,
                    _level,
                    _percentage,
                    _priceMoney,
                    _formula
                    ));
        }

        for (int i = 0; i < Boosters.Count; i++)
        {
            var _node = Boosters.ElementAt(i);
            Boosters[_node.Key] = Boosters[_node.Key].OrderBy(x => x.level).ToList();
        }

        SaveFile();
        LogAll();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log Booster db : \n";
        for (int i = 0; i < Boosters.Count; i++)
        {
            result += string.Format("{0}, {1}\n", Boosters.ElementAt(i).Key, Boosters.ElementAt(i).Value.Last().level);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _boosterID = 0;
        int _itemGroupID = 0;
        int _level = 0;
        int _percentage = 0;
        long _priceMoney = 0;
        string _formula = "";
        int str_len = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _boosterID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _itemGroupID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _level = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _percentage = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _priceMoney = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            str_len = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _formula = Encoding.UTF8.GetString(_data, seek, str_len); seek += str_len;

            if (!Boosters.ContainsKey(_itemGroupID)) Boosters.Add(_itemGroupID, new List<BoosterMember>());
            Boosters[_itemGroupID].Add(new BoosterMember(_boosterID, _itemGroupID, _level, _percentage, _priceMoney, _formula));
        }

        for (int i = 0; i < Boosters.Count; i++)
        {
            var _node = Boosters.ElementAt(i);
            Boosters[_node.Key] = Boosters[_node.Key].OrderBy(x => x.level).ToList();
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        int _count = 0;
        for (int i = 0; i < Boosters.Count; i++) _count += Boosters.ElementAt(i).Value.Count;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _count);
        for (int i = 0; i < Boosters.Count; i++)
        {
            var _list = Boosters.ElementAt(i).Value;
            for (int j = 0; j < _list.Count; j++)
            {
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].boosterID);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].itemGroupID);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].level);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].percentage);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].priceMoney);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].formula);
            }
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, _count, seek);
        for (int i = 0; i < Boosters.Count; i++)
        {
            var _list = Boosters.ElementAt(i).Value;
            for (int j = 0; j < _list.Count; j++)
            {
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].boosterID, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].itemGroupID, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].level, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].percentage, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].priceMoney, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].formula, seek);
            }
        }

        return result;
    }
}
