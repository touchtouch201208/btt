﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class ConfigDB
{
    public class ConfigMember
    {
        public int configID { get; private set; }
        public string configDesc { get; private set; }
        public int configInt { get; private set; }
        public float configFloat { get; private set; }
        public string configString { get; private set; }

        public ConfigMember
            (int _configID,
            string _configDesc,
            int _configInt,
            float _configFloat,
            string _configString
            )
        {
            configID = _configID;
            configDesc = _configDesc;
            configInt = _configInt;
            configFloat = _configFloat;
            configString = _configString;
        }
    }

    public Dictionary<int, ConfigMember> Configs = new Dictionary<int, ConfigMember>();
    public int Version = -1;

    //

    public const int CONFIG_HEART_RECHARGE_TIME = 1;

    private static ConfigDB _instance = null;
    public static ConfigDB Instance
    {
        get
        {
            if (_instance == null) _instance = new ConfigDB();

            return _instance;
        }
    }

    public ConfigDB()
    {
        FileSavePath = Application.persistentDataPath + "/ConfigDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Configs.Clear();
        
        Version = _json["dataVersion"].AsArray[0]["dataVersion"].AsInt;

        var _db = _json["configDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            string _str = "";
            if (!string.IsNullOrEmpty(_db.AsArray[i]["configString"])) _str = _db.AsArray[i]["configString"];
            ConfigMember _info = new ConfigMember(
                _db.AsArray[i]["configID"].AsInt,
                _db.AsArray[i]["configDesc"],

                _db.AsArray[i]["configInt"].AsInt,
                _db.AsArray[i]["configFloat"].AsFloat,
                _str
                );

            Configs.Add(_info.configID, _info);
        }

        SaveFile();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log config db : \n";
        for (int i = 0; i < Configs.Count; i++)
        {
            result += string.Format("{0}, {1}\n", Configs.ElementAt(i).Value.configID, Configs.ElementAt(i).Value.configDesc);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _configID = 0;
        string _configDesc = "";
        int _configInt = 0;
        float _configFloat = 0;
        string _configString = "";
        int str_len = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _configID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            str_len = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _configDesc = Encoding.UTF8.GetString(_data, seek, str_len); seek += str_len;

            _configInt = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _configFloat = BitConverter.ToSingle(_data, seek); seek += sizeof(float);

            str_len = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _configString = Encoding.UTF8.GetString(_data, seek, str_len); seek += str_len;

            Configs.Add(_configID,
                new ConfigMember(_configID,
                _configDesc,
                _configInt,
                _configFloat,
                _configString
                )
                );
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Configs.Count);
        for (int i = 0; i < Configs.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Configs.ElementAt(i).Value.configID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Configs.ElementAt(i).Value.configDesc);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Configs.ElementAt(i).Value.configInt);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Configs.ElementAt(i).Value.configFloat);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Configs.ElementAt(i).Value.configString);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, Configs.Count, seek);
        for (int i = 0; i < Configs.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, Configs.ElementAt(i).Value.configID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Configs.ElementAt(i).Value.configDesc, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Configs.ElementAt(i).Value.configInt, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Configs.ElementAt(i).Value.configFloat, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Configs.ElementAt(i).Value.configString, seek);
        }

        return result;
    }
}
