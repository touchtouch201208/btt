﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class LevelDB
{
    public class LevelMember
    {
        public int level { get; private set; }
        public long maxExp { get; private set; }
        public int unlockCity { get; private set; }
        public int freeChest { get; private set; }

        public LevelMember
            (int _level,
            long _maxExp,
            int _unlockCity,
            int _freeChest
            )
        {
            level = _level;
            maxExp = _maxExp;
            unlockCity = _unlockCity;
            freeChest = _freeChest;
        }
    }

    public Dictionary<int, LevelMember> Levels = new Dictionary<int, LevelMember>();
    public int LastLevel = -1;
    public int Version = -1;
    
    private static LevelDB _instance = null;
    public static LevelDB Instance
    {
        get
        {
            if (_instance == null) _instance = new LevelDB();

            return _instance;
        }
    }

    public LevelDB()
    {
        FileSavePath = Application.persistentDataPath + "/LevelDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Levels.Clear();

        Version = _json["dataVersion"].AsArray[1]["dataVersion"].AsInt;

        var _db = _json["levelDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            Levels.Add(_db.AsArray[i]["level"].AsInt,
                new LevelMember(
                    _db.AsArray[i]["level"].AsInt,
                    _db.AsArray[i]["maxExp"].AsLong,
                    _db.AsArray[i]["unlockCity"].AsInt,
                    _db.AsArray[i]["freeChest"].AsInt)
                    );
        }

        LastLevel = Levels.Last().Value.level;
        SaveFile();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log level db : \n";
        for (int i = 0; i < Levels.Count; i++)
        {
            result += string.Format("{0}, {1}\n", Levels.ElementAt(i).Key, Levels.ElementAt(i).Value.maxExp);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _level = 0;
        long _maxExp = 0;
        int _unlockCity = 0;
        int _freeChest = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _level = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _maxExp = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _unlockCity = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _freeChest = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            
            Levels.Add(_level, new LevelMember(_level, _maxExp, _unlockCity, _freeChest));
        }

        LastLevel = Levels.Last().Value.level;
        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Levels.Count);
        for (int i = 0; i < Levels.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Levels.ElementAt(i).Value.level);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Levels.ElementAt(i).Value.maxExp);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Levels.ElementAt(i).Value.unlockCity);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Levels.ElementAt(i).Value.freeChest);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, Levels.Count, seek);
        for (int i = 0; i < Levels.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, Levels.ElementAt(i).Value.level, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Levels.ElementAt(i).Value.maxExp, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Levels.ElementAt(i).Value.unlockCity, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Levels.ElementAt(i).Value.freeChest, seek);
        }

        return result;
    }
}
