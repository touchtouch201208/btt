﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class FormulaDB
{
    public class FormulaMember
    {
        public int formulaID { get; private set; }
        public int itemID { get; private set; }
        public int levelUnlock { get; private set; }
        public int levelRemove { get; private set; }
        public float successRate { get; private set; }
        public long priceMoney { get; private set; }
        public float time { get; private set; }
        public int cardDisplay { get; private set; }
        public string formula { get; private set; } // {id}:{amount}+{id}:{amount}+...
        public int reciptItemID { get; private set; }

        public FormulaMember
            (int _formulaID,
            int _itemID,
            int _unlock,
            int _remove,
            float _rate,
            long _price,
            float _time,
            int _cardDisplay,
            string _formula
            )
        {
            formulaID = _formulaID;
            itemID = _itemID;
            levelUnlock = _unlock;
            levelRemove = _remove;
            successRate = _rate;
            priceMoney = _price;
            time = _time;
            cardDisplay = _cardDisplay;
            formula = _formula;
            reciptItemID = int.Parse(formula.Split('+')[0].Split(':')[0]);
        }
    }

    public Dictionary<int, FormulaMember> Formulas = new Dictionary<int, FormulaMember>();
    public List<int> FormulaItmes = new List<int>();
    public int Version = -1;

    //

    private static FormulaDB _instance = null;
    public static FormulaDB Instance
    {
        get
        {
            if (_instance == null) _instance = new FormulaDB();

            return _instance;
        }
    }

    public FormulaDB()
    {
        FileSavePath = Application.persistentDataPath + "/FormulaDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Formulas.Clear();
        FormulaItmes.Clear();

        Version = _json["dataVersion"].AsArray[9]["dataVersion"].AsInt;

        var _db = _json["formulaDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            string _str = "";
            if (!string.IsNullOrEmpty(_db.AsArray[i]["formula"])) _str = _db.AsArray[i]["formula"];

            FormulaMember _info = new FormulaMember(
                _db.AsArray[i]["formulaID"].AsInt,
                _db.AsArray[i]["itemID"].AsInt,
                _db.AsArray[i]["levelUnlock"].AsInt,
                _db.AsArray[i]["levelRemove"].AsInt,
                _db.AsArray[i]["successRate"].AsFloat,
                _db.AsArray[i]["priceMoney"].AsLong,
                _db.AsArray[i]["time"].AsFloat,
                _db.AsArray[i]["cardDisplay"].AsInt,
                _str
                );

            Formulas.Add(_info.formulaID, _info);
            FormulaItmes.Add(_info.itemID);
        }

        SaveFile();
        LogAll();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log Formula db : \n";
        for (int i = 0; i < Formulas.Count; i++)
        {
            result += string.Format("{0} : {1} , {2}\n",
                Formulas.ElementAt(i).Value.formulaID,
                Formulas.ElementAt(i).Value.itemID,
                Formulas.ElementAt(i).Value.formula);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _formulaID = 0;
        int _itemID = 0;
        int _unlock = 0;
        int _remove = 0;
        float _rate = 0;
        long _price = 0;
        float _time = 0;
        int _cardDisplay = 0;
        string _formula = "";
        int str_len = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _formulaID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _itemID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _unlock = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _remove = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rate = BitConverter.ToSingle(_data, seek); seek += sizeof(float);
            _price = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _time = BitConverter.ToSingle(_data, seek); seek += sizeof(float);
            _cardDisplay = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            str_len = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _formula = Encoding.UTF8.GetString(_data, seek, str_len); seek += str_len;

            FormulaMember _info =
                new FormulaMember(_formulaID,
                _itemID,
                _unlock,
                _remove,
                _rate,
                _price,
                _time,
                _cardDisplay,
                _formula
                );

            Formulas.Add(_info.formulaID, _info);
            FormulaItmes.Add(_info.itemID);
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.Count);
        for (int i = 0; i < Formulas.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.formulaID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.itemID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.levelUnlock);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.levelRemove);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.successRate);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.priceMoney);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.time);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.cardDisplay);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Formulas.ElementAt(i).Value.formula);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.Count, seek);
        for (int i = 0; i < Formulas.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.formulaID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.itemID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.levelUnlock, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.levelRemove, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.successRate, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.priceMoney, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.time, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.cardDisplay, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Formulas.ElementAt(i).Value.formula, seek);
        }

        return result;
    }
}
