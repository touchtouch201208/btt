﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class ItemDB
{
    public class ItemMember
    {
        public int itemID { get; private set; }
        public int itemGroupID { get; private set; }
        public long medianPrice { get; private set; }
        public long lowPrice { get; private set; }
        public long highPrice { get; private set; }
        public int stdDev { get; private set; }
        public int levelUnlock { get; private set; }
        public int levelRemove { get; private set; }
        public int trades { get; private set; }
        public long price { get; private set; }
        public int time { get; private set; } // minitue
        public string itemName
        {
            get
            {
                return Language.Str.ITEMNAME(itemID);
            }
        }
        public string itemGroupName
        {
            get
            {
                return Language.Str.ITEMGROUPNAME(itemGroupID);
            }
        }

        public ItemMember
            (
            int _itemID,
            int _itemGroupID,
            long _medianPrice,
            long _lowPrice,
            long _highPrice,
            int _stdDev,
            int _unlock,
            int _remove,
            int _trades,
            long _price,
            int _time
            )
        {
            itemID = _itemID;
            itemGroupID = _itemGroupID;
            medianPrice = _medianPrice;
            lowPrice = _lowPrice;
            highPrice = _highPrice;
            stdDev = _stdDev;
            levelUnlock = _unlock;
            levelRemove = _remove;
            trades = _trades;
            price = _price;
            time = _time;
        }
    }

    public Dictionary<int, ItemMember> ItemDatas = new Dictionary<int, ItemMember>();
    public int Version = -1;

    private Dictionary<int, Sprite> ItemImgs = new Dictionary<int, Sprite>();
    private Dictionary<int, Color> ItemColors = new Dictionary<int, Color>();

    //

    private static ItemDB _instance = null;
    public static ItemDB Instance
    {
        get
        {
            if (_instance == null) _instance = new ItemDB();

            return _instance;
        }
    }

    public ItemDB()
    {
        ItemColors.Add(0, new Color(1.0f, 0.6f, 0.11f)); // 
        ItemColors.Add(1, new Color(1.0f, 0.75f, 1.0f)); // 
        ItemColors.Add(2, new Color(0.75f, 0.75f, 0.75f)); // 
        ItemColors.Add(3, new Color(0.75f, 0.75f, 1.0f)); // 
        ItemColors.Add(4, new Color(0.5f, 0.75f, 1.0f)); // 
        ItemColors.Add(5, new Color(0.75f, 1.0f, 0.5f)); // 
        ItemColors.Add(6, new Color(0.75f, 1.0f, 0.75f)); // 
        ItemColors.Add(7, new Color(1.0f, 0.75f, 0.5f)); // 
        ItemColors.Add(8, new Color(1.0f, 0.75f, 0.75f)); // 

        FileSavePath = Application.persistentDataPath + "/ItemDB.dat";
        LoadFile();
    }

    public Sprite GetitemImg(int _id)
    {
        if (ItemImgs.ContainsKey(_id)) return ItemImgs[_id];
        else
        {
            Sprite _img;

            if (_id == -1) _img = Resources.Load<Sprite>("UI/Common/icon_cash");
            else _img = Resources.Load<Sprite>("UI/Items/item_" + _id);
            if (_img != null) ItemImgs.Add(_id, _img);
            return _img;
        }
    }

    public Color GetGroupColor(int _id)
    {
        if (ItemColors.ContainsKey(_id)) return ItemColors[_id];
        else return Color.white;
    }

    public string GetGroupName(int _id)
    {
        return Language.Str.ITEMGROUPNAME(_id);
    }

    public void InitDB(JSONNode _json)
    {
        ItemDatas.Clear();

        Version = _json["dataVersion"].AsArray[5]["dataVersion"].AsInt;

        var _db = _json["itemDB"];

        string _log = "";

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            _log += string.Format("{0}\t{1}\n", _db.AsArray[i]["itemID"].AsInt, _db.AsArray[i]["itemName"].ToString());

            ItemMember _info = new ItemMember(
                _db.AsArray[i]["itemID"].AsInt,
                _db.AsArray[i]["itemGroupID"].AsInt,
                _db.AsArray[i]["medianPrice"].AsInt,
                _db.AsArray[i]["lowPrice"].AsInt,
                _db.AsArray[i]["highPrice"].AsInt,
                _db.AsArray[i]["stdDev"].AsInt,
                _db.AsArray[i]["levelUnlock"].AsInt,
                _db.AsArray[i]["levelRemove"].AsInt,
                _db.AsArray[i]["trades"].AsInt,
                _db.AsArray[i]["price"].AsLong,
                _db.AsArray[i]["time"].AsInt
                );
            
            ItemDatas.Add(_info.itemID, _info);
        }
        SaveFile();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log item db : \n";
        for (int i = 0; i < ItemDatas.Count; i++)
        {
            result += string.Format("{0}, {1}, {2}, {3}, {4}, {5}\n",
                ItemDatas.ElementAt(i).Value.itemID,
                ItemDatas.ElementAt(i).Value.itemGroupID,
                ItemDatas.ElementAt(i).Value.medianPrice,
                ItemDatas.ElementAt(i).Value.lowPrice,
                ItemDatas.ElementAt(i).Value.highPrice,
                ItemDatas.ElementAt(i).Value.stdDev);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _itemID = 0;
        int _itemGroupID = 0;
        long _medianPrice = 0;
        long _lowPrice = 0;
        long _highPrice = 0;
        int _stdDev = 0;
        int _unlock = 0;
        int _remove = 0;
        int _trades = 0;
        long _price = 0;
        int _time = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _itemID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _itemGroupID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _medianPrice = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _lowPrice = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _highPrice = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _stdDev = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _unlock = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _remove = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _trades = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _price = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _time = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            ItemDatas.Add(_itemID,
                new ItemMember(
                    _itemID,
                    _itemGroupID,
                    _medianPrice,
                    _lowPrice,
                    _highPrice,
                    _stdDev,
                    _unlock,
                    _remove,
                    _trades,
                    _price,
                    _time
                    )
                    );
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;
        
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.Count);
        for (int i = 0; i < ItemDatas.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.itemID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.itemGroupID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.medianPrice);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.lowPrice);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.highPrice);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.stdDev);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.levelUnlock);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.levelRemove);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.trades);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.price);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, ItemDatas.ElementAt(i).Value.time);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.Count, seek);
        for (int i = 0; i < ItemDatas.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.itemID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.itemGroupID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.medianPrice, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.lowPrice, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.highPrice, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.stdDev, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.levelUnlock, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.levelRemove, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.trades, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.price, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, ItemDatas.ElementAt(i).Value.time, seek);
        }

        return result;
    }
}
