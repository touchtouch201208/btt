﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class AssistantDB
{
    public class AssistantMember
    {
        public int assistantID;
        public int adjustmentFactor;
        public long priceMoney;
        public int priceGem;

        public AssistantMember
            (
            int _assistantID,
            int _adjustmentFactor,
            long _priceMoney,
            int _priceGem
            )
        {
            assistantID = _assistantID;
            adjustmentFactor = _adjustmentFactor;
            priceMoney = _priceMoney;
            priceGem = _priceGem;
        }
    }

    public Dictionary<int, AssistantMember> Assistants = new Dictionary<int, AssistantMember>();
    public int Version = -1;

    private static AssistantDB _instance = null;
    public static AssistantDB Instance
    {
        get
        {
            if (_instance == null) _instance = new AssistantDB();

            return _instance;
        }
    }

    public AssistantDB()
    {
        FileSavePath = Application.persistentDataPath + "/AssistantDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Assistants.Clear();

        Version = _json["dataVersion"].AsArray[12]["dataVersion"].AsInt;

        var _db = _json["assistantDB"];
        
        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            Assistants.Add(_db[i]["assistantID"].AsInt,
                new AssistantMember(
                    _db.AsArray[i]["assistantID"].AsInt,
                    _db.AsArray[i]["adjustmentFactor"].AsInt,
                    _db.AsArray[i]["priceMoney"].AsLong,
                    _db.AsArray[i]["priceGem"].AsInt
                    ));
        }

        SaveFile();
        LogAll();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log Assistant db : \n";
        for (int i = 0; i < Assistants.Count; i++)
        {
            result += string.Format("{0}, {1}\n", Assistants.ElementAt(i).Key, Assistants.ElementAt(i).Value);
        }
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _id = 0;
        int _factor = 0;
        long _priceMoney = 0;
        int _priceGem = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _id = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _factor = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _priceMoney = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _priceGem = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            Assistants.Add(_id, new AssistantMember(_id, _factor, _priceMoney, _priceGem));
        }
        
        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;
        
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Assistants.Count);
        for (int i = 0; i < Assistants.Count; i++)
        {
            var _node = Assistants.ElementAt(i);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _node.Value.assistantID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _node.Value.adjustmentFactor);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _node.Value.priceMoney);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _node.Value.priceGem);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, Assistants.Count, seek);
        for (int i = 0; i < Assistants.Count; i++)
        {
            var _node = Assistants.ElementAt(i);
            seek = NetworkManager.Instance.AddByteStreamData(result, _node.Value.assistantID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, _node.Value.adjustmentFactor, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, _node.Value.priceMoney, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, _node.Value.priceGem, seek);
        }

        return result;
    }
}
