﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class FactoryDB
{
    public class FactoryMember
    {
        public float time { get; private set; }
        public long value { get; private set; }
        public int step { get; private set; }
        public int maxStep { get; private set; }
        public long buildCost { get; private set; }

        public FactoryMember
            (float _time,
            long _value,
            int _step,
            int _maxStep,
            long _buildCost
            )
        {
            time = _time;
            value = _value;
            step = _step;
            maxStep = _maxStep;
            buildCost = _buildCost;
        }
    }

    public Dictionary<int, Dictionary<int, FactoryMember>> FactoryDatas = new Dictionary<int, Dictionary<int, FactoryMember>>();
    public int Version = -1;

    public int FactoryMaxLevel = 100;

    //

    private static FactoryDB _instance = null;
    public static FactoryDB Instance
    {
        get
        {
            if (_instance == null) _instance = new FactoryDB();

            return _instance;
        }
    }

    public FactoryDB()
    {
        FileSavePath = Application.persistentDataPath + "/FactoryDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        FactoryDatas.Clear();

        Version = _json["dataVersion"].AsArray[2]["dataVersion"].AsInt;

        var _db = _json["factoryDB"];
        
        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            int _id = _db.AsArray[i]["factoryID"].AsInt;
            int _level = _db.AsArray[i]["level"].AsInt;
            FactoryMember _info = new FactoryMember(
                _db.AsArray[i]["time"].AsFloat,
                _db.AsArray[i]["value"].AsLong,
                _db.AsArray[i]["step"].AsInt,
                _db.AsArray[i]["maxStep"].AsInt,
                _db.AsArray[i]["buildCost"].AsLong
                );

            if (!FactoryDatas.ContainsKey(_id)) FactoryDatas.Add(_id, new Dictionary<int, FactoryMember>());
            FactoryDatas[_id].Add(_level, _info);
        }

        FactoryMaxLevel = FactoryDatas.ElementAt(0).Value.Keys.Max();
        SaveFile();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        /*
        string result = "log factory db : \n";
        for (int i = 0; i < FactoryDatas.Count; i++)
        {
            result += string.Format("{0}, {1}, {2}, {3}\n",
                FactoryDatas.ElementAt(i).Key,
                FactoryDatas.ElementAt(i).Value.time,
                FactoryDatas.ElementAt(i).Value.value,
                FactoryDatas.ElementAt(i).Value.buildCost);
        }
        Debug.Log(result);
        */
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _id = 0;
        int _level = 0;
        float _time = 0;
        long _value = 0;
        int _step = 0;
        int _maxStep = 0;
        long _buildCost = 0;
        
        for (int i = 0; i < _db_count; i++)
        {
            _id = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _level = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _time = BitConverter.ToSingle(_data, seek); seek += sizeof(float);
            _value = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _step = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _maxStep = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _buildCost = BitConverter.ToInt64(_data, seek); seek += sizeof(long);

            if (!FactoryDatas.ContainsKey(_id)) FactoryDatas.Add(_id, new Dictionary<int, FactoryMember>());
            FactoryDatas[_id].Add(_level, new FactoryMember(_time, _value, _step, _maxStep, _buildCost));
        }

        FactoryMaxLevel = FactoryDatas.ElementAt(0).Value.Keys.Max();
        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        int _count = 0;
        for (int i = 0; i < FactoryDatas.Count; i++) _count += FactoryDatas.ElementAt(i).Value.Count;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _count);
        for (int i = 0; i < FactoryDatas.Count; i++)
        {
            for(int j = 0; j<FactoryDatas.ElementAt(i).Value.Count; j++)
            {
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Key);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Value.ElementAt(j).Key);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.time);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.value);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.step);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.maxStep);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.buildCost);
            }
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, _count, seek);
        for (int i = 0; i < FactoryDatas.Count; i++)
        {
            for (int j = 0; j < FactoryDatas.ElementAt(i).Value.Count; j++)
            {
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Key, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Value.ElementAt(j).Key, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.time, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.value, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.step, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.maxStep, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, FactoryDatas.ElementAt(i).Value.ElementAt(j).Value.buildCost, seek);
            }
        }

        return result;
    }
}
