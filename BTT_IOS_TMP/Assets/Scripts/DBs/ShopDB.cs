﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class ShopDB
{
    public class ShopMember
    {
        public int shopID { get; private set; }
        public string name { get; private set; }
        public int shopGroupID { get; private set; }
        public int level { get; private set; }
        public int rewardSlot { get; private set; }
        public int rewardGem { get; private set; }
        public int rewardShopID { get; private set; }
        public long rewardMoney { get; private set; }
        public int rewardHeart { get; private set; }
        public long rewardSecurityTime { get; private set; }
        public string itemKey { get; private set; }
        public int priceGem { get; private set; }
        public long priceMoney { get; private set; }
        public int priceAD { get; private set; }
        public long time { get; private set; }
        public int completeGem { get; private set; }

        public ShopMember
            (
             int _shopID,
         string _name,
         int _shopGroupID,
         int _level,
         int _rewardSlot,
         int _rewardGem,
         int _rewardShopID,
         long _rewardMoney,
         int _rewardHeart,
         long _rewardSecurityTime,
         string _itemKey,
         int _priceGem,
         long _priceMoney,
         int _priceAD,
         long _time,
         int _completeGem
            )
        {
            shopID = _shopID;
            name = _name;
            shopGroupID = _shopGroupID;
            level = _level;
            rewardSlot = _rewardSlot;
            rewardGem = _rewardGem;
            rewardShopID = _rewardShopID;
            rewardMoney = _rewardMoney;
            rewardHeart = _rewardHeart;
            rewardSecurityTime = _rewardSecurityTime;
            itemKey = _itemKey;
            priceGem = _priceGem;
            priceMoney = _priceMoney;
            priceAD = _priceAD;
            time = _time;
            completeGem = _completeGem;
        }
    }

    public Dictionary<int, ShopMember> Shops = new Dictionary<int, ShopMember>();
    public List<ShopMember> Gems = new List<ShopMember>();
    public List<ShopMember> Moneys = new List<ShopMember>();
    public List<ShopMember> Heart = new List<ShopMember>();
    public Dictionary<int, ShopMember> Inventory = new Dictionary<int, ShopMember>();
    public List<ShopMember> Assistatn = new List<ShopMember>();
    public Dictionary<int, ShopMember> Warehouse = new Dictionary<int, ShopMember>();
    public Dictionary<int, ShopMember> RND = new Dictionary<int, ShopMember>();
    public List<ShopMember> Security = new List<ShopMember>();
    public List<ShopMember> Box = new List<ShopMember>();
    public List<ShopMember> UnlockCity = new List<ShopMember>();
    public List<ShopMember> Booster = new List<ShopMember>();
    public int Version = -1;

    public const int SHOP_GROUP_GEM = 1;
    public const int SHOP_GROUP_MONEY = 2;
    public const int SHOP_GROUP_HEART = 3;
    public const int SHOP_GROUP_INVENTORY = 4;
    public const int SHOP_GROUP_ASSISTANT = 5;
    public const int SHOP_GROUP_WAREHOUSE = 6;
    public const int SHOP_GROUP_RND = 7;
    public const int SHOP_GROUP_SECURITY = 8;
    public const int SHOP_GROUP_BOX = 9;
    public const int SHOP_GROUP_UNLOCKCITY = 10;
    public const int SHOP_GROUP_Booster = 11;

    public const int PRICE_TYPE_MONEY = 0;
    public const int PRICE_TYPE_GEM = 1;
    public const int PRICE_TYPE_AD = 2;
    public const int PRICE_TYPE_COMPLETE_GEM = 3;

    public const int REWARD_TYPE_ITEM = 0;
    public const int REWARD_TYPE_RND_CARD = 1;

    //
    
    private static ShopDB _instance = null;
    public static ShopDB Instance
    {
        get
        {
            if (_instance == null) _instance = new ShopDB();

            return _instance;
        }
    }

    public ShopDB()
    {
        FileSavePath = Application.persistentDataPath + "/ShopDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Shops.Clear();
        Gems.Clear();
        Moneys.Clear();
        Heart.Clear();
        Inventory.Clear();
        Assistatn.Clear();
        Warehouse.Clear();
        RND.Clear();
        Security.Clear();
        Box.Clear();
        UnlockCity.Clear();
        Booster.Clear();

        Version = _json["dataVersion"].AsArray[10]["dataVersion"].AsInt;

        var _db = _json["shopDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            int _group = _db.AsArray[i]["shopGroupID"].AsInt;

            ShopMember _info = new ShopMember(
                _db.AsArray[i]["shopID"].AsInt,
                _db.AsArray[i]["name"].ToString().Replace("\"", ""),
                _db.AsArray[i]["shopGroupID"].AsInt,
                _db.AsArray[i]["level"].AsInt,
                _db.AsArray[i]["rewardSlot"].AsInt,
                _db.AsArray[i]["rewardGem"].AsInt,
                _db.AsArray[i]["rewardShopID"].AsInt,
                _db.AsArray[i]["rewardMoney"].AsLong,
                _db.AsArray[i]["rewardHeart"].AsInt,
                _db.AsArray[i]["rewardSecurityTime"].AsLong,
                _db.AsArray[i]["itemKey"].ToString().Replace("\"", ""),
                _db.AsArray[i]["priceGem"].AsInt,
                _db.AsArray[i]["priceMoney"].AsLong,
                _db.AsArray[i]["priceAD"].AsInt,
                _db.AsArray[i]["time"].AsLong,
                _db.AsArray[i]["completeGem"].AsInt
                );
            
            Shops.Add(_info.shopID, _info);
            switch(_group)
            {
                case SHOP_GROUP_GEM: Gems.Add(_info); break;
                case SHOP_GROUP_MONEY: Moneys.Add(_info); break;
                case SHOP_GROUP_HEART: Heart.Add(_info); break;
                case SHOP_GROUP_INVENTORY: Inventory.Add(_info.level, _info); break;
                case SHOP_GROUP_ASSISTANT: Assistatn.Add(_info); break;
                case SHOP_GROUP_WAREHOUSE: Warehouse.Add(_info.level, _info); break;
                case SHOP_GROUP_RND: RND.Add(_info.level, _info); break;
                case SHOP_GROUP_SECURITY: Security.Add(_info); break;
                case SHOP_GROUP_BOX: Box.Add(_info); break;
                case SHOP_GROUP_UNLOCKCITY: UnlockCity.Add(_info); break;
                case SHOP_GROUP_Booster: Booster.Add(_info); break;
            }
        }

        SaveFile();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log shop db : \n";
        for (int i = 0; i < Shops.Count; i++)
        {
            for(int j = 0; j<Shops.Count; j++)
                result += string.Format("{0}, {1}\n", Shops.ElementAt(i).Value.shopID, Shops.ElementAt(i).Value.name);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        Shops.Clear();
        Gems.Clear();
        Moneys.Clear();
        Heart.Clear();
        Inventory.Clear();
        Assistatn.Clear();
        Warehouse.Clear();
        RND.Clear();
        Security.Clear();
        Box.Clear();
        UnlockCity.Clear();
        Booster.Clear();

        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _shopID = 0;
        string _name = "";
        int _shopGroupID = 0;
        int _level = 0;
        int _rewardSlot = 0;
        int _rewardGem = 0;
        int _rewardShopID = 0;
        long _rewardMoney = 0;
        int _rewardHeart = 0;
        long _rewardSecurityTime = 0;
        string _itemKey = "";
        int _priceGem = 0;
        long _priceMoney = 0;
        int _priceAD = 0;
        long _time = 0;
        int _completeGem = 0;
        int str_len = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _shopID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            str_len = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _name = Encoding.UTF8.GetString(_data, seek, str_len); seek += str_len;
            _shopGroupID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _level = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardSlot = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardGem = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardShopID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardMoney = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _rewardHeart = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardSecurityTime = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            str_len = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _itemKey = Encoding.UTF8.GetString(_data, seek, str_len); seek += str_len;
            _priceGem = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _priceMoney = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _priceAD = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _time = BitConverter.ToInt64(_data, seek); seek += sizeof(long);
            _completeGem = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            ShopMember _info = 
                new ShopMember(
                _shopID,
                _name,
                _shopGroupID,
                _level,
                _rewardSlot,
                _rewardGem,
                _rewardShopID,
                _rewardMoney,
                _rewardHeart,
                _rewardSecurityTime,
                _itemKey,
                _priceGem,
                _priceMoney,
                _priceAD,
                _time,
                _completeGem
                );

            Shops.Add(_info.shopID, _info);
            switch (_info.shopGroupID)
            {
                case SHOP_GROUP_GEM: Gems.Add(_info); break;
                case SHOP_GROUP_MONEY: Moneys.Add(_info); break;
                case SHOP_GROUP_HEART: Heart.Add(_info); break;
                case SHOP_GROUP_INVENTORY: Inventory.Add(_info.level, _info); break;
                case SHOP_GROUP_ASSISTANT: Assistatn.Add(_info); break;
                case SHOP_GROUP_WAREHOUSE: Warehouse.Add(_info.level, _info); break;
                case SHOP_GROUP_RND: RND.Add(_info.level, _info); break;
                case SHOP_GROUP_SECURITY: Security.Add(_info); break;
                case SHOP_GROUP_BOX: Box.Add(_info); break;
                case SHOP_GROUP_UNLOCKCITY: UnlockCity.Add(_info); break;
                case SHOP_GROUP_Booster: Booster.Add(_info); break;
            }
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.Count);
        for (int i = 0; i < Shops.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.shopID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.name);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.shopGroupID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.level);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.rewardSlot);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.rewardGem);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.rewardShopID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.rewardMoney);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.rewardHeart);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.rewardSecurityTime);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.itemKey);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.priceGem);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.priceMoney);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.priceAD);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.time);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Shops.ElementAt(i).Value.completeGem);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, Shops.Count, seek);
        for (int i = 0; i < Shops.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.shopID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.name, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.shopGroupID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.level, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.rewardSlot, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.rewardGem, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.rewardShopID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.rewardMoney, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.rewardHeart, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.rewardSecurityTime, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.itemKey, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.priceGem, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.priceMoney, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.priceAD, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.time, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, Shops.ElementAt(i).Value.completeGem, seek);
        }

        return result;
    }
}