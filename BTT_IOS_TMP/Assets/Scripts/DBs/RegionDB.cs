﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class RegionDB
{
    public Dictionary<int, string> regions = new Dictionary<int, string>();
    public int Version = -1;

    private static RegionDB _instance = null;
    public static RegionDB Instance
    {
        get
        {
            if (_instance == null) _instance = new RegionDB();

            return _instance;
        }
    }

    public RegionDB()
    {
        FileSavePath = Application.persistentDataPath + "/RegionDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        regions.Clear();

        Version = _json["dataVersion"].AsArray[4]["dataVersion"].AsInt;

        var _db = _json["regionDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            regions.Add(_db.AsArray[i]["regionCode"].AsInt, _db.AsArray[i]["regionName"]);
        }

        SaveFile();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log Region db : \n";
        for (int i = 0; i < regions.Count; i++)
        {
            result += string.Format("{0}, {1}\n", regions.ElementAt(i).Key, regions.ElementAt(i).Value);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _code = 0;
        int _namelength = 0;
        string _name = "";

        for (int i = 0; i < _db_count; i++)
        {
            _code = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _namelength = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _name = Encoding.UTF8.GetString(_data, seek, _namelength); seek += _namelength;

            regions.Add(_code, _name);
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, regions.Count);
        for (int i = 0; i < regions.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, regions.ElementAt(i).Key);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, regions.ElementAt(i).Value);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, regions.Count, seek);
        for (int i = 0; i < regions.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, regions.ElementAt(i).Key, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, regions.ElementAt(i).Value, seek);
        }

        return result;
    }
}
