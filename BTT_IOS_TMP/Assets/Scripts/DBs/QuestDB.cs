﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class QuestDB
{
    public class QuestMember
    {
        public int questID { get; private set; }
        public int questType { get; private set; }
        public int reserveInt1 { get; private set; }
        public int reserveInt2 { get; private set; }
        public int rewardType { get; private set; }
        public int rewardAmount { get; private set; }
        public int skipIf { get; private set; }
        public int repeat { get; private set; }
        public string name
        {
            get
            {
                return Language.Str.QUESTNAMES(questID);
            }
        }

        public QuestMember
            (
            int _QuestID ,
            int _questType ,
            int _reserveInt1 ,
            int _reserveInt2 ,
            int _rewardType ,
            int _rewardAmount ,
            int _skipIf ,
            int _repeat
            )
        {
            questID = _QuestID;
            questType = _questType;
            reserveInt1 = _reserveInt1;
            reserveInt2 = _reserveInt2;
            rewardType = _rewardType;
            rewardAmount = _rewardAmount;
            skipIf = _skipIf;
            repeat = _repeat;
        }
    }

    public Dictionary<int, QuestMember> QuestDatas = new Dictionary<int, QuestMember>();
    public List<QuestMember> QuestList = new List<QuestMember>();
    public int Version = -1;

    public const int QUESTTYPE_FACTORY_UPGRADE = 1;
    public const int QUESTTYPE_TRAVEL = 2;
    public const int QUESTTYPE_BUY_BOOST = 3;
    public const int QUESTTYPE_BUY_ASSISTANT = 4;
    public const int QUESTTYPE_MARKET_BUY = 5;
    public const int QUESTTYPE_MARKET_SELL = 6;
    public const int QUESTTYPE_LEVELUP = 7;
    public const int QUESTTYPE_FACTORY_OPEN = 8;
    public const int QUESTTYPE_BUY_MANAGER = 9;
    public const int QUESTTYPE_INVENTORY_UPGRADE = 10;
    public const int QUESTTYPE_TRAVEL_UNLOCK = 11;
    public const int QUESTTYPE_RND_UPGRADE = 12;
    public const int QUESTTYPE_RND_UNLOCK = 13;

    private static QuestDB _instance = null;
    public static QuestDB Instance
    {
        get
        {
            if (_instance == null) _instance = new QuestDB();

            return _instance;
        }
    }

    public QuestDB()
    {
        FileSavePath = Application.persistentDataPath + "/QuestDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        QuestDatas.Clear();
        QuestList.Clear();

        Version = _json["dataVersion"].AsArray[15]["dataVersion"].AsInt;

        var _db = _json["questDB"];

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            QuestMember _info = new QuestMember(
                _db.AsArray[i]["questID"].AsInt,
                _db.AsArray[i]["questType"].AsInt,
                _db.AsArray[i]["reserveInt1"].AsInt,
                _db.AsArray[i]["reserveInt2"].AsInt,
                _db.AsArray[i]["rewardType"].AsInt,
                _db.AsArray[i]["rewardAmount"].AsInt,
                _db.AsArray[i]["skipIf"].AsInt,
                _db.AsArray[i]["repeat"].AsInt
                );

            QuestDatas.Add(_info.questID, _info);
            QuestList.Add(_info);
        }

        SaveFile();
        LogAll();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log Quest db : \n";
        for (int i = 0; i < QuestDatas.Count; i++)
        {
            result += string.Format("{0}, {1}, {2}, {3}, {4}, {5}\n",
                QuestDatas.ElementAt(i).Value.questID,
                QuestDatas.ElementAt(i).Value.questType,
                QuestDatas.ElementAt(i).Value.reserveInt1,
                QuestDatas.ElementAt(i).Value.reserveInt2,
                QuestDatas.ElementAt(i).Value.rewardType,
                QuestDatas.ElementAt(i).Value.rewardAmount);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _QuestID = 0;
        int _questType = 0;
        int _reserveInt1 = 0;
        int _reserveInt2 = 0;
        int _rewardType = 0;
        int _rewardAmount = 0;
        int _skipIf = 0;
        int _repeat = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _QuestID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _questType = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _reserveInt1 = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _reserveInt2 = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardType = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _rewardAmount = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _skipIf = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _repeat = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

            QuestDatas.Add(_QuestID,
                new QuestMember(
                    _QuestID,
                    _questType,
                    _reserveInt1,
                    _reserveInt2,
                    _rewardType,
                    _rewardAmount,
                    _skipIf,
                    _repeat
                    )
                    );
        }

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.Count);
        for (int i = 0; i < QuestDatas.Count; i++)
        {
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.questID);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.questType);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.reserveInt1);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.reserveInt2);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.rewardType);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.rewardAmount);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.skipIf);
            stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, QuestDatas.ElementAt(i).Value.repeat);
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.Count, seek);
        for (int i = 0; i < QuestDatas.Count; i++)
        {
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.questID, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.questType, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.reserveInt1, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.reserveInt2, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.rewardType, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.rewardAmount, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.skipIf, seek);
            seek = NetworkManager.Instance.AddByteStreamData(result, QuestDatas.ElementAt(i).Value.repeat, seek);
        }

        return result;
    }
}
