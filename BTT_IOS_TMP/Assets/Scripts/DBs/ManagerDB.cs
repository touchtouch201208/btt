﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

public class ManagerDB
{
    public class ManagerMember
    {
        public int factoryID { get; private set; }
        public int level { get; private set; }
        public long priceMoney { get; private set; }
        private Sprite _img = null;
        public Sprite Img
        {
            get
            {
                if(_img == null)
                    _img = Resources.Load<Sprite>(string.Format("UI/Managers/managers_{0}_{1}", factoryID, level));
                return _img;
            }
        }

        public ManagerMember
            (int _factoryID,
            int _level,
            long _priceMoney
            )
        {
            factoryID = _factoryID;
            level = _level;
            priceMoney = _priceMoney;
        }
    }

    public Dictionary<int, List<ManagerMember>> Managers = new Dictionary<int, List<ManagerMember>>();
    public List<ManagerMember> Managerlist = new List<ManagerMember>();
    public int Version = -1;

    private static ManagerDB _instance = null;
    public static ManagerDB Instance
    {
        get
        {
            if (_instance == null) _instance = new ManagerDB();

            return _instance;
        }
    }

    public ManagerDB()
    {
        FileSavePath = Application.persistentDataPath + "/ManagerDB.dat";
        LoadFile();
    }

    public void InitDB(JSONNode _json)
    {
        Managers.Clear();
        
        Version = _json["dataVersion"].AsArray[11]["dataVersion"].AsInt;

        var _db = _json["managerDB"];

        int _factoryID = 0;
        int _level = 0;
        long _priceMoney = 0;

        for (int i = 0; i < _db.AsArray.Count; i++)
        {
            _factoryID = _db.AsArray[i]["factoryID"].AsInt;
            _level = _db.AsArray[i]["level"].AsInt;
            _priceMoney = _db.AsArray[i]["priceMoney"].AsLong;

            var _node = new ManagerMember(
                    _factoryID,
                    _level,
                    _priceMoney
                    );

            if (!Managers.ContainsKey(_factoryID)) Managers.Add(_factoryID, new List<ManagerMember>());
            Managers[_factoryID].Add(_node);
            Managerlist.Add(_node);
        }

        for(int i = 0; i<Managers.Count; i++)
        {
            var _node = Managers.ElementAt(i);
            Managers[_node.Key] = Managers[_node.Key].OrderBy(x => x.level).ToList();
        }
        //Managerlist = Managerlist.OrderBy(x => x.level).ThenBy(x=>x.factoryID).ToList();
        //Managerlist = Managerlist.OrderBy(x => x.level).ToList();

        SaveFile();
        LogAll();
    }

    private void LogAll()
    {
#if UNITY_EDITOR
        string result = "log Manager db : \n";
        for (int i = 0; i < Managerlist.Count; i++)
        {
            result += string.Format("{0}, {1}\n", Managerlist[i].factoryID, Managerlist[i].level);
        }
        Debug.Log(result);
#endif
    }

    //---------------------------------
    // * fuction file

    private string FileSavePath = "";

    public void LoadFile()
    {
        if (File.Exists(FileSavePath))
        {
            var b = new BinaryFormatter();
            var f = File.Open(FileSavePath, FileMode.Open);
            object obj = b.Deserialize(f);
            f.Close();

            LoadDataFromStream((byte[])obj);
        }
    }

    public void SaveFile()
    {
        var b = new BinaryFormatter();
        var f = File.Open(FileSavePath, FileMode.OpenOrCreate);

        b.Serialize(f, GetDataStream());
        f.Close();
    }

    //---------------------------------
    // * fuction stream

    public void LoadDataFromStream(byte[] _data)
    {
        int seek = 0;

        //---------------------

        Version = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _db_count = BitConverter.ToInt32(_data, seek); seek += sizeof(int);

        int _factoryID = 0;
        int _level = 0;
        long _priceMoney = 0;

        for (int i = 0; i < _db_count; i++)
        {
            _factoryID = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _level = BitConverter.ToInt32(_data, seek); seek += sizeof(int);
            _priceMoney = BitConverter.ToInt64(_data, seek); seek += sizeof(long);

            var _node = new ManagerMember(_factoryID, _level, _priceMoney);
            if (!Managers.ContainsKey(_factoryID)) Managers.Add(_factoryID, new List<ManagerMember>());
            Managers[_factoryID].Add(_node);
            Managerlist.Add(_node);
        }

        for (int i = 0; i < Managers.Count; i++)
        {
            var _node = Managers.ElementAt(i);
            Managers[_node.Key] = Managers[_node.Key].OrderBy(x => x.level).ToList();
        }
        //Managerlist = Managerlist.OrderBy(x => x.level).ThenBy(x => x.factoryID).ToList();
        //Managerlist = Managerlist.OrderBy(x => x.level).ToList();

        SaveFile();
    }

    public byte[] GetDataStream()
    {
        int stream_size = 0;

        int _count = 0;
        for (int i = 0; i < Managers.Count; i++) _count += Managers.ElementAt(i).Value.Count;

        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, Version);
        stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _count);
        for (int i = 0; i < Managers.Count; i++)
        {
            var _list = Managers.ElementAt(i).Value;
            for(int j = 0; j< _list.Count; j++)
            {
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].factoryID);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].level);
                stream_size = NetworkManager.Instance.AddbyteStreamSize(stream_size, _list[j].priceMoney);
            }
        }

        //------------------------------------
        //------------------------------------

        byte[] result = new byte[stream_size];

        int seek = 0;

        seek = NetworkManager.Instance.AddByteStreamData(result, Version, seek);
        seek = NetworkManager.Instance.AddByteStreamData(result, _count, seek);
        for (int i = 0; i < Managers.Count; i++)
        {
            var _list = Managers.ElementAt(i).Value;
            for (int j = 0; j < _list.Count; j++)
            {
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].factoryID, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].level, seek);
                seek = NetworkManager.Instance.AddByteStreamData(result, _list[j].priceMoney, seek);
            }
        }

        return result;
    }
}
