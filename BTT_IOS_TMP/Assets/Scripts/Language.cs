﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System;

public class Language
{
    //public enum LanguageCode { EN = 0, KR, FR, DE, BR, RU, VI, ES, ID, TH, JP, CHS, CHT };
    public enum LanguageCode { EN = 0, DE, FR, ES, BR, RU, KR, CHS, CHT, JP, ID, TH, VI };
    public static LanguageCode Code = LanguageCode.EN;

    public static Language instance = new Language();
    public static LanguagePackBase Str;

    private Dictionary<LanguageCode, LanguagePackBase> Languages;

    Language()
    {
        Languages = new Dictionary<LanguageCode, LanguagePackBase>();

#if UNITY_EDITOR

        if (string.IsNullOrEmpty(PlayerPrefs.GetString("Language")))
            SetLanguega(LanguageCode.KR);
        else SetLanguega((LanguageCode)Enum.Parse(typeof(LanguageCode), PlayerPrefs.GetString("Language")));
#else

        if (string.IsNullOrEmpty(PlayerPrefs.GetString("Language")))
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.French: Code = LanguageCode.FR; break;
                case SystemLanguage.German: Code = LanguageCode.DE; break;
                case SystemLanguage.Portuguese: Code = LanguageCode.BR; break;
                case SystemLanguage.Russian: Code = LanguageCode.RU; break;
                case SystemLanguage.Vietnamese: Code = LanguageCode.VI; break;
                case SystemLanguage.Spanish: Code = LanguageCode.ES; break;
                case SystemLanguage.Indonesian: Code = LanguageCode.ID; break;
                case SystemLanguage.Thai: Code = LanguageCode.TH; break;
                case SystemLanguage.Japanese: Code = LanguageCode.JP; break;
                case SystemLanguage.ChineseSimplified: Code = LanguageCode.CHS; break;
                case SystemLanguage.ChineseTraditional: Code = LanguageCode.CHT; break;
                case SystemLanguage.Korean: Code = LanguageCode.KR; break;
                case SystemLanguage.English:
                default: Code = LanguageCode.EN; break;
            }

            SetLanguega(Code);
        }
        else SetLanguega((LanguageCode)Enum.Parse(typeof(LanguageCode), PlayerPrefs.GetString("Language")));
#endif
    }

    public void SetLanguega(LanguageCode _lan)
    {
        Code = _lan;
        PlayerPrefs.SetString("Language", Code.ToString());

        if(!Languages.ContainsKey(_lan))
        {
            switch(_lan)
            {
                case LanguageCode.EN:
                default:
                    Languages.Add(_lan, new LanguagePackEN());
                    break;

                case LanguageCode.KR: Languages.Add(_lan, new LanguagePackKO()); break;
                case LanguageCode.FR: Languages.Add(_lan, new LanguagePackFR()); break;
                case LanguageCode.DE: Languages.Add(_lan, new LanguagePackDE()); break;
                case LanguageCode.BR: Languages.Add(_lan, new LanguagePackBR()); break;
                case LanguageCode.RU: Languages.Add(_lan, new LanguagePackRU()); break;
                case LanguageCode.VI: Languages.Add(_lan, new LanguagePackVI()); break;
                case LanguageCode.ES: Languages.Add(_lan, new LanguagePackES()); break;
                case LanguageCode.ID: Languages.Add(_lan, new LanguagePackID()); break;
                case LanguageCode.TH: Languages.Add(_lan, new LanguagePackTH()); break;
                case LanguageCode.JP: Languages.Add(_lan, new LanguagePackJP()); break;
                case LanguageCode.CHS: Languages.Add(_lan, new LanguagePackCHS()); break;
                case LanguageCode.CHT: Languages.Add(_lan, new LanguagePackCHT()); break;
            }
        }

        Str = Languages[_lan];
    }

    public static string GetLanguageName(LanguageCode _lan)
    {
        switch (_lan)
        {
            case LanguageCode.EN: return "English";
            case LanguageCode.KR: return "한국어";
            case LanguageCode.FR: return "Français ";
            case LanguageCode.DE: return "Deutsch";
            case LanguageCode.BR: return "Português";
            case LanguageCode.RU: return "русский";
            case LanguageCode.VI: return "Tiếng Việt";
            case LanguageCode.ES: return "Español";
            case LanguageCode.ID: return "Bahasa Indonesia";
            case LanguageCode.TH: return "ภาษาไทย";
            case LanguageCode.JP: return "日本語";
            case LanguageCode.CHS: return "簡體中文";
            case LanguageCode.CHT: return "繁體中文";
            default: return "";
        }
    }

    public static int GetLanguageCountryID(LanguageCode _lan)
    {
        switch (_lan)
        {
            case LanguageCode.EN: return 1;
            case LanguageCode.KR: return 8;
            case LanguageCode.FR: return 20;
            case LanguageCode.DE: return 22;
            case LanguageCode.BR: return 27;
            case LanguageCode.RU: return 23;
            case LanguageCode.VI: return 17;
            case LanguageCode.ES: return 21;
            case LanguageCode.ID: return 14;
            case LanguageCode.TH: return 16;
            case LanguageCode.JP: return 9;
            case LanguageCode.CHS: return 10;
            case LanguageCode.CHT: return 10;
            default: return 1;
        }
    }
}
