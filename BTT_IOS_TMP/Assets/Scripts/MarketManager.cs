﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SimpleJSON;

public class MarketManager : MonoBehaviour {

    public Sprite Source_BG_Inven;
    public Sprite Source_BG_Market;

    public Sprite Source_IconBG_Inven;
    public Sprite Source_IconBG_Market;

    public Sprite Source_PriceIcon_Up1;
    public Sprite Source_PriceIcon_Up2;
    public Sprite Source_PriceIcon_Down1;
    public Sprite Source_PriceIcon_Down2;

    public Sprite Source_UnlockBtn_Off;
    public Sprite Source_UnlockBtn_On;

    public Sprite Source_Sale_Off;
    public Sprite Source_Sale_Bonus;

    [Header("Base Objects")]
    public GameObject MainBase;
    public GameObject TravelBase;
    public GameObject MarketBase;

    public GameObject BtnTravel;
    public GameObject BtnMarket;
    public GameObject BtnMarketExit;
    public GameObject BtnMarketBack;
    public GameObject BtnInventory;
    public GameObject BtnMenu;

    //--------------------------------
    [Header("Market Home Base")]
    public RectTransform MarketHomeBase;
    public Text MarketHomeName;
    public Button MarketHomeBtnAll;
    public Button MarketHomeBtnSale;
    private UICityButtonNodes[] MarketHomeBtns;
    private bool MarketHomeState = true;
    private int MarketHomeSortState = -1;
    private const int MARKET_HOME_SORT_GROUP_1 = 0;
    private const int MARKET_HOME_SORT_GROUP_2 = 1;
    private const int MARKET_HOME_SORT_GROUP_3 = 2;
    private const int MARKET_HOME_SORT_GROUP_4 = 3;
    private const int MARKET_HOME_SORT_GROUP_5 = 4;
    private const int MARKET_HOME_SORT_GROUP_6 = 5;
    private const int MARKET_HOME_SORT_GROUP_7 = 6;
    private const int MARKET_HOME_SORT_GROUP_8 = 7;
    private const int MARKET_HOME_SORT_ALL = 10;
    public const int MARKET_HOME_SORT_SALE = 11;

    [Header("Market Objects")]
    public RectTransform MarketScrollRoot;
    private UIScrollNodeProduct[] MarketScrollNodes;
    private int MarketScrollPage = 0;
    public Button MarketScrollBtnSortGroup;
    public Button MarketScrollBtnSortName;
    public Button MarketScrollBtnSortPrice;
    public Button MarektScrollBtnUnlock;
    public InputField MarketScrollSerch;

    //--------------------------------
    private int CurrentCity
    {
        get { return NetworkManager.Instance.mUserData.currentCityCode; }
        set { NetworkManager.Instance.mUserData.currentCityCode = value; }
    }
    
    [Header("Travel Base")]
    public RectTransform CanvasRoot;
    public RectTransform TravelMap;
    public RectTransform TravelMapImg;
    public Transform TravelUseHeartImg;
    public GameObject TravelUnlockGaugeBase;
    public Image TravelUnlockPointGague;
    public RectTransform TravelUnlockPointIcon;
    public GameObject TravelUnlockPointEffect;
    public Button TravelUnlockPointButton;
    private UICityButtonNodes[] TravelCityButtons;
    private Dictionary<int, UICityButtonNodes> TravelCityBtnMap = new Dictionary<int, UICityButtonNodes>();
    public RectTransform Airplain;
    private int TravelNowPage = 0;
    private Vector3 TravelScrollTouchRoot = Vector3.zero;
    private float TravelScrollVal = 0;
    private Vector2 TravelScrollPageDepth;
    private Vector2[] TravelAreaPositions = new Vector2[3];

    //--------------------------------

    [Header("Travel Unlock Popup")]
    public GameObject TravelPopupBase;
    public Image TravelPopupCityImg;
    public Text TravelPopupName;
    public Text TravelPopupMessage;
    public Text TravelPopupLevelMessage;
    public Text TravelPopupInfoLow;
    public Text TravelPopupInfoHigh;
    public Button TravelPopupBtnUnlockMoney;
    public Button TravelPopupBtnUnlockGem;
    public Button TravelPopupBtnCancel;

    //--------------------------------

    [Header("Travel Animation")]
    public GameObject TravelAnimationBase;
    public RectTransform TravelAniAirplain;
    public RectTransform TravelAniAirplainFrom;
    public RectTransform TravelAniAirplainTo;
    public RectTransform TravelAniFrom;
    public Image TravelAniFromImg;
    public Text TravelAniFromName;
    public RectTransform TravelAniTo;
    public Image TravelAniToImg;
    public Text TravelAniToName;

    //--------------------------------

    [Header("Market Popups")]
    public GameObject SellPopup;
    private Slider SellAmountSlide;
    private Text SellLabelCurrent;
    private Text SellLabelPrice;
    private Image SellImgSale;
    private Text SellLabelProductRootPrice;
    private Text SellLabelProductCurrentPrice;
    private Text SellLabelProductCurrentBoost;
    private UIItemIcon SellItemIcon;
    private Text SellItemName;
    private Button SellBtnConform;
    private Button SellBtnCancel;
    private int SellCurrentIdx = -1;

    public GameObject BuyPopup;
    [HideInInspector]
    public Slider BuyAmountSlide;
    private Text BuyLabelCurrent;
    private Text BuyLabelPrice;
    private Image BuyImgSale;
    private Text BuyLabelProductRootPrice;
    private Text BuyLabelProductCurrentPrice;
    private Text BuyLabelProductCurrentBoost;
    private UIItemIcon BuyItemIcon;
    private Text BuyItemName;
    private Button BuyBtnConform;
    private Button BuyBtnCancel;
    private int BuyCurrentIdx = -1;

    [Header("Burglary Popup")]
    public GameObject BurglaryPopup;
    public Text BurglaryLabel;

    [Header("GreatOffer Popup")]
    public GameObject GreatOfferPopup;

    private Dictionary<int, UserData.Product> MarketEnterProducts = new Dictionary<int, UserData.Product>();

    private DateTime GreatOfferTime = DateTime.MinValue;
    private int GreatOfferCount = 0;

    public static MarketManager Instance = null;

    void Awake()
    {
        Instance = this;

        MarketHomeBtns = MarketHomeBase.transform.GetComponentsInChildren<UICityButtonNodes>();
        for(int i = 0; i<MarketHomeBtns.Length; i++)
        {
            int _idx = i;
            MarketHomeBtns[i].transform.Find("GaugeBG").Find("Gauge").GetComponent<Image>().color
                 = ItemDB.Instance.GetGroupColor(MarketHomeBtns[i].CityID);
            MarketHomeBtns[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                SetMarketHomeSortState(MarketHomeBtns[_idx].CityID);
            });
        }

        MarketHomeBtnAll.onClick.AddListener(delegate { SetMarketHomeSortState(MARKET_HOME_SORT_ALL); });
        MarketHomeBtnSale.onClick.AddListener(delegate { SetMarketHomeSortState(MARKET_HOME_SORT_SALE); });

        MarketScrollNodes = MarketScrollRoot.GetComponentsInChildren<UIScrollNodeProduct>();

        for (int i = 0; i < MarketScrollNodes.Length; i++)
        {
            int idx = i;
            MarketScrollNodes[i].transform.Find("BtnSell").GetComponent<Button>().onClick.AddListener(delegate
            {
                SellCurrentIdx = idx;

                var _product = UserData.Instance.InventoryProducts[MarketScrollNodes[idx].ProductID];
                SellItemIcon.SetItem(_product.ID);
                SellItemName.text = _product.name;
                SellAmountSlide.maxValue = _product.amount;
                SellAmountSlide.value = _product.amount;
                OnSellSlideValueChange();

                int _group_id = ItemDB.Instance.ItemDatas[_product.ID].itemGroupID;
                var _boostlevel = UserData.Instance.Boosters[_group_id];
                var _node = UserData.Instance.CurrentMarketAllInfos[MarketScrollNodes[idx].ProductID];

                SellLabelProductRootPrice.text = "$" + new Money(_node.CurrentPrice.ToString()).ToWon();
                if (_boostlevel > 0)
                {
                    SellImgSale.gameObject.SetActive(true);
                    SellImgSale.sprite = _node.CurrentPrice > _node.OriginPrice ? Source_Sale_Bonus : Source_Sale_Off;
                    SellLabelProductCurrentPrice.text = "$" + new Money(_node.BoostPrice.ToString()).ToWon();
                    SellLabelProductCurrentBoost.text =
                    string.Format("{0}({1}% {2})",
                    Language.Str.BOOST,
                    BoosterDB.Instance.Boosters[ItemDB.Instance.ItemDatas[_product.ID].itemGroupID][_boostlevel - 1].percentage,
                    _node.CurrentPrice > _node.OriginPrice ? Language.Str.BONUS : Language.Str.OFF);
                    SellLabelProductRootPrice.transform.Find("Img").gameObject.SetActive(true);
                }
                else
                {
                    SellImgSale.gameObject.SetActive(false);
                    SellLabelProductRootPrice.transform.Find("Img").gameObject.SetActive(false);
                }
                
                SellPopup.SetActive(true);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
            });

            MarketScrollNodes[i].transform.Find("BtnBuy").GetComponent<Button>().onClick.AddListener(delegate
            {
                if(UserData.Instance.isInventoryFull())
                {
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_INVENTORY_FULL);
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                }
                else
                {
                    BuyCurrentIdx = idx;

                    int _id = MarketScrollNodes[idx].ProductID;
                    BuyItemIcon.SetItem(_id);
                    BuyItemName.text = ItemDB.Instance.ItemDatas[_id].itemName;

                    int _group_id = ItemDB.Instance.ItemDatas[_id].itemGroupID;
                    var _boostlevel = UserData.Instance.Boosters[_group_id];
                    var _node = UserData.Instance.CurrentMarketAllInfos[_id];

                    int _left_count = UserData.Instance.GetInventoryLeftCount();
                    long _buy_max_tmp = UserData.Instance.TotalMoney.GetInt64() / _node.BoostPrice;
                    int _buy_max = _buy_max_tmp > int.MaxValue ? int.MaxValue : (int)_buy_max_tmp;
                    BuyAmountSlide.maxValue = _left_count < _buy_max ? _left_count : _buy_max;
                    BuyAmountSlide.value = 0;
                    OnBuySlideValueChange();

                    BuyLabelProductRootPrice.text = "$" + new Money(_node.CurrentPrice.ToString()).ToWon();
                    if (_boostlevel > 0)
                    {
                        BuyImgSale.gameObject.SetActive(true);
                        BuyImgSale.sprite = _node.CurrentPrice > _node.OriginPrice ? Source_Sale_Bonus : Source_Sale_Off;
                        BuyLabelProductCurrentPrice.text = "$" + new Money(_node.BoostPrice.ToString()).ToWon();
                        BuyLabelProductCurrentBoost.text =
                        string.Format("{0}({1}% {2})",
                        Language.Str.BOOST,
                        BoosterDB.Instance.Boosters[ItemDB.Instance.ItemDatas[_id].itemGroupID][_boostlevel - 1].percentage,
                        _node.CurrentPrice > _node.OriginPrice ? Language.Str.BONUS : Language.Str.OFF);
                        BuyLabelProductRootPrice.transform.Find("Img").gameObject.SetActive(true);
                    }
                    else
                    {
                        BuyImgSale.gameObject.SetActive(false);
                        BuyLabelProductRootPrice.transform.Find("Img").gameObject.SetActive(false);
                    }

                    BuyPopup.SetActive(true);
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
                }
            });
        }

        MarketScrollBtnSortGroup.onClick.AddListener(MarketSortByGroup);
        MarketScrollBtnSortName.onClick.AddListener(MarketSortByName);
        MarketScrollBtnSortPrice.onClick.AddListener(MarketSortByPrice);
        MarketScrollSerch.onValueChanged.AddListener(delegate(string _val)
        {
            MarketScrollPage = 0;
            SetMarketProductScrollByName();
        });

        MarektScrollBtnUnlock.onClick.AddListener(delegate
        {
            _current_list_available = _current_list_available == UserData.AVAILABLE ? UserData.UNAVAILABLE : UserData.AVAILABLE;
            SetMarketProductAvailableList();
        });

        //-----------------------------

        var _sell_base = SellPopup.transform.Find("Base");
        SellAmountSlide = _sell_base.transform.Find("Slider").GetComponent<Slider>();
        SellLabelCurrent = SellAmountSlide.handleRect.transform.GetChild(0).GetComponent<Text>();
        var _sell_price_bg = _sell_base.transform.Find("PriceBG");
        SellLabelPrice = _sell_price_bg.transform.Find("Price").GetComponent<Text>();
        SellLabelProductRootPrice = _sell_base.transform.Find("RootPrice").GetComponent<Text>();
        SellImgSale = SellLabelProductRootPrice.transform.Find("Sale").GetComponent<Image>();
        SellLabelProductCurrentPrice = SellImgSale.transform.Find("CurrentPrice").GetComponent<Text>();
        SellLabelProductCurrentBoost = SellImgSale.transform.Find("BoostLabel").GetComponent<Text>();
        SellItemIcon = _sell_base.transform.Find("Img").GetComponent<UIItemIcon>();
        SellItemName = _sell_base.transform.Find("Name").GetComponent<Text>();
        SellBtnConform = _sell_base.transform.Find("BtnSell").GetComponent<Button>();
        SellBtnCancel = _sell_base.transform.Find("BtnCancel").GetComponent<Button>();

        var _buy_base = BuyPopup.transform.Find("Base");
        BuyAmountSlide = _buy_base.transform.Find("Slider").GetComponent<Slider>();
        BuyLabelCurrent = BuyAmountSlide.handleRect.transform.GetChild(0).GetComponent<Text>();
        var _buy_price_bg = _buy_base.transform.Find("PriceBG");
        BuyLabelPrice = _buy_price_bg.transform.Find("Price").GetComponent<Text>();
        BuyLabelProductRootPrice = _buy_base.transform.Find("RootPrice").GetComponent<Text>();
        BuyImgSale = BuyLabelProductRootPrice.transform.Find("Sale").GetComponent<Image>();
        BuyLabelProductCurrentPrice = BuyImgSale.transform.Find("CurrentPrice").GetComponent<Text>();
        BuyLabelProductCurrentBoost = BuyImgSale.transform.Find("BoostLabel").GetComponent<Text>();
        BuyItemIcon = _buy_base.transform.Find("Img").GetComponent<UIItemIcon>();
        BuyItemName = _buy_base.transform.Find("Name").GetComponent<Text>();
        BuyBtnConform = _buy_base.transform.Find("BtnBuy").GetComponent<Button>();
        BuyBtnCancel = _buy_base.transform.Find("BtnCancel").GetComponent<Button>();

        SellAmountSlide.onValueChanged.AddListener(delegate 
        {
            OnSellSlideValueChange();
        });

        BuyAmountSlide.onValueChanged.AddListener(delegate
        {
            OnBuySlideValueChange();
        });

        SellBtnConform.onClick.AddListener(delegate
        {
            if(SellAmountSlide.value > 0)
            {
                int _item_id = MarketScrollNodes[SellCurrentIdx].ProductID;
                var _product = UserData.Instance.InventoryProducts[_item_id];
                int _amount = (int)SellAmountSlide.value;

                if(_amount > 0)
                {
                    Money _sell_price = new Money(UserData.Instance.CurrentMarketAllInfos[_item_id].BoostPrice.ToString());
                    _sell_price.MulMoney(_amount);
                    MainScene.Instance.OnGetMoney(_sell_price);
                    UserData.Instance.SubProductInventory(_item_id, _amount);
                    
                    //get exp
                    if (MarketEnterProducts.ContainsKey(_item_id))
                    {
                        long _root_price = (MarketEnterProducts[_item_id].totalPrice / MarketEnterProducts[_item_id].amount) * _amount;
                        long _exp = (UserData.Instance.CurrentMarketAllInfos[_item_id].CurrentPrice * _amount) - _root_price;

                        if(_exp > 0) MainScene.Instance.GetEXP(_exp);

                        if (_amount >= MarketEnterProducts[_item_id].amount) MarketEnterProducts.Remove(_item_id);
                        else
                        {
                            MarketEnterProducts[_item_id].totalPrice =
                            (MarketEnterProducts[_item_id].totalPrice / MarketEnterProducts[_item_id].amount)
                            * (MarketEnterProducts[_item_id].amount - _amount);
                            MarketEnterProducts[_item_id].amount -= _amount;
                        }
                        UserData.Instance.OnTradeItem(ItemDB.Instance.ItemDatas[_item_id].itemGroupID);
                    }

                    RNDCenterManager.Instance.SetUnlockItemBtnUI(ItemDB.Instance.ItemDatas[_item_id].itemGroupID);
                    QuestList.Instance.OnMarketSell(_item_id, ItemDB.Instance.ItemDatas[_item_id].itemGroupID);

                    SetMarketProductScroll();
                    WareHouseManager.Instance.UpdateMainAmount();
                    SellPopup.SetActive(false);

                    BackToMarketHome();
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SELL);

                    UIEffectManager.Instance.SetEffect(
                        3,
                        SellBtnConform.transform,
                        MainScene.Instance.Txt_TotalMoney.transform,
                        -1);
                }
            }
        });
        SellBtnCancel.onClick.AddListener(delegate
        {
            SellPopup.SetActive(false);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
        });

        BuyBtnConform.onClick.AddListener(delegate
        {
            if (!UserData.Instance.isInventoryFull()
                && BuyAmountSlide.value > 0)
            {
                int _id = MarketScrollNodes[BuyCurrentIdx].ProductID;
                int _amount = (int)BuyAmountSlide.value;

                if(_amount > 0)
                {
                    Money _buy_price = new Money(UserData.Instance.CurrentMarketInfos[_id].BoostPrice.ToString());
                    _buy_price.MulMoney(_amount);
                    if (UserData.Instance.TotalMoney.CompareMoney(_buy_price))
                    {
                        MainScene.Instance.OnUseMoney(_buy_price);
                        UserData.Instance.AddProductInventory(
                            _id,
                            _amount,
                            (int)UserData.Instance.CurrentMarketInfos[_id].CurrentPrice);

                        SetMarketProductScroll();
                        WareHouseManager.Instance.UpdateMainAmount();
                        BuyPopup.SetActive(false);

                        UIEffectManager.Instance.SetEffect(
                            _amount,
                            BuyBtnConform.transform,
                            WareHouseManager.Instance.InventoryImgRoot,
                            _id
                        );

                        RNDCenterManager.Instance.SetUnlockItemBtnUI(ItemDB.Instance.ItemDatas[_id].itemGroupID);
                        QuestList.Instance.OnMarketBuy(_id, ItemDB.Instance.ItemDatas[_id].itemGroupID);

                        BackToMarketHome();
                        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUY);
                    }
                    else MainScene.Instance.SetPopupToShop(ShopDB.PRICE_TYPE_MONEY);
                }
            }
        });
        BuyBtnCancel.onClick.AddListener(delegate
        {
            BuyPopup.SetActive(false);
            SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
        });

        //---------------------------------

        TravelCityButtons = TravelMap.GetComponentsInChildren<UICityButtonNodes>(true);
        for (int i = 0; i < TravelCityButtons.Length; i++)
        {
            int _idx = i;
            TravelCityButtons[i].GetComponent<Button>().onClick.AddListener(delegate
            {
                int _id = TravelCityButtons[_idx].CityID;
                if(UserData.Instance.CityInfos[_id] == UserData.AVAILABLE)
                {
                    MoveToCity(_id);
                }
                else
                {
                    OpenTravelPopup(_id);
                }
            });
            TravelCityBtnMap.Add(TravelCityButtons[i].CityID, TravelCityButtons[i]);
        }

        TravelUnlockPointButton.onClick.AddListener(delegate
        {
            if (!MainScene.Instance.OpenUnlockCity())
                PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_UNLOCK_ALL);
        });

        //---------------------------------

        TravelPopupBtnUnlockMoney.onClick.AddListener(delegate { UnlockCityWithMoney(); });
        TravelPopupBtnUnlockGem.onClick.AddListener(delegate { UnlockCityWithGem(); });
        TravelPopupBtnCancel.onClick.AddListener(delegate { ExitTravelPopup(); });

        //---------------------------------

        ItemGroupColors.Add(0, new Color(1.0f,0.6f,0.11f)); // 
        ItemGroupColors.Add(1, new Color(1.0f, 0.75f, 1.0f)); // 
        ItemGroupColors.Add(2, new Color(0.75f, 0.75f, 0.75f)); // 
        ItemGroupColors.Add(3, new Color(0.75f, 0.75f, 1.0f)); // 
        ItemGroupColors.Add(4, new Color(0.5f, 0.75f, 1.0f)); // 
        ItemGroupColors.Add(5, new Color(0.75f, 1.0f, 0.5f)); // 
        ItemGroupColors.Add(6, new Color(0.75f, 1.0f, 0.75f)); // 
        ItemGroupColors.Add(7, new Color(1.0f, 0.75f, 0.5f)); // 
        ItemGroupColors.Add(8, new Color(1.0f, 0.75f, 0.75f)); // 

        //---------------------------------

        TravelAreaPositions[0] = new Vector2(1130, -85); // america
        TravelAreaPositions[1] = new Vector2(-120, -350); // europe
        TravelAreaPositions[2] = new Vector2(-1195, 5); // asia
    }

    void Start()
    {
        TravelScrollPageDepth = new Vector2(-TravelMap.sizeDelta.x * TravelMap.localScale.x, 0);
    }

    private void SetMarketProductScroll()
    {
        MarketScrollRoot.anchoredPosition = new Vector2(0, MarketScrollPage * 78);

        int _total_count = 0;

        if(MarketHomeState)
        {
            _total_count = UserData.Instance.InventoryProducts.Count;
            MarketScrollRoot.sizeDelta =
                new Vector2(
                    MarketScrollRoot.sizeDelta.x,
                    (_total_count * 78) + MarketHomeBase.sizeDelta.y
                    );

            MarketHomeBase.gameObject.SetActive(true);
            MarketHomeBase.anchoredPosition = new Vector2(0, _total_count * -78);
            MarketHomeName.text = //CityDB.Instance.CityDatas[NetworkManager.Instance.mUserData.currentCityCode].cityName.ToUpper();
                Language.Str.CITYNAME(NetworkManager.Instance.mUserData.currentCityCode).ToUpper();

            for(int i = 0; i<MarketHomeBtns.Length; i++)
            {
                var _node = UserData.Instance.GetCurrentUnlockItem(MarketHomeBtns[i].CityID);
                var _gauge_bg = MarketHomeBtns[i].transform.Find("GaugeBG");
                _gauge_bg.Find("Gauge").GetComponent<Image>().fillAmount =
                    (float)(_node.Trades) / ItemDB.Instance.ItemDatas[_node.ID].trades;
                _gauge_bg.Find("Label").GetComponent<Text>().text =
                    string.Format("{0}\n<size=12>{1}/{2}</size>",
                    ItemDB.Instance.GetGroupName(ItemDB.Instance.ItemDatas[_node.ID].itemGroupID),
                    _node.Trades, ItemDB.Instance.ItemDatas[_node.ID].trades
                    );
            }
        }
        else
        {
            _total_count =
            UserData.Instance.InventoryProducts.Count
            + UserData.Instance.CurrentMarketInfos.Count;

            MarketScrollRoot.sizeDelta =
                new Vector2(
                    MarketScrollRoot.sizeDelta.x,
                    650 + ((_total_count - 9) * 78) + 78
                    );

            MarketHomeBase.gameObject.SetActive(false);
        }

        SetMarketProductScrollNodes();
    }

    private void SetMarketProductScrollNodes()
    {
        for (int i = 0; i < MarketScrollNodes.Length; i++)
        {
            var _rect = MarketScrollNodes[i].GetComponent<RectTransform>();
            _rect.anchoredPosition =
                new Vector2(
                    _rect.anchoredPosition.x,
                    (MarketScrollPage * -78)
                    + (i * -78));

            int _node_idx = i + MarketScrollPage;
            int _total_count =
                MarketHomeState ?
                UserData.Instance.InventoryProducts.Count :
                UserData.Instance.InventoryProducts.Count +
                UserData.Instance.CurrentMarketInfos.Count;
            if (_node_idx >= _total_count)
            {
                _rect.gameObject.SetActive(false);
            }
            else
            {
                //인벤토리 상품 : 판매
                if (_node_idx < UserData.Instance.InventoryProducts.Count)
                {
                    _rect.Find("BG").GetComponent<Image>().sprite = Source_BG_Inven;
                    _rect.Find("IconBG").GetComponent<Image>().sprite = Source_IconBG_Inven;
                    _rect.Find("BG").GetComponent<Image>().color =
                            _rect.Find("IconBG").GetComponent<Image>().color = Color.white;

                    var _inven_node = UserData.Instance.InventoryProducts.ElementAt(_node_idx).Value;
                    var _city_node = UserData.Instance.CurrentMarketAllInfos[_inven_node.ID];

                    MarketScrollNodes[i].ProductID = _inven_node.ID;
                    _rect.Find("Icon").GetComponent<UIItemIcon>().SetItem(_inven_node.ID);
                    _rect.Find("Name").GetComponent<Text>().text = _inven_node.name;
                    _rect.Find("Name").GetChild(0).GetComponent<Text>().text = string.Format("{0} {1}", _inven_node.amount, Language.Str.EA);
                    _rect.Find("Info").GetComponent<Text>().text =
                    string.Format("{0} @\n${1}",
                    _inven_node.amount,
                    //new Money(_inven_node.MedianPrice.ToString()).ToMoneyWon());
                    new Money((_inven_node.totalPrice / _inven_node.amount).ToString()).ToMoneyWon()
                    );
                    _rect.Find("Price").GetComponent<Text>().text =
                        Language.Str.MONEYUNIT + " " + new Money(_city_node.CurrentPrice.ToString()).ToMoneyWon();

                    if(_city_node.CurrentPrice == _city_node.OriginPrice)
                    {
                        _rect.Find("Price").GetComponent<Text>().color = Color.black;
                        _rect.Find("Price").GetChild(0).gameObject.SetActive(false);
                    }
                    else if(_city_node.CurrentPrice > _city_node.OriginPrice)
                    {
                        _rect.Find("Price").GetComponent<Text>().color = Color.red;
                        _rect.Find("Price").GetChild(0).gameObject.SetActive(true);
                        _rect.Find("Price").GetChild(0).GetComponent<Image>().sprite =
                            (_city_node.CurrentPrice - _city_node.OriginPrice >= ItemDB.Instance.ItemDatas[_inven_node.ID].stdDev)
                            ? Source_PriceIcon_Up2 : Source_PriceIcon_Up1;
                    }
                    else
                    {
                        _rect.Find("Price").GetComponent<Text>().color = Color.blue;
                        _rect.Find("Price").GetChild(0).gameObject.SetActive(true);
                        _rect.Find("Price").GetChild(0).GetComponent<Image>().sprite =
                            (_city_node.OriginPrice - _city_node.CurrentPrice >= ItemDB.Instance.ItemDatas[_inven_node.ID].stdDev)
                            ? Source_PriceIcon_Down2 : Source_PriceIcon_Down1;
                    }

                    _rect.Find("Locked").gameObject.SetActive(false);
                    _rect.Find("BtnSell").gameObject.SetActive(true);
                    _rect.Find("BtnBuy").gameObject.SetActive(false);
                }
                //시장 상품 : 구입
                else
                {
                    int _idx = _node_idx - UserData.Instance.InventoryProducts.Count;
                    int _id = UserData.Instance.CurrentMarketInfos.ElementAt(_idx).Key;
                    var _node = UserData.Instance.CurrentMarketInfos.ElementAt(_idx).Value;

                    if (!string.IsNullOrEmpty(MarketScrollSerch.text))
                    {
                        if(_node.Name.IndexOf(MarketScrollSerch.text, StringComparison.OrdinalIgnoreCase) < 0)
                        {
                            _rect.gameObject.SetActive(false);
                            continue;
                        }
                    }
                    
                    int _groupid = ItemDB.Instance.ItemDatas[_id].itemGroupID;
                    _rect.Find("BG").GetComponent<Image>().sprite = Source_BG_Market;
                    _rect.Find("IconBG").GetComponent<Image>().sprite = Source_IconBG_Market;

                    _rect.Find("BG").GetComponent<Image>().color =
                        _rect.Find("IconBG").GetComponent<Image>().color =
                            ItemGroupColors.ContainsKey(_groupid) ? ItemGroupColors[_groupid] : Color.white;

                    MarketScrollNodes[i].ProductID = _id;
                    _rect.Find("Icon").GetComponent<UIItemIcon>().SetItem(_id);
                    _rect.Find("Name").GetComponent<Text>().text = _node.Name;
                    _rect.Find("Name").GetChild(0).GetComponent<Text>().text = "";
                    _rect.Find("Info").GetComponent<Text>().text = "";
                    _rect.Find("Price").GetComponent<Text>().text =
                        Language.Str.MONEYUNIT + new Money(_node.CurrentPrice.ToString()).ToMoneyWon();

                    if (_node.CurrentPrice == _node.OriginPrice)
                    {
                        _rect.Find("Price").GetComponent<Text>().color = Color.black;
                        _rect.Find("Price").GetChild(0).gameObject.SetActive(false);
                    }
                    else if (_node.CurrentPrice > _node.OriginPrice)
                    {
                        _rect.Find("Price").GetComponent<Text>().color = Color.red;
                        _rect.Find("Price").GetChild(0).gameObject.SetActive(true);
                        _rect.Find("Price").GetChild(0).GetComponent<Image>().sprite =
                            (_node.CurrentPrice - _node.OriginPrice >= ItemDB.Instance.ItemDatas[_id].stdDev)
                            ? Source_PriceIcon_Up2 : Source_PriceIcon_Up1;
                    }
                    else
                    {
                        _rect.Find("Price").GetComponent<Text>().color = Color.blue;
                        _rect.Find("Price").GetChild(0).gameObject.SetActive(true);
                        _rect.Find("Price").GetChild(0).GetComponent<Image>().sprite =
                            (_node.OriginPrice - _node.CurrentPrice >= ItemDB.Instance.ItemDatas[_id].stdDev)
                            ? Source_PriceIcon_Down2 : Source_PriceIcon_Down1;
                    }

                    //if (NetworkManager.Instance.mUserData.level < ItemDB.Instance.ItemDatas[_id].levelUnlock)
                    if (UserData.Instance.InventoryProductInfos[_id].isAvailable == UserData.UNAVAILABLE)
                    {
                        _rect.Find("Locked").gameObject.SetActive(true);
                        //_rect.Find("Locked").GetChild(1).GetComponent<Text>().text = Language.Str.MARKET_LOCKED;//"Lv " + ItemDB.Instance.ItemDatas[_id].levelUnlock;
                        _rect.Find("Locked").GetChild(1).GetComponent<Text>().text = "R&D Req";
                        _rect.Find("BtnBuy").gameObject.SetActive(false);
                        _rect.Find("BtnSell").gameObject.SetActive(false);
                    }
                    else
                    {
                        _rect.Find("Locked").gameObject.SetActive(false);
                        _rect.Find("BtnBuy").gameObject.SetActive(_node.Available == UserData.AVAILABLE);
                        _rect.Find("BtnSell").gameObject.SetActive(false);
                    }
                }

                _rect.gameObject.SetActive(true);
            }
        }
    }

    private void OnSellSlideValueChange()
    {
        Money _sell_price = new Money(UserData.Instance.CurrentMarketAllInfos[MarketScrollNodes[SellCurrentIdx].ProductID].BoostPrice.ToString());
        _sell_price.MulMoney(SellAmountSlide.value);
        SellLabelPrice.text = "$" + _sell_price.ToMoneyWon();
        SellLabelCurrent.text = SellAmountSlide.value.ToString("0");

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SLIDE);
    }

    private void OnBuySlideValueChange()
    {
        Money _buy_price = new Money(UserData.Instance.CurrentMarketInfos[MarketScrollNodes[BuyCurrentIdx].ProductID].BoostPrice.ToString());
        _buy_price.MulMoney(BuyAmountSlide.value);
        BuyLabelPrice.text = "$" + _buy_price.ToMoneyWon();
        BuyLabelPrice.color = UserData.Instance.TotalMoney.CompareMoney(_buy_price) ? Color.black : Color.red;
        BuyLabelCurrent.text = BuyAmountSlide.value.ToString("0");

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_SLIDE);
    }
    
    private int _current_list_available = UserData.UNAVAILABLE;
    private int _current_sort = 0;
    private int _current_sort_order = 0;
    private void SetMarketProductScrollByName()
    {
        if (string.IsNullOrEmpty(MarketScrollSerch.text))
        {
            int _before_sort = _current_sort;
            _current_sort = -1;
            switch (_before_sort)
            {
                case 0: MarketSortByGroup(); break;
                case 1: MarketSortByName(); break;
                case 2: MarketSortByPrice(); break;
                default: SetMarketProductScroll(); break;
            }
            return;
        }
        
        if (_current_sort == -1) _current_sort = 0;
        if (MarketHomeState) SetMarketHomeSortState(MARKET_HOME_SORT_ALL);

        UserData.Instance.CurrentMarketInfos
            = UserData.Instance.CurrentMarketInfos.OrderBy(
                x => x.Value.Name.IndexOf(MarketScrollSerch.text, StringComparison.OrdinalIgnoreCase) < 0).ToDictionary(
                k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);

        MarketScrollRoot.anchoredPosition = new Vector2(0, MarketScrollPage * 78);

        int _total_count = 0;
        for (int i = 0; i < UserData.Instance.CurrentMarketInfos.Count; i++)
        {
            if (UserData.Instance.CurrentMarketInfos.ElementAt(i).Value.Name.
                IndexOf(MarketScrollSerch.text, StringComparison.OrdinalIgnoreCase) >= 0)
                _total_count++;
        }

        MarketScrollRoot.sizeDelta =
            new Vector2(
                MarketScrollRoot.sizeDelta.x,
                650 + ((_total_count - 9) * 78) + 78
                );

        SetMarketProductScrollNodes();
    }

    private void MarketSortByGroup()
    {
        if (!string.IsNullOrEmpty(MarketScrollSerch.text)) MarketScrollSerch.text = "";

        if (_current_sort == 0) _current_sort_order = _current_sort_order == 0 ? 1 : 0;
        else _current_sort = 0;

        if (_current_sort_order == 0)
        {
            UserData.Instance.CurrentMarketInfos
            = UserData.Instance.CurrentMarketInfos.OrderBy(
                x => ItemDB.Instance.ItemDatas[x.Value.ProductID].itemGroupID).ToDictionary(
                k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);
        }
        else
        {
            UserData.Instance.CurrentMarketInfos
            = UserData.Instance.CurrentMarketInfos.OrderByDescending(
                x => ItemDB.Instance.ItemDatas[x.Value.ProductID].itemGroupID).ToDictionary(
                k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);
        }

        MarketScrollPage = 0;
        SetMarketProductScroll();
    }

    private void MarketSortByName()
    {
        if (!string.IsNullOrEmpty(MarketScrollSerch.text)) MarketScrollSerch.text = "";

        if (_current_sort == 1) _current_sort_order = _current_sort_order == 0 ? 1 : 0;
        else _current_sort = 1;

        if (_current_sort_order == 0)
        {
            UserData.Instance.CurrentMarketInfos
               = UserData.Instance.CurrentMarketInfos.OrderBy(
                   x => x.Value.Name).ToDictionary(
                   k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);
        }
        else
        {
            UserData.Instance.CurrentMarketInfos
               = UserData.Instance.CurrentMarketInfos.OrderByDescending(
                   x => x.Value.Name).ToDictionary(
                   k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);
        }

        MarketScrollPage = 0;
        SetMarketProductScroll();
    }

    private void MarketSortByPrice()
    {
        if (!string.IsNullOrEmpty(MarketScrollSerch.text)) MarketScrollSerch.text = "";

        if (_current_sort == 2) _current_sort_order = _current_sort_order == 0 ? 1 : 0;
        else _current_sort = 2;

        if (_current_sort_order == 0)
        {
            UserData.Instance.CurrentMarketInfos
               = UserData.Instance.CurrentMarketInfos.OrderBy(
                   x => x.Value.CurrentPrice).ToDictionary(
                   k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);
        }
        else
        {
            UserData.Instance.CurrentMarketInfos
               = UserData.Instance.CurrentMarketInfos.OrderByDescending(
                   x => x.Value.CurrentPrice).ToDictionary(
                   k => k.Key, k => UserData.Instance.CurrentMarketInfos[k.Key]);
        }

        MarketScrollPage = 0;
        SetMarketProductScroll();
    }

    private Dictionary<int, Color> ItemGroupColors = new Dictionary<int, Color>();

	// Update is called once per frame
	void Update () {

        if (TravelBase.activeSelf
            && !TravelPopupBase.activeSelf
            && !MoveProcess)
        {
            if (Input.GetMouseButtonDown(0)) TravelScrollTouchRoot = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            if (TravelScrollTouchRoot != Vector3.zero)
            {
                if (Input.GetMouseButton(0))
                {
                    TravelScrollVal = (Camera.main.ScreenToViewportPoint(Input.mousePosition) - TravelScrollTouchRoot).x * CanvasRoot.sizeDelta.x;
                    if (Mathf.Abs(TravelScrollVal) >= 160)
                    {
                        if (TravelScrollVal < 0) SetNextTravelPage();
                        else SetPrevTravelPage();
                        TravelScrollTouchRoot = Vector3.zero;
                        TravelScrollVal = 0;
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    TravelScrollTouchRoot = Vector3.zero;
                    TravelScrollVal = 0;
                }
            }
            
            if(TravelNowPage < 0)
            {
                int _pos_idx = 3 + (3 * ((TravelNowPage + 1) / -3)) + TravelNowPage;
                TravelMap.anchoredPosition =
                Vector2.Lerp(
                    TravelMap.anchoredPosition,
                    TravelAreaPositions[_pos_idx]
                    + (Vector2.right * TravelScrollVal)
                    + (TravelScrollPageDepth * ((TravelNowPage-2) / 3)),
                    0.1f);
            }
            else
            {
                TravelMap.anchoredPosition =
                Vector2.Lerp(
                    TravelMap.anchoredPosition,
                    TravelAreaPositions[TravelNowPage%3]
                    + (Vector2.right * TravelScrollVal)
                    + (TravelScrollPageDepth * (TravelNowPage / 3)),
                    0.1f);
            }
        }

        if(MarketBase.activeSelf)
        {
            int _now_page = (int)(MarketScrollRoot.anchoredPosition.y / 78);
            if (_now_page < 0) _now_page = 0;
            if (MarketScrollPage != _now_page)
            {
                MarketScrollPage = _now_page;
                SetMarketProductScrollNodes();
            }
        }
	}

    public void OnEscape()
    {
        if (BurglaryPopup.activeSelf) BurglaryPopup.SetActive(false);
        else if (MarketBase.activeSelf)
        {
            if (SellPopup.activeSelf) { SellPopup.SetActive(false); SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE); }
            else if (BuyPopup.activeSelf) { BuyPopup.SetActive(false); SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE); }
            else if (BtnMarketBack.activeSelf) BackToMarketHome();
            else ExitMarket();
        }
        else if (TravelBase.activeSelf
            && !TravelAnimationBase.activeSelf)
        {
            if (TravelPopupBase.activeSelf) ExitTravelPopup();
            else ExitTravel();
        }
    }

    public void OpenTravel(int _cityID = -1)
    {
        if (_cityID == -1) _cityID = CurrentCity;

        if(NetworkManager.Instance.mUserData.heart <= 0)
        {
            MainScene.Instance.OpenHeartPopup();
            return;
        }

        bool _isAll_unlock = true;
        for(int i = 0; i<TravelCityButtons.Length; i++)
        {
            bool _isActive = UserData.Instance.CityInfos.ContainsKey(TravelCityButtons[i].CityID)
            && UserData.Instance.CityInfos[TravelCityButtons[i].CityID] == UserData.AVAILABLE;
            TravelCityButtons[i].transform.GetChild(0).gameObject.SetActive(!_isActive);
            if (!_isActive) _isAll_unlock = false;
        }
        
        if (_cityID != -1)
        {
            if(CurrentCity != -1)
                Airplain.position = TravelCityBtnMap[CurrentCity].GetComponent<RectTransform>().position;

            switch(CityDB.Instance.CityDatas[_cityID].regionCode)
            {
                case 1: TravelNowPage = 0; break; // 아메리카
                case 2: TravelNowPage = 2; break; // 아시아
                case 3: TravelNowPage = 1; break; // 유럽
            }
        }

        TravelMap.transform.parent.GetComponent<Image>().raycastTarget = true;
        /*
        if(TravelNowPage < 0) TravelNowPage = 3 + (3 * ((TravelNowPage + 1) / -3)) + TravelNowPage;
        else TravelNowPage = TravelNowPage % 3;
        */
        TravelMap.anchoredPosition = TravelAreaPositions[TravelNowPage];
        SetTravelMapPage();

        if (_isAll_unlock) TravelUnlockGaugeBase.SetActive(false);
        else
        {
            TravelUnlockGaugeBase.SetActive(true);
            float _unlock_rate = NetworkManager.Instance.mUserData.cityUnlockPoint / 20.0f;
            TravelUnlockPointGague.fillAmount = _unlock_rate;
            TravelUnlockPointIcon.anchoredPosition = new Vector2(TravelUnlockPointGague.GetComponent<RectTransform>().sizeDelta.x * _unlock_rate, 0);
            TravelUnlockPointButton.image.sprite =
                _unlock_rate >= 1.0f ? Source_UnlockBtn_On : Source_UnlockBtn_Off;
            TravelUnlockPointButton.interactable = _unlock_rate >= 1.0f;
            TravelUnlockPointEffect.SetActive(_unlock_rate >= 1.0f);
        }

        TravelPopupBase.SetActive(false);
        TravelAnimationBase.SetActive(false);
        TravelUseHeartImg.gameObject.SetActive(false);
        TravelBase.SetActive(true);

        SoundManager.Instance.PlayBGM(SoundManager.BGM_TRAVEL);
    }

    public void ExitTravel()
    {
        if (MoveProcess) return;
        
        TravelBase.SetActive(false);
        SoundManager.Instance.PlayBGM(SoundManager.BGM_GAME);
    }
    
    public void SetNextTravelPage()
    {
        TravelNowPage++;
        SetTravelMapPage();
    }

    public void SetPrevTravelPage()
    {
        TravelNowPage--;
        SetTravelMapPage();
    }

    private void SetTravelMapPage()
    {
        int _val = TravelNowPage;
        if (_val < 0) _val = _val - 2;
        TravelMapImg.anchoredPosition = Vector2.right * (TravelMap.sizeDelta.x * (_val / 3));
    }

    private int UnlockTargetCitiy = -1;
    private ShopDB.ShopMember UnlockCityPrice = null;
    public void OpenTravelPopup(int _city)
    {
        UnlockTargetCitiy = _city;

        TravelPopupCityImg.sprite = MainScene.Instance.GetCityImgs(_city);
        TravelPopupName.text = CityDB.Instance.CityDatas[_city].FullName.ToUpper();

        TravelPopupMessage.text = string.Format(Language.Str.TRAVEL_FORMAT_UNLOCK_TITLE, CityDB.Instance.CityDatas[_city].cityName);

        TravelPopupInfoLow.text = string.Format("{0} {1}", ItemDB.Instance.GetGroupName(CityDB.Instance.CityPriceInfoLow[_city]), Language.Str.PRICES);
        TravelPopupInfoHigh.text = string.Format("{0} {1}", ItemDB.Instance.GetGroupName(CityDB.Instance.CityPriceInfoHigh[_city]), Language.Str.PRICES);

        /*
        int _next_city_level = 0;
        for (int i = NetworkManager.Instance.mUserData.level+1;
            i <= LevelDB.Instance.Levels.Last().Value.level; i++)
        {
            if(LevelDB.Instance.Levels[i].unlockCity == 1)
            {
                _next_city_level = i;
                break;
            }
        }
        TravelPopupLevelMessage.text = string.Format("Next city unlock at level {0}.",_next_city_level);
        */

        int _city_count = 0;
        for(int i = 0; i<UserData.Instance.CityInfos.Count; i++)
            if (UserData.Instance.CityInfos.ElementAt(i).Value == UserData.AVAILABLE) _city_count++;
        
        if (_city_count < 10) UnlockCityPrice = ShopDB.Instance.UnlockCity[0];
        else UnlockCityPrice = ShopDB.Instance.UnlockCity[1];

        TravelPopupBtnUnlockMoney.transform.Find("Label").GetComponent<Text>().text =
            string.Format("${0}", Money.NumToMoney(UnlockCityPrice.priceMoney.ToString()));

        TravelPopupBtnUnlockGem.transform.Find("Label").GetComponent<Text>().text =
            string.Format("<size=32>{1}</size>\n{0}", UnlockCityPrice.priceGem, Language.Str.TRAVEL_UNLOCK_NOW);
        
        TravelPopupBase.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_POPUP);
    }

    private void UnlockCityWithMoney()
    {
        if(!MainScene.Instance.CheckPrice(new Money(UnlockCityPrice.priceMoney.ToString())))
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestUnlockCity(
                UnlockCityPrice.shopID,
                ShopDB.PRICE_TYPE_MONEY,
                UnlockTargetCitiy,
                delegate (JSONNode N)
                {
                    for (int i = 0; i < TravelCityButtons.Length; i++)
                    {
                        bool _isActive = UserData.Instance.CityInfos.ContainsKey(TravelCityButtons[i].CityID)
                        && UserData.Instance.CityInfos[TravelCityButtons[i].CityID] == UserData.AVAILABLE;
                        TravelCityButtons[i].transform.GetChild(0).gameObject.SetActive(!_isActive);
                    }

                    TravelPopupBase.SetActive(false);
                    QuestList.Instance.OnTravelUnlock();
                    PopupManager.Instance.SetOffPopup();
                    //PopupManager.Instance.SetPopup("Unlock city confirm!");

                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_CITY_UNLOCK);
                },
                delegate (int _code, string _message)
                {
                    Debug.Log("on fail : " + _code + " / " + _message);
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_UNLOCK_ERROR);
                },

                delegate (string _message)
                {
                    Debug.Log("on error");
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_UNLOCK_FAIL);
                });
        }
    }

    private void UnlockCityWithGem()
    {
        if (!MainScene.Instance.CheckPrice(UnlockCityPrice.priceGem))
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestUnlockCity(
                UnlockCityPrice.shopID,
                ShopDB.PRICE_TYPE_GEM,
                UnlockTargetCitiy,
                delegate (JSONNode N)
                {
                    for (int i = 0; i < TravelCityButtons.Length; i++)
                    {
                        bool _isActive = UserData.Instance.CityInfos.ContainsKey(TravelCityButtons[i].CityID)
                        && UserData.Instance.CityInfos[TravelCityButtons[i].CityID] == UserData.AVAILABLE;
                        TravelCityButtons[i].transform.GetChild(0).gameObject.SetActive(!_isActive);
                    }

                    TravelPopupBase.SetActive(false);
                    QuestList.Instance.OnTravelUnlock();
                    PopupManager.Instance.SetOffPopup();
                    //PopupManager.Instance.SetPopup("Unlock city confirm!");

                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_CITY_UNLOCK);
                    IGAWorksManager.onRetention("UseCashUnlockCity");
                },
                delegate (int _code, string _message)
                {
                    Debug.Log("on fail : " + _code + " / " + _message);
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_UNLOCK_ERROR);
                },

                delegate (string _message)
                {
                    Debug.Log("on error");
                    PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_UNLOCK_FAIL);
                });
        }
    }

    private void ExitTravelPopup()
    {
        TravelPopupBase.SetActive(false);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void OpenMarket()
    {
        MainBase.SetActive(false);
        MarketBase.SetActive(true);

        BtnMarket.SetActive(false);
        BtnTravel.SetActive(false);
        BtnMarketExit.SetActive(true);
        BtnMarketBack.SetActive(false);

        BtnInventory.SetActive(true);
        BtnMenu.SetActive(false);

        MarketEnterProducts.Clear();
        for (int i = 0; i < UserData.Instance.InventoryProducts.Count; i++)
        {
            var _node = UserData.Instance.InventoryProducts.ElementAt(i);
            MarketEnterProducts.Add(_node.Key,
                new UserData.Product(_node.Value));
        }

        MarketScrollPage = 0;
        MarketHomeState = true;
        MarketHomeSortState = -1;
        SetMarketProductAvailableList();
    }

    public void SetMarketProductAvailableList()
    {
        if (!MarketBase.activeSelf) return;
        
        if (_current_list_available == UserData.AVAILABLE)
        {
            UserData.Instance.CurrentMarketInfos =
            UserData.Instance.CurrentMarketAllInfos.Where(
                x =>
                x.Value.Available == UserData.AVAILABLE // 시장에 있고
                && UserData.Instance.InventoryProductInfos[x.Value.ProductID].isAvailable == UserData.AVAILABLE // 내가 언락하였을때
                && GetMarketHomeSort(x.Value)
                ).ToDictionary(
                k => k.Key, k => UserData.Instance.CurrentMarketAllInfos[k.Key]);

            MarektScrollBtnUnlock.transform.GetChild(0).GetComponent<Text>().text = Language.Str.UNLOCK;
        }
        else
        {
            UserData.Instance.CurrentMarketInfos =
            UserData.Instance.CurrentMarketAllInfos.Where(
                x =>
                x.Value.Available == UserData.AVAILABLE // 시장에 있고
                && UserData.Instance.InventoryProductInfos[x.Value.ProductID].isAvailable != 2 // 업그레이드로 사라진 아이템이 아닐때
                && GetMarketHomeSort(x.Value)
                ).ToDictionary(
                k => k.Key, k => UserData.Instance.CurrentMarketAllInfos[k.Key]);

            MarektScrollBtnUnlock.transform.GetChild(0).GetComponent<Text>().text = Language.Str.LOCK;
        }

        if(MarketHomeSortState == MARKET_HOME_SORT_SALE)
        {
            _current_sort = 2;
            _current_sort_order = 0;
        }

        int _before_sort = _current_sort;
        _current_sort = -1;
        switch (_before_sort)
        {
            case 0: MarketSortByGroup(); break;
            case 1: MarketSortByName(); break;
            case 2: MarketSortByPrice(); break;
            default: SetMarketProductScroll(); break;
        }
    }

    private bool GetMarketHomeSort(UserData.MarketProduct _product)
    {
        switch (MarketHomeSortState)
        {
            default: return true;
            case MARKET_HOME_SORT_GROUP_1:
            case MARKET_HOME_SORT_GROUP_2:
            case MARKET_HOME_SORT_GROUP_3:
            case MARKET_HOME_SORT_GROUP_4:
            case MARKET_HOME_SORT_GROUP_5:
            case MARKET_HOME_SORT_GROUP_6:
            case MARKET_HOME_SORT_GROUP_7:
            case MARKET_HOME_SORT_GROUP_8:
                return ItemDB.Instance.ItemDatas[_product.ProductID].itemGroupID == MarketHomeSortState;

            case MARKET_HOME_SORT_SALE:
                return _product.OriginPrice - _product.CurrentPrice >=
                   ItemDB.Instance.ItemDatas[_product.ProductID].stdDev;
        }
    }

    public void BackToMarketHome()
    {
        BtnMarketExit.SetActive(true);
        BtnMarketBack.SetActive(false);

        MarketScrollPage = 0;
        MarketHomeState = true;
        MarketHomeSortState = -1;
        SetMarketProductAvailableList();

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_MARKET_EXIT);
    }

    public void SetMarketHomeSortState(int _state)
    {
        BtnMarketExit.SetActive(false);
        BtnMarketBack.SetActive(true);

        MarketScrollPage = 0;
        MarketHomeState = false;
        MarketHomeSortState = _state;
        SetMarketProductAvailableList();
        
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_MARKET_SELECT);
    }

    public void ExitMarket()
    {
        MarketBase.SetActive(false);
        MainBase.SetActive(true);

        BtnMarket.SetActive(false);
        BtnTravel.SetActive(true);
        BtnMarketExit.SetActive(false);
        BtnMarketBack.SetActive(false);

        BtnInventory.SetActive(false);
        BtnMenu.SetActive(true);

        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_CLOSE);
    }

    public void MoveToCity(int _id)
    {
        if (MoveProcess) return;
        if (_id == CurrentCity) return;

        /*
        UITextAppearManager.Instance.SetAppear(
            TravelCityBtnMap[_id].transform,
            "-1 Heart",
            30,
            Color.white,
            new Color(1, 0.3f, 0.5f)
            );
        */

        TravelUseHeartImg.position = TravelCityBtnMap[_id].transform.position;
        TravelUseHeartImg.gameObject.SetActive(true);
        TravelUseHeartImg.GetComponent<Rigidbody>().velocity = Vector3.zero;
        TravelUseHeartImg.GetComponent<Rigidbody>().AddForce(0, 150, 0);

        //SoundManager.Instance.PlayEffects(SoundManager.EFFECT_TOCITY);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_CITY_SELECT);
        StartCoroutine(MoveToCityAnimation(_id));
    }

    private bool MoveProcess = false;
    public IEnumerator MoveToCityAnimation(int _id)
    {
        MoveProcess = true;

        yield return new WaitForSeconds(0.5f);

        TravelPopupBase.SetActive(false);

        TravelAniFromImg.sprite = MainScene.Instance.GetCityImgs(CurrentCity);
        TravelAniFromName.text = CityDB.Instance.CityDatas[CurrentCity].cityName.ToUpper();
        TravelAniToImg.sprite = MainScene.Instance.GetCityImgs(_id);
        TravelAniToName.text = CityDB.Instance.CityDatas[_id].cityName.ToUpper();

        Vector2 _from_target = new Vector2(-1000, 0);
        Vector2 _to_target = new Vector2(25, 0);

        TravelAniFrom.anchoredPosition = new Vector2(-25, 0);
        TravelAniTo.anchoredPosition = new Vector2(1000, 0);

        TravelAniAirplain.position = TravelAniAirplainFrom.position;

        TravelAnimationBase.SetActive(true);
        SoundManager.Instance.PlayEffects(SoundManager.EFFECT_AIRPLANE);

        float _timer = 0;
        while(_timer < 3.0f)
        {
            _timer += Time.deltaTime;

            if(_timer > 1.0f)
            {
                TravelAniAirplain.position = Vector3.Lerp(TravelAniAirplain.position, TravelAniAirplainTo.position, 0.1f);
                TravelAniFrom.anchoredPosition = Vector2.Lerp(TravelAniFrom.anchoredPosition, _from_target, 0.1f);
                TravelAniTo.anchoredPosition = Vector2.Lerp(TravelAniTo.anchoredPosition, _to_target, 0.1f);
            }

            yield return null;
        }

        PopupManager.Instance.SetPopupWait();

        int _before_city = CurrentCity;
        NetworkManager.Instance.RequestTravel
            (
            _id,
            UserData.Instance.MarketStepCode,
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();

                /*
                BtnTravel.SetActive(false);
                BtnMarket.SetActive(true);
                TravelBase.SetActive(false);
                */

                MainScene.Instance.OnCityChange(CurrentCity);

                MoveProcess = false;
                SoundManager.Instance.PlayBGM(SoundManager.BGM_GAME);
                
                if (!string.IsNullOrEmpty(N["burglary"].ToString()))
                {
                    BurglaryPopup.SetActive(true);
                    SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BURGLER);

                    var _info = N["burglary"];
                    Money _stolen_money = new Money(_info["stolenMoney"]);
                    if (!string.IsNullOrEmpty(_info["stolenItemID"].ToString()))
                    {
                        int _stolen_item_id = _info["stolenItemID"].AsInt;
                        int _stolen_item_amount = _info["stolenAmount"].AsInt;

                        BurglaryLabel.text = string.Format(
                            Language.Str.POPUP_TRAVEL_BURGLARY_FORMAT_ITEM
                            , _stolen_money.ToWon()
                            , ItemDB.Instance.ItemDatas[_stolen_item_id].itemName
                            , _stolen_item_amount
                            );

                        RNDCenterManager.Instance.SetUnlockItemBtnUI(ItemDB.Instance.ItemDatas[_stolen_item_id].itemGroupID);
                        WareHouseManager.Instance.UpdateMainAmount();
                    }
                    else
                    {
                        BurglaryLabel.text = string.Format(
                            Language.Str.POPUP_TRAVEL_BURGLARY_FORMAT_MONEY
                            , _stolen_money.ToWon()
                            );
                    }
                }

                TravelBase.SetActive(false);
                BtnTravel.SetActive(false);
                OpenMarket();

                //great offer popup check
                if (
                GreatOfferTime == DateTime.MinValue
                || (GreatOfferTime.AddMinutes(5) - NetworkManager.Instance.GetServerNow()).TotalSeconds < 0)
                {
                    GreatOfferTime = NetworkManager.Instance.GetServerNow();
                    GreatOfferCount = 1;
                }
                else GreatOfferCount++;

                if (GreatOfferCount > 4
                && NetworkManager.Instance.mUserData.heart <= 5)
                {
                    GreatOfferCount = 0;
                    GreatOfferTime = DateTime.MinValue;
                    StartCoroutine(OpenGreatOfferPopup());
                }

                //set quest
                QuestList.Instance.OnTravel(_id, CityDB.Instance.CityDatas[_id].regionCode);

                IGAWorksManager.onRetention("UseHeart");
                IGAWorksManager.onRetention("Travel" + CurrentCity);
            },

            delegate (int _code, string _message)
            {
                Debug.Log("on fail : " + _code + " / " + _message);
                MoveProcess = false;
                CurrentCity = _before_city;

                PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_ERROR);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                OpenTravel();
            },
            
            delegate (string _message)
            {
                Debug.Log("on error");
                MoveProcess = false;
                CurrentCity = _before_city;

                PopupManager.Instance.SetPopup(Language.Str.POPUP_TRAVEL_FAIL);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
                OpenTravel();
            }
            );
    }

    IEnumerator OpenGreatOfferPopup()
    {
        while (BurglaryPopup.activeSelf) yield return null;

        GreatOfferPopup.SetActive(true);
    }

    public void GetGreatOfferAD()
    {
        mAdsManager.Instance.ShowAD(delegate
        {
            PopupManager.Instance.SetPopupWait();
            NetworkManager.Instance.RequestBuyShopItem(
            ShopDB.Instance.Heart[1].shopID,
            ShopDB.PRICE_TYPE_AD,
            delegate (JSONNode N)
            {
                PopupManager.Instance.SetOffPopup();

                if (NetworkManager.Instance.mUserData.heart < 10) MainScene.Instance.OpenHeartMoreGreatOffer();

                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_BUTTON_OK);

                GreatOfferPopup.SetActive(false);
                ClearOffer();

                IGAWorksManager.onRetention("ADHeart2");
            },
            delegate (int _code, string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            },
            delegate (string _message)
            {
                PopupManager.Instance.SetPopup(Language.Str.POPUP_HEART_FAILED);
                SoundManager.Instance.PlayEffects(SoundManager.EFFECT_ERROR);
            }
            );
        });
    }

    public void ClearOffer()
    {
        GreatOfferTime = DateTime.MinValue;
        GreatOfferCount = 0;
    }

    //-------------------------

    public void SetMarketScrollPage(int _id) { MarketScrollRoot.anchoredPosition = new Vector2(MarketScrollRoot.anchoredPosition.x, _id * 78); }
}
