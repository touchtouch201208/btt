#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("GjkUExcXFRATBAx6ZmZiYSg9PWWjIkr+SBYgnnqhnQ/Md2HtdUx3rpkLm8zrWX7nFbkwIhD6CizqQhvBpyi/5h0cEoAZozMEPGbHLh/JcARrMnNhYWd/d2Eyc3Fxd2Jmc3xxdy80dTKYIXjlH5DdzPmxPetBeEl2ZWU8c2Jifnc8cX1/PXNiYn53cXMhJEgicCMZIhsUEUcWFAEQR0EjAQ2DyQxVQvkX/0xrlj/5JLBFXkf+IgMUEUcWGAEYU2JifncyW3xxPCMV/m8rkZlBMsEq1qOtiF0Yee057hQiHRQRRw8BExPtFhciERMT7SIPHY8v4TlbOgja7NynqxzLTA7E2S9bymSNIQZ3s2WG2z8QERMSE7GQE8skbdOVR8u1i6sgUOnKx2OMbLNAfHYycX18dntme318YTJ9dDJnYXdLtRcbbgVSRAMMZsGlmTEpVbHHfWJ+dzJAfX1mMlFTIgwFHyIkIiYgZnt0e3FzZncycGsyc3xrMmJzYGZifncyUXdgZnt0e3FzZnt9fDJTZ3WdGqYy5dm+PjJ9YqQtEyKepVHdus5sMCfYN8fLHcR5xrA2MQPls74ikBapIpARsbIREBMQEBMQIh8UGzxStOVVX20aTCINFBFHDzEWCiIEV2wNXnlChFOb1mZwGQKRU5UhmJNoIpATZCIcFBFHDx0TE+0WFhEQEw2XkZcJiy9VJeC7iVKcPsajggDKQHd+e3N8cXcyfXwyZnp7YTJxd2Cs5mGJ/MB2HdlrXSbKsCzrau152tsLYOdPHMdtTYngNxGoR51fTx/jkgY5wntVhmQb7OZ5nzxStOVVX20yfXQyZnp3MmZ6d3wyc2Jifntxc353Mlt8cTwjNCI2FBFHFhkBD1NiMlFTIpATMCIfFBs4lFqU5R8TExMXEhGQEx0SIpATGBCQExMS9oO7G5ATEhQbOJRalOVxdhcTIpPgIjgUcH53MmFmc3x2c2B2MmZ3YH9hMnO5sWOAVUFH0709U6Hq6fFi3/SxXoeMaB62VZlJxgQlIdnWHV/cBnvDHxQbOJRalOUfExMXFxIRkBMTEk5gc3Fme3F3MmFmc2Z3f3d8ZmE8IicgIyYiISRIBR8hJyIgIisgIyYie3R7cXNme318MlNnZnp9YHtmayM28PnDpWLNHVfzNdjjf2r/9acFBTiUWpTlHxMTFxcSInAjGSIbFBFHNCI2FBFHFhkBD1NiYn53MlF3YGYyc3x2MnF3YGZ7dHtxc2Z7fXwyYnYnMQdZB0sPoYbl5I6M3UKo00pCPjJxd2Bme3R7cXNmdzJifX57cWudYZNy1AlJGz2AoOpWWuJyKowH5xYUARBHQSMBIgMUEUcWGAEYU2JipQmvgVA2ADjVHQ+kX45McdpZkgVmen1ge2ZrIwQiBhQRRxYRAR9TYtJxIWXlKBU+RPnIHTMcyKhhC12nbVO6iuvD2HSONnkDwrGp9gk40Q09IpPRFBo5FBMXFxUQECKTpAiToSSLXj9qpf+eic7hZYngZMBlIl3TFBFHDxwWBBYGOcJ7VYZkG+zmeZ8EIgYUEUcWEQEfU2JifncyQH19ZhpMIpATAxQRRw8yFpATGiKQExYiQriYx8j27sIbFSWiZ2cz");
        private static int[] order = new int[] { 35,26,16,23,40,56,36,8,15,9,51,29,22,27,58,43,52,36,39,40,21,54,35,31,25,28,49,38,50,40,32,41,41,38,58,58,42,50,39,47,46,43,46,53,49,51,51,54,49,55,52,59,56,57,59,56,56,57,58,59,60 };
        private static int key = 18;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
