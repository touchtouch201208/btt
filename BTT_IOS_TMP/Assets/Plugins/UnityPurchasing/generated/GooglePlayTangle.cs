#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("ubZ8aBtfVSqlsodzEatD8nF/dVz2stpJhK+ea+C3btuDb2FnaRcQniuUDUY1bAtNWx+g1PdzZ9Yopc4U3dAAfMdfShlJizFAIOD09ctMR8bU7nwrg8d7UvAOZmSDlfdnmjl42V5UIj37Z0Ro31ZKMWxUT2z2hHINfjKsdHs57mqSxF+NeY7HsaeoO0QBGe+Kul2XJzM3oNkf732+aZFApwa0NxQGOzA/HLB+sME7Nzc3MzY1Ah/tySvwSm1Ps7Q2bH0Ww4eNFzm0Nzk2BrQ3PDS0Nzc2lKMNQ97Wd2jViqygZuNSdc2Dn6s/8xtOHhgMgEtire/HD/nE0B6crH+jRLzFImBkDNxaskqzFJ9GSHRwXpn+Y3U7stkfDpBaF2ntBzQ1NzY3");
        private static int[] order = new int[] { 11,10,12,13,9,9,7,9,11,12,10,12,12,13,14 };
        private static int key = 54;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
